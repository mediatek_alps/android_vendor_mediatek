/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.connectivity;

import android.app.Activity;
import android.content.Context;
import android.net.PskKeyManager;
import android.os.Bundle;
import android.util.Log;
import android.net.ConnectivityManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;
import android.view.View.OnClickListener;

import com.android.org.conscrypt.PSKKeyManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

public class CdsSocketActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "CDSINFO/CdsSocketActivity";
    private Context mContext;
    private Toast mToast;
    private static final String[] DEFAULT_CONN_LIST = new String[] {"Wi-Fi", "Mobile"};
    Spinner mConnSpinner = null;
    private int mSelectConnType = ConnectivityManager.TYPE_MOBILE;
    private EditText mReportPercent = null;
    private ConnectivityManager mConnMgr = null;
    private Button mRefreshBtn = null;
    private Button mServerBtn = null;
    private Button mClientBtn = null;
    private SecureWebServer mSecureWebServer = null;

    private ListView mSocketListview;
    private TextView mSocketInfo;
    private SimpleAdapter mSocketAdapter;
    ArrayList<HashMap<String, String>> mSocketList = new ArrayList<HashMap<String, String>>();

    private static final int    SERVER_SSL_PORT = 4931;
    private static final String CROSSMOUNT_PSK = "CrossMount_PSK";

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        setContentView(R.layout.cds_socket);

        mContext = this.getBaseContext();

        mSocketListview    = (ListView) findViewById(R.id.SocketListview);
        mSocketAdapter = new SimpleAdapter(
            this,
            mSocketList,
            R.layout.simple_list_item2,
            new String[] { "name", "port" },
            new int[] { android.R.id.text1, android.R.id.text2 });
        mSocketListview.setAdapter(mSocketAdapter);

        mRefreshBtn = (Button) this.findViewById(R.id.btn_refresh);
        mServerBtn = (Button) this.findViewById(R.id.btn_server);
        mClientBtn = (Button) this.findViewById(R.id.btn_client);

        if (mRefreshBtn != null) {
            mRefreshBtn.setOnClickListener(new OnClickListener() {
                public void onClick(View arg0) {
                    listSocketPort();
                }
            });
        }

        if (mServerBtn != null) {
            mServerBtn.setOnClickListener(new OnClickListener() {
                public void onClick(View arg0) {
                    runSslServer();
                }
            });
        }

        if (mClientBtn != null) {
            mClientBtn.setOnClickListener(new OnClickListener() {
                public void onClick(View arg0) {
                    runSslClient();
                }
            });
        }

        mSocketInfo = (TextView) this.findViewById(R.id.socket_info);
        Log.i(TAG, "CdsSocketActivity is started");
    }

    @Override
    protected void onResume() {
        listSocketPort();
        showSocketOptionInfo();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    public void onClick(View v) {
        int buttonId = v.getId();
        listSocketPort();

    }

    private void runSslServer() {
        Log.i(TAG, "runSslServer");

        if (mSecureWebServer != null) {
            mSecureWebServer.stop();
            mSecureWebServer = null;
            mServerBtn.setText(R.string.run_server);
        } else {
            Log.i(TAG, "runSslServer with start");
            mSecureWebServer = new SecureWebServer(SERVER_SSL_PORT);
            mSecureWebServer.init();
            mSecureWebServer.start();
            mServerBtn.setText(R.string.stop_server);
        }
    }

    private void runSslClient() {
        Log.i(TAG, "runSslClient");
        SecureWebClient client = new SecureWebClient(SERVER_SSL_PORT);
        client.init();
        client.start();
    }

    private void showSocketOptionInfo() {

        try {
            Socket s = new Socket();
            StringBuilder builder = new StringBuilder("Default Socket Option Info:");
            builder.append("\r\nRX Buf. size:").append(s.getReceiveBufferSize()).append("\r\nTX Buf. size:").append(s.getSendBufferSize()).
            append("\r\nRX Socket Timeout:").append(s.getSoTimeout()).append("\r\nTX Socket Timeout:").append(s.getSoSndTimeout()).
            append("\r\nLinger time:").append(s.getSoLinger()).append("\r\nTCP no delay: ").append(s.getTcpNoDelay()).
            append("\r\nOOBINLINE:").append(s.getOOBInline()).append("\r\nTraffic class: ").append(s.getTrafficClass());

            mSocketInfo.setText(builder.toString());
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    private void listSocketPort() {
        try {
            mSocketList.clear();
            accessibleListeningPorts("/proc/net/tcp", true);
            accessibleListeningPorts("/proc/net/tcp6", true);
            accessibleListeningPorts("/proc/net/udp", true);
            accessibleListeningPorts("/proc/net/udp6", true);
            mSocketAdapter.notifyDataSetChanged();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException ee) {
            ee.printStackTrace();
        }
    }

    private void accessibleListeningPorts(String procFilePath, boolean isTcp) throws IOException {
        String socketEntry = "";
        HashMap<String, String> socketItem;
        String socketName = "";
        String socketInfo = "";
        int    i = 0;

        List<ParsedProcEntry> entries = ParsedProcEntry.parse(procFilePath);

        for (ParsedProcEntry entry : entries) {
            String addrPort = entry.localAddress.getHostAddress() + ':' + entry.port;

            if (isPortListening(entry.state, isTcp)) {
                socketItem = new HashMap<String, String>();
                i++;
                String uidname = "" + i;

                if (entry.uid != 0) {
                    uidname = mContext.getPackageManager().getNameForUid(entry.uid);
                    socketName = "uid(" + entry.uid + "):" + uidname;
                } else {
                    socketName = "System(" + i + ")";
                }

                socketInfo = entry.localAddress.getHostAddress() + ":" + entry.port;

                socketItem.put("name", socketName);
                socketItem.put("port", socketInfo);
                mSocketList.add(socketItem);

                Log.d(TAG, "length:" + mSocketList.size());

                socketEntry = "\nFound port listening on addr="
                              + entry.localAddress.getHostAddress() + ", port="
                              + entry.port + ", UID=" + entry.uid + " in "
                              + procFilePath;

                Log.i(TAG, socketEntry);

            }
        }
    }

    private boolean isPortListening(String state, boolean isTcp) throws IOException {
        // 0A = TCP_LISTEN from include/net/tcp_states.h
        String listeningState = isTcp ? "0A" : "07";
        return listeningState.equals(state);
    }

    private static class ParsedProcEntry {
        private final InetAddress localAddress;
        private final int port;
        private final String state;
        private final int uid;

        private ParsedProcEntry(InetAddress addr, int port, String state, int uid) {
            this.localAddress = addr;
            this.port = port;
            this.state = state;
            this.uid = uid;
        }


        private static List<ParsedProcEntry> parse(String procFilePath) throws IOException {

            List<ParsedProcEntry> retval = new ArrayList<ParsedProcEntry>();
            /*
            * Sample output of "cat /proc/net/tcp" on emulator:
            *
            * sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  ...
            * 0: 0100007F:13AD 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0   ...
            * 1: 00000000:15B3 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0   ...
            * 2: 0F02000A:15B3 0202000A:CE8A 01 00000000:00000000 00:00000000 00000000     0   ...
            *
            */

            File procFile = new File(procFilePath);
            Scanner scanner = null;

            try {
                scanner = new Scanner(procFile);

                while (scanner.hasNextLine()) {
                    String nextLine = scanner.nextLine();

                    if (nextLine == null) {
                        Log.e(TAG, "nextLine is null");
                        return retval;
                    }

                    String line = nextLine.trim();

                    // Skip column headers
                    if (line.startsWith("sl")) {
                        continue;
                    }

                    String[] fields = line.split("\\s+");
                    final int expectedNumColumns = 12;

                    if (fields.length < expectedNumColumns) {
                        Log.e(TAG, procFilePath + " should have at least " + expectedNumColumns
                              + " columns of output " + fields);
                    }

                    String state = fields[3];
                    int uid = Integer.parseInt(fields[7]);
                    InetAddress localIp = addrToInet(fields[1].split(":")[0]);
                    int localPort = Integer.parseInt(fields[1].split(":")[1], 16);

                    retval.add(new ParsedProcEntry(localIp, localPort, state, uid));
                }
            } finally {
                if (scanner != null) {
                    scanner.close();
                }
            }

            return retval;
        }

        /**
         * Convert a string stored in little endian format to an IP address.
         */
        private static InetAddress addrToInet(String s) throws UnknownHostException {
            int len = s.length();

            if (len != 8 && len != 32) {
                throw new IllegalArgumentException(len + "");
            }

            byte[] retval = new byte[len / 2];

            for (int i = 0; i < len / 2; i += 4) {
                retval[i] = (byte) ((Character.digit(s.charAt(2 * i + 6), 16) << 4)
                                    + Character.digit(s.charAt(2 * i + 7), 16));
                retval[i + 1] = (byte) ((Character.digit(s.charAt(2 * i + 4), 16) << 4)
                                        + Character.digit(s.charAt(2 * i + 5), 16));
                retval[i + 2] = (byte) ((Character.digit(s.charAt(2 * i + 2), 16) << 4)
                                        + Character.digit(s.charAt(2 * i + 3), 16));
                retval[i + 3] = (byte) ((Character.digit(s.charAt(2 * i), 16) << 4)
                                        + Character.digit(s.charAt(2 * i + 1), 16));
            }

            return InetAddress.getByAddress(retval);
        }
    }

    /**
     * Utility class for PskKeyManager.
     *
     */
    private class CrossMountPskKeyManager extends PskKeyManager
        implements PSKKeyManager {

        public CrossMountPskKeyManager() {
            super();
        }

        @Override
        public SecretKey getKey(String identityHint, String identity, Socket socket) {
            return new SecretKeySpec(CROSSMOUNT_PSK.getBytes(), "RAW");
        }

        @Override
        public SecretKey getKey(String identityHint, String identity, SSLEngine engine) {
            return new SecretKeySpec(CROSSMOUNT_PSK.getBytes(), "RAW");
        }
    };

    /**
     * Utility class for support PSK/TLS over socket server.
     *
     */
    class SecureWebServer {
        private static final String SRV_TAG = "SecureWebServer";
        private int mPort = 0;
        private InetAddress mAddress;
        private boolean mIsRunning = true;
        private SSLServerSocketFactory mSslSf;
        private SSLServerSocket mSslSocket;

        public SecureWebServer(int port) {
            this(InetAddress.getLoopbackAddress(), port);
        }

        public SecureWebServer(InetAddress addr, int port) {
            mPort = port;
            mAddress = addr;
        }

        protected void init() {
            try {
                PskKeyManager pskKeyManager = new CrossMountPskKeyManager();

                // Get an SSL context using the TLS protocol
                SSLContext sslContext = SSLContext.getInstance("TLS");

                sslContext.init(
                    new KeyManager[] {pskKeyManager},
                    new TrustManager[0], // No TrustManagers needed for TLS-PSK
                    null // Use the default source of entropy
                );
                mSslSf = (SSLServerSocketFactory) sslContext.getServerSocketFactory();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        protected void start() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d(SRV_TAG, "Secure Web Server is starting up on port:" + mPort);

                    try {
                        mSslSocket = (SSLServerSocket) mSslSf.createServerSocket(
                                                    mPort, 50, mAddress);
                        mSslSocket.setNeedClientAuth(false);
                        mSslSocket.setWantClientAuth(false);
                        mSslSocket.setUseClientMode(false);
                        mSslSocket.setEnabledCipherSuites(mSslSocket.getEnabledCipherSuites());
                        String[] ciphers = mSslSocket.getEnabledCipherSuites();

                        for (int i = 0; i < ciphers.length; i++) {
                            Log.d(SRV_TAG, "ciphers-" + i + ":" + ciphers[i]);
                        }

                        //mSslSocket.setEnabledProtocols(new String [] { "TLSv1.2" });
                        String[] pros = mSslSocket.getEnabledProtocols();

                        for (int i = 0; i < pros.length; i++) {
                            Log.d(SRV_TAG, "pros-" + i + ":" + pros[i]);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }

                    Log.d(SRV_TAG, "Waiting for connection");

                    while (mIsRunning) {
                        try {
                            Socket cSocket = mSslSocket.accept();
                            Log.d(SRV_TAG, "Connected, sending data.");
                            BufferedReader in = new BufferedReader(
                                new InputStreamReader(cSocket.getInputStream()));
                            PrintWriter out = new PrintWriter(cSocket
                                                              .getOutputStream());
                            String str = ".";

                            while (!str.equals("")) {
                                str = in.readLine();

                                if (str == null) {
                                    break;
                                }

                                out.println(str);
                                out.flush();
                            }

                            cSocket.close();
                            Log.d(SRV_TAG, "client socket is finished");
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                }
            }).start();
        }

        protected void stop() {
            try {
                mIsRunning = false;

                if (mSslSocket != null) {
                    mSslSocket.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    };

    /**
     * Utility class for support PSK/TLS over socket client.
     *
     */
    class SecureWebClient {
        private static final String CLIENT_TAG = "SecureWebClient";
        private int mPort = 0;
        private InetAddress mAddress;
        private SSLSocketFactory mSslSf;
        private SSLSocket mSslSocket;

        public SecureWebClient(int port) {
            this(InetAddress.getLoopbackAddress(), port);
        }

        public SecureWebClient(InetAddress addr, int port) {
            mPort = port;
            mAddress = addr;
        }

        protected void init() {
            try {
                PskKeyManager pskKeyManager = new CrossMountPskKeyManager();

                // Get an SSL context using the TLS protocol
                SSLContext sslContext = SSLContext.getInstance("TLS");

                sslContext.init(
                    new KeyManager[] {pskKeyManager},
                    new TrustManager[0], // No TrustManagers needed for TLS-PSK
                    null // Use the default source of entropy
                );
                mSslSf = (SSLSocketFactory) sslContext.getSocketFactory();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        protected void start() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mSslSocket = (SSLSocket) mSslSf.createSocket(
                                    mAddress.getHostAddress(), mPort);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return;
                    }

                    Log.d(CLIENT_TAG, "Client is connected");

                    try {
                        Log.d(CLIENT_TAG, "Connected, sending data.");
                        BufferedReader in = new BufferedReader(
                            new InputStreamReader(mSslSocket.getInputStream()));
                        PrintWriter out = new PrintWriter(mSslSocket
                                                          .getOutputStream());
                        out.println("Hello world");
                        out.flush();
                        mSslSocket.close();
                        Log.d(CLIENT_TAG, "client socket is finished");
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
            }).start();
        }
    };
}