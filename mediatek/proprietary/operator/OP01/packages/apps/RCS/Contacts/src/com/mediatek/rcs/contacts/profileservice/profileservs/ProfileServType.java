/*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein
* is confidential and proprietary to MediaTek Inc. and/or its licensors.
* Without the prior written permission of MediaTek inc. and/or its licensors,
* any reproduction, modification, use or disclosure of MediaTek Software,
* and information contained herein, in whole or in part, shall be strictly prohibited.
*/
/* MediaTek Inc. (C) 2014. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
* AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
* NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
* SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
* SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
* THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
* THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
* CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
* SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
* CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
* AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
* OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
* MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
* The following software/firmware and/or related documentation ("MediaTek Software")
* have been modified by MediaTek Inc. All revisions are subject to any receiver's
* applicable license agreements with MediaTek Inc.
*/

package com.mediatek.rcs.contacts.profileservice.profileservs;

import android.net.Uri;

import com.mediatek.rcs.contacts.profileservice.profileservs.xcap.ProfileXcapElement;
import com.mediatek.rcs.contacts.profileservice.profileservs.xcap.ProfileXcapException;
import com.mediatek.rcs.contacts.profileservice.utils.ProfileConstants;
import com.mediatek.rcs.contacts.profileservice.utils.ProfileServiceLog;
import com.mediatek.xcap.client.XcapClient;
import com.mediatek.xcap.client.uri.XcapUri;

import java.io.IOException;
import java.io.StringReader;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.auth.Credentials;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicHeader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * ProfileServType (PS type) abstract class.
 */
public abstract class ProfileServType extends ProfileXcapElement {

    public static final String TAG = "ProfileServType";

    private static Document mCurrDoc = null;  //static, only on belongs to ProfileServType, actually, it is the pcc.xml 
    private static Document mPortraitDoc = null;
    
    protected Element mRoot = null;

    //public static final String AUTH_XCAP_3GPP_INTENDED = "X-3GPP-Intended-Identity";

    private static final String testProfileXml = 
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
        "    <pcc xmlns=\"urn:oma:xml:cab:pcc\" type=\"individual\">\r\n" +
        "        <person-details>\r\n" +
        "            <comm-addr xml:lang=\"en\">\r\n" +
        "                <tel pref=\"1\" tel-type=\"Mobile\">\r\n" +
        "                    <tel-nb>\r\n" +
        "                        <tel-string>13812345678</tel-string>\r\n" +
        "                    </tel-nb>\r\n" +
        "                    <label>Mobile</label>\r\n" +
        "                </tel>\r\n" +
        "                <tel pref=\"2\" tel-type=\"Fax\">\r\n" +
        "                    <tel-nb>\r\n" +
        "                        <tel-string>25692320</tel-string>\r\n" +
        "                    </tel-nb>\r\n" +
        "                    <label>office Fax</label>\r\n" +
        "                </tel>\r\n" +
        "                <tel pref=\"3\" tel-type=\"Work\">\r\n" +
        "                    <tel-nb>\r\n" +
        "                        <tel-string>28975642</tel-string>\r\n" +
        "                    </tel-nb>\r\n" +
        "                    <label>Office Phone</label>\r\n" +
        "                </tel>\r\n" +
        "                <uri-entry pref=\"1\" addr-uri-type=\"email\">\r\n" +
        "                    <addr-uri>13812345678</addr-uri>\r\n" +
        "                    <label>email</label>\r\n" +
        "                </uri-entry>\r\n" +
        "            </comm-addr>\r\n" +
        "            <name>\r\n" +
        "                <name-entry pref=\"1\" xml:lang=\"zh-CN\" name-type=\"LegalName\">\r\n" +
        "                    <first>yang</first>\r\n" +
        "                    <family>wang</family>\r\n" +
        "                </name-entry>\r\n" +
        "            </name>\r\n" +
        "            <birth xml:lang=\"en\">\r\n" +
        "                <birth-date>\r\n" +
        "                    <date>1957-07-09</date>\r\n" +
        "                </birth-date>\r\n" +
        "            </birth>\r\n" +
        "            <address>\r\n" +
        "                <address-entry pref=\"1\" addr-type=\"Home\" xml:lang=\"zh-CN\">\r\n" +
        "                    <address-data>\r\n" +
        "                        <addr-string>jiuxianqiao road 6</addr-string>\r\n" +
        "                        <label>address</label>\r\n" +
        "                    </address-data>\r\n" +
        "                </address-entry>\r\n" +        
        "                <address-entry pref=\"2\" addr-type=\"Work\" xml:lang=\"zh-CN\">\r\n" +
        "                    <address-data>\r\n" +
        "                        <addr-string>jiuxianqiao road 6</addr-string>\r\n" +
        "                        <label>office</label>\r\n" +
        "                    </address-data>\r\n" +
        "                </address-entry>\r\n" +
        "            </address>\r\n" +
        "            <career>\r\n" +
        "                <employer>\"mediatek\"</employer>\r\n" +
        "                <duty>engineer</duty>\r\n" +
        "            </career>\r\n" +
        "        </person-details>\r\n" +
        "    </pcc>\r\n";

    private static final String testPortraitXml = 
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
        "    <content xmlns=\"urn:oma:xml:prs:pres-content\">\r\n" +
        "        <mime-type>image/png</mime-type>\r\n" +
        "        <encoding>base64</encoding>\r\n" +
        "        <data>myPortrait</data>\r\n" +
        "    </content>\r\n";

    
    /**
     * Constructor.
     *
     * @param documentUri       XCAP document URI
     * @param parentUri         XCAP root directory URI
     * @param intendedId        X-3GPP-Intended-Id
     * @param credential        for authentication
     * @param fromXml whether from existed xml document or not
     * @param param params to init a xml document
     * @param param contentType illustrate pcc, or particial or portrait
     * @throws ProfileXcapException    if XCAP error
     * @throws ParserConfigurationException if parser configuration error
     */
    public ProfileServType(XcapUri xcapUri, String parentUri, String intendedId,
            Credentials credential, boolean fromXml, Object params, int contentType) throws ProfileXcapException {
        super(xcapUri, parentUri, intendedId, credential);

        /*init the serv in a xml format, get content from db,but not remote server 
            the pre-condition is the format is defined by spec and keep constantly*/

        if (fromXml){
            ProfileServiceLog.d(TAG, "ProfileServType, contentType: " + contentType);
            switch (contentType) {
                case ProfileConstants.CONTENT_TYPE_PORTRAIT:
                case ProfileConstants.CONTENT_TYPE_CONTACT_PORTRAIT:                    
                    loadContentDocument(true); //reload new xml from server
                    if (mPortraitDoc != null) {
                        initServiceInstance(mPortraitDoc);
                        mPortraitDoc = null;
                    }
                    break;

                case ProfileConstants.CONTENT_TYPE_PCC:
                    loadContentDocument(false); //reload new xml from server
                    break;
                    
                case ProfileConstants.CONTENT_TYPE_PART:
                    if (mCurrDoc != null) {
                        initServiceInstance(mCurrDoc);
                    } else {
                        //error log
                    }
                    break;
                    
                default:
                    //error log
                    break;
            }
        }  else {
            initServiceInstance(params);
        }
    }

    private void loadContentDocument(boolean isPortrait) throws ProfileXcapException {
        ProfileServiceLog.d(TAG, "loadContentDocument, isPortrait: " + isPortrait);
        String xmlContent = getContent(isPortrait);
        /*
        if (xmlContent == null) {
            ProfileServiceLog.d(TAG, "error, no xmlContent gotten, for test");
            if (isPortrait) {
                xmlContent = testPortraitXml;
            } else {
                xmlContent = testProfileXml;
            }
        }
        */
        if (xmlContent == null) {
            ProfileServiceLog.d(TAG, "xmlContent is null");
        }
        
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xmlContent));

            if (isPortrait) {
                mPortraitDoc = db.parse(is);
            } else {
                mCurrDoc = db.parse(is);
            }
            
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            throw new ProfileXcapException(500);
        } catch (SAXException e) {
            e.printStackTrace();
            // Throws a server error
            throw new ProfileXcapException(500);
        } catch (IOException e) {
            e.printStackTrace();
            // Throws a server error
            throw new ProfileXcapException(500);
        }
    }
    
    private String getContent(boolean isPortrait) throws ProfileXcapException {
        ProfileServiceLog.d(TAG, "ProfileServType getContent");
        String targetUri = null;
        XcapClient xcapClient = new XcapClient();
        HttpResponse response = null;
        String ret = null;
        HttpEntity entity;
        Header[] headers = null;

        try {
            if (isPortrait) {
                headers = new Header[6];
            } else {
                headers = new Header[5];
            }
            headers[0] = new BasicHeader(AUTH_XCAP_3GPP_INTENDED, mIntendedId);
            headers[1] = new BasicHeader(HOST_NAME, "122.70.137.46");
            headers[2] = new BasicHeader(USER_AGENT, "XDM-client/OMA1.0");
            

            if (isPortrait) {
                //headers[3] = new BasicHeader(IF_NONE_MATCH, mPortraitETag);
                headers[3] = new BasicHeader(CONTENT_TYPE, "application/vnd.oma.pres-content+xml");
                headers[4] = new BasicHeader(X_RESOLUTION, "640");
                headers[5] = new BasicHeader(IF_NONE_MATCH, mPortraitETag);
            } else {
                headers[3] = new BasicHeader(IF_NONE_MATCH, mPccETag);
                headers[4] = new BasicHeader(CONTENT_TYPE, "application/vnd.oma.cab-pcc+xml");
            }

            if (mCredentials == null) {
                ProfileServiceLog.d(TAG, "mCredentials is null");
            } 
            xcapClient.setAuthenticationCredentials(mCredentials);
            /*actually, the pcc and the portrait is the same, because they are both get the whole xml file,
                    no node information is needed*/
            if (isPortrait) {
                targetUri = mXcapUri.toURI().toString();
            } else {
                targetUri = mXcapUri.toURI().toString();
            }
            response = xcapClient.get(new URI(targetUri), headers);

            if (response != null) {
                if (response.getStatusLine().getStatusCode() == 200) {
                    entity = response.getEntity();
                    InputStream is = entity.getContent();
                    // convert stream to string
                    ret = convertStreamToString(is);
                    if (response.containsHeader(ETAG)) {
                        Header eTagHeader = response.getFirstHeader(ETAG);
                        if (isPortrait) {
                            mPortraitETag = eTagHeader.getValue();
                            ProfileServiceLog.d(TAG, "mPortraitETag: " + mPortraitETag);
                        } else {
                            mPccETag = eTagHeader.getValue();
                            ProfileServiceLog.d(TAG, "mPccETag: " + mPccETag);
                        }
                    }
                } else {
                    ret = null;
                    throw new ProfileXcapException(response.getStatusLine().getStatusCode());
                }
            }else {
                ProfileServiceLog.d(TAG, "response is null");
                throw new ProfileXcapException(new ConnectException());
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            ProfileServiceLog.d(TAG, "catch IOException on getContent");
            e.printStackTrace();
            throw new ProfileXcapException(e);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } finally {
            xcapClient.shutdown();
        }
        return ret;
    }

    protected Element getRootElement () {
        return mRoot;
    }

    /**
     * this function is call by super class ProfileServType when set profile
     * format a xml docment, and set the node content by params
     * abstract function, every subclass need implement this function
     * @ param params is the init value needed to set
     */
    public abstract void initServiceInstance(Object params) throws ProfileXcapException ;

    /**
     * this function is call by super class ProfileServType when get profile
     * parse the pcc.xml gotten from network
     * abstract function, every subclass need implement this function
     * @ param domDoc is the pcc.xml gotten from network
     */
    public abstract void initServiceInstance(Document domDoc) throws ProfileXcapException;
}
