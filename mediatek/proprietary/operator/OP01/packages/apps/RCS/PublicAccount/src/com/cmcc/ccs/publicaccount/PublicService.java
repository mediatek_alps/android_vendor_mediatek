package com.cmcc.ccs.publicaccount;

public class PublicService {
    public static final int OK = 0;
    public static final int TIMEOUT = -1;
    public static final int UNKNOW = -2;
    public static final int INTERNEL_ERROR = -3;
    public static final int OFFLINE = -4;
}
