package com.mediatek.rcs.common.service;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

import org.gsma.joyn.JoynServiceListener;
import com.cmcc.ccs.profile.ProfileListener;
import com.cmcc.ccs.profile.ProfileService;
import com.mediatek.gifdecoder.GifDecoder;
import com.mediatek.rcs.common.provider.GroupMemberData;
import com.mediatek.rcs.common.RCSMessageManager;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.LruCache;

public class PortraitService
                   implements PortraitAsyncHelper.OnQueryCompleteListener {
    private static final String TAG = "PortraitService";
    private Context mContext;
    private static String mDefaultImage;
    private static String mBlankImage;
    
    private static ProfileService mProfileService;
    private static boolean mProfileServiceConnected;
    
    public final static int TOKEN_QUERY_ONE = 0;
    public final static int TOKEN_QUERY_ONE_ONLY_CONTACT = 1; 
    public final static int TOKEN_QUERY_GROUP = 2;
    public final static int TOKEN_QUERY_GROUP_THUMBNAIL = 3;
    
    private final static int MAX_CACHE_GROUP = 10;
    private final static int MAX_CACHE_GROUPTHUMBNIAL = 20;
    private final static int MAX_CACHE_PORTRAIT = 200;
    
    private static Portrait mMyPortrait;
    // Key is group chat id String, 
    // Value is a HashMap, key is number, value is name
    private final static LruCache<String, LinkedHashMap<String, String>> mGroupCache
        = new LruCache<String, LinkedHashMap<String, String>>(MAX_CACHE_GROUP);
    // key is chatId, value is group thumbnail
    private final static LruCache<String, Thumbnail> mGroupThumbnail
        = new LruCache<String, Thumbnail>(MAX_CACHE_GROUPTHUMBNIAL);
    // Key is mobile number String, Value is Portrait
    private final static LruCache<String, Portrait> mPortraitCache
        = new LruCache<String, Portrait>(MAX_CACHE_PORTRAIT);
    // Key is number, Value is ChatId
    private final static Map<String, String> mQueryProfile = 
            new ConcurrentHashMap<String, String>();
    
    private final Set<UpdateListener> mListeners = new CopyOnWriteArraySet<UpdateListener>();
    public interface UpdateListener {
        // will pass the result to caller
        public void onPortraitUpdate(Portrait p, String chatId);
        //after query group info done, notify caller the group number set
        public void onGroupUpdate(String chatId, Set<String> numberSet);
        // will pass the result to caller
        public void onGroupThumbnailUpdate(String chatId, Bitmap thumbnail);
    }
    
    public void addListener(UpdateListener listener) {
        Log.d(TAG, "addListener: " + listener);
        mListeners.add(listener);
    }
    
    public void removeListener(UpdateListener listener) {
        Log.d(TAG, "removeListener: " + listener);
        mListeners.remove(listener);
    }
    
    /**
     * This API first directly return data from Cache, maybe old, then will
     * async query the latest data from Contacts, include name and head image;
     * after query done, will callback by 
     * UpdateListener.onPortraitUpdate(Portrait, chatId=null) to caller
     * @param number
     * @return
     */
    public Portrait requestPortrait(String number) {
        Log.d(TAG, "requestPortrait,number=" + number);
        queryContactsAsync(TOKEN_QUERY_ONE_ONLY_CONTACT, number);
        return getPortrait(null, number);
    }
    
    /**
     * This API first directly return data from Cache, maybe old, then will
     * async query the latest data from GroupMember Provider and Contacts, 
     * include name and head image; after query done, will callback by 
     * UpdateListener.onPortraitUpdate(Portrait, chatId) to caller
     * @param number
     * @return
     */
    public Portrait requestMemberPortrait(String chatId, String number, Boolean queryProfile) {
    	Log.d(TAG, "requestMemberPortrait, chatId=" + chatId + ",number=" + number
    	        + ",queryProfile=" + queryProfile);
    	queryMember(TOKEN_QUERY_ONE, chatId, number, queryProfile);
        return getPortrait(chatId, number);
    }

    /**
     * This API only directly return data from Cache, because we assume Caller 
     * call updateGroup() before, so the current cache is the latest.
     * @param chatId
     * @param number
     * @return
     */
    public Portrait getMemberPortrait(String chatId, String number) {
        Log.d(TAG, "getMemberPortrait, chatId=" + chatId + ",number=" + number);
        return getPortrait(chatId, number);
    }
    

    /**
     * When enter Group Chat or display Group Member Settings, need call this
     * API. This API will read the latest group member info from RCS Group
     * Provider, then query all member from Contacts Provider; after finish,
     * callback to caller by UpdateListener.onGroupUpdate(chatId, numberSet)
     * @param chatId
     * @param queryProfile, useless, will be phase out,
     * @return group member number Set
     */
    public Set<String> updateGroup(String chatId, boolean queryProfile) {
        
        queryGroup(chatId);
        
        Set<String> groupSet = new LinkedHashSet<String>();
        if (mGroupCache.get(chatId) != null) {
            groupSet.addAll(mGroupCache.get(chatId).keySet());
        }
        Log.d(TAG, "updateGroup, chatId=" + chatId + ",member count=" + groupSet.size());
        return groupSet;
    }
    
    /**
     * This API will directly return data from cache, maybe old, then it will async query
     * the newest image, and callback to caller by UpdateListener.onGroupThumbnailUpdate()
     * @param chatId
     * @return Bitmap Group Thumbnail
     */
    public Bitmap requestGroupThumbnail(String chatId) {
    	Log.d(TAG, "requestGroupThumbnail, chatId=" + chatId);
    	queryGroupThumbnail(chatId);
    	
    	Thumbnail thumbnail = mGroupThumbnail.get(chatId);
    	if (thumbnail != null) {
            return thumbnail.bitmap;
        }
        return composeGroupThumbnail(chatId);
    }
    
    /**
     * A sync api, get myself profile from ccs provider
     * @return Portrait
     */
    public Portrait getMyProfile() {
        
        if (mMyPortrait != null) {
            return mMyPortrait;
        }
        
        String[] projections = {ProfileService.PHONE_NUMBER,
                ProfileService.FIRST_NAME,
                ProfileService.LAST_NAME,
                ProfileService.PORTRAIT,
                ProfileService.PORTRAIT_TYPE};
        Uri uri = Uri.parse("content://com.cmcc.ccs.profile");
        Cursor c = mContext.getContentResolver().query(uri, projections, null, null, null);
        String number = null, name = null;
        String image = null, imageType = null;
        int count = -1;
        try {
            if (c != null && c.moveToFirst()) {
                number = c.getString(c.getColumnIndex(ProfileService.PHONE_NUMBER));
                name = c.getString(c.getColumnIndex(ProfileService.FIRST_NAME));
                if (TextUtils.isEmpty(name)) {
                    name = c.getString(c.getColumnIndex(ProfileService.LAST_NAME));
                }
                image = c.getString(c.getColumnIndex(ProfileService.PORTRAIT));
                imageType = c.getString(c.getColumnIndex(ProfileService.PORTRAIT_TYPE));
                
                Log.i(TAG, "getMyProfile, number=" + number + ",name=" + name
                      + ",imageType=" + imageType);
                image = decodeFristFrameIfGif(image, imageType);
            }
        } finally {
            if (c != null) {
                count = c.getCount();
                c.close();
            }
        }
        
        Portrait p;
        String myNumber = RCSMessageManager.getInstance(mContext).getMyNumber();
        if (count >= 1) {
            if (TextUtils.isEmpty(name)) name = myNumber;
            if (TextUtils.isEmpty(image)) image = mDefaultImage;
            p  = new Portrait(myNumber, name, image, Portrait.FROM_CONTACTS);
            mMyPortrait = p;
        } else {
            p = new Portrait(myNumber, myNumber, mDefaultImage, Portrait.FROM_CONTACTS);
        }
        return p;
    }
    
    public static Bitmap decodeString(String imgStr) {
        byte[] data = Base64.decode(imgStr, Base64.DEFAULT);
        Bitmap bm = BitmapFactory.decodeByteArray(data, 0, data.length);
        return bm;
    }

    // mName only come from Contacts APP
    // mImage come from Contacts, or Group member provider, or Profile
    public class Portrait {
        public String mNumber;
        public String mName;
        public String mImage;
        
        private int mType;
        private final static int FROM_DEFAULT = 0;
        private final static int FROM_CONTACTS = 1;
        private final static int FROM_PROFILE = 2;
        private final static int FROM_GROUP = 3;
        
        public Portrait(String number, String name, String image, int type) {
            mNumber = number;
            mName = name;
            mImage = image;
            mType = type;
        }
    }

    public PortraitService(Context cntx, int defaultImageResourceId, int blankImageResourceId) {
        mContext = cntx;
        if (mDefaultImage == null) {
            mDefaultImage = genImageStrById(defaultImageResourceId);
        }
        if (mBlankImage == null) {
            mBlankImage = genImageStrById(blankImageResourceId);
        }
        if (mProfileService == null) {
            mProfileService = new ProfileService(mContext, new PortraitJoynServiceListener());
            mProfileService.connect();
            mProfileService.addProfileListener(new PortraitProfileListener());
            Uri uri = Uri.parse("content://com.cmcc.ccs.profile");
            mContext.getContentResolver().registerContentObserver(uri, true, mMyProfileObserver);
        }
    }
    
    private String genImageStrById(int resId) {
        Bitmap bm = BitmapFactory.decodeResource(mContext.getResources(), resId);
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
        return Base64.encodeToString(byteStream.toByteArray(), Base64.DEFAULT);
    }
    
    private final static ContentObserver mMyProfileObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfUpdate) {
            Log.d(TAG, "getMyProfile mMyProfileObserver,onChanged");
            mMyPortrait = null;
        }
    };
    
    private Portrait getPortrait(String chatId, String number) {

        Portrait p = new Portrait(number, null, null, Portrait.FROM_DEFAULT);
        Portrait cacheP = getProtraitInternal(number);
        if (cacheP != null) {
            p.mImage = cacheP.mImage;
            p.mName = cacheP.mName;
            p.mType = cacheP.mType;
        }

        if (chatId != null) {
            if (mGroupCache.get(chatId) != null) {
                String nickNameInGroup = mGroupCache.get(chatId).get(number);
                if (!TextUtils.isEmpty(nickNameInGroup)) {
                    p.mName = nickNameInGroup;
                }
            }
        }
        if (p.mName == null) p.mName = number;
        if (p.mNumber == null) p.mNumber = number;
        if (chatId == null && p.mType != Portrait.FROM_CONTACTS) {
            p.mImage = null;
        }
        if (p.mImage == null) p.mImage = mDefaultImage;
        
        Log.d(TAG, "getPortrait,chatId=" + chatId + ",number=" + p.mNumber + ",name=" + p.mName);
        return p;
    }
    
    private Portrait getProtraitInternal(String number) {
        return mPortraitCache.get(number);
    }
    
    // callback from PortraitAsyncHelper, after load contact name and portrait
    public void onContactQueryComplete(int token, PortraitAsyncHelper.WorkerArgs args) {
        Log.i(TAG, "onContactQueryComplete, token=" + token);
        String number = null, name = null, image = null;
        String myNumber = RCSMessageManager.getInstance(mContext).getMyNumber();
        boolean isMe = false;

        for (int i = 0; i < args.number.size(); i++) {
            number = args.number.get(i);
            name = args.name.get(i);
            image = args.image.get(i);
            if(name.isEmpty()) name = null;
            if(image.isEmpty()) image = null;
            if (myNumber != null && myNumber.equals(number)) {
                isMe = true;
                updatePortraitCacheForMyself();
            } else {
                updatePortraitCache(number, name, image, Portrait.FROM_CONTACTS);
            }
        }
        String chatId = args.chatId;
        notifyAllListener(token, chatId, args.number, number);

        if (chatId != null && args.number.size() == 1 && image == null 
            && args.queryProfile && !isMe) {
            queryProfile(token, chatId, number);
        }
    }

    
    // callback from PortraitAsyncHelper, after query RCS group member provider
    public void onGroupMemberQueryComplete(int token, PortraitAsyncHelper.WorkerArgs args) {
        Log.i(TAG, "onGroupMemberQueryComplete, token=" + token);
        Cursor cursor = args.cursor;
        if (cursor == null) {
            Log.i(TAG, "onGroupMemberQueryComplete, curor=null");
            return;
        }

        String chatId = null, number = null, name = null, image = null;
        // maybe multi chatId, store member number as key, name as value
        Map<String, LinkedHashMap<String, String>> tempMap =
                new HashMap<String, LinkedHashMap<String, String>>();
        LinkedHashMap<String, String> groupMap;
        Set<String> numberSet = new LinkedHashSet<String>();
        boolean memberExistInGroup = false;

        try {
            if (cursor.moveToFirst()) {
                do {
                    chatId = cursor.getString(cursor.getColumnIndex(GroupMemberData.COLUMN_CHAT_ID));
                    name = cursor.getString(cursor.getColumnIndex(GroupMemberData.COLUMN_CONTACT_NAME));
                    number = cursor.getString(cursor.getColumnIndex(GroupMemberData.COLUMN_CONTACT_NUMBER));
                    image = cursor.getString(cursor.getColumnIndex(GroupMemberData.COLUMN_PORTRAIT));

                    updatePortraitCache(number, null, image, Portrait.FROM_GROUP);

                    groupMap = tempMap.get(chatId);
                    if (groupMap == null) groupMap = new LinkedHashMap<String, String>();
                    groupMap.put(number, name);
                    tempMap.put(chatId, groupMap);
                    numberSet.add(number);
                    
                    if (token == TOKEN_QUERY_ONE && chatId.equals(args.chatId) 
                        && number.equals(args.number.get(0))) {
                        memberExistInGroup = true;
                        break;
                    }
                } while (cursor.moveToNext()); 
            }
        } finally {
            cursor.close();
        }
        if (token == TOKEN_QUERY_GROUP || token == TOKEN_QUERY_GROUP_THUMBNAIL) {
            for (String key : tempMap.keySet()) {
                mGroupCache.put(key, tempMap.get(key));
            }
        } else if (token == TOKEN_QUERY_ONE){ // only query one member
            if (memberExistInGroup == true) {
                if (mGroupCache.get(chatId) != null) {
                    mGroupCache.get(chatId).put(number, name);
                } else {
                    mGroupCache.put(chatId, tempMap.get(chatId));
                }
            } else {
                numberSet.add(args.number.get(0));
                chatId = args.chatId;
            }
        }
        
        if (token == TOKEN_QUERY_GROUP_THUMBNAIL) {
            if (chatId == null) {
                chatId = args.chatId;
            }
        	Thumbnail thumbnail = mGroupThumbnail.get(chatId);
        	if (thumbnail != null) {
        		if (!numberSet.containsAll(thumbnail.numbers) ||
            		(thumbnail.numbers.size() < MAX_COMPOSE_COUNT && 
            		numberSet.size() > thumbnail.numbers.size())) {
                    // need update group thunbnail
            	} else {
            		Log.d(TAG, "onGroupMemberQueryComplete, no need update group thunbnail");
            		return;
            	}
            }
            Log.d(TAG, "onGroupMemberQueryComplete, need update group thunbnail");
        }
        
        Log.i(TAG, "onGroupMemberQueryComplete, will query Contacts, numberSet=" + numberSet);
        if (numberSet.size() > 0) {
            ArrayList<String> numberList = new ArrayList<String>(numberSet);
            queryContactsAsync(token, numberList, chatId, args.queryProfile);
        }
    }
    
    private void notifyAllListener(int token, String chatId, ArrayList<String> numberList,
            String number) {
        for (UpdateListener l : mListeners) {
            Log.d(TAG, "notifyAllListener, updating listener: " + l);
            if (token == TOKEN_QUERY_GROUP) {
                Set<String> s = new LinkedHashSet<String>(numberList);
                Log.d(TAG, "updateGroup, chatId=" + chatId + ",member count=" + s.size()
                        + ",notify");
                l.onGroupUpdate(chatId, s);
            } else if (token == TOKEN_QUERY_ONE){ //contact - profile - default
                Portrait p = getPortrait(chatId, number);
                l.onPortraitUpdate(p, chatId);
            } else if (token == TOKEN_QUERY_ONE_ONLY_CONTACT) { //contact - default
                Portrait p = getPortrait(null, number);
                l.onPortraitUpdate(p, chatId);
            } else if (token == TOKEN_QUERY_GROUP_THUMBNAIL) {
                Bitmap thumbnail = composeGroupThumbnail(chatId);
                l.onGroupThumbnailUpdate(chatId, thumbnail);
            }
        }
    }
    
    private void updatePortraitCache(String number, String name, String image, int type) {
        Portrait p = getProtraitInternal(number);
        if (p != null) {
            if (type == Portrait.FROM_CONTACTS) p.mName = name; // only store name form Contact
            
            if (image != null) {
                p.mImage = image;
                p.mType = type;
            }
            
            if ((type == Portrait.FROM_CONTACTS && p.mType == Portrait.FROM_CONTACTS) ||
                (type == Portrait.FROM_CONTACTS && p.mType != Portrait.FROM_CONTACTS
                && image != null) ||
                (type != Portrait.FROM_CONTACTS && p.mType != Portrait.FROM_CONTACTS) ||
                (type != Portrait.FROM_CONTACTS && p.mType == Portrait.FROM_CONTACTS
                && p.mImage == null)) {
                p.mImage = image;
                p.mType = type;
            }
            
        } else {
            p = new Portrait(number, name, image, type);
            mPortraitCache.put(number, p);
        }
    }
    
    private void updatePortraitCacheForMyself() {
        Portrait myPortrait;
        if (mMyPortrait == null) {
            myPortrait = getMyProfile();
        } else {
            myPortrait = mMyPortrait;
        }
        mPortraitCache.put(myPortrait.mNumber, myPortrait);
    }
    
    private final static int MAX_COMPOSE_COUNT = 3;
    private class Thumbnail {
    	public Bitmap bitmap;
    	public Set<String> numbers = new HashSet<String>();
    	public Thumbnail(Bitmap b, Set<String> n) {
    		bitmap = b;
    		numbers.addAll(n);
    	}
    	
    }
    private Bitmap composeGroupThumbnail(String chatId) {
        Set<String> numberSet = new LinkedHashSet<String>();
        Set<String> numberSelectedSet = new LinkedHashSet<String>();
        List<String> imageList = new ArrayList<String>();
        Portrait p;
        if (mGroupCache.get(chatId) != null) {
            numberSet.addAll(mGroupCache.get(chatId).keySet());
        }
        
        for (String number : numberSet) {
            if ((p = getProtraitInternal(number)) != null) {
                if (p.mImage != null) {
                    imageList.add(p.mImage);
                    numberSelectedSet.add(number);
                }
            }
            if (imageList.size() == MAX_COMPOSE_COUNT)
                break;
        }

        fillGroupThumbnail(imageList, numberSet);
        Bitmap image = drawGroupThumbnail(imageList);
        if (image != null) {
            Log.d(TAG, "composeGroupThumbnail, chatId=" + chatId 
                    + ",numberSelectedSet=" + numberSelectedSet);
            Thumbnail groupThumbnail = new Thumbnail(image, numberSelectedSet);
            mGroupThumbnail.put(chatId, groupThumbnail);
        }
        return image;
    }
    
    private void fillGroupThumbnail(List<String> imageList, Set<String> numberSet) {
        int imageCount = imageList.size();
        int numberCount = numberSet.size();
        
        if (imageCount == MAX_COMPOSE_COUNT)
            return;
        if (numberCount >= MAX_COMPOSE_COUNT) {
            for (int i = imageCount; i < MAX_COMPOSE_COUNT; i++) {
                imageList.add(mDefaultImage);
            }
        } else {
            for (int i = imageCount; i < numberCount; i++) {
                imageList.add(mDefaultImage);
            }
            for (int i = numberCount; i < MAX_COMPOSE_COUNT; i++) {
                imageList.add(mBlankImage);
            }
        }
    }
    
    private Bitmap drawGroupThumbnail(List<String> imageList) {
        if (imageList.size() != MAX_COMPOSE_COUNT) {
            Log.i(TAG, "drawGroupThumbnail size is wrong");
            return null;
        }
        int MIN_SIZE = 96;
        int GAP = 3;
        Bitmap Frist = decodeString(imageList.get(0));
        Bitmap Secon = decodeString(imageList.get(1));
        Bitmap Thrid = decodeString(imageList.get(2));
        if (Frist.getWidth() < MIN_SIZE || Frist.getHeight() < MIN_SIZE)
            Frist = Bitmap.createScaledBitmap(Frist, MIN_SIZE, MIN_SIZE, true);
        int block = Frist.getWidth() / 16;
        Frist = Bitmap.createBitmap(Frist, block * 4, 0, block * 9, Frist.getHeight());
        Secon = Bitmap.createScaledBitmap(Secon, block * 7, Frist.getHeight() / 2, true);
        Thrid = Bitmap.createScaledBitmap(Thrid, block * 7, Frist.getHeight() / 2, true);
        Bitmap thumbnail = Bitmap.createBitmap(Frist.getWidth() + GAP + Secon.getWidth(),
                                       Frist.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(thumbnail);
        canvas.drawBitmap(Frist, 0, 0, null);
        canvas.drawBitmap(Secon, Frist.getWidth() + GAP, 0, null);
        canvas.drawBitmap(Thrid, Frist.getWidth() + GAP, Secon.getHeight() + GAP, null);
        canvas.save(Canvas.ALL_SAVE_FLAG);
        canvas.restore();
        Frist.recycle();
        Secon.recycle();
        Thrid.recycle();
        
        return thumbnail;
    }
    
    private void queryGroup(String chatId) {
        PortraitAsyncHelper.startQueryRcsGroup(TOKEN_QUERY_GROUP, mContext, chatId, this, null);
    }
    
    private void queryMember(int token, String chatId, String number, boolean queryProfile) {
        PortraitAsyncHelper.startQueryRcsMember(token, mContext, chatId, number, this,
                queryProfile);
    }

    private void queryGroupThumbnail(String chatId) {
        PortraitAsyncHelper.startQueryRcsGroup(TOKEN_QUERY_GROUP_THUMBNAIL, mContext, chatId, this,
                null);
    	//PortraitAsyncHelper.startQueryRcsGroup(TOKEN_QUERY_GROUP, mContext, chatId, this, null);
    }
    
    private void queryContactsAsync(int token, String number) {
        ArrayList<String> numberList = new ArrayList<String>();
        numberList.add(number);
        PortraitAsyncHelper.startObtainPhotoAsync(token, mContext, numberList, this, null, false);
    }
    
    private void queryContactsAsync(int token, ArrayList<String> numberList, String chatId, 
            boolean queryProfile) {
        PortraitAsyncHelper.startObtainPhotoAsync(token, mContext, numberList, this, chatId,
                queryProfile);
    }
    
    private void queryProfile(int token, String chatId, String number) {
        Log.i(TAG, "queryProfile, number=" + number);
        if (mProfileServiceConnected == false) {
            Log.i(TAG, "queryProfile, but ProfileService hasnot connected yet, number=" + number);
            return;
        }
        if (!mQueryProfile.containsKey(number)) {
            mProfileService.getContactPortrait(number);
        }
        mQueryProfile.put(number, chatId);
    }
    
    private class PortraitJoynServiceListener implements JoynServiceListener {

        @Override
        public void onServiceConnected() {
            Log.i(TAG, "PortraitJoynServiceListener, onServiceConnected");
            mProfileServiceConnected = true;
        }

        @Override
        public void onServiceDisconnected(int error) {
            Log.i(TAG, "PortraitJoynServiceListener, onServiceDisconnected, error=" + error);
            mProfileServiceConnected = false;
        }
    }
    
    private class PortraitProfileListener extends ProfileListener {

        @Override
        public void onUpdateProfile(int result) {
            // Do nothing
        }

        @Override
        public void onGetProfile(int result) {
         // Do nothing
        }
        
        /*for internal use*/
        public void onGetContactPortrait (int result, String portriat, String number,
                String mimeType){
            
            String chatId = mQueryProfile.remove(number);
            Log.i(TAG, "queryProfile onGetContactPortrait, result=" + result + ",chatId=" + chatId
                    + ",number=" + number + ",mime=" + mimeType);
            if (result != ProfileService.OK || chatId == null) return;
            
            String image = decodeFristFrameIfGif(portriat, mimeType);            
            
            updatePortraitCache(number, null, image, Portrait.FROM_PROFILE);
            if (chatId != null) {
                notifyAllListener(TOKEN_QUERY_ONE, chatId, null, number);
            }
            
            //update groupmember provider
            PortraitAsyncHelper.startUpdateRcsMember(TOKEN_QUERY_ONE, mContext, number, image,
                    null);
            
        }
    }
    
    private String decodeFristFrameIfGif(String portriat, String type) {
        if (type != null && type.equals("GIF")) {
            byte[] data = Base64.decode(portriat, Base64.DEFAULT);
            GifDecoder gifDecoder = new GifDecoder(data, 0, data.length);
            Bitmap firstFrame = gifDecoder.getFrameBitmap(0);
            
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            firstFrame.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
            return Base64.encodeToString(byteStream.toByteArray(), Base64.DEFAULT);
        }
        return portriat;
    }
    
}
