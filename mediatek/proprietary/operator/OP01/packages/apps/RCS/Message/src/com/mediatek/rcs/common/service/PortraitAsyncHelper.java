package com.mediatek.rcs.common.service;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.PhoneLookup;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.mediatek.rcs.common.provider.GroupMemberData;
import com.mediatek.rcs.common.utils.RCSUtils;

/**
 * Helper class for loading RCS contacts or profile photo asynchronously.
 */
public class PortraitAsyncHelper {
	
	private static final String TAG = "PortraitAsyncHelper";

    /**
     * Interface for a WorkerHandler result return.
     */
    public interface OnQueryCompleteListener {
        /**
         * Called when the image load is complete.
         */
        public void onContactQueryComplete(int token, WorkerArgs args);
        
        /**
         * Called when RCS group member provider query complete.
         */
        public void onGroupMemberQueryComplete(int token, WorkerArgs args);
    }

    // constants
    private static final int EVENT_QUERY_CONTACT = 1;
    private static final int EVENT_QUERY_RCS_GROUP = 2;
    private static final int EVENT_QUERY_PROFILE = 3;
    private static final int EVENT_UPDATE_RCS_GROUP = 4;

    /**
     * Thread worker class that handles the task of query
     */
    private class ResultHandler extends Handler {
        public ResultHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            WorkerArgs args = (WorkerArgs) msg.obj;
            if (args.listener == null)
                return;
            
            switch (msg.arg1) {
                case EVENT_QUERY_CONTACT:
                    args.listener.onContactQueryComplete(msg.what, args);
                    break;
                case EVENT_QUERY_RCS_GROUP:
                    args.listener.onGroupMemberQueryComplete(msg.what, args);
                default:
                    break;
            }
        }
    };

    /** Handler run on a worker thread to query asynchronously. */
    private static Handler sThreadHandler;

    /** For forcing the system to call its constructor */
    @SuppressWarnings("unused")
    private static PortraitAsyncHelper sInstance;

    static {
        sInstance = new PortraitAsyncHelper();
    }

    /**
     * Private constructor for static class
     */
    private PortraitAsyncHelper() {
        HandlerThread thread = new HandlerThread("PortraitAsyncHelper");
        thread.start();
        sThreadHandler = new WorkerHandler(thread.getLooper());
    }

    public static final class WorkerArgs {
        public Context context;
        public ArrayList<String> name;
        public ArrayList<String> number;
        public ArrayList<String> image;
        public String chatId;
        public Object cookie;
        public Cursor cursor;
        public boolean queryProfile;
        public Handler resultHandler;
        public OnQueryCompleteListener listener;
    }

    /**
     * Thread worker class that handles the task of query
     */
    private class WorkerHandler extends Handler {
        public WorkerHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.arg1) {
                case EVENT_QUERY_CONTACT:
                    loadNameandImageFormContacts(msg);
                    break;
                case EVENT_QUERY_RCS_GROUP:
                    queryRcsGroupMember(msg);
                    break;
                case EVENT_QUERY_PROFILE:
                    break;
                case EVENT_UPDATE_RCS_GROUP:
                    updateRcsGroupMember(msg);
                    break;
                default:
                    break;
            }

            // send the reply to the enclosing class.
            WorkerArgs args = (WorkerArgs) msg.obj;
            Message reply = args.resultHandler.obtainMessage(msg.what);
            reply.arg1 = msg.arg1;
            reply.obj = msg.obj;
            reply.sendToTarget();
        }
        
        private void loadNameandImageFormContacts(Message msg) {
            WorkerArgs args = (WorkerArgs) msg.obj;
            InputStream inputStream = null;
            Cursor cursor = null;
            int hasImageCount = 0;
            Log.d(TAG, "loadNameandImageFormContacts: " + args.number);
            
            for (int i = 0; i < args.number.size(); i++) {
            try {
                Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,
                    Uri.encode(args.number.get(i)));
                cursor = args.context.getContentResolver().query(uri,
                    new String[]{PhoneLookup._ID, PhoneLookup.DISPLAY_NAME},
                    null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    // Look for the name
                    String name = cursor.getString(cursor.getColumnIndex(PhoneLookup.DISPLAY_NAME));
                    args.name.add(i, name);
                    // Look for the ID
                    long contactId = cursor.getLong(cursor.getColumnIndex(PhoneLookup._ID));
                    Uri displayPhotoUri = ContentUris.withAppendedId(
                        Contacts.CONTENT_URI, contactId);

                    try {
                        inputStream = Contacts.openContactPhotoInputStream(
                                args.context.getContentResolver(), displayPhotoUri);
                        if (inputStream != null) {
                            try {
                                byte data[] = new byte[inputStream.available()];
                                inputStream.read(data, 0, data.length);
                                String image = Base64.encodeToString(data, Base64.DEFAULT);
                                args.image.add(i, image);
                                hasImageCount++;
                            } catch (IOException e) {
                                Log.e(TAG, "parse contact image error");
                            }
                        } else {
                            args.image.add(i, new String()); // add empty string
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Error opening photo input stream", e);
                    } finally {
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e) {
                                Log.e(TAG, "Unable to close input stream.", e);
                            }
                        }
                    }
                } else {
                    args.name.add(i, new String()); // add empty string
                    args.image.add(i, new String()); // add empty string
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            }
            Log.d(TAG, "loadNameandImageFormContacts completed, name=" + args.name);
        }
        
        private void queryRcsGroupMember(Message msg) {
            WorkerArgs args = (WorkerArgs) msg.obj;
            String chatId = args.chatId;
            Cursor cursor = null;
            String selection = TextUtils.isEmpty(chatId) ? 
                    null : GroupMemberData.COLUMN_CHAT_ID + "='" + chatId + "'";
            if (selection != null) {
                selection  = selection + " AND " +
                    GroupMemberData.COLUMN_STATE + "<>" + GroupMemberData.STATE.STATE_PENDING;
            }
            if (!TextUtils.isEmpty(chatId) && args.number != null 
                   && !TextUtils.isEmpty(args.number.get(0))) {
                selection = GroupMemberData.COLUMN_CONTACT_NUMBER + "='" + args.number.get(0) + "'";
            }
            Log.d(TAG, "queryRcsGroupMember: Selection=" + selection);
            cursor = args.context.getContentResolver().query(
                    RCSUtils.RCS_URI_GROUP_MEMBER, RCSUtils.PROJECTION_GROUP_MEMBER, 
                    selection, null, null);
            args.cursor = cursor;
        }
        
        private void updateRcsGroupMember(Message msg) {
            WorkerArgs args = (WorkerArgs) msg.obj;
            Log.d(TAG, "updateRcsGroupMember: number=" + args.number.get(0));
            
            ContentValues cv = new ContentValues();
            cv.put(GroupMemberData.COLUMN_PORTRAIT, args.image.get(0));
            String where = GroupMemberData.COLUMN_CONTACT_NUMBER + "='" 
                              + args.number.get(0) + "'";
            args.context.getContentResolver().update(RCSUtils.RCS_URI_GROUP_MEMBER, cv, where, null);
        }
    }

    /**
     * Starts an asynchronous query form Contacts. After finishing the load,
     * {@link OnQueryCompleteListener#onContactQueryComplete(int, String, String, Object)}
     * will be called.
     *
     * @param token Arbitrary integer which will be returned as the first argument of
     * {@link OnQueryCompleteListener#onContactQueryComplete(int, String, String, Object)}
     * @param context Context object used to do the time-consuming operation.
     * @param phoneNumber mobile number to be used to fetch the name and photo
     * @param listener Callback object which will be used when the asynchronous load is done.
     * Can be null, which means only the asynchronous load is done while there's no way to
     * obtain the loaded photos.
     * @param cookie Arbitrary object the caller wants to remember, which will become the
     * fourth argument of {@link OnQueryCompleteListener#onContactQueryComplete(int, String,
     * String, Object)}. Can be null, at which the callback will also has null for the argument.
     */
    public static final void startObtainPhotoAsync(int token, Context context,
            ArrayList<String> phoneNumber, OnQueryCompleteListener listener,
            String chatId, boolean queryProfile) {    
        // setup arguments
        WorkerArgs args = new WorkerArgs();
        args.resultHandler = createResultHandler();
        args.chatId = chatId;
        args.context = context;
        args.number = phoneNumber;
        args.name = new ArrayList<String>();
        args.image = new ArrayList<String>();
        args.listener = listener;
        args.queryProfile = queryProfile;

        // setup message arguments
        Message msg = sThreadHandler.obtainMessage(token);
        msg.arg1 = EVENT_QUERY_CONTACT;
        msg.obj = args;

        // notify the thread to begin working
        sThreadHandler.sendMessage(msg);
    }
    
    public static final void startQueryRcsGroup(int token, Context context, String chatId,
            OnQueryCompleteListener listener, Object cookie) {
        // setup arguments
        WorkerArgs args = new WorkerArgs();
        args.resultHandler = createResultHandler();
        args.cookie = cookie;
        args.context = context;
        args.chatId = chatId;
        args.listener = listener;
        args.queryProfile = false;
        
        // setup message arguments
        Message msg = sThreadHandler.obtainMessage(token);
        msg.arg1 = EVENT_QUERY_RCS_GROUP;
        msg.obj = args;

        // notify the thread to begin working
        sThreadHandler.sendMessage(msg);
    }
    
    public static final void startQueryRcsMember(int token, Context context, String chatId,
            String number, OnQueryCompleteListener listener, boolean queryProfile) {
        // setup arguments
        WorkerArgs args = new WorkerArgs();
        args.resultHandler = createResultHandler();
        args.cookie = null;
        args.context = context;
        args.chatId = chatId;
        args.number = new ArrayList<String>();
        args.number.add(number);
        args.listener = listener;
        args.queryProfile = queryProfile;
        
        // setup message arguments
        Message msg = sThreadHandler.obtainMessage(token);
        msg.arg1 = EVENT_QUERY_RCS_GROUP;
        msg.obj = args;

        // notify the thread to begin working
        sThreadHandler.sendMessage(msg);
    }

    public static final void startUpdateRcsMember(int token, Context context, String number,
            String image, Object cookie) {
        // setup arguments
        WorkerArgs args = new WorkerArgs();
        args.resultHandler = createResultHandler();
        args.cookie = cookie;
        args.context = context;
        args.number = new ArrayList<String>();
        args.image = new ArrayList<String>();
        args.number.add(number);
        args.image.add(image);
        args.listener = null;
        
        // setup message arguments
        Message msg = sThreadHandler.obtainMessage(token);
        msg.arg1 = EVENT_UPDATE_RCS_GROUP;
        msg.obj = args;

        // notify the thread to begin working
        sThreadHandler.sendMessage(msg);
    }

    private static final Handler createResultHandler() {
        return sInstance.new ResultHandler(Looper.getMainLooper());
    }
}
