/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
 
package com.mediatek.rcs.common;

import com.mediatek.rcs.common.IpMessageConsts.FeatureId;
import com.mediatek.rcs.common.binder.RCSServiceManager;
import com.mediatek.rcs.common.service.IRCSChatService;

import android.content.Context;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

/**
 * This class manages the rcs service.
 */

public class RCSMessageServiceManager {
    protected static final String TAG = "RCSMessageServiceManager";
    private static final String PROVISION_INFO_VERSION_ONE = "-1";
    private static final String PROVISION_INFO_VERSION_ZERO = "0";
    private static final String PERMENTLY_DISABLE_VALUE = "-3";
    private static final long PROVISION_INFO_VILIDITY_ONE = -1;
    private static final long PROVISION_INFO_VILIDITY_ZERO = 0;
    public static int INTEGRATION_MODE = 0;
    
    static RCSMessageServiceManager sInstance;

    private RCSMessageServiceManager(Context context) {
    }
    
    public static void initialize(Context context) {
        if (sInstance == null) {
            sInstance = new RCSMessageServiceManager(context);
        }
    }
    
    public static RCSMessageServiceManager getInstance() {
        if (sInstance == null) {
            throw new RuntimeException("RCSMessageServiceManager getInstance: not initialize");
        }
        return sInstance;
    }

    //rcs is register succesful
    public boolean serviceIsReady() {
        Log.d(TAG, "serviceIsReady() entry");
        IRCSChatService service = RCSServiceManager.getInstance().getChatService();
        boolean status = false;
        try {
            status = service.getRegistrationStatus();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return status;
    }



    public void startIpService() {
        Log.d(TAG, "startIpService() entry ");
    }

    //rcs is provisioning successful
    public boolean isActivated() {
        Log.d(TAG, "isActivated() entry");
        boolean active = false;
        IRCSChatService service = RCSServiceManager.getInstance().getChatService();
        try {
            if (service != null) {
                String mNumber = service.getMSISDN();
                if (!TextUtils.isEmpty(mNumber)) {
                    active = true;
                }
            } else {
                Log.e(TAG, "service is not ready");
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return active;
    }

    public boolean isActivated(int subId) {
        Log.d(TAG, "isActivated(int subId) entry with simId is " + subId);
        return true;
    }


    public boolean isEnabled() {
        Log.d(TAG, "isEnabled() entry ");
        return true;
    }

    public boolean isEnabled(int simId) {
        Log.d(TAG, "isEnabled(int simId) entry with simId is " + simId);
        return true;
    }
    
    public int getActivationStatus() {
        return 0;
    }

    public int getActivationStatus(int simId) {
        return 0;
    }

    public void enableIpService() {
        return;
    }

    public void enableIpService(int simId) {
        return;
    }

    public void disableIpService() {
        return;
    }

    public void disableIpService(int simId) {
        return;
    }


    public boolean isFeatureSupported(int featureId) {
    	Log.d(TAG, "isFeatureSupported() featureId is " + featureId);
        switch (featureId) {
            case FeatureId.FILE_TRANSACTION:
            case FeatureId.EXTEND_GROUP_CHAT:
            case FeatureId.GROUP_MESSAGE:
                return true;
            case FeatureId.PARSE_EMO_WITHOUT_ACTIVATE:
            case FeatureId.CHAT_SETTINGS:
            case FeatureId.ACTIVITION:
            case FeatureId.ACTIVITION_WIZARD:
            case FeatureId.MEDIA_DETAIL:
            case FeatureId.TERM:
             case FeatureId.EXPORT_CHAT:
            case FeatureId.APP_SETTINGS:
          		return false;
            default:
                return false;
        }
    }

    public boolean isAlwaysSendMessageByJoyn() {
        return false;
    }

    public boolean isAlwaysSendFileByJoyn() {
        return false;
    }

    public int getIntegrationMode() {
        return IpMessageConsts.IntegrationMode.FULLY_INTEGRATED;
    }

    public void checkDefaultSmsAppChanged() {
    }

    public int getDisableServiceStatus() {
        return IpMessageConsts.DisableServiceStatus.ENABLE;
    }
}
