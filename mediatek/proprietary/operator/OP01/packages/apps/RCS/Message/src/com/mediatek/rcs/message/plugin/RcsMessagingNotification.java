package com.mediatek.rcs.message.plugin;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Threads;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.mediatek.mms.ipmessage.IpMessagingNotification;
import com.mediatek.rcs.message.R;
import com.mediatek.rcs.message.utils.RcsMessageUtils;

import com.mediatek.rcs.common.IpMessage;
import com.mediatek.rcs.common.IpMessageConsts;
import com.mediatek.rcs.common.IpImageMessage;
import com.mediatek.rcs.common.RCSMessageManager;
import com.mediatek.rcs.common.RCSMessageContactManager;
import com.mediatek.rcs.common.provider.ThreadMapCache;
import com.mediatek.rcs.common.provider.ThreadMapCache.MapInfo;
import com.mediatek.rcs.common.service.Participant;
import com.mediatek.rcs.common.utils.ContextCacher;

public class RcsMessagingNotification extends IpMessagingNotification {
    private static String TAG = "RcseMessagingNotification";
    
    private static String NEW_MESSAGE_ACTION = "com.mediatek.mms.ipmessage.newMessage";
    private static int NEW_GROUP_INVITATION_NOTIFY_ID = 321;

    private Context mContext;
//    private IpMessageReceiver mIpMessageReceiver;
    private IntentFilter mIntentFilter;

    @Override
    public boolean IpMessagingNotificationInit(Context context) {
        mContext = context;
//        mIpMessageReceiver = new IpMessageReceiver();
//        mIntentFilter = new IntentFilter();
//        mIntentFilter.addAction(NEW_MESSAGE_ACTION);
//        context.registerReceiver(mIpMessageReceiver, mIntentFilter);
//        return true;
        return super.IpMessagingNotificationInit(context);
    }

    @Override
    public String onIpFormatBigMessage(String number, String sender) {
        boolean isGroup = false;
//        String ipSender = IpMessageContactManager.getInstance(mContext).getNameByNumber(number);
//        if (!ipSender.isEmpty()) {
//            sender = ipSender;
//        }
//        return sender;
        if (isGroup) {
            //TODO: format group sender
            return sender;
        }
        return super.onIpFormatBigMessage(number, sender);
    }
    
    @Override
    public boolean isIpAttachMessage(long msgId, Cursor cursor) {
        long ipmsgId = cursor.getLong(cursor.getColumnIndex(Sms.IPMSG_ID));
        if (ipmsgId > 0) {
            return false;
        } else if (ipmsgId < 0) {
            return true;
        }
//        IpMessage ipMessage = RCSMessageManager.getInstance(mContext).getIpMsgInfo(msgId);
//        if (null != ipMessage) {
//            int ipMessageType = ipMessage.getType();
//            if (ipMessageType != IpMessageConsts.IpMessageType.TEXT) {
//                return false;
//            } else {
//                return true;
//            }
//        }
        return super.isIpAttachMessage(msgId, cursor);
    }
    
    public String onNewIpMsgContent(long msgId, Cursor cursor) {
        Context pluginContext = ContextCacher.getPluginContext();
        long ipMsgId = cursor.getLong(6);
        if (ipMsgId == 0) {
            return null;
        }
        IpMessage ipMessage = RCSMessageManager.getInstance(pluginContext).getIpMsgInfo(ipMsgId);
        
        if (null != ipMessage && ipMessage.getBurnedMessage()) {
            return pluginContext.getString(R.string.menu_burned_msg);
        } else if ( null != ipMessage){
            int ipMessageType = ipMessage.getType();
            if (ipMessageType == IpMessageConsts.IpMessageType.PICTURE) {
                return "[Picture]";
            } else if(ipMessageType == IpMessageConsts.IpMessageType.VIDEO) {
                return "[Video]";
            } else if (ipMessageType == IpMessageConsts.IpMessageType.VOICE) {
                return "[Voice]";
            } else if(ipMessageType == IpMessageConsts.IpMessageType.VCARD) {
                return "[Vcard]";
            } else if (ipMessageType == IpMessageConsts.IpMessageType.GEOLOC) {
                return "[Geolocation]";
            }
        }
        return null;
    }
    
    @Override
    public Bitmap getIpBitmap(long msgId, Cursor cursor) {
        IpMessage ipMessage = RCSMessageManager.getInstance(mContext).getIpMsgInfo(msgId);
        if (null != ipMessage) {
            int ipMessageType = ipMessage.getType();
            if (ipMessageType == IpMessageConsts.IpMessageType.PICTURE) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                String filePath = ((IpImageMessage) ipMessage).getPath();
                return BitmapFactory.decodeFile(filePath);
            }
        }
        return null;
    }
    
    @Override
    public boolean onIpgetNewMessageNotificationInfo(String number, long threadId) {
//        if (number != null && number.startsWith(IpMessageConsts.GROUP_START)) {
//            Log.d(TAG, "new group message received.");
//            Intent clickIntent = new Intent(IpMessageConsts.ACTION_GROUP_NOTIFICATION_CLICKED);
//            clickIntent.putExtra(IpMessageConsts.RemoteActivities.KEY_THREAD_ID, threadId);
//            clickIntent.putExtra(IpMessageConsts.RemoteActivities.KEY_BOOLEAN, false);
//            return true;
//        }
//        return false;
        return super.onIpgetNewMessageNotificationInfo(number, threadId);
    }
    
    @Override
    public String getIpNotificationTitle(String number, long threadId, String title) {
        boolean isGroupChat = RcsMessageUtils.isGroupchat(threadId);
        if (isGroupChat) {
            MapInfo info = ThreadMapCache.getInstance().getInfoByThreadId(threadId);
            if (info != null) {
                String groupName = info.getNickName();
                if (TextUtils.isEmpty(groupName)) {
                    groupName = info.getSubject();
                }
                if (TextUtils.isEmpty(groupName)) {
                    groupName = title;
                }
                return groupName;
            }
        }
        return title;
    }

    @Override
    public boolean setIpSmallIcon(Notification.Builder noti, String number) {
        // / M: modify for ip message
//        if (IpMessageServiceMananger.getInstance(mContext).getIntegrationMode() == IpMessageConsts.IntegrationMode.CONVERGED_INBOX
//                && number.startsWith(IpMessageConsts.JOYN_START)) {
//            noti.setSmallIcon(R.drawable.notify_chat_message);
//            return false;
//        } else {
//            return true;
//        }
        return super.setIpSmallIcon(noti, number);
    }

    @Override
    public String ipBuildTickerMessage(String address, String displayAddress) {
        
        boolean isGroup = false;
        if (isGroup) {
            return RCSMessageContactManager.getInstance(mContext).getNameByNumber(address);
//            return IpMessageContactManager.getInstance(mContext).getNameByNumber(address);
        } else {
            return displayAddress;
        }
    }

    /** Use RcsMessageUtils.unblockingNotifyNewIpMessage(...) instead
     * NotificationReceiver receives some kinds of ip message notification and
     * notify the listeners.
     */
//    public class RcsMessageReceiver extends BroadcastReceiver {
//        private static final String TAG = "IpMessageReceiver";
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Log.d(TAG, "onReceive: " + "Action = " + intent.getAction());
//            new ReceiveNotificationTask(context).execute(intent);
//        }
//
//        private class ReceiveNotificationTask extends AsyncTask<Intent, Void, Void> {
//            private Context mContext;
//
//            ReceiveNotificationTask(Context context) {
//                mContext = context;
//            }
//
//            @Override
//            protected Void doInBackground(Intent... intents) {
//                Intent intent = intents[0];
//                String action = intent.getAction();
//                if (NewMessageAction.ACTION_NEW_MESSAGE.equals(action)) {
//                    handleNewMessage(intent);
//                } else {
//                    Log.w(TAG, "unknown notification type.");
//                }
//                return null;
//            }
//
//            private void handleNewMessage(Intent intent) {
//                Log.d(TAG, "handleNewMessage");
//                long messageId = intent.getLongExtra(NewMessageAction.IP_MESSAGE_KEY, 0);
//                if (messageId <= 0) {
//                    Log.e(TAG, "get ip message failed.");
//                    return;
//                }
//                Log.d(TAG, "new message id:" + messageId);
//
//                String from = IpMessageContactManager.getInstance(mContext).getNumberByMessageId(
//                        messageId);
//                Log.d(TAG, "\t displyFrom:" + from);
//                long threadId = Telephony.Threads.getOrCreateThreadId(mContext, from);
//                Log.d(TAG, "\t threadid:" + threadId);
//                // add for ipmessage
//                RcsMessageUtils.blockingIpUpdateNewMessageIndicator(mContext, threadId, false,
//                        null);
//                if (RcsMessageUtils.isIpPopupNotificationEnable()) {
//                    notifyNewIpMessageDialog(messageId);
//                }
//                RcsMessageUtils.notifyIpWidgetDatasetChanged(mContext);
//            }
//        }
//
//        // Dialog mode
//        private void notifyNewIpMessageDialog(long id) {
//            Log.d(TAG, "notifyNewIpMessageDialog,id:" + id);
//            Context context = mContext.getApplicationContext();
//            if (RcsMessageUtils.isIpHome(context)) {
//                Log.d(TAG, "at launcher");
//                Intent smsIntent = RcsMessageUtils.getDialogModeIntent(context);
//                Uri smsUri = Uri.parse("content://sms/" + id);
//                smsIntent.putExtra("com.android.mms.transaction.new_msg_uri", smsUri.toString());
//                smsIntent.putExtra("ipmessage", true);
//                smsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(smsIntent);
//            } else {
//                Log.d(TAG, "not at launcher");
//            }
//        }
//    }
    
//    public static void NotifyNewGroupInviteNotification(b) {
//        
//    }
    

    public static void cancelNewGroupInviations(Context context) {
        NotificationManager nm = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(NEW_GROUP_INVITATION_NOTIFY_ID);
    }
    

    public static void updateNewGroupInvitation(Participant p, String subject, long threadId) {
       
        asynUpdateNewGroupInvitation(p, subject, threadId);
    }
    public static void asynUpdateNewGroupInvitation(final Participant p, final String subject, 
                                            final long threadId) {
        new Thread(new Runnable() {
            
            @Override
            public void run() {
                // TODO Auto-generated method stub
                Context hostContext = ContextCacher.getHostContext();
                Context pluginContext = ContextCacher.getPluginContext();
                int count = 0;
                boolean isCurrentThreadExist = false;
                if (hostContext != null) {
                    Cursor cursor = hostContext.getContentResolver().query(RcsConversation.URI_CONVERSATION, 
                            RcsConversation.PROJECTION_NEW_GROUP_INVITATION,
                            RcsConversation.SELECTION_NEW_GROUP_INVITATION_BY_STATUS,
                            null, null);
                    if (cursor != null) {
                        try {
                            count = cursor.getCount();
                            cursor.moveToFirst();
                            do {
                                long thread_id = cursor.getLong(0);
                                if (thread_id == threadId) {
                                    isCurrentThreadExist = true;
                                    break;
                                }
                            } while(cursor.moveToNext());
                        } catch (Exception e) {
                            // TODO: handle exception
                        } finally {
                            cursor.close();
                        }
                    }
                    
                } else {
                    throw new RuntimeException("RcsMessagingNotification ->asynUpdateNewGroupInvitation, context is null");
                }
                if (!isCurrentThreadExist) {
                    count ++;
                }
                notifyNewGroupInvitation(p, subject, threadId, count);
            }
        }, "asynUpdateNewGroupInvitation").start();
    }
    
    public static final String NOTIFICATION_RINGTONE = "pref_key_ringtone";
    public static final String NOTIFICATION_MUTE = "pref_key_mute";
    public static final String MUTE_START = "mute_start";
    public static final String DEFAULT_RINGTONE = "content://settings/system/notification_sound";
    public static void notifyNewGroupInvitation(final Participant p, final String subject, 
            final long threadId, int count) {
        long timeMillis = System.currentTimeMillis();
        Context pluginContext = ContextCacher.getPluginContext();
        Context hostContext = ContextCacher.getHostContext();
        
        int smallIcon = RcsMessageUtils.getNotificationResourceId();
        if (smallIcon == 0) {
            return;
        }

        String contentTitle = null;
        String contentText = null;
        Bitmap largeIcon = BitmapFactory.decodeResource(pluginContext.getResources(), R.drawable.group_example);;
        String tiker = subject + pluginContext.getString(R.string.group_invite_tiker);
        

        if (count == 1) {
            contentTitle = subject;
            if (TextUtils.isEmpty(contentTitle)) {
                contentTitle = pluginContext.getString(R.string.group_chat);
            }
            String invitee = p.getDisplayName();
            if (TextUtils.isEmpty(invitee)) {
                String number = p.getContact();
                if (!TextUtils.isEmpty(number)) {
                    Log.e(TAG, "[notifyNewGroupInvitation]: number is null");
                    invitee = RcsMessageUtils.getContactNameByNumber(number);
                }
            }
            
            if (TextUtils.isEmpty(invitee)) {
                invitee = "Unknown";
            }
            contentText = pluginContext.getString(R.string.one_group_invite_content, invitee);
        } else {
            contentTitle = pluginContext.getString(R.string.group_chat);
            contentText = pluginContext.getString(R.string.more_group_invite_content, count);
        }
        
        int defaults = getNotificationDefaults(hostContext);

        //content intent
        Intent clickIntent = null;
        clickIntent = new Intent();
        clickIntent.setClassName(hostContext, "com.android.mms.transaction.MessagingNotificationProxyReceiver");
        clickIntent.putExtra("thread_count", count);
        clickIntent.putExtra("thread_id", threadId);
//        clickIntent.putExtra("message_count", count);
        PendingIntent pIntent = PendingIntent.getBroadcast(hostContext, 0, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // notify sound
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(hostContext);
        String muteStr = sp.getString(NOTIFICATION_MUTE, Integer.toString(0));
        long appMute = Integer.parseInt(muteStr);
        long appMuteStart = sp.getLong(MUTE_START, 0);
        if (appMuteStart > 0 && appMute > 0) {
            long currentTime = (System.currentTimeMillis() / 1000);
            if ((appMute * 3600 + appMuteStart / 1000) <= currentTime) {
                appMute = 0;
                appMuteStart = 0;
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(hostContext).edit();
                editor.putLong(MUTE_START, 0);
                editor.putString(NOTIFICATION_MUTE, String.valueOf(appMute));
                editor.apply();
            }
        }
        /// M:Code analyze 017, modify the logic of playing sound @{
        Uri ringtone = null;
        if (appMute == 0) {
            String ringtoneStr = sp.getString(NOTIFICATION_RINGTONE, null);
            ringtoneStr = checkRingtone(hostContext, ringtoneStr);
            ringtone = TextUtils.isEmpty(ringtoneStr) ? null : Uri.parse(ringtoneStr);
        }
//        smallIcon = R.drawable.stat_notify_sms;
        
        final Notification.Builder notifBuilder = new Notification.Builder(hostContext)
        .setWhen(timeMillis)
        .setContentTitle(contentTitle)
        .setContentText(contentText)
        .setLargeIcon(largeIcon)
        .setDefaults(defaults)
        .setContentIntent(pIntent)
        .setSmallIcon(smallIcon)
        .setSound(ringtone);
        
        Notification notification = notifBuilder.build();
        NotificationManager nm = (NotificationManager)
                hostContext.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(NEW_GROUP_INVITATION_NOTIFY_ID, notification);
    }

    public static final String checkRingtone(Context context, String ringtoneUri) {
        if (!TextUtils.isEmpty(ringtoneUri)) {
            InputStream inputStream = null;
            boolean invalidRingtone = true;
            try {
                inputStream = context.getContentResolver().openInputStream(Uri.parse(ringtoneUri));
            } catch (FileNotFoundException ex) {
            } finally {
                if (inputStream != null) {
                    invalidRingtone = false;
                    try {
                        inputStream.close();
                    } catch (IOException ex) {
                    }
                }
            }
            if (invalidRingtone) {
                ringtoneUri = DEFAULT_RINGTONE;
            }
        }
        return ringtoneUri;
    }

    protected static void processNotificationSound(Context context, Notification notification, Uri ringtone) {
        int state = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getCallState();
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager.shouldVibrate(AudioManager.VIBRATE_TYPE_NOTIFICATION)) {
            /* vibrate on */
            notification.defaults |= Notification.DEFAULT_VIBRATE;
        }
        notification.sound = ringtone;
    }
    
    protected static int getNotificationDefaults(Context context) {
        int defaults = 0;
        int state = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getCallState();
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager.shouldVibrate(AudioManager.VIBRATE_TYPE_NOTIFICATION)) {
            /* vibrate on */
            defaults |= Notification.DEFAULT_VIBRATE;
        }
        defaults |= Notification.DEFAULT_LIGHTS;
        return defaults;
    }

}
