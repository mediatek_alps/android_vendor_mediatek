/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.rcs.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import android.content.Context;

/**
 * Provide chat management related interface
 */
public class RCSMessageChatManger {
    private static final String TAG = "RCSMessageChatManger";
    private static final Map<String, Integer> VISIBLE_ITEM_MAP = new ConcurrentHashMap<String, Integer>(); 

    private static RCSMessageChatManger sInstance;
    private RCSMessageChatManger(Context context) {

    }
    
    public static RCSMessageChatManger getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new RCSMessageChatManger(context);
        }
        return sInstance;
    }

    public boolean needShowInviteDlg(long threadId) {
        return false;
    }

    public boolean handleInviteDlgLater(long threadId) {
        return false;
    }

    public boolean handleInviteDlg(long threadId) {
        return false;
    }

    /**
     * Check if need show reminder dialog
     * @param threadId thread ID
     * @return 0: reminder invalid; 1: reminder invite;
     *         2: reminder actived; 3: reminder switch;
     *         4: reminder enable.
     */
    public int needShowReminderDlg(long threadId) {
        return 0;
    }

    public boolean needShowSwitchAcctDlg(long threadId) {
        return false;
    }


//    @Override
//    public void enterChatMode(String number) {
//        super.enterChatMode(number);
//    }
//
//    
//
//    @Override
//    public void exitFromChatMode(String number) {
//        super.exitFromChatMode(number);
//    }
}
