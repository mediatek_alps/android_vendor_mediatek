package com.mediatek.rcs.common.binder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import com.mediatek.rcs.common.IBurnMessageCapabilityListener;
import com.mediatek.rcs.common.INotifyListener;
import com.mediatek.rcs.common.RCSMessageManager;
import com.mediatek.rcs.common.provider.RCSDataBaseUtils;
import com.mediatek.rcs.common.service.IRCSChatService;
import com.mediatek.rcs.common.service.IRCSChatServiceListener;
import com.mediatek.rcs.common.utils.Logger;


public class RCSServiceManager {

    private static final String TAG = "RCSServiceManager";
    private static RCSServiceManager sInstance = null;
    private IRCSChatService mChatService = null;
    private Context mContext = null;
    private IRCSChatServiceListener mListener = null;
    
    private List<INotifyListener> mNotifyListeners = new ArrayList<INotifyListener>();
    
    private List<IBurnMessageCapabilityListener> mBurnMsgCapListeners = new ArrayList<IBurnMessageCapabilityListener>();

    private Map<String, GroupInfo> mInvitingGroup = new ConcurrentHashMap<String, GroupInfo>();

    private RCSServiceManager(Context context) {
        Logger.d(TAG, "call constructor");
        mContext = context;
        Intent intent = new Intent("com.mediatek.rcs.RemoteService");
        intent.setPackage("com.android.mms");
        mContext.startService(intent);
        mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName name, IBinder service) {
            Logger.d(TAG, "service connect!!");
            mChatService = IRCSChatService.Stub.asInterface(service);
            mListener = new RCSChatServiceListener();
            try {
                mChatService.addRCSChatServiceListener(mListener);
                List<String> chatIds = RCSDataBaseUtils.getAvailableGroupChatIds();
                mChatService.startGroups(chatIds);
                RCSMessageManager.getInstance(mContext).deleteLastBurnedMessage();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        public void onServiceDisconnected(ComponentName name) {
            Logger.d(TAG, "service disconnect!!");
            try {
                mChatService.removeRCSChatServiceListener(mListener);
                mChatService = null;
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        
    };
    
    public IRCSChatServiceListener getServiceListener() {
        return mListener;
    }

    public void registNotifyListener(INotifyListener listener) {
        mNotifyListeners.add(listener);
    }

    public void unregistNotifyListener(INotifyListener listener) {
        mNotifyListeners.remove(listener);
    }

    public void registBurnMsgCapListener(IBurnMessageCapabilityListener listener) {
        mBurnMsgCapListeners.add(listener);
    }

    public void unregistBurnMsgCapListener(IBurnMessageCapabilityListener listener) {
        mBurnMsgCapListeners.remove(listener);
    }

    public List<INotifyListener> getNotifyListeners() {
        return mNotifyListeners;
    }

    public List<IBurnMessageCapabilityListener> getBurnMsgCapListeners() {
        return mBurnMsgCapListeners;
    }

    public static void createManager(Context context) {
        Logger.d(TAG, "createManager, entry");
        if (sInstance == null) {
            sInstance = new RCSServiceManager(context);
        }
    }

    public static RCSServiceManager getInstance() {
        return sInstance;
    }

    public IRCSChatService getChatService() {
        return mChatService;
    }

    public boolean getBurnMsgCap(String contact) {
        Logger.d(TAG, "getBurnMsgCap for " + contact);
        boolean cap = false;
        try {
            mChatService.getBurnMessageCapability(contact);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return cap;
    }

    public void recordInvitingGroup(String chatId, String subject, List<String> participants) {

        mInvitingGroup.put(chatId, new GroupInfo(chatId, subject, participants));
    }

    public GroupInfo getInvitingGroup(String chatId) {
        return mInvitingGroup.get(chatId);
    }

    public void removeInvitingGroup(String chatId) {
        mInvitingGroup.remove(chatId);
    }

    class GroupInfo {
        private List<String> mParticipants;
        private String mSubject;
        private String mChatId;
        
        public GroupInfo(String chatId, String subject, List<String> participants) {
            mParticipants = participants;
            mSubject = subject;
            mChatId = chatId;
        }
        
        public String getSubject() {
            return mSubject;
        }
        
        public List<String> getParticipants() {
            return mParticipants;
        }
    }

    @Override
    public void finalize() {
        try {
            Logger.d(TAG, "finalize()!!");
            mContext.unbindService(mConnection);
            super.finalize();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
