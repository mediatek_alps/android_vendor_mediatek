/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.rcs.message.ft;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;
import java.sql.Blob;

import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import android.util.Log;
import android.widget.Toast;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.content.Intent;
import android.app.NotificationManager;
import android.os.AsyncTask;
import android.content.Context;
import android.content.Intent;
import android.media.MediaFile;

import org.gsma.joyn.JoynServiceException;
import org.gsma.joyn.JoynServiceNotAvailableException;
import org.gsma.joyn.ft.FileTransfer;
import org.gsma.joyn.ft.FileTransferIntent;
import org.gsma.joyn.ft.FileTransferListener;
import org.gsma.joyn.ft.FileTransferService;
import org.gsma.joyn.ft.FileTransferServiceConfiguration;
import org.gsma.joyn.ft.NewFileTransferListener;

import com.mediatek.rcs.common.utils.RCSUtils;
import org.gsma.joyn.ft.MultiFileTransferLog;


import com.mediatek.rcs.message.ft.FileTransferChatWindow;
import com.mediatek.rcs.message.chat.GsmaManager;
import com.mediatek.rcs.common.service.FileStruct;
import com.mediatek.rcs.common.MessageStatusUtils.IFileTransfer;
import com.mediatek.rcs.common.MessageStatusUtils.IFileTransfer.Status;
import java.util.Random;

import com.mediatek.rcs.common.provider.RCSDataBaseUtils;
import com.mediatek.rcs.message.chat.RCSChatServiceImpl;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Threads;
import android.text.TextUtils;
import android.util.Log;
import com.mediatek.rcs.common.utils.ContextCacher;


public class FileTransferManager {
    public static final String TAG = "RCS/FileTransferManager";

    private SentFileTransferManager mOutGoingFileTransferManager = new SentFileTransferManager();
    FileTransferService mFileTransferService = null;
    private static FileTransferManager INSTANCE = null;
    protected ReceiveFileTransferManager mReceiveFileTransferManager = new ReceiveFileTransferManager();
    //private static ChatManagerImpl mModel = null;
    //protected static final ChatManagerImpl chatManager = ChatManagerImpl
    //        .getInstance();
    //protected static final Handler mWorkerHandler = chatManager
    //        .getWorkHandler();

    public static final int FILETRANSFER_ENABLE_OK = 0;
    public static final int FILETRANSFER_DISABLE_REASON_NOT_REGISTER = 1;
    public static final int FILETRANSFER_DISABLE_REASON_CAPABILITY_FAILED = 2;
    public static final int FILETRANSFER_DISABLE_REASON_REMOTE = 3;

    private static final String AUTO_ACCEPT = "autoAccept";
    private static final String CHAT_SESSION_ID = "chatSessionId";
    private static final String ISGROUPTRANSFER = "isGroupTransfer";
    private static final String CHAT_ID = "chatId";

    private static final Random RANDOM = new Random();

    // The blank space text
    private static final String BLANK_SPACE = " ";
    // The empty text
    private static final String EMPTY_STRING = "";
    // The seprator text
    private static final String SEPRATOR = ",";

    protected static RCSChatServiceImpl mService = null;

    private FileTransferManager(RCSChatServiceImpl service) {
        //mModel = ChatManagerImpl.getInstance();
        mService = service;
    }

    public static FileTransferManager getInstance(RCSChatServiceImpl service) {
        Log.v(TAG, "getInstance() FileTransferManager");
        if (INSTANCE == null) {
            INSTANCE = new FileTransferManager(service);
        }
        return INSTANCE;
    }

    public SentFileTransferManager getSentFileTransferManager() {
        return mOutGoingFileTransferManager;
    }

    public ReceiveFileTransferManager getReceiveFileTransferManager() {
        return mReceiveFileTransferManager;
    }

    /**
     * This class is used to manage the whole sent file transfers and make it
     * work in queue
     */
    private static class SentFileTransferManager implements
            SentFileTransfer.IOnSendFinishListener {
        private static final String TAG = "SentFileTransferManager";

        private static final int MAX_ACTIVATED_SENT_FILE_TRANSFER_NUM = 1;

        private ConcurrentLinkedQueue<SentFileTransfer> mPendingList = new ConcurrentLinkedQueue<SentFileTransfer>();

        private CopyOnWriteArrayList<SentFileTransfer> mActiveList = new CopyOnWriteArrayList<SentFileTransfer>();

        private CopyOnWriteArrayList<SentFileTransfer> mResendableList = new CopyOnWriteArrayList<SentFileTransfer>();

        private synchronized void checkNext() {
            int activatedNum = mActiveList.size();
            if (activatedNum < MAX_ACTIVATED_SENT_FILE_TRANSFER_NUM) {
                Log.w(TAG, "checkNext() current activatedNum is "
                        + activatedNum
                        + " will find next file transfer to send");
                final SentFileTransfer nextFileTransfer = mPendingList.poll();
                if (null != nextFileTransfer) {
                    Log.w(TAG,
                            "checkNext() next file transfer found, just send it!");

                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                            nextFileTransfer.send();
                        }
                    });
                    mActiveList.add(nextFileTransfer);
                } else {
                    Log.w(TAG,
                            "checkNext() next file transfer not found, pending list is null");
                }
            } else {
                Log.w(TAG, "checkNext() current activatedNum is "
                        + activatedNum
                        + " MAX_ACTIVATED_SENT_FILE_TRANSFER_NUM is "
                        + MAX_ACTIVATED_SENT_FILE_TRANSFER_NUM
                        + " so no need to find next pending file transfer");
            }
        }
/*
        public void resumesFileSend() {
            Log.w(TAG, "resumeFileSend 01 entry");
            SentFileTransfer resume_file = null;
            try {
                resume_file = mActiveList.get(0);
            } catch (Exception e) {
                Log.d(TAG, "resumeFileSend exception");
            }
            if (resume_file != null) {
                Log.d(TAG, "resumeFileSend not null");
                resume_file.onResume();
            }
            Log.d(TAG, "resumeFileSend 01 exit");
        }
*/
        public void onAddSentFileTransfer(SentFileTransfer sentFileTransfer) {
            Log.w(TAG, "onAddSentFileTransfer() entry, sentFileTransfer =  "
                    + sentFileTransfer);
            if (null != sentFileTransfer) {
                Log.w(TAG, "onAddSentFileTransfer() entry, file "
                        + sentFileTransfer.mFileStruct + " is going to be sent");
                sentFileTransfer.mOnSendFinishListener = this;
                mPendingList.add(sentFileTransfer);
                checkNext();
            }
        }

      /*

        public void onChatDestroy(Object tag) {
            Log.d(TAG, "onChatDestroy entry, tag is " + tag);
            clearTransferWithTag(tag, FILETRANSFER_ENABLE_OK);
        }

        public void clearTransferWithTag(Object tag, int reason) {
            Log.d(TAG, "onFileTransferNotAvalible() entry tag is " + tag
                    + " reason is " + reason);
            if (null != tag) {
                Log.d(TAG, "onFileTransferNotAvalible() tag is " + tag);
                ArrayList<SentFileTransfer> toBeDeleted = new ArrayList<SentFileTransfer>();
                for (SentFileTransfer fileTransfer : mActiveList) {
                    if (tag.equals(fileTransfer.mChatTag)) {
                        Log.d(TAG,
                                "onFileTransferNotAvalible() sent file transfer with chatTag "
                                        + tag + " found in activated list");
                        fileTransfer.onNotAvailable(reason);
                        fileTransfer.onDestroy();
                        toBeDeleted.add(fileTransfer);
                    }
                }
                if (toBeDeleted.size() > 0) {
                    Log.d(TAG,
                            "onFileTransferNotAvalible() need to remove some file transfer from activated list");
                    mActiveList.removeAll(toBeDeleted);
                    toBeDeleted.clear();
                }
                for (SentFileTransfer fileTransfer : mPendingList) {
                    if (tag.equals(fileTransfer.mChatTag)) {
                        Log.d(TAG,
                                "onFileTransferNotAvalible() sent file transfer with chatTag "
                                        + tag + " found in pending list");
                        fileTransfer.onNotAvailable(reason);
                        toBeDeleted.add(fileTransfer);
                    }
                }
                if (toBeDeleted.size() > 0) {
                    Log.d(TAG,
                            "onFileTransferNotAvalible() need to remove some file transfer from pending list");
                    mPendingList.removeAll(toBeDeleted);
                    toBeDeleted.clear();
                }
                for (SentFileTransfer fileTransfer : mResendableList) {
                    if (tag.equals(fileTransfer.mChatTag)) {
                        Log.d(TAG,
                                "onFileTransferNotAvalible() sent file transfer with chatTag "
                                        + tag
                                        + " found in mResendableList list");
                        fileTransfer.onNotAvailable(reason);
                        toBeDeleted.add(fileTransfer);
                    }
                }
                if (toBeDeleted.size() > 0) {
                    Log.d(TAG,
                            "onFileTransferNotAvalible() "
                                    + "need to remove some file transfer from mResendableList list");
                    mResendableList.removeAll(toBeDeleted);
                    toBeDeleted.clear();
                }
            }
        }
        */

        public void resendFileTransfer(SentFileTransfer sentFileTransfer, boolean haveInstance) {
            if (sentFileTransfer != null && haveInstance) {
                Log.d(TAG, "resendFileTransfer 1 , sentFileTransfer != null && haveInstance");
                mResendableList.remove(sentFileTransfer);
                mActiveList.add(sentFileTransfer);
                sentFileTransfer.resend();
            } else {
                Log.d(TAG, "resendFileTransfer 2 , no have instance");
                mActiveList.add(sentFileTransfer);
                sentFileTransfer.resend();
            }
        }
   /*
        public void pauseFileTransfer(String targetFileTransferTag) {
            SentFileTransfer fileTransfer = findActiveFileTransfer(targetFileTransferTag);
            Log.w(TAG, "pauseFileTransfer() the file transfer with tag "
                    + targetFileTransferTag + " is " + fileTransfer);
            if (null != fileTransfer) {
                Log.w(TAG, "pauseFileTransfer() the file transfer with tag "
                        + targetFileTransferTag + " found, ");
                fileTransfer.onPause();
            }
        }

        public void resumeFileTransfer(String targetFileTransferTag) {
            SentFileTransfer fileTransfer = findActiveFileTransfer(targetFileTransferTag);
            Log.w(TAG, "resumeFileTransfer() the file transfer with tag "
                    + targetFileTransferTag + " is " + fileTransfer);
            if (null != fileTransfer) {
                Log.w(TAG, "resumeFileTransfer() the file transfer with tag "
                        + targetFileTransferTag + " found");
                fileTransfer.onResume();
            }
        }
        */
/*
        public void cancelFileTransfer(String targetFileTransferTag) {
            Log.d(TAG,
                    "cancelFileTransfer() begin to cancel file transfer with tag "
                            + targetFileTransferTag);
            SentFileTransfer fileTransfer = findPendingFileTransfer(targetFileTransferTag);
            if (null != fileTransfer) {
                Log.d(TAG,
                        "cancelFileTransfer() the target file transfer with tag "
                                + targetFileTransferTag
                                + " found in pending list");
                fileTransfer.onCancel();
                fileTransfer.onDestroy();
                mPendingList.remove(fileTransfer);
            } else {
                fileTransfer = findActiveFileTransfer(targetFileTransferTag);

                if (null != fileTransfer) {
                Log.w(TAG,
                        "cancelFileTransfer() the target file transfer with tag "
                                + targetFileTransferTag
                                    + " found in active list");
                    fileTransfer.onCancel();
                    fileTransfer.onDestroy();
                    onSendFinish(fileTransfer, Result.REMOVABLE);
                    return;
                }
            } 
                fileTransfer = findResendableFileTransfer(targetFileTransferTag);

                if (null != fileTransfer) {
                    Log.w(TAG,
                            "cancelFileTransfer() the target file transfer with tag "
                                    + targetFileTransferTag
                                    + " found in active list");
                    fileTransfer.onCancel();
                    fileTransfer.onDestroy();
                mResendableList.remove(fileTransfer);
            }

            return;
        }
        */

        @Override
        public void onSendFinish(final SentFileTransfer sentFileTransfer,
                final Result result) {
            Log.w(TAG, "onSendFinish(): sentFileTransfer = " + sentFileTransfer
                    + ", result = " + result);
            if (mActiveList.contains(sentFileTransfer)) {
                //sentFileTransfer.cancelNotification();
                Log.w(TAG, "onSendFinish() file transfer "
                        + sentFileTransfer.mFileStruct + " with "
                        + result + " remove it from activated list");
                switch (result) {
                case RESENDABLE:
                    mResendableList.add(sentFileTransfer);
                    mActiveList.remove(sentFileTransfer);

                    Log.v(TAG, "resend test:  mResendableList.add and  mActiveList.remove");

                    
                    break;
                case REMOVABLE:
                    mActiveList.remove(sentFileTransfer);

                       Log.v(TAG, "resend test:  mActiveList.remove");
                    break;
                default:
                    break;
                }
                AsyncTask.execute(new Runnable() {
                    public void run() {
                        checkNext();
                    }
                });
            }
        }

        private SentFileTransfer findActiveFileTransfer(String targetTag) {
            Log.w(TAG, "findActiveFileTransfer entry, targetTag is "
                    + targetTag);
            return findFileTransferByTag(mActiveList, targetTag);
        }

        private SentFileTransfer findPendingFileTransfer(String targetTag) {
            Log.w(TAG, "findPendingFileTransfer entry, targetTag is "
                    + targetTag);
            return findFileTransferByTag(mPendingList, targetTag);
        }

        private SentFileTransfer findResendableFileTransfer(String targetTag) {
            Log.d(TAG, "findResendableFileTransfer entry, targetTag is "
                    + targetTag);
            return findFileTransferByTag(mResendableList, targetTag);
        }

        private SentFileTransfer findFileTransferByTag(
                Collection<SentFileTransfer> whereToFind, String targetTag) {
            if (null != whereToFind && null != targetTag) {
                for (SentFileTransfer sentFileTransfer : whereToFind) {
                    String fileTransferTag = sentFileTransfer.mFileTransferTag;
                    if (targetTag.equals(fileTransferTag)) {
                        Log.w(TAG,
                                "findFileTransferByTag() the file transfer with targetTag "
                                        + targetTag + " found");
                        return sentFileTransfer;
                    }
                }
                Log.w(TAG, "findFileTransferByTag() not found targetTag "
                        + targetTag);
                return null;
            } else {
                Log.e(TAG, "findFileTransferByTag() whereToFind is "
                        + whereToFind + " targetTag is " + targetTag);
                return null;
            }
        }
    }

    /**
     * This class describe one single out-going file transfer, and control the
     * status itself
     */
    public static class SentFileTransfer {
    
        private static final String TAG = "SentFileTransfer";
        public static final int ONE2ONE = 0;
        public static final int ONE2MULTI = 1;
        public static final int GROUP = 2;
        
        protected int mDuration = 0;
        protected String mMimeType = null;
        boolean isSendToMulti = false;

        protected String mFileTransferTag = null;
        protected FileStruct mFileStruct = null;

        protected FileTransfer mFileTransferObject = null;
        protected FileTransferListener mFileTransferListener = null;
        protected String mChatSessionId;
        protected boolean mBurnType = false;

        protected int mChatType = ONE2ONE;
        protected String mContact = null;
        protected Set<String> mContacts = null;
        protected String mChatId;

        protected FileTransferChatWindow FTChatWindow = null;
        protected long mSmsId;
        
        //it is for one2one
        public SentFileTransfer(String contact, String filePath, boolean needSaveDb, boolean isBurn, String fid) {
            Log.d(TAG, "SentFileTransfer(), for one2one FT");
            
            if (fid == null) {
                int dummyId = generateFileTransferTag();
                long dummIpMsgId = Long.valueOf(dummyId);
                mFileTransferTag = (Long.valueOf(dummIpMsgId)).toString();
            } else {
                mFileTransferTag = fid;
            }
            
            mChatType = ONE2ONE;
            mBurnType = isBurn;
            mDuration = getDuration(filePath); //get duration form filepath
            mContact = contact;  //one2one chat, we will save sms db through contact
        
            mFileStruct = new FileStruct(filePath,
                                extractFileNameFromPath(filePath),
                                getFileSize(filePath), 
                                mFileTransferTag, 
                                new Date(),
                                contact, 
                                getThumnailFile(filePath),
                                mBurnType, 
                                mDuration);

            mMimeType = MediaFile.getMimeTypeForFile(mFileStruct.mName);

            FTChatWindow = new FileTransferChatWindow(mFileStruct,mChatType,mService);

            if (needSaveDb) {
                FTChatWindow.saveSendFileTransferToSmsDB();
            }  else {
                FTChatWindow.updateFTStatus(IFileTransfer.Status.TRANSFERING);
            } 
        }

        //it is for group
        public SentFileTransfer(Set<String> contacts, String filePath, String chatId, String chatSessionId, boolean needSaveDb, long dummyId) {
            Log.d(TAG, "SentFileTransfer(), for group FT");

            mFileTransferTag = (Long.valueOf(-dummyId)).toString();

            Log.d(TAG, "SentFileTransfer(), mFileTransferTag = " + mFileTransferTag);
            mChatType = GROUP;
            mBurnType = false;          
            mDuration = getDuration(filePath);
            mContacts = contacts;
            mChatId = chatId;
            mChatSessionId = chatSessionId;
            
            mFileStruct = new FileStruct(filePath, 
                extractFileNameFromPath(filePath), 
                getFileSize(filePath), 
                mFileTransferTag, 
                new Date(),
                contacts, 
                getThumnailFile(filePath),
                mDuration);

            mMimeType = MediaFile.getMimeTypeForFile(mFileStruct.mName);

            FTChatWindow = new FileTransferChatWindow(mFileStruct,mChatType,chatId,mService);
         }

        //it is for one2multi
        public SentFileTransfer(Set<String> contacts, String filePath, boolean needSaveDb, String fid) {
            Log.d(TAG, "SentFileTransfer(), for one2multi FT");
            Log.d(TAG, "SentFileTransfer(), contacts = " + contacts + 
                                                "filePath = " + filePath + 
                                                "needSaveDb = " + needSaveDb +
                                                "fid = " + fid);
            
            if (fid == null) {
                int dummyId = generateFileTransferTag();
                long dummIpMsgId = Long.valueOf(dummyId);
                mFileTransferTag = (Long.valueOf(dummIpMsgId)).toString();
            } else {
                mFileTransferTag = fid;
            }
            mChatType = ONE2MULTI;
            mBurnType = false;          
            mDuration = getDuration(filePath);
            mContacts = contacts;
            
            mFileStruct = new FileStruct(filePath, 
                extractFileNameFromPath(filePath), 
                getFileSize(filePath), 
                mFileTransferTag, 
                new Date(),
                contacts, 
                getThumnailFile(filePath),
                mDuration);

            mMimeType = MediaFile.getMimeTypeForFile(mFileStruct.mName);

            FTChatWindow = new FileTransferChatWindow(mFileStruct,mChatType,mService);
            if (needSaveDb) {
                FTChatWindow.saveSendFileTransferToSmsDB();
            } else {
                FTChatWindow.updateFTStatus(IFileTransfer.Status.TRANSFERING);
            }
        }
       
        protected void resend() {    
            Log.v(TAG, "resend test:  resend() entry! ");
            
            GsmaManager instance = GsmaManager.getInstance();
            FileTransferService fileTransferService = null;
            if (mFileTransferListener == null) {
                mFileTransferListener = new FileTransferSenderListener();
            }
            if (instance != null) {
                try {
                    fileTransferService = instance.getFileTransferApi();
                     mFileTransferObject =
                         fileTransferService.resumeFileTransfer(mFileTransferTag,mFileTransferListener);
                } catch (JoynServiceException e) {
                    Log.e(TAG, "Can't get fileTransferService! ");
                }
            }
            Log.e(TAG, "resend test --- mFileTransferObject = " + mFileTransferObject);
            
            Log.e(TAG, "resend test --- old fid = " + mFileTransferTag);
            try {
                if (null != mFileTransferObject) {
                    mFileStruct.mSize = mFileTransferObject.getFileSize();
                    final String fileTransferId = mFileTransferObject.getTransferId();
                    Log.e(TAG, "resend test --- new fid = " + fileTransferId);

                    FTChatWindow.updateInfo(fileTransferId);
                    FTChatWindow.updateFTStatus(Status.TRANSFERING);

                    mFileTransferTag = fileTransferId;
                    mFileStruct.mFileTransferTag = mFileTransferTag;
                   
                } else {
                    //move sentFileTransfer into resendable list
                    onFileTransferFinished(IOnSendFinishListener.Result.RESENDABLE);
                }
            } catch (JoynServiceException e) {
                Log.e(TAG, "reAcceptFileTransferInvitation() update fail !");
            }

        }

        protected void send() {
                    Log.w(TAG, "checkNext() send new file");
                    
                    GsmaManager instance = GsmaManager.getInstance();
                    FileTransferService fileTransferService = null;
                    if (instance != null) {
                        try {
                            fileTransferService = instance.getFileTransferApi();
                        } catch (JoynServiceException e) {
                            Log.e(TAG, "Can't get fileTransferService! ");
                        }
        
                        if (fileTransferService != null) {
                            try {
                                mFileTransferListener = new FileTransferSenderListener();
                                if (mChatType == ONE2ONE) {
                                    // send to one2one
                                    if (mBurnType == true) {
                                        Log.d(TAG, "Send a burn ft ");
                                        mFileTransferObject = fileTransferService
                                                .transferBurnFile(
                                                        mContact,
                                                        mFileStruct.mFilePath,
                                                        getThumnailFile(mFileStruct.mFilePath),
                                                        mFileTransferListener);
                                    } else if (mMimeType != null && (mMimeType.contains("audio")
                                            || mMimeType.contains("video"))) {
                                        Log.d(TAG, "Send a audio or video with dur ");
                                        mFileTransferObject = fileTransferService
                                                .transferMedia(
                                                        mContact,
                                                        mFileStruct.mFilePath,
                                                        getThumnailFile(mFileStruct.mFilePath),
                                                        mDuration,
                                                        mFileTransferListener);
                                    }else if (mFileStruct.mName.toLowerCase().endsWith(".xml")) {
                                        //geolocation
                                         Log.d(TAG, "Send a one2one geolocation");
                                         mFileTransferObject = fileTransferService
                                                .transferGeoLocFile(
                                                        mContact,
                                                        mFileStruct.mFilePath,
                                                        getThumnailFile(mFileStruct.mFilePath),
                                                        mFileTransferListener);
                                    } else {
                                        Log.d(TAG, "Send a one2one ft ");
                                        mFileTransferObject = fileTransferService
                                                .transferFile(
                                                        mContact,
                                                        mFileStruct.mFilePath,
                                                        getThumnailFile(mFileStruct.mFilePath),
                                                        mFileTransferListener);
                                    }
                                } else if (mChatType == GROUP) {
                                    // TODO: send to group
                                  
                                      Log.w(TAG,"checkNext() send new group file thumbnail not supported"); 
                                      mFileTransferObject =
                                          fileTransferService.transferFileToGroup
                                          (mChatSessionId,mContacts,mFileStruct.mFilePath,
                                            mDuration,
                                            mFileTransferListener);
                                      
                                } else if (mChatType == ONE2MULTI) {
                                    // send to multi
                                    Log.w(TAG,"send a one2multi filetransfer"); 
                                    mFileTransferObject =
                                        fileTransferService.transferFileToMultirecepient(mContacts,
                                        mFileStruct.mFilePath, true, mFileTransferListener, mDuration);
                                }
        
                                if (null != mFileTransferObject &&
                                    mFileTransferObject
                                            .getTransferId() != null) {
                                            
                                        //mFileStruct.mSize = mFileTransferObject
                                        //        .getFileSize();
                                        
                                        String fileTransferId = mFileTransferObject
                                                .getTransferId();

                                        String oldFileTransferTag = mFileTransferTag;
                                        mFileTransferTag = fileTransferId;
                                        mFileStruct.mFileTransferTag = fileTransferId;
                                        
                                        FTChatWindow.updateInfo(fileTransferId,oldFileTransferTag); //update sms ipmsgId
                                    
                                } else { 
                                    Log.e(TAG, "send() failed, mFileTransferObject is null, filePath is "
                                                    + mFileStruct.mFilePath);
                                    
                                    FTChatWindow.setSendFail(mFileTransferTag);
                                    onFileTransferFinished(IOnSendFinishListener.Result.REMOVABLE);
                                }
                            } catch (JoynServiceException e) {
                                e.printStackTrace();
                                /*
                                if (null != mFileTransferObject &&
                                    mFileTransferObject
                                            .getTransferId() != null) {
                                        mFileStruct.mSize = mFileTransferObject
                                                .getFileSize();
                                        String fileTransferId = mFileTransferObject
                                                .getTransferId();

                                        String oldFileTransferTag = mFileTransferTag;
                                        mFileTransferTag = fileTransferId;
                                        mFileStruct.mFileTransferTag = fileTransferId;
                                        
                                        FTChatWindow.updateInfo(fileTransferId,oldFileTransferTag);
                                } else {
                                    FTChatWindow.setSendFail();
                                    onFileTransferFinished(IOnSendFinishListener.Result.REMOVABLE);
                                }
                                */
                                
                            }
                        }
                    }
                }

/*
        private void onPrepareResend() {
            Log.d(TAG, "onPrepareResend() file " + mFileStruct.mFilePath
                    + " to " + mParticipant);
            if (null != mFileTransfer) {
                mFileTransfer.setStatus(Status.PENDING);
            }
        }
  */
    /*

        private void onPause() {
            // Log.d(TAG, "onPause() file " + mFileStruct.mFilePath
            // + " to " + mParticipant);
            Log.d(TAG, "onPause() file 123");
            if (null != mFileTransfer) {
                try {
                    Log.d(TAG, "onPause 1");
                    mFileTransferObject.pauseTransfer();
                } catch (JoynServiceException e) {
                    e.printStackTrace();
                }
            }
        }
  


        private void onResume() {
            // Log.d(TAG, "onResume() file " + mFileStruct.mFilePath
            // + " to " + mParticipant);
            Log.d(TAG, "onResume 123");
            if (null != mFileTransfer) {
                try {
                    Log.d(TAG, "onResume 1");
                    if (null != mFileTransferObject) {
                        // if(mFileTransferObject.isSessionPaused()){ ////TODo
                        // check this
                        Log.d(TAG, "onResume 2");
                        mFileTransferObject.resumeTransfer();
                        // }
                    }
                } catch (JoynServiceException e) {
                    e.printStackTrace();
                }
            }
        }
       

        private void onCancel() {
            Log.d(TAG, "onCancel() entry: mFileTransfer = " + mFileTransfer);
            if (null != mFileTransfer) {
                mFileTransfer.setStatus(Status.CANCEL);
            }
        }

        private void onFailed() {
            Log.d(TAG, "onFailed() entry: mFileTransfer = " + mFileTransfer);
            if (null != mFileTransfer) {
                mFileTransfer.setStatus(Status.FAILED);
            }
        }

        private void onNotAvailable(int reason) {
            Log.d(TAG, "onNotAvailable() reason is " + reason);
            if (FILETRANSFER_ENABLE_OK == reason) {
                return;
            } else {
                switch (reason) {
                case FILETRANSFER_DISABLE_REASON_REMOTE:
                    onFailed();
                    break;
                case FILETRANSFER_DISABLE_REASON_CAPABILITY_FAILED:
                case FILETRANSFER_DISABLE_REASON_NOT_REGISTER:
                    onCancel();
                    break;
                default:
                    Log.w(TAG, "onNotAvailable() unknown reason " + reason);
                    break;
                }
            }
        }

        private void onDestroy() {
            Log.d(TAG, "onDestroy() sent file transfer mFilePath "
                    + ((null == mFileStruct) ? null : mFileStruct.mFilePath)
                    + " mFileTransferObject = " + mFileTransferObject
                    + ", mFileTransferListener = " + mFileTransferListener);
            if (null != mFileTransferObject) {
                try {
                    if (null != mFileTransferListener) {
                        mFileTransferObject
                                .removeEventListener(mFileTransferListener);
                    }
                    cancelNotification();
                    mFileTransferObject.abortTransfer();
                } catch (JoynServiceException e) {
                    e.printStackTrace();
                }
            }
        }
        */

        protected void onFileTransferFinished(
                IOnSendFinishListener.Result result) {
            Log.d(TAG, "onFileTransferFinished() mFileStruct = " + mFileStruct
                    + ", file = "
                    + ((null == mFileStruct) ? null : mFileStruct.mFilePath)
                    + ", mOnSendFinishListener = " + mOnSendFinishListener
                    + ", mFileTransferListener = " + mFileTransferListener
                    + ", result = " + result);
            if (null != mOnSendFinishListener) {
                mOnSendFinishListener.onSendFinish(SentFileTransfer.this,
                        result);
                if (null != mFileTransferObject) {
                    try {
                        if (null != mFileTransferListener) {
                            mFileTransferObject
                                    .removeEventListener(mFileTransferListener);
                        }
                    } catch (JoynServiceException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    /**
     * File transfer session event listener
     */
    private class FileTransferSenderListener extends FileTransferListener {
            private static final String TAG = "M0CFF FileTransferSenderListener";

            public void onTransferPaused() {
                Log.d(TAG, "onTransferPaused() this file is "
                        + mFileStruct.mFilePath);
            }

            /**
             * Callback called when the file transfer is started
             */
            public void onTransferStarted() {
                Log.d(TAG, "onTransferStarted() this file is "
                        + mFileStruct.mFilePath);
            }

            /**
             * Callback called when the file transfer has been aborted
             */
            public void onTransferAborted() {
                Log.v(TAG,
                        "File transfer onTransferAborted(): mFileTransferTag = "
                                + mFileTransferTag);

                /*
                if (mParticipant == null) {
                    Log.d(TAG,
                            "FileTransferSenderListener onTransferAborted mParticipant is null");
                    return;
                }
                
                if (mFileTransfer != null) {
                    mFileTransfer.setStatus(Status.FAILED);
                    IChatBase chat = mModel.getChat(mChatTag); // TODo Check
                                                               // this
                    Log.v(TAG, "onTransferAborted(): chat = " + chat);
                    // if (chat instanceof ChatImpl) {
                    // ((ChatManagerImpl) chat).checkCapabilities(); // need
                    // shuo check
                    // }
                }
                try {
                    mFileTransferObject.removeEventListener(this);
                } catch (JoynServiceException e) {
                    e.printStackTrace();
                }
                */
                mFileTransferObject = null;
                RCSDataBaseUtils.updateFileTransferFail(mFileTransferTag);
                onFileTransferFinished(IOnSendFinishListener.Result.RESENDABLE);
            }

            /**
             * Callback called when the transfer has failed
             * 
             * @param error
             *            Error
             * @see FileTransfer.Error
             */
            public void onTransferError(int error) {
                Log.v(TAG, "onTransferError(),error = " + error
                        + ", mFileTransferTag =" + mFileTransferTag);

                Log.v(TAG, "resend test:  onTransferError!!!! ");
                
                switch (error) {
                case FileTransfer.Error.TRANSFER_FAILED:
                    Log.d(TAG,
                            "onTransferError(), the file transfer invitation is failed.");

                    FTChatWindow.updateFTStatus(Status.FAILED);
                    onFileTransferFinished(IOnSendFinishListener.Result.RESENDABLE);
                    break;

                case FileTransfer.Error.INVITATION_DECLINED:
                    Log.d(TAG,
                            "onTransferError(), your file transfer invitation has been rejected");
                   
                    FTChatWindow.updateFTStatus(Status.FAILED);
                    onFileTransferFinished(IOnSendFinishListener.Result.RESENDABLE);
                    break;
                case FileTransfer.Error.SAVING_FAILED:
                    Log.d(TAG, "onTransferError(), saving of file failed");
                    
                    FTChatWindow.updateFTStatus(Status.FAILED);
                    onFileTransferFinished(IOnSendFinishListener.Result.RESENDABLE);
                    break;
                default:
                    Log.e(TAG, "onTransferError() unknown error " + error);

                    FTChatWindow.updateFTStatus(Status.FAILED);
                    onFileTransferFinished(IOnSendFinishListener.Result.RESENDABLE);
                    break;
                }
            }

            public void onTransferProgress(long currentSize, long totalSize) {
                
                Log.d(TAG, "onTransferProgress() the file is transferring, currentSize is " 
                    + currentSize + " total size is " + totalSize);
                
            }

            /**
             * Callback called when the file has been transferred
             * 
             * @param filename
             *            Filename including the path of the transferred file
             */
            public void onFileTransferred(String filename) {
                Log.d(TAG, "onFileTransferred() entry, successfuly, fileName is " + filename);
                FTChatWindow.updateFTStatus(Status.FINISHED);         
                onFileTransferFinished(IOnSendFinishListener.Result.REMOVABLE);
            }

            public void onTransferResumed(String oldFTid, String newFTId) {
                // TODO:
                Log.d(TAG, "onTransferResumed() this file is "
                        + mFileStruct.mFilePath);
            }
        }


        /*
         * private static String buildPercentageLabel(Context context, long
         * totalBytes, long currentBytes) { if (totalBytes <= 0) { return null;
         * } else { final int percent = (int) (100 * currentBytes / totalBytes);
         * return context.getString(R.string.ft_percent, percent); } }
         */

        protected IOnSendFinishListener mOnSendFinishListener = null;

        protected interface IOnSendFinishListener {
            static enum Result {
                REMOVABLE, // This kind of result indicates that this File
                // transfer should be removed from the manager
                RESENDABLE
                // This kind of result indicates that this File transfer will
                // have a chance to be resent in the future
            };

            void onSendFinish(SentFileTransfer sentFileTransfer, Result result);
        }
    /*
        public void onNetworkStatusChanged(boolean isConnected) {
            // Now there is no NetworkChangedReceiver
            boolean hhtpFT = false;
            try {
                if (mFileTransferObject != null) {
                    hhtpFT = mFileTransferObject.isHttpFileTransfer();
                }
            } catch (JoynServiceException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(TAG, "onNetworkStatusChanged Model" + isConnected + "Http: "
                    + hhtpFT);

            if (!isConnected && !hhtpFT) {
                onFailed();
                onFileTransferFinished(IOnSendFinishListener.Result.RESENDABLE);
            }
        }
        */
    }


    /**
     * This class is used to manage the whole receive file transfers
     */
    protected class ReceiveFileTransferManager {
        private static final String TAG = "ReceiveFileTransferManager";

        private CopyOnWriteArrayList<ReceiveFileTransfer> mActiveList = new CopyOnWriteArrayList<ReceiveFileTransfer>();

        /**
         * Handle a new file transfer invitation (one2one OR group chat)
         */
        public synchronized void addReceiveFileTransfer(FileTransfer fileTransferObject, boolean isBurn, String chatId, boolean isGroup) {
            Log.d(TAG, "addReceiveFileTransfer() entry, fileTransferObject = "
                    + fileTransferObject);
            if (null != fileTransferObject) {
                ReceiveFileTransfer receiveFileTransfer =
                new ReceiveFileTransfer(fileTransferObject,isBurn,chatId,isGroup);
                mActiveList.add(receiveFileTransfer);
            }
        }

        public synchronized void addReceiveFileTransfer(String fid, boolean isBurn, String chatId, boolean isGroup, String contact, Set<String> contacts) {
            ReceiveFileTransfer receiveFileTransfer =
            new ReceiveFileTransfer(fid,isBurn,chatId,isGroup,contact,contacts);
            mActiveList.add(receiveFileTransfer);
        }

        public ReceiveFileTransfer getReceiveTransfer() {
            ReceiveFileTransfer resume_file = null;
            Log.d(TAG, "getReceiveTransfer 1");
            try {
                resume_file = mActiveList.get(0);
            } catch (Exception e) {
                Log.d(TAG, "getReceiveTransfer exception");
            }
            return resume_file;
        }

        /**
         * remove receive file transfer from mActiveList and add it to
         * mReDownLoadList
         */
        public synchronized void removeReceiveFileTransfer(
                ReceiveFileTransfer receiveFileTransfer) {
            Log.d(TAG,
                    "removeReceiveFileTransfer() entry, receiveFileTransfer = "
                            + receiveFileTransfer);
            if (null != receiveFileTransfer) {
                mActiveList.remove(receiveFileTransfer);
                Log.d(TAG,
                        "removeReceiveFileTransfer() the file transfer with receiveFileTransfer: "
                                + receiveFileTransfer);
            }
        }

        /**
         * Cancel all the receive file transfers
         */
        public synchronized void cancelReceiveFileTransfer() {
            Log.d(TAG, "cancelReceiveFileTransfer entry");
            ArrayList<ReceiveFileTransfer> tempList = new ArrayList<ReceiveFileTransfer>(
                    mActiveList);
            int size = tempList.size();
            for (int i = 0; i < size; i++) {
                ReceiveFileTransfer receiveFileTransfer = tempList.get(i);
                if (null != receiveFileTransfer) {
                    receiveFileTransfer.cancelFileTransfer();
                }
            }
        }

        /**
         * Search an existing file transfer in the receive list form ActiveList
         */
        public synchronized ReceiveFileTransfer findFileTransferByTagFromActiveList(
                Object targetTag) {
            if (null != mActiveList && null != targetTag) {
                for (ReceiveFileTransfer receiveFileTransfer : mActiveList) {
                    Object fileTransferTag = receiveFileTransfer.mFileTransferTag;
                    if (targetTag.equals(fileTransferTag)) {
                        Log.d(TAG,
                                "findFileTransferByTag() the file transfer with targetTag "
                                        + targetTag + " found");
                        return receiveFileTransfer;
                    }
                }
                Log.d(TAG, "findFileTransferByTag() not found targetTag "
                        + targetTag);
                return null;
            } else {
                Log.e(TAG, "findFileTransferByTag(), targetTag is " + targetTag);
                return null;
            }
        }
    }

    /**
     * This class describe one single in-coming file transfer, and control the
     * status itself
     */
    protected class ReceiveFileTransfer {
        private static final String TAG = "ReceiveFileTransfer";
        
        protected String mFileTransferTag = null;
        protected FileStruct mFileStruct = null;
        protected FileTransfer mFileTransferObject = null;
        protected FileTransferListener mFileTransferListener = null;

        protected static final int ONE2ONE = 0;
        protected static final int GROUP = 2;
        protected String mContact = null;
        protected Set<String> mContacts = null;
        protected long smsId = -1;
        protected int mChatType = ONE2ONE;

        protected long ipMsgId = -1;

        private FileTransferChatWindow FTChatWindow;
        boolean isAutoAccept = false;
        boolean mBurn = false;
        String mChatId;

        
        
        public ReceiveFileTransfer(FileTransfer fileTransferObject, boolean isBurn, String chatId, boolean isGroup) {
            if (null != fileTransferObject) {
                Log.d(TAG, "ReceiveFileTransfer() constructor FileTransfer is "
                        + fileTransferObject);
                
                handleReceiveFileTransferInvitation(fileTransferObject, chatId, isGroup,isBurn);
            }
        }

        public ReceiveFileTransfer(String fid,boolean isBurn,String chatId, boolean isGroup, String contact, Set<String> contacts) {
            mFileTransferTag = fid;
            mBurn = isBurn;
            mChatId = chatId;

            String filePath = null;
            String fileName = null;
            long fileSize = 0;
            int duration = 0;

            if (isGroup) {
                mChatType = GROUP;
                mContacts = contacts;
                mFileStruct = new FileStruct(filePath,
                    fileName,
                    fileSize,
                    fid,
                    new Date(),
                    mContacts,
                    getThumnailFile(filePath), 
                    duration);
                 
            } else {
                mChatType = ONE2ONE;        
                mContact = contact;
                mFileStruct = new FileStruct(filePath,
                    fileName,
                    fileSize,
                    fid,
                    new Date(),
                    mContact,
                    getThumnailFile(filePath), 
                    isBurn,
                    duration);
            }
         
            mFileTransferListener = new FileTransferReceiverListener();
            FTChatWindow = new FileTransferChatWindow(mFileStruct,mChatType,chatId,mService);
        }
        

        protected void handleReceiveFileTransferInvitation(FileTransfer fileTransferObject, String chatId, boolean isGroup, boolean isBurn) {
            Log.d(TAG, "handleFileTransferInvitation() entry ");
            if (fileTransferObject == null) {
                    Log.e(TAG, "handleFileTransferInvitation, fileTransferSession is null!");
                return;
            }

            mBurn = isBurn;
            mChatId = chatId; // we need the chatId for saving sms db
            
            if (isGroup) {
                mChatType = GROUP;          
            } else {
                mChatType = ONE2ONE;
                try{
                    String number = RCSUtils.extractNumberFromUri(fileTransferObject
                                    .getRemoteContact());
                    mContact = number; // we get this contact number for save sms db
                } catch(Exception e) {
                    e.printStackTrace();
                }  
            }

            try {
                mFileTransferObject = fileTransferObject;
                mFileTransferTag = mFileTransferObject.getTransferId();
                mFileTransferListener = new FileTransferReceiverListener();
                mFileTransferObject.addEventListener(mFileTransferListener);
                mFileStruct = FileStruct.from(mFileTransferObject,isBurn, null);
                FTChatWindow = new FileTransferChatWindow(mFileStruct,mChatType,chatId,mService);                
            } catch (JoynServiceException e) {
                e.printStackTrace();
                }
                catch (Exception e) {
                    e.printStackTrace();
            }

            isAutoAccept = isAutoDownLoad(mFileStruct.mName);
        
            if (mFileStruct != null && !isAutoAccept) {
                Log.d(TAG,"call saveReceiveFileTransferToSmsDB, isAUtoAccept = " + isAutoAccept);
                smsId = FTChatWindow.saveReceiveFileTransferToSmsDB();
            } else if (isAutoAccept) {
                Log.d(TAG,"call acceptFileTransferInvitation, isAUtoAccept = " + isAutoAccept);
                acceptFileTransferInvitation();
            }
               
        }

        private boolean isAutoDownLoad(String filename) {
            String mimeType = MediaFile.getMimeTypeForFile(filename);
            if (mimeType != null) {
            if (mimeType.contains(RCSUtils.FILE_TYPE_VIDEO)) {
                return false;
            } else if (mimeType.contains(RCSUtils.FILE_TYPE_IMAGE)) {
                return false;
            }
        }
            return true;
        }

        protected void acceptFileTransferInvitation() {
            if (mFileTransferObject != null) {
                try {
                    // Received file size in byte
                    long receivedFileSize = mFileTransferObject.getFileSize();
                    long currentStorageSize = RCSUtils.getFreeStorageSize();
                    Log.d(TAG, "receivedFileSize = " + receivedFileSize
                            + "/currentStorageSize = " + currentStorageSize);
                    if (currentStorageSize > 0) {
                        if (receivedFileSize <= currentStorageSize) {
                            mFileTransferObject.acceptInvitation();
                            if (!isAutoAccept) {
                                FTChatWindow.updateFTStatus(IFileTransfer.Status.TRANSFERING);
                            }
                        } else {
                            mFileTransferObject.rejectInvitation();
             
                            FTChatWindow.updateFTStatus(IFileTransfer.Status.REJECTED);
                            Log.d(TAG,"acceptFileTransferInvitation(),fail, because no enough storage to download it");
                        }
                    } else {
                        mFileTransferObject.rejectInvitation();
                        FTChatWindow.updateFTStatus(IFileTransfer.Status.REJECTED);
                        Log.d(TAG, "acceptFileTransferInvitation(),fail, because no enough storage to download it");
                    }
                } catch (JoynServiceException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {            
                FTChatWindow.updateFTStatus(IFileTransfer.Status.FAILED);
                Log.d(TAG, "acceptFileTransferInvitation(), mFileTransferObject is null");
            }
        }

        protected void reAcceptFileTransferInvitation() {
            Log.w(TAG, "reAcceptFileTransferInvitation() entry...");
            GsmaManager instance = GsmaManager.getInstance();
            FileTransferService fileTransferService = null;
            if (instance != null) {
                try {
                    fileTransferService = instance.getFileTransferApi();
                     mFileTransferObject =
                     fileTransferService.resumeFileTransfer(mFileTransferTag,mFileTransferListener);
                } catch (JoynServiceException e) {
                    Log.e(TAG, "Can't get fileTransferService! ");
                }
            }
            Log.e(TAG, "reaccept yangfeng test --- old fid = "+mFileTransferTag);
            try {
                if (null != mFileTransferObject) {
                    mFileStruct.mSize = mFileTransferObject.getFileSize();
                    final String fileTransferId = mFileTransferObject
                            .getTransferId();
                    Log.e(TAG, "reaccept yangfeng test --- new fid = " + fileTransferId);

                    //FTChatWindow.updateInfo(fileTransferId,mFileTransferTag,smsId); 
                    FTChatWindow.updateFTStatus(Status.TRANSFERING);
                    mFileTransferTag = fileTransferId;
                    mFileStruct.mFileTransferTag = mFileTransferTag;
                }
            } catch (JoynServiceException e) {
                Log.e(TAG, "reAcceptFileTransferInvitation() update fail !");
            }

        }

/*
        protected void rejectFileTransferInvitation() {
            try {
                if (mFileTransferObject != null) {
                    if (mFileTransferListener != null) {
                        mFileTransferObject
                                .removeEventListener(mFileTransferListener);
                        mFileTransferListener = null;
                    } else {
                        Log.w(TAG,
                                "rejectFileTransferInvitation(), mFileTransferReceiverListener is null!");
                    }
                    mFileTransferObject.rejectInvitation();
                    if (mFileTransfer != null) {
                        mFileTransfer.setStatus(IFileTransfer.Status.REJECTED);
                        fileTransferStatus = IFileTransfer.Status.REJECTED;
                    }
                } else {
                    mFileTransfer.setStatus(IFileTransfer.Status.FAILED);
                    fileTransferStatus = IFileTransfer.Status.FAILED;
                    Log.e(TAG,
                            "rejectFileTransferInvitation(), mFileTransferObject is null!");
                }
            } catch (JoynServiceException e) {
                e.printStackTrace();
            }
            mReceiveFileTransferManager.removeReceiveFileTransfer(this);
        }
        */

        protected void cancelFileTransfer() {
            if (null != mFileTransferObject) {
                try {
                    mFileTransferObject.abortTransfer();
                } catch (JoynServiceException e) {
                    e.printStackTrace();
                }
            }
            onFileTransferCancel();
        }

        protected void onPauseReceiveTransfer() {
            Log.v(TAG, "onPauseReceiveTransfer 1");
            if (null != mFileTransferObject) {
                try {
                    Log.v(TAG, "onPauseReceiveTransfer 1");
                    mFileTransferObject.pauseTransfer();
                } catch (JoynServiceException e) {
                    Log.v(TAG, "onPauseReceiveTransfer exception" + e);
                    e.printStackTrace();
                }
            }
            // onFileTransferCancel();
        }

        protected void onResumeReceiveTransfer() {
            Log.v(TAG, "onResumeReceiveTransfer");
            if (null != mFileTransferObject) {
                try {
                    Log.v(TAG, "onResumeReceiveTransfer 1");
                    // if(mFileTransferObject.isSessionPaused()){ //TODo check
                    // this
                    // Log.v(TAG,"onResumeReceiveTransfer 2");
                    mFileTransferObject.resumeTransfer();
                    // }
                } catch (JoynServiceException e) {
                    Log.v(TAG, "onResumeReceiveTransfer exception" + e);
                    e.printStackTrace();
                }
            }
            // onFileTransferCancel();
        }

        protected void onFileTransferFailed(boolean dataSentCompleted) {
            Log.v(TAG,
                    "onFileTransferFailed() entry : mFileTransferSession = "
                            + mFileTransferObject);
            // notify file transfer canceled
            /*
            if (FTChatWindow != null) {
                if (dataSentCompleted) {
                    FTChatWindow.setStatus(IFileTransfer.Status.FINISHED);
                } else {
                    FTChatWindow.setStatus(IFileTransfer.Status.CANCELED);
                }
            }
            */
            if (mFileTransferListener != null) {
                if (mFileTransferObject != null) {
                    try {
                        mFileTransferObject
                                .removeEventListener(mFileTransferListener);
                    } catch (JoynServiceException e) {
                        e.printStackTrace();
                    }
                }
            }

            // mReceiveFileTransferManager.removeReceiveFileTransfer(this);
        }

        /**
         * A file transfer in progress was canceled by remote.
         */
        protected void onFileTransferCanceled() {
        /*
            Log.v(TAG,
                    "canceledFileTransfer() entry : mFileTransferListener = "
                            + mFileTransferListener + "mFileTransfer = "
                            + mFileTransfer + ", mFileTransferObject = "
                            + mFileTransferObject); */
            // notify file transfer canceled
            FTChatWindow.updateFTStatus(IFileTransfer.Status.CANCELED);
            
            if (mFileTransferListener != null) {
                if (mFileTransferObject != null) {
                    try {
                        mFileTransferObject
                                .removeEventListener(mFileTransferListener);
                    } catch (JoynServiceException e) {
                        e.printStackTrace();
                    }
                }
            }
            // mReceiveFileTransferManager.removeReceiveFileTransfer(this);
        }

        protected void onFileTransferFailed() {
           
            // notify file transfer failed on timeout
            FTChatWindow.updateFTStatus(IFileTransfer.Status.FAILED);
            
            if (mFileTransferListener != null) {
                if (mFileTransferObject != null) {
                    try {
                        mFileTransferObject
                                .removeEventListener(mFileTransferListener);
                    } catch (JoynServiceException e) {
                        e.printStackTrace();
                    }
                }
            }
           // mReceiveFileTransferManager.removeReceiveFileTransfer(this);
        }

        /**
         * You cancel a file transfer.
         */
        protected void onFileTransferCancel() {
           
            // notify file transfer cancel
            FTChatWindow.updateFTStatus(IFileTransfer.Status.CANCEL);
            
            if (mFileTransferListener != null) {
                if (mFileTransferObject != null) {
                    try {
                        mFileTransferObject
                                .removeEventListener(mFileTransferListener);
                    } catch (JoynServiceException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        /**
         * File transfer session event listener
         */
        private class FileTransferReceiverListener extends FileTransferListener {
            private static final String TAG = "M0CFF FileTransferReceiverListener";
            private long mTotalSize = 0;
            private long mCurrentSize = 0;

            /**
             * Callback called when the file transfer is started
             */
            public void onTransferStarted() {
                Log.v(TAG, "onTransferStarted() entry");
            }

            /**
             * Callback called when the file transfer has been aborted
             */
            public void onTransferAborted() {
                Log.v(TAG, "onTransferAborted, File transfer onTransferAborted");
                onFileTransferFailed();
            }

            /**
             * Callback called when the transfer has failed
             * 
             * @param error
             *            Error
             * @see FileTransfer.Error
             */
            public void onTransferError(int error) {
                // TODO:
                switch (error) {
                case FileTransfer.Error.TRANSFER_FAILED:
                    Log.d(TAG,
                            "onTransferError(), the file transfer invitation is failed.");
                    onFileTransferFailed();
                    break;

                default:
                    Log.e(TAG, "onTransferError() unknown error " + error);
                    onFileTransferFailed();
                    break;
                }
            }

            /**
             * Callback called during the transfer progress
             * 
             * @param currentSize
             *            Current transferred size in bytes
             * @param totalSize
             *            Total size to transfer in bytes
             */
            public void onTransferProgress(long currentSize, long totalSize) {
                Log.d(TAG, "handleTransferProgress() entry: currentSize = "
                        + currentSize + ", totalSize = " + totalSize);
                mTotalSize = totalSize;
                mCurrentSize = currentSize;
            }

            /**
             * Callback called when the file has been transferred
             * 
             * @param filename
             *            Filename including the path of the transferred file
             */
            public void onFileTransferred(String filename) {
                Log.d(TAG, "onFileTransferred() entry: filename = " + filename);
                if (isAutoAccept) {
                    Log.d(TAG,"is auto download , so saveReceiveFileTransferToSmsDB");
                    FTChatWindow.saveReceiveFileTransferToSmsDB();
                } else {
                    Log.d(TAG,"setFilePath and updateFTStatus");
                    FTChatWindow.setFilePath(filename);
                    FTChatWindow.updateFTStatus(IFileTransfer.Status.FINISHED);
                } 
     
                ReceiveFileTransfer receiveFileTransfer = mReceiveFileTransferManager
                        .findFileTransferByTagFromActiveList(mFileTransferTag);
                if (null != receiveFileTransfer) {
                    mReceiveFileTransferManager
                            .removeReceiveFileTransfer(receiveFileTransfer);
                    return;
                }
            }

            public void onTransferResumed(String oldFTid, String newFTId) {
                // TODO:
            }

            public void onTransferPaused() {
                // TODO
            }

        }

    }

    /**************************** common function *******************************/

    //new: generate sentfiletransfer for one2one chat
    private SentFileTransfer generateSentFileTransfer(String contact, String filePath, boolean needSaveDb, boolean isBurn, String fid) {
        return new SentFileTransfer(contact, filePath, needSaveDb, isBurn, fid);
    }


    //new: generate sentfiletransfer for group chat
    private SentFileTransfer generateSentFileTransfer(Set<String> contacts, String filePath, String chatId, String chatSessionId, boolean needSaveDb, long dummyId) {
        return new SentFileTransfer(contacts, filePath, chatId, chatSessionId, needSaveDb, dummyId);
    }
    
    //new: generate sentfiletransfer for one2multi chat
    private SentFileTransfer generateSentFileTransfer(Set<String> contacts, String filePath, boolean needSaveDb, String fid) {
        return new SentFileTransfer(contacts, filePath, needSaveDb,fid);
    }
 
    //new: send filetransfer in one2one chat
    public void handleSendFileTransferInvitation(String contact, String filePath, boolean isBurn) {
        Log.d(TAG, "handleSendFileTransferInvitation entry, it is for one2one ft");
        boolean needSaveDb = true;
        String fid = null;
        mOutGoingFileTransferManager
            .onAddSentFileTransfer(generateSentFileTransfer(contact,filePath,needSaveDb,isBurn, fid));     
    }

    //new: send filetransfer in One2Multi chat
    public void handleSendFileTransferInvitation(List<String> contacts, String filePath) {
        Log.d(TAG, "handleSendFileTransferInvitation entry, it is for one2multi ft");
        Set<String> ctcts = new HashSet<String>();
        ctcts.addAll(contacts);
        boolean needSaveDb = true;
        String fid = null;
        mOutGoingFileTransferManager
            .onAddSentFileTransfer(generateSentFileTransfer(ctcts,filePath,needSaveDb,fid));     
    }

    //new: It is for send a group file transfer, called by shuo
    public void handleSendFileTransferInvitation
    (List<String> contacts, String filePath, String chatId, String chatSessionId, long dummyId) {
        Log.d(TAG, "handleSendFileTransferInvitation entry, it is for group chat ft");
        boolean needSaveDb = true;
        Set<String> ctcts = new HashSet<String>();
        ctcts.addAll(contacts);
        mOutGoingFileTransferManager
            .onAddSentFileTransfer(generateSentFileTransfer(ctcts,filePath,chatId,chatSessionId,needSaveDb,dummyId));     
    }
    
    //new: resend in one2one and one2multi
    public void handleResendFileTransfer(long ipMsgId) {
        FTInfo ftInfo = getIpFTInfo(ipMsgId);
        if (isDummyId(-ipMsgId)) {
            // it is a failed message not recorded is stack
            // so we should send it not resend
            //there is no sentFileTransfer object
            
            Log.d(TAG, "handleResendFileTransfer, isDummyId");
            boolean needSaveDb = false;

            SentFileTransfer sentFileTransfer = null;

            if (ftInfo.contacts == null && ftInfo.contact != null) {
                //it is for one2one
                sentFileTransfer = generateSentFileTransfer
                        (ftInfo.contact,ftInfo.filePath,needSaveDb,
                        ftInfo.isBurn,ftInfo.fid);
            } else if (ftInfo.contacts != null && ftInfo.contact == null) {
                //it is for one2multi
                sentFileTransfer = generateSentFileTransfer
                        (ftInfo.contacts,ftInfo.filePath,needSaveDb,ftInfo.fid);
            }
            if (sentFileTransfer != null) {
                mOutGoingFileTransferManager
                        .onAddSentFileTransfer(sentFileTransfer);  
            }                  
        } else {
             //String fid = RCSUtils.getFTids(ipMsgId);
             Log.d(TAG, "handleResendFileTransfer, is no dummy !");
             SentFileTransfer fileTransfer = 
                mOutGoingFileTransferManager.findResendableFileTransfer(ftInfo.fid);
                boolean needSaveDb = false;
                boolean haveInstance = true;
                
                if (null != fileTransfer) {
                    Log.d(TAG, "handleResendFileTransfer, null != fileTransfer");
                    mOutGoingFileTransferManager.resendFileTransfer(fileTransfer,haveInstance);
                } else {
                    //there is no sentFileTransfer object
                    Log.d(TAG, "handleResendFileTransfer, null == fileTransfer");

                    if (ftInfo.contacts == null && ftInfo.contact != null) {
                        //it is for one2one
                        fileTransfer = generateSentFileTransfer
                                (ftInfo.contact,ftInfo.filePath,needSaveDb,
                                ftInfo.isBurn,ftInfo.fid);
                    } else if (ftInfo.contacts != null && ftInfo.contact == null) {
                        //it is for one2multi
                        fileTransfer = generateSentFileTransfer
                                (ftInfo.contacts,ftInfo.filePath,needSaveDb,ftInfo.fid);
                    }
                    haveInstance = false;
                    mOutGoingFileTransferManager.resendFileTransfer(fileTransfer,haveInstance);
              }
         }             
    }

    //new: resend in group, it will called by shuo
    public void handleResendFileTransfer(long ipMsgId ,String chatId, String chatSessionId) {
        FTInfo ftInfo = getIpFTInfo(ipMsgId);

        if (isDummyId(-ipMsgId)) {
            Log.d(TAG, "handleResendFileTransfer in group chat, isDummyId");

            boolean needSaveDb = false;
            SentFileTransfer sentFileTransfer = null;
            
            sentFileTransfer = generateSentFileTransfer
                        (ftInfo.contacts,
                        ftInfo.filePath,
                        chatId,
                        chatSessionId,
                        needSaveDb,
                        ipMsgId);
            mOutGoingFileTransferManager
                        .onAddSentFileTransfer(sentFileTransfer);
        } else {
            SentFileTransfer fileTransfer = 
                mOutGoingFileTransferManager.findResendableFileTransfer(ftInfo.fid);
            boolean needSaveDb = false;
            boolean haveInstance = true;
            if (null != fileTransfer) {
                Log.d(TAG, "handleResendFileTransfer in group chat, null != fileTransfer");
                mOutGoingFileTransferManager.resendFileTransfer(fileTransfer,haveInstance);
            } else {
                Log.d(TAG, "handleResendFileTransfer in group chat, null == fileTransfer");
            }
        }
    }

    private class FTInfo {
        FTInfo(String fp, String ct, boolean ib, String ft) {
            filePath = fp;
            contact = ct;
            isBurn = ib;
            fid = ft;
        }

        FTInfo(String fp, Set<String> ct, boolean ib, String ft) {
            filePath = fp;
            contacts = ct;
            isBurn = ib;
            fid = ft;
        }
        
        String contact = null;
        Set<String> contacts = null;
        String filePath = null;
        boolean isBurn = false;
        String fid = null;
    }

    private FTInfo getIpFTInfo(long ipMsgId) {
        ContentResolver resolver = ContextCacher.getHostContext().getContentResolver();         
        Cursor cursor;
        String selection = Sms.IPMSG_ID + "=" + ipMsgId;
        cursor = resolver.query(RCSUtils.SMS_CONTENT_URI, RCSUtils.PROJECTION_FOR_DUMY_FILETRANSFER,
            selection, null, null);
        String body = null;
        String remote = null;
        int Type = Sms.MESSAGE_TYPE_OUTBOX;

         try {
            if (cursor != null && cursor.moveToFirst()) {
                body = cursor.getString(cursor.getColumnIndex(Sms.BODY));
                remote = cursor.getString(cursor.getColumnIndex(Sms.ADDRESS));
                Type = cursor.getInt(cursor.getColumnIndex(Sms.TYPE));
            }
         } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        String fid;
        String filePath = getFliePathfromBody(body);
        
        String contact = remote;
        boolean isBurn = false;
        if (isDummyId(-ipMsgId)) {
            fid = (Long.valueOf(-ipMsgId)).toString();
        } else {
            long id = -ipMsgId;
            //URI uri = RCSUtils.RCS_URI_FT;
            String where = RCSUtils.KEY_EVENT_ROW_ID + "='" + id + "'";
            cursor = resolver.query(RCSUtils.RCS_URI_FT, RCSUtils.PROJECTION_FILETRANSFER_ID, where, null, null);
            try {
                if (cursor.moveToFirst()) {
                    fid = cursor.getString(cursor.getColumnIndex(RCSUtils.KEY_EVENT_FT_ID));
                    Log.d(TAG, "getIpFTInfo() fid is " + fid);
                } else {
                    fid = null;
                    Log.d(TAG, "getIpFTInfo() fid error");               
                }
            } finally {
                if (null != cursor) {
                    cursor.close();
                }
            }
        }
        FTInfo info = null;
        
        if (contact.contains(SEPRATOR)) {
            // it is one2multi
            String[] contacts;
            contacts = contact.split(SEPRATOR);
            Set<String> remotes = new HashSet<String>();
            
            for (String recipient : contacts) {
                remotes.add(recipient);
            }
            info = new FTInfo(filePath, remotes, isBurn, fid);
        } else {
            // it is one2one
            info = new FTInfo(filePath, contact, isBurn, fid);
        }

        return info;      
    }

    private String getFliePathfromBody(String body) {
        String filePath;
        filePath = body.substring(6);
        return filePath;
    }
    
    //new called by shuo
    public void handleRecevieFileTransferInvitationInGroup(Intent invitation) {
        handleRecevieFileTransferInvitation(invitation);
    }

    //new: handle receive file transfer invite
    public void handleRecevieFileTransferInvitation(Intent invitation) {
        String fid = invitation
                .getStringExtra(FileTransferIntent.EXTRA_TRANSFER_ID);
        boolean isAutoAccept = invitation.getBooleanExtra(AUTO_ACCEPT, false);
        byte[] thumbNail = invitation.getByteArrayExtra("thumbnail");
        String chatSessionId = invitation.getStringExtra(CHAT_SESSION_ID);
        boolean isGroup = invitation.getBooleanExtra(ISGROUPTRANSFER, false);
        String chatId = invitation.getStringExtra(CHAT_ID);
        boolean isBurn = Boolean.valueOf(invitation.getStringExtra(FileTransferIntent.EXTRA_BURN));
        if (handleFileTransferInvitation(fid,isBurn,chatId,isGroup)) {
            Log.d(TAG, "Have handleFileTransferInvitation ");
        } else {
            Log.d(TAG, "handleFileTransferInvitation occur some error!");
        }
    }

    //new: handle invitaiton (one2one and group)
    public boolean handleFileTransferInvitation(String fid, boolean isBurn, String chatId, boolean isGroup) {
        Log.w(TAG, "handleFileTransferInvitation() entry, fid = " + fid + "isBurn = " + isBurn + "isGroup = " + isGroup);

        GsmaManager instance = GsmaManager.getInstance();
        FileTransferService fileTransferService = null;

        if (instance != null) {
            try {
                fileTransferService = instance.getFileTransferApi();
            } catch (JoynServiceException e) {
                Log.e(TAG, "get fileTransferService fail !");
            }

            if (fileTransferService != null) {
                try {
                    FileTransfer fileTransferObject = fileTransferService.getFileTransfer(fid);
                    if (fileTransferObject == null) {
                        Log.w(TAG,
                                "handleFileTransferInvitation-The getFileTransferSession is null");
                        return false;
                    }                                   
                    mReceiveFileTransferManager.addReceiveFileTransfer(fileTransferObject,isBurn,chatId,isGroup);
                    return true;
                } catch (JoynServiceException e) {
                    Log.e(TAG,
                            "M0CFF handleFileTransferInvitation-getChatSession fail");
                    e.printStackTrace();
                } catch (Exception e) {
                    Log.e(TAG,
                            "M0CFF handleFileTransferInvitation-getParticipants fail");
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    //new : download a filetransfer
    public void handleAcceptFileTransfer(String fileTransferTag) {
        Log.e(TAG,"handleAcceptFileTransfer() enter");
        ReceiveFileTransfer receiveFileTransfer = 
                mReceiveFileTransferManager.findFileTransferByTagFromActiveList(fileTransferTag);
        if (null != receiveFileTransfer) {
            Log.e(TAG,"receiveFileTransfer != null");
            receiveFileTransfer.acceptFileTransferInvitation();
        }
    }


    //new: call by shuo,download a filetransfer from group
    public void handleAcceptFileTransferInGroup(String fileTransferTag) {
        ReceiveFileTransfer receiveFileTransfer = 
                mReceiveFileTransferManager.findFileTransferByTagFromActiveList(fileTransferTag);
        if (null != receiveFileTransfer) {
            receiveFileTransfer.acceptFileTransferInvitation();
        }
    }

    //new: redownload in one2one
    public void handleReAcceptFileTransfer(String fileTransferTag, long ipMsgId) {
        Log.w(TAG, "handleReAcceptFileTransfer() entry...");
        ReceiveFileTransfer receiveFileTransfer = 
                mReceiveFileTransferManager.findFileTransferByTagFromActiveList(fileTransferTag);
        if (null != receiveFileTransfer) {
            Log.w(TAG, "null != receiveFileTransfer, call reAcceptFileTransferInvitation directly");
            receiveFileTransfer.reAcceptFileTransferInvitation();
        } else {
            ////if chat didn't exsit
            GsmaManager instance = GsmaManager.getInstance();
            FileTransferService fileTransferService = null;

            if (instance != null) {
                try {
                    fileTransferService = instance.getFileTransferApi();
                } catch (JoynServiceException e) {
                    Log.e(TAG, "get fileTransferService fail !");
                }

                if (fileTransferService != null) {
                    boolean isGroup = false;
                    String chatId = null; 
                    FTInfo ftInfo = getIpFTInfo(ipMsgId);
                    mReceiveFileTransferManager.addReceiveFileTransfer(fileTransferTag,
                        ftInfo.isBurn,chatId,isGroup,ftInfo.contact,ftInfo.contacts);
                }
            }
            
            receiveFileTransfer = 
                    mReceiveFileTransferManager.findFileTransferByTagFromActiveList(fileTransferTag);
            
            if (null != receiveFileTransfer) {
                Log.d(TAG, "handleAcceptFileTransfer() entry, now reAcceptFileTransferInvitation after reboot" );
                receiveFileTransfer.reAcceptFileTransferInvitation();
            }
        }
    }

    //new: redownload in group
    public void handleReAcceptFileTransferInGroup(String fileTransferTag, long ipMsgId) {
    /*
        ReceiveFileTransfer receiveFileTransfer = 
                mReceiveFileTransferManager.findFileTransferByTagFromActiveList(fid);
        if (null != receiveFileTransfer) {
            receiveFileTransfer.reAcceptFileTransferInvitation();
        } else {
            ////if chat didn't exsit
            GsmaManager instance = GsmaManager.getInstance();
            FileTransferService fileTransferService = null;

            if (instance != null) {
                try {
                    fileTransferService = instance.getFileTransferApi();
                } catch (JoynServiceException e) {
                    Log.e(TAG, "get fileTransferService fail !");
                }

                if (fileTransferService != null) {
                    try {     
                        mReceiveFileTransferManager.addReceiveFileTransfer(fid,isBurn,chatId,true);
                        return true;
                    } catch (JoynServiceException e) {
                        Log.e(TAG,
                                "M0CFF handleFileTransferInvitation-getChatSession fail");
                        e.printStackTrace();
                    } catch (Exception e) {
                        Log.e(TAG,
                                "M0CFF handleFileTransferInvitation-getParticipants fail");
                        e.printStackTrace();
                    }
                }
            }         
            receiveFileTransfer = 
                    mReceiveFileTransferManager.findFileTransferByTagFromActiveList(fileTransferTag);
            
            if (null != receiveFileTransfer) {
                Log.d(TAG, "handleAcceptFileTransfer() entry, now reAcceptFileTransferInvitation after reboot" );
                receiveFileTransfer.reAcceptFileTransferInvitation();
            }
        }
        */
    }

    


































/*

        public void handleCancelFileTransfer(String fileTransferTag) {
            Log.v(TAG, "handleCancelFileTransfer() , fileTransferTag = " + fileTransferTag);
            // if (chat == null) {
            // chat = getOne2oneChatByContact((String) tag);
            // }
           
             mOutGoingFileTransferManager.cancelFileTransfer(fileTransferTag);
             
        }


        public void handleResumeFileTransfer(Object chatWindowTag,
                String fileTransferTag, int option) {
            Log.v(TAG, "handleResumeFileTransfer() TAG: " + fileTransferTag
                    + "OPTION: " + option);
            if (option == 0)
                mOutGoingFileTransferManager.resumeFileTransfer(fileTransferTag);
            else {
                IChatBase chat = mModel.getChat(chatWindowTag);
                if (chat instanceof One2OneChat) {
                    One2OneChat oneOneChat = ((One2OneChat) chat);
                    // oneOneChat.handleResumeReceiveFileTransfer(fileTransferTag);
                }
             
            }
        }
        */

    public static String extractFileNameFromPath(String filePath) {
        if (null != filePath) {
            int lastDashIndex = filePath.lastIndexOf("/");
            if (-1 != lastDashIndex && lastDashIndex < filePath.length() - 1) {
                String fileName = filePath.substring(lastDashIndex + 1);
                return fileName;
            } else {
                Log.e(TAG, "extractFileNameFromPath() invalid file path:" + filePath);
                return null;
            }
        } else {
            Log.e(TAG, "extractFileNameFromPath() filePath is null");
            return null;
        }
    }

    public static long getFileSize(String fileName) {
        return RCSUtils.getFileSize(fileName);
    }

    public static String getThumnailFile(String fileName){
        // now stack can handle it
        return null;
    }

    public static int getDuration(String filePath) {
        return 0;
    }

/*
    private static Set<String> convertParticipantsToContactsSet(List<Participant> participants) {
        Set<String> pList = new HashSet<String>();
        int size = participants.size();
        for (int i = 0; i < size; i++) {
            pList.add(participants.get(i).getContact());
        }
        return pList;
    }
*/
    private static int generateFileTransferTag() {
        int messageTag = RANDOM.nextInt(1000) + 1;
        messageTag = Integer.MAX_VALUE - messageTag;
        Log.d(TAG, "generateMessageTag() messageTag: " + messageTag);
        return messageTag;
    }

    private boolean isDummyId(long ipmsgId) {
       if (ipmsgId > Integer.MAX_VALUE - 1001 && ipmsgId < Integer.MAX_VALUE ) {
           return true;
       } else {
           return false;
       }
   }


}

