package com.mediatek.rcs.message.plugin;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.DialogInterface.OnClickListener;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.media.RingtoneManager;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.NetworkInfo.State;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.StatFs;
import android.preference.Preference;
import android.provider.MediaStore;
import android.provider.Settings;
import android.provider.Telephony;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.MediaStore.Audio;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Video;
import android.provider.MediaStore.Video.Thumbnails;
import android.provider.Telephony.Mms;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Threads;
import android.telephony.SubscriptionManager;
import android.telephony.SubscriptionInfo;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.cmcc.ccs.blacklist.CCSblacklist;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.PhoneConstants;
import com.mediatek.mms.ipmessage.IpComposeActivity;
import com.mediatek.mms.ipmessage.IpComposeActivityCallback;
import com.mediatek.mms.ipmessage.IpContact;
import com.mediatek.mms.ipmessage.IpConversation;
import com.mediatek.mms.ipmessage.IpWorkingMessageCallback;
import com.mediatek.mms.ipmessage.IIpMessageItem;
import com.mediatek.mms.ipmessage.IpMessageItem;
import com.mediatek.mms.ipmessage.IpMessageListAdapter;
import com.mediatek.rcs.common.GroupManager;
import com.mediatek.rcs.common.IGroupActionListener;
import com.mediatek.rcs.common.IInitGroupListener;
import com.mediatek.rcs.common.INotificationsListener;
import com.mediatek.rcs.common.IpAttachMessage;
import com.mediatek.rcs.common.IpImageMessage;
import com.mediatek.rcs.common.IpMessage;
import com.mediatek.rcs.common.IpMessageConsts;
import com.mediatek.rcs.common.RCSGroup;
import com.mediatek.rcs.common.IpMessageConsts.IpMessageStatus;
import com.mediatek.rcs.common.IpMessageConsts.IpMessageType;
import com.mediatek.rcs.common.IpTextMessage;
import com.mediatek.rcs.common.IpVCardMessage;
import com.mediatek.rcs.common.IpVideoMessage;
import com.mediatek.rcs.common.IpVoiceMessage;
import com.mediatek.rcs.common.IpGeolocMessage;
import com.mediatek.rcs.common.MessageStatusUtils.IFileTransfer.Status;
import com.mediatek.rcs.common.RCSMessageContactManager;
import com.mediatek.rcs.common.RCSMessageManager;
import com.mediatek.rcs.common.RCSMessageNotificationsManager;
import com.mediatek.rcs.common.RCSMessageServiceManager;
import com.mediatek.rcs.message.data.RcsProfile;
import com.mediatek.rcs.message.group.PortraitManager;
import com.mediatek.rcs.message.group.PortraitManager.MemberInfo;
import com.mediatek.rcs.message.plugin.RCSEmoji;
import com.mediatek.rcs.message.ui.CreateGroupActivity;
import com.mediatek.rcs.message.ui.RcsSettingsActivity;
import com.mediatek.rcs.message.utils.RcsMessageConfig;
import com.mediatek.rcs.message.utils.RcsMessageUtils;
import com.mediatek.rcs.message.utils.ThreadNumberCache;
import com.mediatek.rcs.message.R;
import com.mediatek.services.rcs.phone.ICallStatusService;
import com.mediatek.services.rcs.phone.IServiceMessageCallback;
import com.mediatek.storage.StorageManagerEx;

import com.mediatek.rcs.message.plugin.RCSMessagePluginExt;

import com.mediatek.rcs.message.provider.FavoriteMsgProvider;

import com.mediatek.telecom.TelecomManagerEx;

import com.google.android.mms.ContentType;
import com.google.android.mms.pdu.PduComposer;
import com.google.android.mms.pdu.PduPersister;
import com.google.android.mms.pdu.RetrieveConf;
import com.google.android.mms.pdu.SendReq;
import static com.google.android.mms.pdu.PduHeaders.MESSAGE_TYPE_NOTIFICATION_IND;
import static com.google.android.mms.pdu.PduHeaders.MESSAGE_TYPE_RETRIEVE_CONF;

//import com.mediatek.rcs.message.data.RCSVCardAttachment;

//import org.gsma.joyn.ft.IFileSpamReportListener;
//import org.gsma.joyn.chat.ISpamReportListener;
import com.mediatek.rcs.common.binder.RCSServiceManager;
import com.mediatek.rcs.common.provider.ThreadMapCache;
import com.mediatek.rcs.common.provider.ThreadMapCache.MapInfo;
import com.mediatek.rcs.common.service.Participant;
import com.mediatek.rcs.common.utils.Logger;

import org.gsma.joyn.ft.FileTransferService;
import org.gsma.joyn.chat.ChatService;
import org.gsma.joyn.JoynServiceException;
import org.gsma.joyn.chat.SpamReportListener;
import org.gsma.joyn.ft.FileSpamReportListener;

import com.mediatek.rcs.common.IpMessageConsts.IpMessageStatus;

import com.mediatek.rcs.message.plugin.RCSVCardAttachment;
import com.mediatek.rcs.message.plugin.RcsMessageListAdapter.OnMessageListChangedListener;
import com.mediatek.rcs.common.utils.RCSUtils;
import com.mediatek.rcs.common.IBurnMessageCapabilityListener;
import android.provider.Telephony.Sms;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.location.Location;

import com.mediatek.rcs.message.location.GeoLocService;
import com.mediatek.rcs.message.location.GeoLocUtils;
import com.mediatek.rcs.message.location.GeoLocXmlParser;
import com.mediatek.rcs.message.R;
import com.mediatek.rcs.common.RCSCacheManager;

public class RcsComposeActivity extends IpComposeActivity implements INotificationsListener, IBurnMessageCapabilityListener{

    private static String TAG = "RcseComposeActivity";



    // / M: host's option menu and context menu
    private static final int MENU_ADD_SUBJECT           = 0;
    private static final int MENU_CALL_RECIPIENT        = 5;
//    private static final int MENU_ADD_QUICK_TEXT        = 8;
//    private static final int MENU_ADD_TEXT_VCARD        = 9;
    private static final int MENU_GROUP_PARTICIPANTS    = 32;
    private static final int MENU_SELECT_MESSAGE        = 101;
    private static final int MENU_SHOW_CONTACT          = 121;
    private static final int MENU_CREATE_CONTACT        = 122;
    private static final int MENU_CHAT_SETTING          = 137;
    //rcs message new option menu item from 5000;
    private static final int MENU_GROUP_CHAT_INFO = 5000;
    private static final int MENU_EDIT_BURNED_MSG = 5001;
    
 // / M: add for ip message, context menu
    private static final int MENU_DELIVERY_REPORT       = 20;
    private static final int MENU_FORWARD_MESSAGE       = 21;
    private static final int MENU_SAVE_MESSAGE_TO_SUB     = 32;
    private static final int MENU_INVITE_FRIENDS_TO_CHAT = 100;
    
//    private static final int MENU_MARK_AS_SPAM = 102;
//    private static final int MENU_REMOVE_SPAM = 103;
//    private static final int MENU_VIEW_ALL_MEDIA = 105;
//    private static final int MENU_INVITE_FRIENDS_TO_IPMSG = 107;
//    private static final int MENU_SEND_BY_SMS_IN_JOYN_MODE = 108;
//    private static final int MENU_SEND_BY_JOYN_IN_SMS_MODE = 109;
    private static final int MENU_RETRY = 200;
//    private static final int MENU_DELETE_IP_MESSAGE = 201;
    private static final int MENU_SHARE = 202;
//    private static final int MENU_SAVE_ATTACHMENT = 205;
    private static final int MENU_VIEW_IP_MESSAGE = 206;
    private static final int MENU_COPY = 207;
    private static final int MENU_SEND_VIA_TEXT_MSG = 208;
    private static final int MENU_EXPORT_SD_CARD = 209;
    private static final int MENU_FORWARD_IPMESSAGE = 210;
    private static final int MENU_FAVORITE   = 211;
//    private static final int MENU_RESEND   = 212;
 //   private static final int MENU_SEND_REPORT = 213;
    private static final int MENU_BLACK_LIST = 214;
    private static final int MENU_REPORT = 1000;

    private static final int MENU_ADD_TO_BOOKMARK = 35;
    private static final int MENU_ADD_ADDRESS_TO_CONTACTS = 27;


    // / M: Ip message add jump to another chat menu. @{
//    private static final int MENU_JUMP_TO_JOYN = 221;
//    private static final int MENU_JUMP_TO_XMS = 222;
    // / @}

    // / M: Ip message add export chat menu. @{
//    private static final int MENU_EXPORT_CHAT = 223;
    // / @}

//    public static final int MSG_LIST_RESEND_IPMSG = 20;
    public static final int OK = 0;
    public static final int UPDATE_SENDBUTTON = 2;

    
    
    private static final String PREFERENCE_SEND_WAY_CHANGED_NAME = "sendway";
    private static final String PREFERENCE_KEY_SEND_WAY = "sendway";
    private static final int PREFERENCE_VALUE_SEND_WAY_UNKNOWN = 0;
    private static final int PREFERENCE_VALUE_SEND_WAY_IM = 1;
    private static final int PREFERENCE_VALUE_SEND_WAY_SMS = 2;
    private static final String PREFERENCE_KEY_SEND_WAY_IM = "sendway_im";
    private static final String PREFERENCE_KEY_SEND_WAY_SMS = "sendway_sms";

    private boolean mIsIpServiceEnabled = false;

    private ImageButton mSendButtonIpMessage; // press to send ipmessage
    private TextView mTypingStatus;
//    private TextView mRemoteStrangerText;
    private TextView mShowCallTimeText;
    private boolean mIsDestroyTypingThread = false;
    private long mLastSendTypingStatusTimestamp = 0L;

    // /M: for indentify that just send common message.
    private boolean mJustSendMsgViaCommonMsgThisTime = false;
    // /M: whether or not show invite friends to use ipmessage interface.
    private boolean mShowInviteMsg = false;

    // /M: working IP message
    private int mIpMessageDraftId = 0;
    private IpMessage mIpMessageDraft;
    private IpMessage mIpMessageForSend;
    private boolean mIsClearTextDraft = false;
    private AlertDialog mReplaceDialog = null;
    private int mCurrentChatMode = IpMessageConsts.ChatMode.JOYN;

    // chat mode number
    private String mChatModeNumber = "";
    private boolean mIsSmsEnabled;

    // ipmessage status
    private boolean mIsIpMessageRecipients = false;

    private String mChatSenderName = "";

    private static final int REQUEST_CODE_IPMSG_TAKE_PHOTO = 200;
    private static final int REQUEST_CODE_IPMSG_RECORD_VIDEO = 201;
    private static final int REQUEST_CODE_IPMSG_SHARE_CONTACT = 203;
    private static final int REQUEST_CODE_IPMSG_CHOOSE_PHOTO = 204;
    private static final int REQUEST_CODE_IPMSG_CHOOSE_VIDEO = 205;
    private static final int REQUEST_CODE_IPMSG_RECORD_AUDIO = 206;
    private static final int REQUEST_CODE_IPMSG_CHOOSE_AUDIO = 208;
    private static final int REQUEST_CODE_IPMSG_SHARE_VCALENDAR = 209;

    // / M: IP message
    public static final int IPMSG_TAKE_PHOTO = 100;
    public static final int IPMSG_RECORD_VIDEO = 101;
    public static final int IPMSG_RECORD_AUDIO = 102;
    public static final int IPMSG_CHOOSE_PHOTO = 104;
    public static final int IPMSG_CHOOSE_VIDEO = 105;
    public static final int IPMSG_CHOOSE_AUDIO = 106;
    public static final int IPMSG_SHARE_CONTACT = 108;
    public static final int IPMSG_SHARE_CALENDAR = 109;
    public static final int IPMSG_SHARE_POSITION    = 110;

    public static final int REQUEST_CODE_IPMSG_PICK_CONTACT = 210;
    public static final int REQUEST_CODE_INVITE_FRIENDS_TO_CHAT = 211;

    public static final int REQUEST_CODE_IPMSG_SHARE_FILE = 214;
    public static final int REQUEST_CODE_IP_MSG_PICK_CONTACTS = 215;
    
    public static final int REQUEST_CODE_DELETE_PARTICIPENT_FROM_CHAT = 500;
    public static final int REQUEST_CODE_IP_MSG_BURNED_MSG_AUDIO = 220;
    private static final int SMS_CONVERT = 0;
    private static final int MMS_CONVERT = 1;
    private static final int SERVICE_IS_NOT_ENABLED = 0;
    private static final int RECIPIENTS_ARE_NOT_IP_MESSAGE_USER = 1;
    // M: add logic for the current ipmessage user can not send ip message
    private static final int RECIPIENTS_IP_MESSAGE_NOT_SENDABLE = 2;

    // /M: for forward ipMsg
    public static final String FORWARD_IPMESSAGE = "forwarded_ip_message";
    public static final String IP_MESSAGE_ID = "ip_msg_id";
    public static final String CHOICE_FILEMANAGER_ACTION = "com.mediatek.filemanager.ADD_FILE";
    public static final String FILE_SCHEMA = "file://";
    public static final String SMS_BODY = "sms_body";

    private static final int RECIPIENTS_LIMIT_FOR_SMS = 50;

    // /M: for check whether or not can convert IpMessage to Common message.
    // /M: can not support translating media through common message , -1;
    private static final int MESSAGETYPE_UNSUPPORT = -1;
    private static final int MESSAGETYPE_COMMON = 0;
    private static final int MESSAGETYPE_TEXT = 1;
    private static final int MESSAGETYPE_MMS = 2;
    private static final int MENU_SELECT_TEXT = 36;

    public static final int ACTION_SHARE = 0;
    public static final String SHARE_ACTION = "shareAction";
    public static final String FORWARD_MESSAGE = "forwarded_message";
    public static final int RESULT_OK = -1;
    public static final int RESULT_CANCELLED = 0;
    private String mIpMessageVcardName = "";
    private ImageView mFullIntegratedView;
    private String mPhotoFilePath = "";

    private String mVideoFilePath = "";
    private String mAudioPath = "";
    
    private String mDstPath = "";
    private int mDuration = 0;
    private String mCalendarSummary = "";
    private EditText mTextEditor;
    private TextWatcher mEditorWatcher;
    private TextView mTextCounter;
    private Handler mUiHandler;

    private Activity mContext;
    private Context mPluginContext;
    private IpComposeActivityCallback mCallback;
    private IpWorkingMessageCallback mWorkingMessage;
    private boolean mDisplayBurned = false;
    private boolean mBurnedCapbility = false;
    private long mThreadId = 0;
    private String mOldcontact= null;
    private boolean mIsGroupChat;
    private String mGroupChatId;
    private boolean mIsChatActive = true;
    private String[] mNumbers;
    private View mBottomPanel;
    private View mEditZone;
    private RCSEmoji mEmoji;
    private ListView mListView;
    private RcsMessageListAdapter mRcsMessageListAdapter;
    private final static int DIALOG_ID_GETLOC_PROGRESS = 101;
    private final static int DIALOG_ID_GEOLOC_SEND_CONFRIM = 102;

    private static Location mLocation = null;
    private GeoLocService mGeoLocSrv = null;
    private RCSGroup mGroup;
    private int mGroupStatus;
    private Dialog mInvitationDialog;
    private Dialog mInvitationExpiredDialog;
    private ProgressDialog mProgressDialog;
    private boolean mIsNeedShowCallTime;
    private ICallStatusService mCallStatusService;
    private static String RCS_MODE_FLAG = "rcsmode";
    private static int RCS_MODE_VALUE = 1;
    private ServiceConnection mCallConnection = null;

     final private int[] unusedOptionMenuForGroup = {MENU_ADD_SUBJECT,
                                                     MENU_CHAT_SETTING,
                                                     MENU_GROUP_PARTICIPANTS,
                                                     MENU_SHOW_CONTACT,
                                                     MENU_CREATE_CONTACT};
     
     private static RcsComposeActivity sComposer;
//     private boolean mIsRcsMode = true;
     
    public RcsComposeActivity(Context context) {
        mPluginContext = context;
    }
    

    @Override
    public void notificationsReceived(Intent intent) {

    }
    
    @Override
    public void onRequestBurnMessageCapabilityResult(String contact, boolean result){

    	String number = getNumber();
    	Log.d(TAG, "onRequestBurnMessageCapabilityResult contact = " + contact+ " result = " + result
    			+ " number = " + number);
    	if ( number == contact) {
    		mBurnedCapbility = result;    		
    	}
    	Log.d(TAG, "onRequestBurnMessageCapabilityResult mBurnedCapbility = " + mBurnedCapbility);
    	return ;
    }
    @Override
    public boolean onIpComposeActivityCreate(Activity context, IpComposeActivityCallback callback, Handler handler, Handler uiHandler, 
            ImageButton sendButton, TextView typingTextView, TextView strangerTextView, View bottomPanel) {
        Log.d(TAG, "onIpComposeActivityCreate enter. sendButton = " + sendButton);
        mContext = context;
        mCallback = callback;
        mUiHandler = handler;
        mSendButtonIpMessage = sendButton;
        mTypingStatus = typingTextView;
        mShowCallTimeText = strangerTextView;
        mBottomPanel = bottomPanel;
        mEditZone = (View) mBottomPanel.getParent();

        // / M: add for ip emoji
        mEmoji = new RCSEmoji(context, (ViewParent)mBottomPanel, mCallback);

//        mIsIpServiceEnabled = RcsMessageConfig.isServiceEnabled(mContext);
        Log.d(TAG, "mIsIpServiceEnabled =" + mIsIpServiceEnabled);
        mContext.registerReceiver(mChangeSubReceiver, new IntentFilter(TelephonyIntents.ACTION_DEFAULT_SMS_SUBSCRIPTION_CHANGED));
        sComposer = this;
        RCSCacheManager.clearCache();
        return false;
    }

    @Override
    public boolean onIpComposeActivityResume(boolean isSmsEnabled,
            EditText textEditor, TextWatcher watcher, TextView textCounter) {
        String[] numbers = mCallback.getConversationInfo();
        mIsSmsEnabled = isSmsEnabled;
        mTextEditor = textEditor;
        mEditorWatcher = watcher;
        mTextCounter = textCounter;
        if (mIpMessageDraft != null) {
            Log.d(TAG, "show IpMsg saveIpMessageForAWhile");
//            saveIpMessageForAWhile(mIpMessageDraft);
//            mCallback.callbackUpdateButtonState(true);
        }
        // / M: add for ip message, notification listener
        RCSMessageNotificationsManager.getInstance(mContext)
        .registerNotificationsListener(this);
        
        RCSMessageNotificationsManager.getInstance(mContext)
        .registerSpamNotificationsListener();
        //getContactCapbility();
//        if(1 == getRecipientSize()) {
//            RCSServiceManager.getInstance().registBurnMsgCapListener(this);
//            Log.d(TAG, "onRequestBurnMessageCapabilityResult getNumber() = " + getNumber());
//            RCSServiceManager.getInstance().getBurnMsgCap(getNumber());
//        }
        //addSpamReportListener(mPluginContext);
        //addFileSpamReportListener(mPluginContext);
        
        if (numbers != null && numbers.length > 0 && numbers[0].length() > 0 || mIsGroupChat) {
            mCallback.enableShareButton(true);
        } else {
            mCallback.enableShareButton(false);
        }
        if (mEmoji != null) {
            mEmoji.setEmojiEditor(textEditor);
        }
        if (mThreadId > 0) {
//            ChatWindowManager.getInstance().setCurrentChat(Long.valueOf(mThreadId));
        }
        
        if (mIsGroupChat) {
            mCallback.hideIpRecipientEditor();
            MapInfo info = ThreadMapCache.getInstance().getInfoByChatId(mGroupChatId);
            if (info != null && info.getStatus() == IpMessageConsts.GroupActionList.GROUP_STATUS_INVALID) {
                setChatActive(false);
            }
        } else if (mIsNeedShowCallTime) {
            mCallback.hideIpRecipientEditor();
            mCallback.updateIpTitle();
        }

        initDualSimState();
        return true;
    }

    @Override
    public boolean onIpComposeActivityPause() {
        Log.d(TAG, "onIpComposeActivityPause enter. mIsIpServiceEnabled = "
                + mIsIpServiceEnabled);
        // / M: add for ip message, notification listener
        RCSMessageNotificationsManager.getInstance(mContext)
                        .unregisterNotificationsListener(this);
        
        RCSMessageNotificationsManager.getInstance(mContext)
        .unregisterSpamNotificationsListener();
        
        RCSServiceManager.getInstance().unregistBurnMsgCapListener(this);
         //removeSpamReportListener(mPluginContext);
        //removeFileSpamReportListener(mPluginContext);
        
//        ChatWindowManager.getInstance().setCurrentChat(null);
        if (!mIsGroupChat) {
            List<String> numbers = getRecipientsList();
            if (numbers != null && numbers.size() == 1) {
                PortraitManager.getInstance().invalidatePortrait(numbers.get(0));
            }
        } else {
            PortraitManager.getInstance().invalidateGroupPortrait(mGroupChatId);
        }
        return true;
    }

    @Override
    public boolean onIpComposeActivityDestroy() {
        Log.d(TAG, "onIpComposeActivityDestroy ");
//        if (mIsGroupChat) {
//            PortraitManager.getInstance().invalidateGroupPortrait(mGroupChatId);
//        }
        if (mIsGroupChat) {
            GroupManager.getInstance(mContext).removeGroupListener(mInitGroupListener);
        }
        mContext.unregisterReceiver(mChangeSubReceiver);
        mRcsMessageListAdapter.removeOnMessageListChangedListener(mMsgListChangedListener);
        sComposer = null;
        return false;
    }
        
    @Override
    public boolean onIpMessageListItemHandler(int msg, long currentMsgId, long threadId, long subId) {
        Log.d(TAG, "onIpMessageListItemHandler msg =" + msg + " currentMsgId =" + currentMsgId);
        return super.onIpMessageListItemHandler(msg, currentMsgId, threadId, subId);
//        if (msg == MSG_LIST_RESEND_IPMSG) {
//            long[][] allFailedIpMsgIds = getAllFailedIpMsgByThreadId(threadId);
//            Log.d(TAG, "mMessageListItemHandler.handleMessage(): Msg_list_reand_ipmsg, "
//                    + "allFailedIpMsg len:" + allFailedIpMsgIds.length);
//            showResendConfirmDialg(currentMsgId, subId, allFailedIpMsgIds);
//            return true;
//        }
//        return false;
    }

    @Override
    public boolean onIpUpdateCounter(CharSequence text, int start, int before, int count) {
        Log.d(TAG, "onIpUpdateCounter text =" + text + " count =" + count);
        if (isRcsMode()) {
            return true;
        }

        return super.onIpUpdateCounter(text, start, before, count);
    }

    @Override
    public boolean onIpDeleteMessageListenerClick(long ipMessageId) {
        Log.d(TAG, "onIpDeleteMessageListenerClick ipMessageId =" + ipMessageId);
        return super.onIpDeleteMessageListenerClick(ipMessageId);
//        if (ipMessageId > 0) { // / M: add for ipmessage
//            // / M: delete ipmessage recorded in external db.
//            long[] ids = new long[1];
//            ids[0] = ipMessageId;
//            Log.d(TAG, "delete ipmessage, id:" + ids[0]);
//            RCSMessageManager.getInstance(mContext).deleteIpMsg(ids, false);
//            return true;
//        }
//        return false;
    }

    @Override
    public boolean onIpDiscardDraftListenerClick() {
        Log.d(TAG, "onIpDiscardDraftListenerClick mIpMessageDraft =" + mIpMessageDraft);
//        /// M: clear IP message draft.@{
//        if (mIpMessageDraft != null) {
//            if (mIpMessageDraft.getId() > 0) {
//                RCSMessageManager.getInstance(mContext).deleteIpMsg(
//                        new long[] {
//                            mIpMessageDraft.getId()
//                        }, true);
//            }
//            mIpMessageDraft = null;
//        }
//        // / @}
//        return true;
        return super.onIpDiscardDraftListenerClick();
    }

    @Override
    public boolean onIpCreateContextMenu(ContextMenu menu, boolean isSmsEnabled,
            boolean isForwardEnabled, IpMessageItem ipMsgItem) {
        Log.d(TAG, "onIpCreateContextMenu ipMsgItem =" + ipMsgItem);
        Context pluginContext = mPluginContext;
//        Context mContext;
        if (ipMsgItem == null) {
            return false;
        }
//        ipMsgItem = new RcsMessageItem();
        
        RcsMessageItem rcseMsgItem = (RcsMessageItem) ipMsgItem;
        long ipMessageId = rcseMsgItem.mIpMessageId;
        long msgId = rcseMsgItem.mMsgId;
        RCSMessageManager msgManager = RCSMessageManager.getInstance(mContext);
        IpMessage ipMessage = msgManager.getIpMsgInfo(mThreadId, ipMessageId);
        RcsMsgListMenuClickListener l = new RcsMsgListMenuClickListener(ipMsgItem);
        if (isSmsEnabled && ipMessageId != 0) {
            
            if (ipMessage != null) {
                MenuItem item = null;
                if (RcsMessageConfig.isServiceEnabled(mContext)) {
//                    int ipStatus = msgManager.getStatus(msgId);
                    int ipStatus = ipMessage.getStatus();
                    if (ipStatus == IpMessageStatus.FAILED || ipStatus == IpMessageStatus.NOT_DELIVERED) {
                        
//                        int commonType = canConvertIpMessageToMessage(ipMessage);
                        if (mIsChatActive) {
                            menu.add(0, MENU_RETRY, 0, pluginContext.getString(R.string.ipmsg_resend))
                            .setOnMenuItemClickListener(l);
                            if (ipMessage.getType() == IpMessageType.TEXT) {
                                menu.add(0, MENU_SEND_VIA_TEXT_MSG, 0,
                                        pluginContext.getString(R.string.ipmsg_send_via_text_msg))
                                        .setOnMenuItemClickListener(l);
                            }
                        }
                    }
                    if (ipStatus == IpMessageStatus.SENT) {
                        menu.add(0, MENU_DELIVERY_REPORT, 0, pluginContext.getString(R.string.ipmsg_delivery_report))
                            .setOnMenuItemClickListener(l);
                    }
                }
                item = menu.findItem(MENU_SAVE_MESSAGE_TO_SUB);
                if (item != null) {
                    menu.removeItem(MENU_SAVE_MESSAGE_TO_SUB);
                }

                if (ipMessage.getType() == IpMessageType.TEXT) {
                    item = menu.findItem(MENU_COPY);
                    if (item == null) {
                        menu.add(0, MENU_COPY, 0, pluginContext.getString(R.string.ipmsg_copy))
                        .setOnMenuItemClickListener(l);
                    }
                    if (!ipMessage.getBurnedMessage()) {
                        menu.add(0, MENU_FAVORITE, 0, pluginContext.getString(R.string.menu_favorite))
                        .setOnMenuItemClickListener(l);
                    }
                } else if(ipMessage.getType() == IpMessageType.PICTURE ||
                         ipMessage.getType() == IpMessageType.VIDEO ||
                         ipMessage.getType() == IpMessageType.VOICE ||
                         ipMessage.getType() == IpMessageType.VCARD ||
                         ipMessage.getType() == IpMessageType.GEOLOC){

                        if ((ipMessage.getStatus() == Sms.MESSAGE_TYPE_INBOX &&
                                ((IpAttachMessage)ipMessage).getRcsStatus() != Status.FINISHED)) {
                            Log.d(TAG, "this is a undownload FT");
                        } else {
                            if (!ipMessage.getBurnedMessage()) {
                                menu.add(0, MENU_EXPORT_SD_CARD, 0, pluginContext.getString(R.string.copy_to_sdcard))
                                .setOnMenuItemClickListener(l);
                                menu.add(0, MENU_FAVORITE, 0, pluginContext.getString(R.string.menu_favorite))
                                .setOnMenuItemClickListener(l);
                            }
                        }

                        item = menu.findItem(MENU_COPY);
                        if (item != null) {
                            item.setVisible(false);
                        }
                        
                        item = menu.findItem(MENU_SELECT_TEXT);
                        if (item != null) {
                            item.setVisible(false);
                        }

                        item = menu.findItem(MENU_SAVE_MESSAGE_TO_SUB);
                        if (item != null) {
                            menu.removeItem(MENU_SAVE_MESSAGE_TO_SUB);
                        }

                        item = menu.findItem(MENU_ADD_TO_BOOKMARK);
                        if (item != null) {
                            menu.removeItem(MENU_ADD_TO_BOOKMARK);
                        }

                        item = menu.findItem(MENU_ADD_ADDRESS_TO_CONTACTS);
                        while (item != null) {
                            menu.removeItem(MENU_ADD_ADDRESS_TO_CONTACTS);
                            item = menu.findItem(MENU_ADD_ADDRESS_TO_CONTACTS);
                        } 
                        
                } else {
                    //TODO: not text 
                    item = menu.findItem(MENU_COPY);
                    if (item != null) {
                        item.setVisible(false);
                    }
                    
                    item = menu.findItem(MENU_SELECT_TEXT);
                    if (item != null) {
                        item.setVisible(false);
                    }
                }
                                   
                    menu.add(0, MENU_REPORT, 0, pluginContext.getString(R.string.ipmsg_report))
                    .setOnMenuItemClickListener(l);
               
            }
        }

        if ("mms".equals(rcseMsgItem.mType)) {
            if (rcseMsgItem.mBoxId == Mms.MESSAGE_BOX_INBOX && rcseMsgItem.mMessageType == MESSAGE_TYPE_NOTIFICATION_IND) {
                Log.d(TAG, "this is a mms,rcseMsgItem.mBoxId =" + rcseMsgItem.mBoxId);
            } else {
                menu.add(0, MENU_FAVORITE, 0, pluginContext.getString(R.string.menu_favorite))
                .setOnMenuItemClickListener(l);
            }
        } else {
            if (ipMessageId == 0) {
                menu.add(0, MENU_FAVORITE, 0, pluginContext.getString(R.string.menu_favorite))
                .setOnMenuItemClickListener(l);
            }
        }
        
        if ("mms".equals(rcseMsgItem.mType)) {
            Log.d(TAG, "rcseMsgItem.mBoxId =" + rcseMsgItem.mBoxId);
        }
        int forwardMenuId = forwardMsgHandler(rcseMsgItem);
        if (MENU_FORWARD_IPMESSAGE == forwardMenuId){
            MenuItem item = menu.findItem(MENU_FORWARD_MESSAGE);
            if (item != null) {
                menu.removeItem(MENU_FORWARD_MESSAGE);
            }
            menu.add(0, MENU_FORWARD_IPMESSAGE, 0, pluginContext.getString(R.string.ipmsg_forward))
            .setOnMenuItemClickListener(l);
        } else if (0 == forwardMenuId){
            MenuItem item = null;
            item = menu.findItem(MENU_FORWARD_MESSAGE);
            if (item != null) {
                menu.removeItem(MENU_FORWARD_MESSAGE);
            }
            
            item = menu.findItem(MENU_FORWARD_IPMESSAGE);
            if (item != null) {
                menu.removeItem(MENU_FORWARD_IPMESSAGE);
            }
        }
        /*
        if (rcseMsgItem.getIpMessage() instanceof IpAttachMessage) {
        if (((IpAttachMessage)(rcseMsgItem.getIpMessage())).getRcsStatus() == Status.FAILED ) {
             
             Log.d(TAG, "((IpAttachMessage)(rcseMsgItem.getIpMessage())).getRcsStatus() =" 
                 + ((IpAttachMessage)(rcseMsgItem.getIpMessage())).getRcsStatus());
                 
             menu.add(0, MENU_RESEND, 0, pluginContext.getString(R.string.menu_resend))
                .setOnMenuItemClickListener(l);
            }
        }
        */
        if (ipMessage != null && ipMessage.getBurnedMessage()) {
               MenuItem itemBurned = menu.findItem(MENU_COPY);
            if (itemBurned != null ) {
                menu.removeItem(MENU_COPY);
            }
            
            itemBurned = menu.findItem(MENU_SELECT_TEXT);
            if (itemBurned != null) {
                menu.removeItem(MENU_SELECT_TEXT);
            }
            
            itemBurned = menu.findItem(MENU_FORWARD_IPMESSAGE);
            if (itemBurned != null) {
                menu.removeItem(MENU_FORWARD_IPMESSAGE);
            }
            itemBurned = menu.findItem(MENU_FORWARD_MESSAGE);
            if (itemBurned != null) {
                menu.removeItem(MENU_FORWARD_MESSAGE);
            }
            itemBurned = menu.findItem(MENU_DELIVERY_REPORT);
            if (itemBurned != null) {
                menu.removeItem(MENU_DELIVERY_REPORT);
            }
            itemBurned = menu.findItem(MENU_REPORT);
            if (itemBurned != null) {
                menu.removeItem(MENU_REPORT);
            }
         }
        return true;
    }

    
    public int forwardMsgHandler(RcsMessageItem rcseMsgItem){
        Intent sendIntent;
        long ipMessageId = rcseMsgItem.mIpMessageId;
        IpMessage ipMessage = RCSMessageManager.getInstance(mContext).getIpMsgInfo(mThreadId,ipMessageId);
        Log.d(TAG, "forwardMsgHandler()  mType =" + rcseMsgItem.mType+ " mIpMessageId = "+rcseMsgItem.mIpMessageId);
        if ("sms".equals(rcseMsgItem.mType)) {
            if (rcseMsgItem.mIpMessageId == 0) {
                //sms forward
                sendIntent = RcsMessageUtils.createForwordIntentFromSms(mContext, rcseMsgItem.mBody);
                if (sendIntent != null){
                    return MENU_FORWARD_IPMESSAGE;
                } else {
                    return MENU_FORWARD_MESSAGE;
                }
            } else {
                // ip message forward
                if (ipMessage == null) {
                    return 0;
                }
                sendIntent = RcsMessageUtils.createForwordIntentFromIpmessage(mContext, ipMessage);
                Log.d(TAG, "forwardMsgHandler()  sendIntent is null   getType = "+ ipMessage.getType());
                if((ipMessage.getType() == IpMessageType.TEXT)) {
                    if (sendIntent != null){
                        Log.d(TAG, "forwardMsgHandler()  sendIntent != null");
                        return MENU_FORWARD_IPMESSAGE;
                    } else {
                        return MENU_FORWARD_MESSAGE;
                    }
                }

                if((ipMessage.getType() == IpMessageType.PICTURE ||
                        ipMessage.getType() == IpMessageType.VIDEO ||
                        ipMessage.getType() == IpMessageType.VOICE ||
                        ipMessage.getType() == IpMessageType.VCARD ||
                        ipMessage.getType() == IpMessageType.GEOLOC)) {
                    Log.d(TAG, "forwardMsgHandler()  sendIntent is null   getStatus = "+ ipMessage.getStatus()+ " getRcsStatus() = "
                            +((IpAttachMessage)ipMessage).getRcsStatus() );
                    
                    if ((ipMessage.getStatus() == Sms.MESSAGE_TYPE_INBOX &&
                            ((IpAttachMessage)ipMessage).getRcsStatus() != Status.FINISHED)) {
                        return 0;
                    } else {
                        if (!ipMessage.getBurnedMessage()) {
                            return MENU_FORWARD_IPMESSAGE;
                        }
                        return 0;
                    }
                }
            }
        }
        else if ("mms".equals(rcseMsgItem.mType)){
            // mms forward
            Uri realUri = ContentUris.withAppendedId(Mms.CONTENT_URI, rcseMsgItem.mMsgId);
            sendIntent = RcsMessageUtils.createForwardIntentFromMms(mContext, realUri);
            if (sendIntent != null){
                return MENU_FORWARD_IPMESSAGE;
            } else {
                return MENU_FORWARD_MESSAGE;
            }
        }
        return MENU_FORWARD_IPMESSAGE;
    }

    private final class RcsMsgListMenuClickListener implements MenuItem.OnMenuItemClickListener {
        private long mIpMessageId;
        private long mMsgId;
        private IpMessageItem mIpMsgItem;
        public RcsMsgListMenuClickListener(IpMessageItem ipMsgItem) {
            mIpMsgItem = ipMsgItem;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            return onIpMenuItemClick(item, mIpMsgItem);
        }
    }

    @Override
    public boolean onIpMenuItemClick(MenuItem item, IpMessageItem ipMsgItem) {
        Log.d(TAG, "onIpMenuItemClick ipMsgItem =" + ipMsgItem);
        if (ipMsgItem != null && onIpMessageMenuItemClick(item, (RcsMessageItem) ipMsgItem)) {
            return true;
        }
        return false;
    }
    
    @Override
    public boolean onIpUpdateTitle(String number, String titleOriginal,
            ImageView ipCustomView, ArrayList<String> titles) {
        Log.d(TAG, "onIpUpdateTitle number =" + number);
        if (mThreadId > 0) {
            MapInfo info = ThreadMapCache.getInstance().getInfoByThreadId(mThreadId);
            if (info != null) {
                String groupName = info.getNickName();
                if (TextUtils.isEmpty(groupName)) {
                    groupName = info.getSubject();
                }
                titles.add(groupName);
                titles.add("");
                return true;
            }
        }
        return super.onIpUpdateTitle(number, titleOriginal, ipCustomView, titles);
//        if (!(IpMessageContactManager.getInstance(mContext).isIpMessageNumber(number))) {
//            return false;
//        }
//        int integrationMode = 0;
//        String title = IpMessageContactManager.getInstance(mContext).getNameByNumber(number);
//        String subTitle = null;
//        if (TextUtils.isEmpty(title)) {
//            title = titleOriginal;
//        }
//        String numberAfterFormat = "";
//        if (!title.equals(number)) {
//            if (number.startsWith(IpMessageConsts.JOYN_START)) {
//                numberAfterFormat = IpMessageUtils.formatNumber(number.substring(4),
//                        mContext.getApplicationContext());
//            }
//            if (!title.equals(numberAfterFormat)) {
//                subTitle = numberAfterFormat;
//            }
//        }
//        integrationMode = IpMessageContactManager.getInstance(mContext).getIntegrationMode(number);
//
//        mFullIntegratedView = ipCustomView;
//        Drawable integratedIcon = IpMessageResourceMananger.getInstance(mContext)
//                .getSingleDrawable(
//                IpMessageConsts.drawable.ipmsg_full_integrated);
//        mFullIntegratedView.setImageDrawable(integratedIcon);
//        if (integrationMode == IpMessageConsts.IntegrationMode.FULLY_INTEGRATED) {
//            mFullIntegratedView.setVisibility(View.VISIBLE);
//        } else {
//            mFullIntegratedView.setVisibility(View.GONE);
//        }
//        titles.add(title);
//        titles.add(subTitle);
//        return true;
    }

    @Override
    public boolean onIpTextChanged(CharSequence s, int start, int before, int count) {
        Log.d(TAG, "onIpTextChanged s=" + s + ", start=" + start + ", before=" + before + ", count=" + count);
        // delete
        if(before > 0) {
            return true;
        }
        if(!TextUtils.isEmpty(s.toString())) {
            if (mTextEditor != null) {
                float textSize = mTextEditor.getTextSize();
                EmojiImpl emojiImpl = EmojiImpl.getInstance(mPluginContext);
                CharSequence str = emojiImpl.getEmojiExpression(s, true, start, count, (int)textSize);
                mTextEditor.removeTextChangedListener(mEditorWatcher);
                mTextEditor.setTextKeepState(str);
                mTextEditor.addTextChangedListener(mEditorWatcher);
            }
        }
        return true;
    }

    @Override
    public boolean onIpRecipientsEditorFocusChange(boolean hasFocus, List<String> numbers) {
        Log.d(TAG, "onIpRecipientsEditorFocusChange hasFocus =" + hasFocus);
//        if (numbers.size() == 1) {
//            updateCurrentChatMode(null, numbers.get(0));
//        } else {
//            updateCurrentChatMode(null, "");
//        }
//        return true;
        return super.onIpRecipientsEditorFocusChange(hasFocus, numbers);
    }

    @Override
    public boolean onIpInitialize(Intent intent, IpWorkingMessageCallback workingMessageCallback) {
        mWorkingMessage = workingMessageCallback;
        long threadId = intent.getLongExtra("thread_id", 0);
        mIsNeedShowCallTime = false;
        if (threadId != 0) {
            mThreadId = threadId;
        } else {
            Uri uri = intent.getData();
            if (uri != null && uri.toString().startsWith("content://mms-sms/conversations/")) {
                String threadIdStr = uri.getPathSegments().get(1);
                threadIdStr = threadIdStr.replaceAll("-", "");
                mThreadId = Long.parseLong(threadIdStr);
            }
        }
        if (intent.getIntExtra(RCS_MODE_FLAG, 0) == RCS_MODE_VALUE) {
            mIsNeedShowCallTime = true;
        }
        if (mIsGroupChat) {
            GroupManager.getInstance(mContext).removeGroupListener(mInitGroupListener);
            dismissInvitationDialog();
            dismissInvitationTimeOutDialog();
        }

        if (mIsNeedShowCallTime) {
            setupCallStatusServiceConnection();
        }
        mIsGroupChat = false;
        if (mThreadId != 0 && RcsMessageUtils.isGroupchat(mThreadId)) {
            mGroupChatId = RcsMessageUtils.getGroupChatIdByThread(mThreadId);
            mIsGroupChat = true;
            mCallback.hideIpRecipientEditor();
            final GroupManager manager = GroupManager.getInstance(mContext);
            manager.addGroupListener(mInitGroupListener);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    mGroup = manager.getRCSGroup(mGroupChatId);
                    mGroup.addActionListener(new RCSGroup.SimpleGroupActionListener() {

                        @Override
                        public void onGroupAborted() {
                            // TODO Auto-generated method stub
                            Log.d(TAG, "onGroupAborted");
                            setChatActive(false);
//                            Toast.makeText(mContext, "add onGroupAborted", Toast.LENGTH_SHORT).show();
                        }
                        @Override
                        public void onSubjectModified(String newSubject) {
                            // TODO Auto-generated method stub
                            Log.d(TAG, "onSubjectModified: " + newSubject);
                            mContext.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    mCallback.updateIpTitle();
                                }
                            });
                        }
                        
                        @Override
                        public void onMeRemoved() {
                            // TODO Auto-generated method stub
                            Log.d(TAG, "onMeRemoved");
                            mContext.runOnUiThread(new Runnable() {
                                
                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    setChatActive(false);
                                }
                            });
                        }
                    });
                    Cursor cursor = mContext.getContentResolver().query(RcsConversation.URI_CONVERSATION, 
                            RcsConversation.PROJECTION_NEW_GROUP_INVITATION,
                            RcsConversation.SELECTION_NEW_GROUP_INVITATION_BY_THREAD,
                            new String[]{""+mThreadId}, null);
                    if (cursor != null) {
                        try {
                            int count = cursor.getCount();
                            boolean ret = cursor.moveToFirst();
                            int status = cursor.getInt(3);
                            if (status == IpMessageConsts.GroupActionList.GROUP_STATUS_INVITING ||
                                status == IpMessageConsts.GroupActionList.GROUP_STATUS_INVITING_AGAIN) {
                                RcsMessagingNotification.cancelNewGroupInviations(mContext);
                                //TODO invitation valid
                                final int state = status;
                                mContext.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // TODO Auto-generated method stub
                                        showInvitaionDialog(state);
                                        setChatActive(false);
                                    }
                                });
                            } else if (status == IpMessageConsts.GroupActionList.GROUP_STATUS_INVITE_EXPAIRED) {
                                RcsMessagingNotification.cancelNewGroupInviations(mContext);
                                //TODO invitation timeout
                                mContext.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // TODO Auto-generated method stub
                                        showInvitaionTimeOutDialog();
                                        setChatActive(false);
                                    }
                                });
                            } else if (status == IpMessageConsts.GroupActionList.GROUP_STATUS_INVALID) {
                                // group is invalid
                                mContext.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // TODO Auto-generated method stub
                                        setChatActive(false);
                                    }
                                });
                            } else {
                                if (!RCSMessageServiceManager.getInstance().isActivated()) {
                                    setChatActive(false);
                                } else {
                                    setChatActive(true);
                                }
                            }
                        } catch (Exception e) {
                            // TODO: handle exception
                        } finally {
                            cursor.close();
                        }
                    }
                }
            }, "onIpInitialize").start();
        }
        
        if (mIsGroupChat) {
            PortraitManager.getInstance().initGroupChatPortrait(mGroupChatId);
        }
        Log.d(TAG, "onIpInitialize number");
        return true;
    }

    @Override
    public boolean onIpSaveInstanceState(Bundle outState, long threadId) {
        Log.d(TAG, "onIpSaveInstanceState threadId =" + threadId);
        return super.onIpSaveInstanceState(outState, threadId);
        // / M: save ipmessage draft if needed.
//        outState.putBoolean("saved_ipmessage_draft", mIpMessageDraft != null);
//        if (numbers != null && numbers.size() == 1) {
//            saveIpMessageDraft(numbers.get(0));
//            return true;
//        } else if (threadId != 0) {
//            // saveIpMessageDraft(number);
//            return true;
//        }
//        return false;
    }

    @Override
    public boolean onIpShowSmsOrMmsSendButton(boolean isMms) {
        Log.d(TAG, "onIpShowSmsOrMmsSendButton, isMms :" + isMms+ "  mDisplayBurned = "+mDisplayBurned);
        return mDisplayBurned;
//        if (!mJustSendMsgViaCommonMsgThisTime
//                && mIsIpServiceEnabled
//                && isNetworkConnected(mContext.getApplicationContext())
//                && ((mIpMessageDraft != null) || isCurrentRecipientIpMessageUser()
//                        && mCurrentChatMode == IpMessageConsts.ChatMode.JOYN)
//                && isCurrentIpmessageSendable() && mSendButtonIpMessage != null) {
//            mSendButtonIpMessage.setImageResource(R.drawable.ic_send_ipmsg);
//            return true;
//        } else {
//            return false;
//        }
    }

    
    @Override
    public boolean onIpPrepareOptionsMenu(IpConversation ipConv, Menu menu) {
        Log.d(TAG, "onIpPrepareOptionsMenu, menu :" + menu);
        // / M: whether has IPMsg APK or not. ture: has ; false: no;
        boolean rcsServiceEnable = RcsMessageConfig.isServiceEnabled(mContext);
        Context pluginContext = mPluginContext;

        // / M: true: the host has been activated.
        boolean hasActivatedHost = RcsMessageConfig.isActivated();
        if (mOldcontact == null) {
        	Log.d(TAG, "onRequestBurnMessageCapabilityResult mOldcontact is null");
        } else {
        	Log.d(TAG, "onRequestBurnMessageCapabilityResult mOldcontact = " + mOldcontact);
        }
        if (getNumber() == null) {
        	Log.d(TAG, "onRequestBurnMessageCapabilityResult getNumber() is null");
        } else {
        	Log.d(TAG, "onRequestBurnMessageCapabilityResult getNumber() = " + getNumber());
        }

        if(getNumber() != null && !(getNumber().equals(mOldcontact)) ){
        	//getContactCapbility();
            if(1 == getRecipientSize()) {
                RCSServiceManager.getInstance().registBurnMsgCapListener(this);                
                mBurnedCapbility = false;
                Log.d(TAG, "onIpPrepareOptionsMenu onRequestBurnMessageCapabilityResult getNumber() = " + getNumber()+ " mBurnedCapbility = "+mBurnedCapbility);
                RCSServiceManager.getInstance().getBurnMsgCap(getNumber());
                mOldcontact = getNumber();
            } else {
            	mBurnedCapbility = false;
            	Log.d(TAG, "onIpPrepareOptionsMenu onRequestBurnMessageCapabilityResult mBurnedCapbility = "+mBurnedCapbility);
            }
            
        }
        
        Log.d(TAG, "[addGroupMenuOptions] group info");
        if (mIsGroupChat) {
            if (mIsChatActive) {
                MenuItem groupInfoItem = menu.add(0, MENU_GROUP_CHAT_INFO, 0, pluginContext.getText(R.string.menu_group_info));
                groupInfoItem.setIcon(pluginContext.getResources().getDrawable(R.drawable.ic_add_chat_holo_dark));
                groupInfoItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                //disable unused menu
                for (int id : unusedOptionMenuForGroup) {
                    MenuItem item = menu.findItem(id);
                    if (item != null) {
                        item.setVisible(false);
                    }
                }
            } else {
                //only can select menu
                int size = menu.size();
                for (int index = 0; index < size; index++) {
                    MenuItem item = menu.getItem(index);
                    if (item.getItemId() != MENU_SELECT_MESSAGE) {
                        item.setVisible(false);
                    }
                }
            }
        } else {
            if (mIsSmsEnabled) {
                if (rcsServiceEnable && !isRecipientsEditorVisible() && getRecipientSize() == 1) {
                    MenuItem item = menu.findItem(MENU_SHOW_CONTACT);
                    if (item != null) {
                        //if show cantact means the catact exist in db
                        menu.add(0, MENU_INVITE_FRIENDS_TO_CHAT, 0, 
                                pluginContext.getString(R.string.menu_invite_friends_to_chat));
                    }
                }
            }
            if(1 == getRecipientSize()) {
	            if (mDisplayBurned) {
	                menu.add(0, MENU_EDIT_BURNED_MSG, 0, 
	                        pluginContext.getString(R.string.menu_cacel_burned_msg));
	            } else {
	                menu.add(0, MENU_EDIT_BURNED_MSG, 0, 
	                        pluginContext.getString(R.string.menu_burned_msg));
	            }

                String number = getNumber();
                boolean isInBlackList = RCSUtils.isIpSpamMessage(mContext, number);
                if (isInBlackList) {
                    menu.add(0, MENU_BLACK_LIST, 0, 
                            pluginContext.getString(R.string.menu_remove_black_list));
                } else {
                    menu.add(0, MENU_BLACK_LIST, 0, 
                            pluginContext.getString(R.string.menu_add_black_list));
                }
            }
        }
        //not allow to edit mms
        MenuItem item = menu.findItem(MENU_ADD_SUBJECT);
        if (item != null) {
            if (isRcsMode()) {
                item.setVisible(false);
            } else {
                item.setVisible(true);
            }
        }
        return true;
    }

    @Override
    public void onIpMsgActivityResult(Context context, int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "[BurnedMsg] onIpMsgActivityResult(), ++++requestCode :" + requestCode);
        if (resultCode != RESULT_OK) {
            Log.d(TAG, "bail due to resultCode=" + resultCode);
            if (resultCode == RESULT_CANCELLED && requestCode == REQUEST_CODE_IP_MSG_BURNED_MSG_AUDIO) {
                Log.d(TAG, " [BurnedMsg]: onIpMsgActivityResult() resultCode = "+resultCode);
            } else {
                return;
            }
            
        }

        // / M: add for ip message
        switch (requestCode) {
            case REQUEST_CODE_INVITE_FRIENDS_TO_CHAT:
                Intent intent = new Intent(CreateGroupActivity.ACTIVITY_ACTION);
                ArrayList<String> allList = new ArrayList<String>();
                String[] numbers = mCallback.getConversationInfo();
                if (numbers != null && numbers.length > 0) {
                    for (String number : numbers) {
                        allList.add(number);
                        Log.v(TAG, "now chat contact number: " + number);
                    }
                }
                if (allList != null && allList.size() > 0) {
                    intent.putStringArrayListExtra(CreateGroupActivity.TAG_CREATE_GROUP_BY_NUMBERS, allList);
                }
                long[] ids = data.getLongArrayExtra("com.mediatek.contacts.list.pickdataresult");
                final long[] contactsId = data.getLongArrayExtra("com.mediatek.contacts.list.pickdataresult");
                if (contactsId != null && contactsId.length > 0) {
                    intent.putExtra(CreateGroupActivity.TAG_CREATE_GROUP_BY_IDS, contactsId);
                }
                mContext.startActivity(intent);
                
                return;
            case REQUEST_CODE_IP_MSG_PICK_CONTACTS:
                long threadId = mCallback.genIpThreadIdFromContacts(data);
                if (threadId <= 0) {
                    Log.d(TAG, "[onIpMsgActivityResult] return thread id <= 0");
                    break;
                }
                
                Intent it = createIntent(mContext.getApplicationContext(), threadId);
                mContext.startActivity(it);
                break;
            case REQUEST_CODE_IPMSG_TAKE_PHOTO:
if (!RcsMessageUtils.isValidAttach(mDstPath, false)) {
                    Toast.makeText(
                            mPluginContext,
                            R.string.ipmsg_err_file,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!RcsMessageUtils.isPic(mDstPath)) {
                    Toast.makeText(
                            mPluginContext,
                            R.string.ipmsg_invalid_file_type,
                            
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                /*
                if (RCSMessageSettingsManager.getInstance(mContext).isPicNeedResize()) {
                    new Thread(mResizePic, "ipmessage_resize_pic").start();
                } else {
                    sendImage(requestCode);
                }*/
                long photoSize = getFileSize(mDstPath);

                if (photoSize > getRcsFileMaxSize() /*&&
                    !RcsSettingsActivity.getSendMSGStatus(RCSUtils.getRcsMessagePluginAppContext())*/) {
                    Log.d(TAG, " photoSize > RCSMaxSize and not org pic sent ");
                    //new Thread(mResizePic, "ipmessage_resize_pic").start();
                    showSendPicAlertDialog();
                }else {
                    sendImage(requestCode);
                }
                return;

            case REQUEST_CODE_IPMSG_RECORD_VIDEO:
                if (!getMediaMsgInfo(data, requestCode)) {
                    Toast.makeText(
                            mPluginContext,
                            R.string.ipmsg_err_file,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
               if (!RcsMessageUtils.isVideo(mDstPath)) {
                    Toast.makeText(
                            mPluginContext,
                            R.string.ipmsg_invalid_file_type,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!RcsMessageUtils.isFileStatusOk(mPluginContext, mDstPath)) {
                    Log.e(TAG, "onIpMsgActivityResult(): record video failed, invalid file");
                    return;
                }

                mIpMsgHandler.postDelayed(mSendVideo, 100);
                return;

            case REQUEST_CODE_IPMSG_SHARE_CONTACT:
                asyncIpAttachVCardByContactsId(data);
                return;

            case REQUEST_CODE_IPMSG_CHOOSE_PHOTO:
                if (!getMediaMsgInfo(data, requestCode)) {
                    Toast.makeText(
                            mPluginContext,
                            R.string.ipmsg_err_file,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!RcsMessageUtils.isPic(mDstPath)) {
                    Toast.makeText(
                            mPluginContext,
                            R.string.ipmsg_invalid_file_type,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                mPhotoFilePath = mDstPath;

                Log.e(TAG, " send image old filename = " + mDstPath);
                
                mDstPath = RcsMessageUtils.getPhotoDstFilePath(mDstPath,mContext);

                Log.e(TAG, " send image new filename = " + mDstPath);

                photoSize = getFileSize(mPhotoFilePath);

                //boolean isSendOrgPic = RcsSettingsActivity.getSendMSGStatus(RCSUtils.getRcsMessagePluginAppContext());
                //Log.d(TAG, " isSendOrgPic = " + isSendOrgPic);
                if (photoSize > getRcsFileMaxSize() /*&& 
                    !isSendOrgPic*/) {
                    // exceed max size and don't org pic sent
                    Log.d(TAG, " photoSize > RCSMaxSize and not org pic sent ");
                    showSendPicAlertDialog();
                }else {
                    RcsMessageUtils.copyFileToDst(mPhotoFilePath,mDstPath);
                    sendImage(requestCode);
                }
                return;

            case REQUEST_CODE_IPMSG_CHOOSE_VIDEO:
                if (!getMediaMsgInfo(data, requestCode)) {
                    if (getFileSize(mDstPath) > getRcsFileMaxSize()) {
                        Toast.makeText(mPluginContext,
                                        R.string.ipmsg_over_file_limit,
                                        Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mPluginContext,
                                        R.string.ipmsg_err_file,
                                        Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                if (!RcsMessageUtils.isVideo(mDstPath)) {
                    Toast.makeText(
                            mPluginContext,
                            R.string.ipmsg_invalid_file_type,
                            Toast.LENGTH_SHORT).show();
                    return;
               }
                if (!RcsMessageUtils.isFileStatusOk(mPluginContext, mDstPath)) {
                    Log.e(TAG, "onIpMsgActivityResult(): choose video failed, invalid file");
                    return;
                }

                mVideoFilePath = mDstPath;

                Log.e(TAG, " send video old filename = " + mDstPath);
                
                mDstPath = RcsMessageUtils.getVideoDstFilePath(mVideoFilePath,mContext);

                Log.e(TAG, " send video new filename = " + mDstPath);
                
                RcsMessageUtils.copyFileToDst(mVideoFilePath,mDstPath);
                mIpMsgHandler.postDelayed(mSendVideo, 100);
                return;

            case REQUEST_CODE_IPMSG_RECORD_AUDIO:
                if (!getMediaMsgInfo(data, requestCode)) {
                    return;
                }
                if (!RcsMessageUtils.isAudio(mDstPath)) {
                    Toast.makeText(
                            mPluginContext,
                            R.string.ipmsg_invalid_file_type,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!RcsMessageUtils.isFileStatusOk(mPluginContext, mDstPath)) {
                    Log.e(TAG, "onIpMsgActivityResult(): record audio failed, invalid file");
                    return;
                }
                mAudioPath = mDstPath;
                mDstPath = RcsMessageUtils.getAudioDstPath(mAudioPath,mContext);

                RcsMessageUtils.copyFileToDst(mAudioPath, mDstPath);
                mIpMsgHandler.postDelayed(mSendAudio, 100);
                return;
            case REQUEST_CODE_IPMSG_CHOOSE_AUDIO:
                if (data != null) {
                    Uri uri = (Uri) data
                            .getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                    if (Settings.System.getUriFor(Settings.System.RINGTONE).equals(uri)) {
                        return;
                    }
                    if (getAudio(data)) {
                        mAudioPath = mDstPath;
                        mDstPath = RcsMessageUtils.getAudioDstPath(mAudioPath,mContext);
                        RcsMessageUtils.copyFileToDst(mAudioPath,mDstPath);
                        mIpMsgHandler.postDelayed(mSendAudio, 100);
                    }
                }
                return;
            case REQUEST_CODE_IP_MSG_BURNED_MSG_AUDIO:
                Log.d(TAG, " [BurnedMsg]: bindIpmsg: change data " );
                mCallback.notifyIpDataSetChanged();
                return;
            default:
                break;
        }
    }

    private void showSendPicAlertDialog() {
        Resources res = mPluginContext.getResources();
        AlertDialog.Builder b = new AlertDialog.Builder(mContext);
        b.setTitle("Picture is exceed maxsize")
            .setMessage("If resize the pic and send ?");
        b.setCancelable(true);
        b.setPositiveButton(android.R.string.ok,
           new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialog, int which) {
                    new Thread(mResizePic, "ipmessage_resize_pic").start();
                }   
           }
        ); 

        b.setNegativeButton(android.R.string.cancel,
            new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        b.create().show();
    }
    @Override
    public boolean onIpHandleForwardedMessage(Intent intent) {
        Log.d(TAG, "onIpHandleForwardedMessage, intent :" + intent);
        return super.onIpHandleForwardedMessage(intent);
//        if (intent.getBooleanExtra(FORWARD_IPMESSAGE, false)) {
//            long ipMsgId = intent.getLongExtra(IP_MESSAGE_ID, 0);
//            mIpMessageDraft = RCSMessageManager.getInstance(mContext).getIpMsgInfo(ipMsgId);
//            mIpMessageDraft.setId(0);
//            if (mIpMessageDraft.getType() == IpMessageType.PICTURE
//                    && !TextUtils.isEmpty(((IpImageMessage) mIpMessageDraft).getCaption())) {
//                mWorkingMessage.setIpText(((IpImageMessage) mIpMessageDraft).getCaption());
//            } else if (mIpMessageDraft.getType() == IpMessageType.VOICE
//                    && !TextUtils.isEmpty(((IpVoiceMessage) mIpMessageDraft).getCaption())) {
//                mWorkingMessage.setIpText(((IpVoiceMessage) mIpMessageDraft).getCaption());
//            } else if (mIpMessageDraft.getType() == IpMessageType.VIDEO
//                    && !TextUtils.isEmpty(((IpVideoMessage) mIpMessageDraft).getCaption())) {
//                mWorkingMessage.setIpText(((IpVideoMessage) mIpMessageDraft).getCaption());
//            }
//            saveIpMessageForAWhile(mIpMessageDraft);
//            return true;
//        }
//        return false;
    }

    @Override
    public boolean onIpInitMessageList(ListView list, IpMessageListAdapter adapter) {
        Log.d(TAG, "onIpInitMessageList, mIsIpMessageRecipients :" + mIsIpMessageRecipients);
        mListView = list;
        mRcsMessageListAdapter = (RcsMessageListAdapter) adapter;
        mRcsMessageListAdapter.addOnMessageListChangedListener(mMsgListChangedListener);
        return super.onIpInitMessageList(list, adapter);
        // / M: add for ip message, online divider
//        if (mIsIpMessageRecipients && getRecipientSize() == 1) {
//            mCallback.setIpOnlineDividerString(getOnlineDividerString(IpMessageContactManager
//                    .getInstance(mContext).getStatusByNumber(getNumber())));
//            return true;
//        }
//        return false;
    }
    
    @Override
    public boolean onIpSaveDraft(long threadId) {
        Log.d(TAG, "onIpSaveDraft, threadId =" + threadId);
        return super.onIpSaveDraft(threadId);
        /// M: add for save IP message draft.@{
//        if (mContext.isFinishing()) {
//            if (mIpMessageDraft != null) {
//                saveIpMessageDraft(number);
//                return true;
//            }
//            if (mIpMessageDraftId > 0) {
//                IpMessageUtils.deleteIpMessageDraft(mContext, threadId);
//            }
//        }
//        return false;
    }

    @Override
    public boolean onIpResetMessage() {
        Log.d(TAG, "onIpResetMessage, mCurrentChatMode :" + mCurrentChatMode);
        return super.onIpResetMessage();
//        if (mCurrentChatMode != IpMessageConsts.ChatMode.XMS) {
//            mIsIpServiceEnabled = RcsMessageConfig.isServiceEnabled(mContext);
//        }
//
//        // / M: add for ip message, update online divider
//        mIsIpMessageRecipients = isIpMessageRecipients();
//        if (mIsIpServiceEnabled && isNetworkConnected(mContext.getApplicationContext())) {
//            if (!TextUtils.isEmpty(mChatModeNumber)) {
//                Log.d(TAG, "resetMessage(): update mChatModeNumber from " + mChatModeNumber
//                        + " to " + getNumber());
//                IpMessageChatManger.getInstance(mContext).exitFromChatMode(mChatModeNumber);
//                mChatModeNumber = getNumber();
//                IpMessageChatManger.getInstance(mContext).enterChatMode(mChatModeNumber);
//            } else if (TextUtils.isEmpty(mChatModeNumber)) {
//                Log.d(TAG,
//                        "resetMessage(): update mChatModeNumber after send message, mChatModeNumber = "
//                                + mChatModeNumber);
//                mChatModeNumber = getNumber();
//                IpMessageChatManger.getInstance(mContext).enterChatMode(mChatModeNumber);
//            }
//        }
//        if (mIpMessageForSend == null || mIsClearTextDraft) {
//            // / M: add for ip message, clear IP message draft
//            mIpMessageDraftId = 0;
//            clearIpMessageDraft();
//            return true;
//        }
//        return false;
    }

    @Override
    public boolean onIpUpdateTextEditorHint() {
        Log.d(TAG, "onIpUpdateTextEditorHint, mIsIpServiceEnabled :" + mIsIpServiceEnabled);
        return super.onIpUpdateTextEditorHint();
//        if (mIsIpServiceEnabled && isNetworkConnected(mContext.getApplicationContext())) {
//            if (null != mIpMessageDraft
//                    || (!mWorkingMessage.requiresIpMms() && isCurrentRecipientIpMessageUser() && mCurrentChatMode == IpMessageConsts.ChatMode.JOYN)) {
//                if (mIsSmsEnabled) {
//                    mTextEditor.setHint(IpMessageResourceMananger.getInstance(mContext)
//                            .getSingleString(
//                            IpMessageConsts.string.ipmsg_hint));
//                } else {
//                    mTextEditor.setHint(R.string.sending_disabled_not_default_app);
//                }
//                updateIpMessageCounter(mWorkingMessage.getIpText(), 0, 0, 0);
//                return true;
//            }
//        }
//        return false;
    }

    public Handler mIpMsgHandler = new Handler() {
        public void handleMessage(Message msg) {
            Log.d(TAG, "mIpMsgHandler handleMessage, msg.what: " + msg.what);
            switch (msg.what) {
                case ACTION_SHARE:
                    if (RcsMessageConfig.isServiceEnabled(mContext)
                            && isNetworkConnected(mContext)
//                            && IpMessageUtils.getSDCardStatus()
                            ) {
                        doMoreAction(msg);
                    }
                    break;
                default:
                    Log.d(TAG, "msg type: " + msg.what + "not handler");
                    break;
            }
            super.handleMessage(msg);
        }
    };
/*
    private void clearIpMessageDraft() {
        Log.d(TAG, "clearIpMessageDraft() mIpMessageDraft = " + mIpMessageDraft);
        mIpMessageDraft = null;
        mCallback.callbackUpdateSendButtonState();
    }

    // / M: add for IP message draft. @{
    private void saveIpMessageDraft(String number) {
        Log.d(TAG, "saveIpMessageDraft() number = " + number);
        mChatModeNumber = number;
        if (mIpMessageDraft == null) {
            Log.e(TAG, "saveIpMessageDraft(): mIpMessageDraft is null!");
            return;
        }
        mIpMessageDraft.setStatus(IpMessageStatus.DRAFT);

        mIpMessageDraft.setTo(number);
        mCallback.setIpDraftState(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "saveIpMessageDraft(): calling API: saveIpMsg().");
                int ret = -1;
                ret = RCSMessageManager.getInstance(mContext).saveIpMsg(
                        mIpMessageDraft, IpMessageSendMode.AUTO);
                if (ret < 0) {
                    Log.w(TAG, "saveIpMessageDraft(): save IP message draft failed.");
                    mCallback.setIpDraftState(false);
                } else {
                    Log.d(TAG, "saveIpMessageDraft(): save IP message draft successfully.");
                }
            }
        }).start();
    }
*/
    @Override
    public boolean loadIpMessagDraft(long threadId) {
        Log.d(TAG, "loadIpMessagDraft() threadId = " + threadId);
        return super.loadIpMessagDraft(threadId);
//        if (mIpMessageDraft != null) {
//            Log.w(TAG, "loadIpMessagDraft(): mIpMessageDraft is not null!");
//            return false;
//        }
//        IpMessage ipMessage = IpMessageUtils.readIpMessageDraft(mContext, threadId);
//        if (ipMessage != null) {
//            mIpMessageDraftId = ipMessage.getId();
//            String caption = IpMessageUtils.getIpMessageCaption(ipMessage);
//            Log.d(TAG, "loadIpMessagDraft(): ipMessage is not null, mIpMessageDraftId = "
//                    + mIpMessageDraftId + "caption = " + caption);
//            if (!TextUtils.isEmpty(caption)) {
//                mWorkingMessage.setIpText(caption);
//                mTextEditor.setText(caption);
//            }
//            saveIpMessageForAWhile(ipMessage);
//            return true;
//        }
//        Log.w(TAG, "loadIpMessagDraft(): ipMessage is null!");
//        return false;
    }
/*
    private void saveIpMessageForAWhile(IpMessage ipMessage) {
        Log.d(TAG, "saveIpMessageForAWhile() ipMessage = " + ipMessage);
        if (mIpMessageDraft != null && mIpMessageForSend != null) {
            showReplaceAttachDialog();
            return;
        }
        mIpMessageDraft = ipMessage;
        mIpMessageForSend = null;
        if (mWorkingMessage.requiresIpMms()) {
            convertIpMessageToMmsOrSms(true);
            mCallback.callbackUpdateSendButtonState();
            return;
        }
        switch (mIpMessageDraft.getType()) {
            case IpMessageType.TEXT:
                IpTextMessage textMessage = (IpTextMessage) mIpMessageDraft;
                mTextEditor.setText(textMessage.getBody());
                break;
            default:
                Log.e(TAG, "saveIpMessageForAWhile(): Error IP message type. type = "
                        + mIpMessageDraft.getType());
                break;
        }
        mJustSendMsgViaCommonMsgThisTime = false;
        onIpUpdateTextEditorHint();
    }

    private String getOnlineDividerString(int currentRecipientStatus) {
        Log.d(TAG, "getOnlineDividerString() currentRecipientStatus = " + currentRecipientStatus);
        if (RcsMessageConfig.isActivated(mContext) && mIsIpMessageRecipients) {
            switch (currentRecipientStatus) {
                case ContactStatus.OFFLINE:
                    Log.d(TAG, "compose.getOnlineDividerString(): OFFLINE");

                    String onlineTimeString = IpMessageResourceMananger.getInstance(mContext)
                            .getSingleString(
                            IpMessageConsts.string.ipmsg_divider_never_online);
                    Log.d(TAG, "compose.getOnlineDividerString(): OFFLINE");
                    return onlineTimeString;
                case ContactStatus.ONLINE:
                case ContactStatus.TYPING:
                case ContactStatus.RECORDING:
                    String name = IpMessageContactManager.getInstance(mContext)
                            .getNameByNumber(getNumber());
                    return String.format(IpMessageResourceMananger.getInstance(mContext)
                                    .getSingleString(IpMessageConsts.string.ipmsg_divider_online),
                            name);
                case ContactStatus.STATUSCOUNT:
                    Log.d(TAG, "compose.getOnlineDividerString(): STATUSCOUNT");
                    break;
                default:
                    Log.w(TAG, "compose.getOnlineDividerString(): unknown user status!");
                    break;
            }
        }
        return "";
    }
*/
    @Override
    public boolean checkIpMessageBeforeSendMessage(long subId, boolean bCheckEcmMode) {
        Log.d(TAG, "checkIpMessageBeforeSendMessage() mCurrentChatMode = " + mCurrentChatMode);
        if (RcsMessageUtils.getConfigStatus()) {
            int mainCardSubId = SubscriptionManager.getDefaultDataSubId();
            if (!SubscriptionManager.isValidSubscriptionId(mainCardSubId) || (int)subId != mainCardSubId) {
                if (!mIsGroupChat) {
                    return false;
                }
            }
        } else {
            if (!mIsGroupChat) {
                return false;//not Config
            } else {
                return true;
            }
        }
        Log.d(TAG, "checkIpMessageBeforeSendMessage() conitnue send");
        mWorkingMessage = mCallback.getWorkingMessage();

        // if the message is mms, need use mms send
        boolean isMms = mWorkingMessage.requiresIpMms();
        if (isMms) {
            if (mIsGroupChat) {
                throw new RuntimeException("can not send mms in group chat");
            }
            return false;
        }

        if (!RCSMessageServiceManager.getInstance().isActivated((int)subId)) {
            //not Activated
            return false;
        }

        boolean serviceReady = RCSMessageServiceManager.getInstance().serviceIsReady();
//        if (!serviceReady && mIsGroupChat) {
//            Toast.makeText(mContext, mPluginContext.getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
//            mCallback.callbackUpdateSendButtonState();
//            return true;
//        }
        if (isNeedShowSendWayChangedDialog(serviceReady, (int)subId)) {
            showSendWayChanged(serviceReady, (int)subId);
            return true;
        } else {
            int sendMode = getLastSendMode();
            if (sendMode == PREFERENCE_VALUE_SEND_WAY_SMS) {
                //TODO if last send mode is sms, always by sms infuture if not indicate?
                return false;
            }
        }

        mIsIpServiceEnabled = RcsMessageConfig.isServiceEnabled(mContext);
        
        mIsIpMessageRecipients = isCurrentRecipientIpMessageUser();

        //if (mIpMessageForSend != null) {
        //    sendMessageForIpMsg(mIpMessageForSend, false, true);
        //    return true;
        //}

        if (null != mIpMessageDraft) {
            // Has IP message draft
            if (!mIsIpServiceEnabled || !isNetworkConnected(mContext.getApplicationContext())) {
                // / M: disabled service
                if (mIpMessageDraft.getType() == IpMessageType.TEXT) {
//                    showIpMessageConvertToMmsOrSmsDialog(SMS_CONVERT, SERVICE_IS_NOT_ENABLED);
                } else {
//                    showIpMessageConvertToMmsOrSmsDialog(MMS_CONVERT, SERVICE_IS_NOT_ENABLED);
                }
                return true;
            }

            // / M: Fix ipmessage bug for ALPS01674002 @{
            if (!mIsIpMessageRecipients || !isCurrentIpmessageSendable()) {
                // / M: non-IP message User
                if (mIpMessageDraft.getType() == IpMessageType.TEXT) {
//                    showIpMessageConvertToMmsOrSmsDialog(SMS_CONVERT,
//                            !mIsIpMessageRecipients ? RECIPIENTS_ARE_NOT_IP_MESSAGE_USER
//                                    : RECIPIENTS_IP_MESSAGE_NOT_SENDABLE);
                } else {
//                    showIpMessageConvertToMmsOrSmsDialog(MMS_CONVERT,
//                            !mIsIpMessageRecipients ? RECIPIENTS_ARE_NOT_IP_MESSAGE_USER
//                                    : RECIPIENTS_IP_MESSAGE_NOT_SENDABLE);
                }
                // / @}
                return true;
            }

            if (sendMessageForIpMsg(mIpMessageDraft, true, true)) {
                mIpMessageDraftId = 0;
//                clearIpMessageDraft();
            }
            return true;
        } else {
            // No IP message draft
            if (mIsIpServiceEnabled
//                    && isNetworkConnected(mContext.getApplicationContext())
                    && mIsIpMessageRecipients
                    && isCurrentIpmessageSendable()) {
                String body = mTextEditor.getText().toString();
                if (body != null && body.length() > 0) {
                    sendIpTextMessage();
                    return true;
                }
            }
            return false;
        }
    }


    private boolean isCurrentRecipientIpMessageUser() {
        return true;
        /// M: fix ALPS01033728, return false while ipmessage plug out.
//        if (IpMessageContactManager.getInstance(mContext).isIpMessageNumber(mChatModeNumber)) {
//            return true;
//        } else {
//            return false;
//        }
    }

    // / M: Fix ipmessage bug ,fix bug ALPS 01556382@{
    private boolean isCurrentIpmessageSendable() {
        return true;
//        if (IpMessageContactManager.getInstance(mContext).getStatusByNumber(mChatModeNumber) == ContactStatus.OFFLINE) {
//            return false;
//        } else {
//            return true;
//        }
    }
    // / @}

    private boolean isIpMessageRecipients() {
        mIsIpMessageRecipients = RCSMessageContactManager.getInstance(mContext).isIpMessageNumber(
                mChatModeNumber);
        return mIsIpMessageRecipients;
    }

    private Runnable mHideReminderRunnable = new Runnable() {
        @Override
        public void run() {
//            Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
//                    "notificationsReceived(): hide reminder.");
            if (null != mTypingStatus) {
                mTypingStatus.setVisibility(View.GONE);
            }
        }
    };
/* not to show cost reminder for cmcc
    // show mms has cost reminder view
    private Runnable mShowMmsReminderRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                    "notificationsReceived(): show mms reminder view.");
            if (null != mTypingStatus) {
                mTypingStatus.setText(RCSMessageResourceMananger.getInstance(mContext)
                        .getSingleString(
                                IpMessageConsts.string.ipmsg_mms_cost_remind));
                mTypingStatus.setVisibility(View.VISIBLE);
            }
        }
    };
*/
    /*
    // show joyn has cost reminder view
    private Runnable mShowJoynReminderRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                    "notificationsReceived(): show joyn reminder view.");
            if (null != mTypingStatus) {
                mTypingStatus.setText(IpMessageResourceMananger.getInstance(mContext)
                        .getSingleString(
                                IpMessageConsts.string.ipmsg_joyn_cost_remind));
                mTypingStatus.setVisibility(View.VISIBLE);
            }
        }
    };
    */
/*
    private final int MMS_COST_REMINDER = 0;
    private final int JOYN_COST_REMINDER = 1;

    private void showReminderView(int type) {
        if (type == MMS_COST_REMINDER) {
            mIpMsgHandler.removeCallbacks(mShowMmsReminderRunnable);
            mIpMsgHandler.post(mShowMmsReminderRunnable);
        } else if (type == JOYN_COST_REMINDER) {
            mIpMsgHandler.removeCallbacks(mShowJoynReminderRunnable);
            mIpMsgHandler.post(mShowJoynReminderRunnable);
        }
        mIpMsgHandler.removeCallbacks(mHideReminderRunnable);
        mIpMsgHandler.postDelayed(mHideReminderRunnable, 2000);
    }

    @Override
    public void notificationsReceived(Intent intent) {
        Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                "compose.notificationsReceived(): start, intent = " + intent);
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            return;
        }
        switch (IpMessageUtils.getActionTypeByAction(action)) {
        // / M: toast and update UI after activate ipmessage service @{
            case IpMessageUtils.IPMSG_REG_STATUS_ACTION:
                int regStatus = intent.getIntExtra(IpMessageConsts.RegStatus.REGSTATUS, 0);
                switch (regStatus) {
                    case IpMessageConsts.RegStatus.REG_OVER:
                        mContext.runOnUiThread(new Runnable() {
                            public void run() {
                                if (mUiHandler != null) {
                                    mUiHandler.sendEmptyMessage(UPDATE_SENDBUTTON);
                                }
                                Toast.makeText(
                                        mContext.getApplicationContext(),
                                        IpMessageResourceMananger
                                                .getInstance(mContext)
                                                .getSingleString(
                                                IpMessageConsts.string.ipmsg_nms_enable_success),
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        break;
                }
                Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                        "notificationsReceived(): regStatus = " + regStatus);
                break;
            // / @}
            case IpMessageUtils.IPMSG_IM_STATUS_ACTION:
                Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                        "notificationsReceived(): IM status, mChatModeNumber = " + mChatModeNumber);
                if (!mIsIpServiceEnabled || !isNetworkConnected(mContext.getApplicationContext())
                        || isRecipientsEditorVisible() || getRecipientSize() != 1
                        || !mIsIpMessageRecipients || TextUtils.isEmpty(mChatModeNumber)) {
                    return;
                }
                final String number = intent.getStringExtra(IpMessageConsts.NUMBER);
                Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                        "notificationsReceived(): number = " + number + ", mChatModeNumber = "
                                + mChatModeNumber);
                if (!TextUtils.isEmpty(number)) {
                    int status = IpMessageContactManager.getInstance(mContext).getStatusByNumber(
                            mChatModeNumber);
                    Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                            "notificationsReceived(): IM status. number = " + number
                                    + ", status = " + status);

                    mChatSenderName = IpMessageContactManager.getInstance(mContext)
                            .getNameByNumber(number);
                    switch (status) {
                        case ContactStatus.TYPING:
                            mIpMsgHandler.post(mShowTypingStatusRunnable);
                            break;
                        case ContactStatus.STOP_TYPING:
                            mIpMsgHandler.post(mHideTypingStatusRunnable);
                            break;
                        case ContactStatus.RECORDING:
                        case ContactStatus.STOP_RECORDING:
                            return;
                        case ContactStatus.ONLINE:
                        case ContactStatus.OFFLINE:
                            Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                                    "notificationsReceived(): user online status changed, "
                                            + "number = " + number + ", mChatModeNumber = "
                                            + mChatModeNumber);
                            mCallback.setIpOnlineDividerString(getOnlineDividerString(
                                    IpMessageContactManager.getInstance(mContext)
                                            .getStatusByNumber(number)));
                            mCallback.updateIpOnlineDividerTime();
                            mContext.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mCallback.notifyIpDataSetChanged();
                                    // / M: Fix ipmessage bug @{
                                    int preChatMode = mCurrentChatMode;
                                    updateCurrentChatMode(null, number);
                                    if (preChatMode != mCurrentChatMode) {
                                        mCallback.showIpOrMmsSendButton(false);
                                        mCallback.invalidateIpOptionsMenu();
                                    }
                                    // / @}
                                }
                            });
                            return;
                        case ContactStatus.STATUSCOUNT:
                        default:
                            return;
                    }
                }
                break;
            case IpMessageUtils.IPMSG_ERROR_ACTION:
            case IpMessageUtils.IPMSG_NEW_MESSAGE_ACTION:
            case IpMessageUtils.IPMSG_REFRESH_CONTACT_LIST_ACTION:
            case IpMessageUtils.IPMSG_REFRESH_GROUP_LIST_ACTION:
            case IpMessageUtils.IPMSG_SERCIVE_STATUS_ACTION:
            case IpMessageUtils.IPMSG_SAVE_HISTORY_ACTION:
            case IpMessageUtils.IPMSG_ACTIVATION_STATUS_ACTION:
                break;
            case IpMessageUtils.IPMSG_IP_MESSAGE_STATUS_ACTION:
                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mCallback.setClearIpCacheFlag(true);
                        mCallback.notifyIpDataSetChanged();
                    }
                });
                break;
            case IpMessageUtils.IPMSG_DOWNLOAD_ATTACH_STATUS_ACTION:
            case IpMessageUtils.IPMSG_SET_PROFILE_RESULT_ACTION:
            case IpMessageUtils.IPMSG_BACKUP_MSG_STATUS_ACTION:
            case IpMessageUtils.IPMSG_RESTORE_MSG_STATUS_ACTION:
            default:
                return;
        }
    }
*/
    private boolean onIpMessageMenuItemClick(MenuItem menuItem, RcsMessageItem rcseMsgItem) {
        long ipMessageId = rcseMsgItem.mIpMessageId;
        long msgId = rcseMsgItem.mMsgId;

        Log.d(TAG, "onIpMessageMenuItemClick(): ");
        switch (menuItem.getItemId()) {
            case MENU_SEND_VIA_TEXT_MSG:
//                if (IpMessageServiceMananger.getInstance(mContext).getIntegrationMode() == IpMessageConsts.IntegrationMode.CONVERGED_INBOX) {
//                    jumpToJoynChat(false, ipMessageId);
//                } else {
//                    if (mWorkingMessage.requiresIpMms()
//                            || !TextUtils.isEmpty(mTextEditor.getText().toString())
//                            || mIpMessageDraft != null) {
//                        showDiscardCurrentMessageDialog(msgId);
//                    } else {
//                        sendViaMmsOrSms(msgId);
//                    }
//                }
                return true;

            case MENU_RETRY:
                IpMessage iPMsg = RCSMessageManager.getInstance(mContext).getIpMsgInfo(ipMessageId);
                if (iPMsg.getType() == IpMessageType.TEXT ) {
                     RCSMessageManager.getInstance(mContext).resendMessage(msgId,
                           rcseMsgItem.mSubId);
                } else {
                    RCSMessageManager.getInstance(mContext).reSendFileTransfer(ipMessageId,
                            mThreadId);
                }
                
                return true;

            case MENU_FORWARD_IPMESSAGE:
                Log.d(TAG, "MENU_FORWARD_IPMESSAGE");
                hideInputMethod();
                forwardIpMsg(mContext, rcseMsgItem);
                return true;

            case MENU_SHARE:
//                IpMessage ipMsp = RCSMessageManager.getInstance(mContext).getIpMsgInfo(msgId);
//                shareIpMsg(ipMsp);
                return true;

            case MENU_EXPORT_SD_CARD:    
                IpMessage ipMessageForSave = RCSMessageManager.getInstance(mContext)
                        .getIpMsgInfo(mThreadId, ipMessageId);
                Log.d(TAG, "onIpMessageMenuItemClick(): Save IP message. msgId = " + ipMessageId);
                copyFile((IpAttachMessage) ipMessageForSave);
                return true;

            case MENU_VIEW_IP_MESSAGE:
//                IpMessage ipMessageForView = RCSMessageManager.getInstance(mContext).getIpMsgInfo(msgId);
//                Log.d(TAG, "onIpMessageMenuItemClick(): View IP message. msgId = " + msgId
//                        + ", type = " + ipMessageForView.getType());
//                if (ipMessageForView.getType() >= IpMessageType.PICTURE
//                        && ipMessageForView.getType() < IpMessageType.UNKNOWN_FILE) {
//                    openMedia(msgId, (IpAttachMessage) ipMessageForView);
//                }
                return true;
            case MENU_FAVORITE:
                if ("sms".equals(rcseMsgItem.mType)) {
                    String address = null;
                    if (rcseMsgItem.mBoxId == 1) {
                        address = rcseMsgItem.mAddress;
                    }
                    if (ipMessageId != 0) {
                        IpMessage ipMessageForFavorite = RCSMessageManager.getInstance(mContext)
                        .getIpMsgInfo(mThreadId, ipMessageId);
                        if (ipMessageId < 0) {
                            IpAttachMessage attachMessage = (IpAttachMessage) ipMessageForFavorite;
                            setAttachIpmessageFavorite(msgId, attachMessage.getType(), attachMessage.getPath(), address);
                        } else {
                            IpTextMessage textMessage = (IpTextMessage) ipMessageForFavorite;
                            setTextIpmessageFavorite(msgId, textMessage.getType(), textMessage.getBody(), address);
                        }
                    } else {
                            setSmsFavorite(mContext,
                            msgId,rcseMsgItem.mBody,address);
                    }
                } else {
                        setMmsFavorite(mContext,msgId, rcseMsgItem.mSubject,
                        getRecipientStr(), rcseMsgItem.mBoxId, rcseMsgItem.mMessageType);
                }
                return true;

             case MENU_REPORT: 
                
                 // Context menu handlers for the spam report.
                 String contact = String.valueOf("100869999");
                 Log.d(TAG, "spam-report:  ipMessageId = " + ipMessageId + "  contact = "+contact + "  mThreadId = "+mThreadId);
                 RCSMessageManager.getInstance(mContext)
                    .initSpamReport(contact,mThreadId,ipMessageId);
                 
                 return true;

             case MENU_DELIVERY_REPORT:
                showIpDeliveryReport(mContext, rcseMsgItem); 
                return true;

            default:
                break;
        }
        return false;
    }

    private void showIpDeliveryReport(Context context, RcsMessageItem rcseMsgItem) {
        Log.d(TAG, "showIpDeliveryReport()");
        long ipMessageId = rcseMsgItem.mIpMessageId;
        long msgId = rcseMsgItem.mMsgId;
        String address = rcseMsgItem.mAddress;
        String status = "UnKnown";
        int ipStatus = rcseMsgItem.getIpMessage().getStatus();
        if (ipStatus == IpMessageStatus.VIEWED) {
            status = "Viewed";
        } else if (ipStatus == IpMessageStatus.DELIVERED) {
            status = "Delivered";
        } else if (ipStatus == IpMessageStatus.OUTBOX) {
            status = "Sending";
        } else if (ipStatus == IpMessageStatus.FAILED) {
            status = "Failed";
        } else if (ipStatus == IpMessageStatus.SENT) {
            status = "Send";
        }
        String report = "Address: "+address+"\nStatus: "+status;

        new AlertDialog.Builder(context)
            .setTitle(mPluginContext.getString(R.string.ipmsg_delivery_report))
            .setMessage(report)
            .setCancelable(true)
            .show();
        return;
    }

    private boolean forwardIpMsg(Context context, RcsMessageItem rcseMsgItem) {
      
        Intent sendIntent = new Intent();
        long ipMessageId = rcseMsgItem.mIpMessageId;
          IpMessage ipMessage = RCSMessageManager.getInstance(mContext).getIpMsgInfo(mThreadId,ipMessageId);
          Log.d(TAG, "forwardMsgHandler()  mType =" + rcseMsgItem.mType+ " mIpMessageId = "+ipMessageId);
          if ("sms".equals(rcseMsgItem.mType)) {
              if (rcseMsgItem.mIpMessageId == 0) {
                  //sms forward
                  sendIntent = RcsMessageUtils.createForwordIntentFromSms(context, rcseMsgItem.mBody);
              } else {
                  // ip message forward
                  sendIntent = RcsMessageUtils.createForwordIntentFromIpmessage(context ,ipMessage);
                  if (sendIntent != null){
                  } else {
                      if((ipMessage.getType() == IpMessageType.PICTURE ||
                              ipMessage.getType() == IpMessageType.VIDEO ||
                              ipMessage.getType() == IpMessageType.VOICE ||
                              ipMessage.getType() == IpMessageType.VCARD ||
                              ipMessage.getType() == IpMessageType.GEOLOC)) {
                        Toast.makeText(context,"no service, can't forward",
                                Toast.LENGTH_SHORT).show();
                          return false;
                      }
              }

              }
          } else if ("mms".equals(rcseMsgItem.mType)){
              // mms forward
              Uri realUri = ContentUris.withAppendedId(Mms.CONTENT_URI, rcseMsgItem.mMsgId);
              sendIntent = RcsMessageUtils.createForwardIntentFromMms(context, realUri);
          }
          
          mContext.startActivity(sendIntent);
          return true;
    }

    private boolean isNetworkConnected(Context context) {
        boolean isNetworkConnected = false;
        ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(mContext.CONNECTIVITY_SERVICE);
        State state = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
        if (State.CONNECTED == state) {
            isNetworkConnected = true;
        }
        if (!isNetworkConnected) {
            state = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState();
            if (State.CONNECTED == state) {
                isNetworkConnected = true;
            }
        }
        return isNetworkConnected;
    }
/*
    private NetworkInfo getActiveNetworkInfo(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return null;
        }
        return connectivity.getActiveNetworkInfo();
    }

    private void showReplaceAttachDialog() {
        if (mReplaceDialog != null) {
            return; // / M: shown already.
        }
        mReplaceDialog = new AlertDialog.Builder(mContext)
                .setTitle(
                        IpMessageResourceMananger.getInstance(mContext)
                                .getSingleString(IpMessageConsts.string.ipmsg_replace_attach))
                .setMessage(
                        IpMessageResourceMananger.getInstance(mContext)
                                .getSingleString(IpMessageConsts.string.ipmsg_replace_attach_msg))
                .setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mIpMessageDraft = null;
                        mReplaceDialog = null;
                        saveIpMessageForAWhile(mIpMessageForSend);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mIpMessageForSend = null;
                        mReplaceDialog = null;
                    }
                }).create();
        mReplaceDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mReplaceDialog = null;
            }
        });
        mReplaceDialog.show();
    }

    private boolean updateIpMessageCounter(CharSequence text, int start, int before, int count) {
        Log.d(TAG, "updateIpMessageCounter()");
        if (mIsIpServiceEnabled && isNetworkConnected(mContext.getApplicationContext())
                && IpMessageUtils.getSDCardStatus() && isCurrentRecipientIpMessageUser()
                && !mWorkingMessage.requiresIpMms() && !mJustSendMsgViaCommonMsgThisTime
                && text.length() > 0) {
            final int length = text.length();
            mUiHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG,
                            "updateIpMessageCounter(): IP text message, mTextEditor.getLineCount() = "
                                    + mTextEditor.getLineCount());
                    if (mTextEditor.getLineCount() > 1) {
                        mTextCounter.setVisibility(View.VISIBLE);
                        mTextCounter.setText(length + "/"
                                + RCSMessageManager.getInstance(mContext).getMaxTextLimit());
                    } else {
                        mTextCounter.setVisibility(View.GONE);
                        mTextCounter.setText(length + "/"
                                + RCSMessageManager.getInstance(mContext).getMaxTextLimit());
                    }
                }
            }, 100);
            return true;
        }
        return false;
    }

    private void startFileManager() {
        Log.d(TAG, "startFileManager()");
        Intent intent = new Intent(CHOICE_FILEMANAGER_ACTION);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        mContext.startActivityForResult(intent, this.REQUEST_CODE_IPMSG_SHARE_FILE);
    }

    private boolean sendFileViaJoyn(Intent data) {
        String fileName = Uri.decode(data.getDataString());
        if (fileName != null && fileName.startsWith(FILE_SCHEMA)) {
            String fileFullPath = fileName.substring(FILE_SCHEMA.length(), fileName.length());
            if (fileFullPath != null) {
                Uri contentUri = null;
                Cursor c = mContext.getContentResolver().query(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        new String[] {
                                MediaStore.MediaColumns._ID, Images.Media.MIME_TYPE
                        }, MediaStore.MediaColumns.DATA + "=?", new String[] {
                            fileFullPath
                        }, null);
                if (c != null && c.getCount() != 0 && c.moveToFirst()) {
                    contentUri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            c.getString(0));
                } else {
                    if (c != null) {
                        c.close();
                    }
                    c = mContext.getContentResolver().query(
                            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                            new String[] {
                                    MediaStore.MediaColumns._ID, Audio.Media.MIME_TYPE
                            }, MediaStore.MediaColumns.DATA + "=?", new String[] {
                                fileFullPath
                            }, null);
                    if (c != null && c.getCount() != 0 && c.moveToFirst()) {
                        contentUri = Uri.withAppendedPath(
                                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, c.getString(0));
                    } else {
                        if (c != null) {
                            c.close();
                        }
                        c = mContext.getContentResolver().query(
                                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                                new String[] {
                                        MediaStore.MediaColumns._ID, Video.Media.MIME_TYPE
                                }, MediaStore.MediaColumns.DATA + "=?", new String[] {
                                    fileFullPath
                                }, null);
                        if (c != null && c.getCount() != 0 && c.moveToFirst()) {
                            contentUri = Uri.withAppendedPath(
                                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI, c.getString(0));
                        }
                    }
                }
                try {
                    if (c != null && c.getCount() != 0 && c.moveToFirst()) {
                        Log.i(TAG, "Get id in MediaStore:" + c.getString(0));
                        Log.i(TAG, "Get content type in MediaStore:" + c.getString(1));
                        Log.i(TAG, "Get uri in MediaStore:" + contentUri);
                        String contentType = c.getString(1);
                        data.setData(contentUri);
                        if (contentType.startsWith("image/")) {
                            onIpMsgActivityResult(mContext, REQUEST_CODE_IPMSG_CHOOSE_PHOTO,
                                    RESULT_OK,
                                    data);
                        } else if (contentType.startsWith("video/")) {
                            onIpMsgActivityResult(mContext, REQUEST_CODE_IPMSG_CHOOSE_VIDEO,
                                    RESULT_OK,
                                    data);
                        } else if (contentType.startsWith("audio/")) {
                            onIpMsgActivityResult(mContext, REQUEST_CODE_IPMSG_CHOOSE_AUDIO,
                                    RESULT_OK,
                                    data);
                        }
                        return true;
                    } else {
                        sendUnknownMsg(fileFullPath);
                        Log.e(TAG, "MediaStore:" + MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                                + " has not this file");
                    }
                } finally {
                    if (c != null) {
                        c.close();
                    }
                }
            }
        }
        return false;
    }

    // / M: Fix ipmessage bug for ALPS 01566645 @{
    public void updateCurrentChatMode(Intent intent, String number) {
        // / M: add for ip message, check recipients is ipmessage User
        mChatModeNumber = number;
        mIsIpMessageRecipients = isCurrentRecipientIpMessageUser();
        // / M: modify for ipmessage because the cotnact status is useless and
        // incorrect now @{
        boolean status = isCurrentIpmessageSendable();
        Log.d(TAG, "mIsIpMessageRecipients = " + mIsIpMessageRecipients + "status = " + status);
        // / @}
        if (IpMessageServiceMananger.getInstance(mContext).getIntegrationMode() == IpMessageConsts.IntegrationMode.ISMS_MODE) {
            if (mIsIpMessageRecipients && status) {
                mCurrentChatMode = IpMessageConsts.ChatMode.JOYN;
            } else {
                mCurrentChatMode = IpMessageConsts.ChatMode.XMS;
            }
            return;
        }

        if (intent != null && intent.hasExtra("chatmode")) {
            mCurrentChatMode = intent.getIntExtra("chatmode", IpMessageConsts.ChatMode.XMS);
        } else {
            if (IpMessageServiceMananger.getInstance(mContext).getIntegrationMode() == IpMessageConsts.IntegrationMode.FULLY_INTEGRATED) {
                Log.d(TAG, "is full integrated mode");
                // / M: modify for ipmessage because the cotnact status is
                // useless and incorrect now
                // if (mIsIpMessageRecipients && status !=
                // ContactStatus.OFFLINE) {
                if (mIsIpMessageRecipients && status) {
                    mCurrentChatMode = IpMessageConsts.ChatMode.JOYN;
                } else {
                    mCurrentChatMode = IpMessageConsts.ChatMode.XMS;
                }
            } else {
                if (number.startsWith(IpMessageConsts.JOYN_START)) {
                    mCurrentChatMode = IpMessageConsts.ChatMode.JOYN;
                } else {
                    mCurrentChatMode = IpMessageConsts.ChatMode.XMS;
                }
            }
        }
        Log.d(TAG, "updateCurrentChatMode mCurrentChatMode = " + mCurrentChatMode);
    }

    // / @}

    private Object mShowTypingLockObject = new Object();
    private Thread mShowTypingThread = new Thread(new Runnable() {
        @Override
        public void run() {
            String showingStr = IpMessageResourceMananger.getInstance(mContext)
                    .getSingleString(IpMessageConsts.string.ipmsg_typing_text);
            final String displayString0 = showingStr + ".    ";
            final String displayString1 = showingStr + "..   ";
            final String displayString2 = showingStr + "...  ";
            int i = 0;
            while (!mIsDestroyTypingThread) {
                while (null != mTypingStatus && mTypingStatus.getVisibility() != View.GONE) {
                    switch (i % 3) {
                        case 0:
                            mIpMsgHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                                            "mShowTypingThread: display 0.");
                                    mTypingStatus.setText(displayString0);
                                }
                            });
                            i++;
                            break;
                        case 1:
                            mIpMsgHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                                            "mShowTypingThread: display 1.");
                                    mTypingStatus.setText(displayString1);
                                }
                            });
                            i++;
                            break;
                        case 2:
                            mIpMsgHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                                            "mShowTypingThread: display 2.");
                                    mTypingStatus.setText(displayString2);
                                }
                            });
                            i++;
                            break;
                        default:
                            break;
                    }
                    synchronized (this) {
                        try {
                            this.wait(1000);
                        } catch (InterruptedException e) {
                            Log.d(TAG, "InterruptedException");
                        }
                    }
                }
                synchronized (mShowTypingLockObject) {
                    try {
                        mShowTypingLockObject.wait();
                    } catch (InterruptedException e) {
                        Log.d(TAG, "InterruptedException");
                    }
                }
            }
            Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG, "mShowTypingThread: destroy thread.");
        }
    }, "showTypingThread");
*/


    private long getRcsFileMaxSize() {
        long maxSize = RCSUtils.getFileTransferMaxSize() * 1024;
        Log.d(TAG, "getRcsFileMaxSize() = " + maxSize);
        return maxSize;
    }

    private long getFileSize(String filepath) {
        return RcsMessageUtils.getFileSize(filepath);
    }

    private Runnable mResizePic = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "mResizePic(): start resize pic.");
            long maxLen = 
                getRcsFileMaxSize() > RcsMessageUtils.getPhotoSizeLimit()? RcsMessageUtils.getPhotoSizeLimit():getRcsFileMaxSize();
            byte[] img = RcsMessageUtils.resizeImg(mPhotoFilePath, (float) maxLen);
            if (null == img) {
                return;
            }
            Log.d(TAG, "mResizePic(): put stream to file.");
            try {
                RcsMessageUtils.nmsStream2File(img, mDstPath);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
            Log.d(TAG, "mResizePic(): post send pic.");
            mIpMsgHandler.postDelayed(mSendPic, 100);
        }
    };


    private boolean sendMessageForIpMsg(final IpMessage ipMessage, boolean isSendSecondTextMessage,
            final boolean isDelDraft) {
        Log.d(TAG, "sendMessageForIpMsg(): start.");
/*

        ipMessage.setTo(mChatModeNumber);
        if (TextUtils.isEmpty(ipMessage.getTo()) && mIpMessageForSend == null) {
//            saveIpMessageForAWhile(ipMessage);
            mCallback.callbackUpdateSendButtonState();
            mCallback.callbackUpdateButtonState(true);
            return false;
        }
        if (!RcsMessageConfig.isServiceEnabled(mContext)) {
            if (mIpMessageForSend == null) {
                mIpMessageDraft = ipMessage;
            }
            //TODO convert to sms send
//            if (ipMessage.getType() == IpMessageType.TEXT) {
//                showIpMessageConvertToMmsOrSmsDialog(SMS_CONVERT, SERVICE_IS_NOT_ENABLED);
//            } else {
//                showIpMessageConvertToMmsOrSmsDialog(MMS_CONVERT, SERVICE_IS_NOT_ENABLED);
//            }
            return false;
        }

        // / M: Fix ipmessage bug for ALPS01674002 @{
        boolean isCurrentIpUser = isCurrentRecipientIpMessageUser();
        if (!isCurrentIpUser || !isCurrentIpmessageSendable()) {
            if (mIpMessageForSend == null) {
                mIpMessageDraft = ipMessage;
            }
            //TODO: convert to sms send
//            if (ipMessage.getType() == IpMessageType.TEXT) {
//                showIpMessageConvertToMmsOrSmsDialog(SMS_CONVERT,
//                        !isCurrentIpUser ? RECIPIENTS_ARE_NOT_IP_MESSAGE_USER
//                                : RECIPIENTS_IP_MESSAGE_NOT_SENDABLE);
//            } else {
//                showIpMessageConvertToMmsOrSmsDialog(MMS_CONVERT,
//                        !isCurrentIpUser ? RECIPIENTS_ARE_NOT_IP_MESSAGE_USER
//                                : RECIPIENTS_IP_MESSAGE_NOT_SENDABLE);
//            }
            // / @}
            return false;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "sendMessageForIpMsg(): calling API: saveIpMsg().");
                int ret = -1;
                ipMessage.setStatus(IpMessageStatus.OUTBOX);

                ret = RCSMessageManager.getInstance(mContext).saveIpMsg(
                        ipMessage, IpMessageSendMode.AUTO);

                if (ret < 0) {
                    Log.w(TAG, "sendMessageForIpMsg(): ");
                } else {
                    if (ipMessage.getType() == IpMessageType.TEXT) {
                        mCallback.asyncDeleteDraftSmsMessage();
                    }
                }
            }
        }).start();

        if (isSendSecondTextMessage && ipMessage.getType() != IpMessageType.TEXT
//                && TextUtils.isEmpty(IpMessageUtils.getIpMessageCaption(ipMessage))
                && mTextEditor != null && mTextEditor.getVisibility() == View.VISIBLE
                && !TextUtils.isEmpty(mTextEditor.getText().toString())) {
            IpTextMessage ipTextMessage = new IpTextMessage();
            ipTextMessage.setBody(mTextEditor.getText().toString());
            ipTextMessage.setTo(mChatModeNumber);
            ipTextMessage.setStatus(IpMessageStatus.OUTBOX);

            final IpMessage ipTextMessageForSend = ipTextMessage;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG,
                            "sendMessageForIpMsg(): send second text IP message, calling API: saveIpMsg().");
                    int ret = -1;
                    
                    ret = RCSMessageManager.getInstance(mContext).saveIpMsg(
                            ipTextMessageForSend, IpMessageSendMode.AUTO);
                    if (ret < 0) {
                        Log.w(TAG,
                                "sendMessageForIpMsg(): send second text IP message failed!");
                    }
                }
            }).start();
        }
        mCallback.syncIpWorkingRecipients();
        mCallback.guaranteeIpThreadId();
        if (mIpMessageForSend == null) {
            mCallback.onPreIpMessageSent();
            Log.d(TAG, "sendMessageForIpMsg(): after guaranteeThreadId()");
        } else {
            mCallback.resetIpMessage();
            mIpMessageForSend = null;
        }
        mCallback.onIpMessageSent();
        return true;
        */
        if (!mIsGroupChat) {
            mWorkingMessage.syncWorkingIpRecipients();
            mChatModeNumber = getRecipientStr();
        }

        ipMessage.setTo(mChatModeNumber);

         new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "sendMessageForIpMsg(): calling API: saveIpMsg().");
                int ret = -1;
                ipMessage.setStatus(IpMessageStatus.OUTBOX);
                if (!mIsGroupChat) {
                    mCallback.guaranteeIpThreadId();
                    mThreadId = mCallback.getCurrentThreadId();
                }
                ret = RCSMessageManager.getInstance(mContext).saveRCSMsg(ipMessage, 0, mThreadId);
                mCallback.onPreIpMessageSent();

                if (ipMessage.getType() == IpMessageConsts.IpMessageType.TEXT) {
                    mIpMessageForSend = null;
                    mCallback.asyncDeleteDraftSmsMessage();
                } else {
                    Message msg = new Message();
                    msg.what = ret;
                    //Bundle data = new Bundle();
                    //data.putInt("res",ret);
                    //msg.setData(data);
                    mFTNotifyHandler.sendMessage(msg);
                }

//                if (ret == RCSMessageManager.SAVE_SUCCESS) {
                    mCallback.onIpMessageSent();
//                }

            }
        }).start();
         
        return true;
    }

    public Handler mFTNotifyHandler = new Handler() {
        public void handleMessage(Message msg) {
            Log.d(TAG, "mIpMsgHandler handleMessage, msg.what: " + msg.what);
            switch (msg.what) {
                    case RCSMessageManager.ERROR_CODE_UNSUPPORT_TYPE:
                        Toast.makeText(
                            mContext,
                            mPluginContext.getString(R.string.ipmsg_invalid_file_type),
                            Toast.LENGTH_SHORT).show();
                        break;
                    case RCSMessageManager.ERROR_CODE_INVALID_PATH:
                        Toast.makeText(
                            mContext,
                            mPluginContext.getString(R.string.ipmsg_invalid_file_type),
                            Toast.LENGTH_SHORT).show();
                        break;
                    case RCSMessageManager.ERROR_CODE_EXCEED_MAXSIZE:
                        Toast.makeText(
                            mContext,
                            mPluginContext.getString(R.string.ipmsg_over_file_limit),
                            Toast.LENGTH_SHORT).show();
                        break;
                    case RCSMessageManager.ERROR_CODE_UNKNOWN:
                        Toast.makeText(
                            mContext,
                            mPluginContext.getString(R.string.ipmsg_invalid_file_type),
                            Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            super.handleMessage(msg);
    }
    };

    private void sendIpTextMessage() {
        String body = mTextEditor.getText().toString();
        if (TextUtils.isEmpty(body)) {
            Log.w(TAG, "sendIpTextMessage(): No content for sending!");
            return;
        }
        IpTextMessage msg = new IpTextMessage();
        msg.setBody(body);
        msg.setType(IpMessageType.TEXT);
        msg.setBurnedMessage(mDisplayBurned);
        mIpMessageForSend = msg;
        mIsClearTextDraft = true;
        sendMessageForIpMsg(msg, false, false);
    }

    private Runnable mSendAudio = new Runnable() {
        public void run() {
            if (RcsMessageUtils.isExistsFile(mDstPath) && RcsMessageUtils.getFileSize(mDstPath) != 0) {
                IpVoiceMessage msg = new IpVoiceMessage();
                msg.setPath(mDstPath);
                msg.setDuration(mDuration);
                msg.setSize(RcsMessageUtils.getFileSize(mDstPath));
                msg.setType(IpMessageType.VOICE);
                msg.setBurnedMessage(mDisplayBurned);
                //mIpMessageForSend = msg;
                sendMessageForIpMsg(msg, false, false);
                mIpMsgHandler.removeCallbacks(mSendAudio);
            }
        }
    };

    private Runnable mSendGeoLocation = new Runnable() {
        public void run() {
            Log.d(TAG, "mSendGeoLocation(): start.");
            if (RcsMessageUtils.isExistsFile(mDstPath) && RcsMessageUtils.getFileSize(mDstPath) != 0) {
                IpGeolocMessage msg = new IpGeolocMessage();
                msg.setPath(mDstPath);
                msg.setType(IpMessageType.GEOLOC);
                //mIpMessageForSend = msg;
                sendMessageForIpMsg(msg, false, false);
                mIpMsgHandler.removeCallbacks(mSendGeoLocation);
            }
        }
    };
    private Runnable mSendPic = new Runnable() {
        public void run() {
            Log.d(TAG, "mSendPic(): start.");
            if (RcsMessageUtils.isExistsFile(mDstPath) && 
                        RcsMessageUtils.getFileSize(mDstPath) != 0) {
                Log.d(TAG, "mSendPic(): start send image.");
                sendImage(REQUEST_CODE_IPMSG_TAKE_PHOTO);
                mIpMsgHandler.removeCallbacks(mSendPic);
                // refreshAndScrollList();
            }
            Log.d(TAG, "mSendPic(): end.");
        }
    };

    private Runnable mSendVideo = new Runnable() {
        public void run() {
            Log.d(TAG, "mSendVideo(): start send video. Path = " + mDstPath);
            if (RcsMessageUtils.isExistsFile(mDstPath) && RcsMessageUtils.getFileSize(mDstPath) != 0) {
                IpVideoMessage msg = new IpVideoMessage();
                msg.setPath(mDstPath);
                msg.setDuration(mDuration);
                msg.setType(IpMessageType.VIDEO);
                msg.setBurnedMessage(mDisplayBurned);
                //mIpMessageForSend = msg;
                sendMessageForIpMsg(msg, false, false);
                mIpMsgHandler.removeCallbacks(mSendVideo);
                // refreshAndScrollList();
            }
        }
    };

    private Runnable mSendVcard = new Runnable() {
        public void run() {
            // / M: cracks, wait activity resume, ensure dialog context valid.
            //if (!mContext.isFinishing()) {
            //    mIpMsgHandler.postDelayed(mSendVcard, 100);
            //    Log.d(TAG, "mSendVcard, wait activity resume.");
            //    return;
            //}

            if (RcsMessageUtils.isExistsFile(mDstPath) && RcsMessageUtils.getFileSize(mDstPath) != 0) {
                IpVCardMessage msg = new IpVCardMessage();
                msg.setPath(mDstPath);
                msg.setName(mIpMessageVcardName);
                msg.setType(IpMessageType.VCARD);
                //mIpMessageForSend = msg;
                sendMessageForIpMsg(msg, false, false);
                mIpMsgHandler.removeCallbacks(mSendVcard);
                // refreshAndScrollList();
            }
        }
    };

    public boolean getMediaMsgInfo(Intent data, int requestCode) {
        if (null == data) {
            Log.e(TAG, "getMediaMsgInfo(): take video error, result intent is null.");
            return false;
        }

        Uri uri = data.getData();
        Cursor cursor = null;
        if (requestCode == REQUEST_CODE_IPMSG_TAKE_PHOTO
                || requestCode == REQUEST_CODE_IPMSG_CHOOSE_PHOTO) {
            final String[] selectColumn = {
                "_data"
            };
            cursor = mContext.getContentResolver().query(uri, selectColumn, null, null, null);
        } else {
            final String[] selectColumn = {
                    "_data", "duration"
            };
            cursor = mContext.getContentResolver().query(uri, selectColumn, null, null, null);
        }
        if (null == cursor) {
            if (requestCode == REQUEST_CODE_IPMSG_RECORD_AUDIO) {
                mDstPath = uri.getEncodedPath();
                mDuration = data.getIntExtra("audio_duration", 0);
                mDuration = mDuration / 1000 == 0 ? 1 : mDuration / 1000;
            } else {
                mPhotoFilePath = uri.getEncodedPath();
                mDstPath = mPhotoFilePath;
            }
            return true;
        }
        if (0 == cursor.getCount()) {
            cursor.close();
            Log.e(TAG, "getMediaMsgInfo(): take video cursor getcount is 0");
            return false;
        }
        cursor.moveToFirst();
        if (requestCode == REQUEST_CODE_IPMSG_TAKE_PHOTO
                || requestCode == REQUEST_CODE_IPMSG_CHOOSE_PHOTO) {
            mPhotoFilePath = cursor.getString(cursor.getColumnIndex("_data"));
            mDstPath = mPhotoFilePath;
        } else {
            mDstPath = cursor.getString(cursor.getColumnIndex("_data"));
            mDuration = cursor.getInt(cursor.getColumnIndex("duration"));
            mDuration = mDuration / 1000 == 0 ? 1 : mDuration / 1000;
        }
        if (null != cursor && !cursor.isClosed()) {
            cursor.close();
        }
        return true;
    }

    private boolean getAudio(Intent data) {
        Uri uri = (Uri) data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
        if (Settings.System.getUriFor(Settings.System.RINGTONE).equals(uri)) {
            return false;
        }
        if (null == uri) {
            uri = data.getData();
        }
        if (null == uri) {
            Log.e(TAG, "getAudio(): choose audio failed, uri is null");
            return false;
        }
        final String scheme = uri.getScheme();
        if (scheme.equals("file")) {
            mDstPath = uri.getEncodedPath();
        } else {
            ContentResolver cr = mContext.getContentResolver();
            Cursor c = cr.query(uri, null, null, null, null);
            c.moveToFirst();
            mDstPath = c.getString(c.getColumnIndexOrThrow(Audio.Media.DATA));
            c.close();
        }

        if (!RcsMessageUtils.isAudio(mDstPath)) {
            Toast.makeText(
                    mPluginContext,
                    R.string.ipmsg_invalid_file_type, Toast.LENGTH_SHORT)
                    .show();
            return false;
        }

        if (!RCSMessageServiceManager.getInstance().isFeatureSupported(  
                IpMessageConsts.FeatureId.FILE_TRANSACTION)
                && !RcsMessageUtils.isFileStatusOk(mPluginContext, mDstPath)) {
            Log.e(TAG, "getAudio(): choose audio failed, invalid file");
            return false;
        }

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(mContext, uri);
            String dur = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            if (dur != null) {
                mDuration = Integer.parseInt(dur);
                mDuration = mDuration / 1000 == 0 ? 1 : mDuration / 1000;
            }
        } catch (Exception ex) {
            Log.e(
                    TAG,
                    "getAudio(): MediaMetadataRetriever failed to get duration for "
                            + uri.getPath());
            return false;
        } finally {
            retriever.release();
        }
        return true;
    }

    private void sendImage(int requestCode) {
        IpImageMessage msg = new IpImageMessage();
        msg.setType(IpMessageType.PICTURE);
        msg.setPath(mDstPath);
        msg.setSize(RcsMessageUtils.getFileSize(mDstPath));
        msg.setBurnedMessage(mDisplayBurned);
        //msg.setRcsStatus(Status.TRANSFERING);
        msg.setStatus(Sms.MESSAGE_TYPE_OUTBOX);
        //mIpMessageForSend = msg;
        Log.d(TAG, "sendImage(): start send message.");
        sendMessageForIpMsg(msg, false, false);
    }

/*
    private void sendUnknownMsg(String path) {
        IpAttachMessage msg = new IpAttachMessage();
        msg.setPath(path);
        msg.setType(IpMessageType.UNKNOWN_FILE);
        mIpMessageForSend = msg;
        sendMessageForIpMsg(mIpMessageForSend, false, false);
    }

    public void getCalendar(Context context, String calendar) {
        Uri calendarUri = Uri.parse(calendar);
        InputStream is = null;
        OutputStream os = null;
        Cursor cursor = mContext.getContentResolver().query(calendarUri, null, null, null, null);
        if (null != cursor) {
            if (0 == cursor.getCount()) {
                Log.e(TAG, "getCalendar(): take calendar cursor getcount is 0");
            } else {
                cursor.moveToFirst();
                mCalendarSummary = cursor.getString(0);
                if (mCalendarSummary != null) {
                    int sub = mCalendarSummary.lastIndexOf(".");
                    mCalendarSummary = mCalendarSummary.substring(0, sub);
                }
            }
            cursor.close();

            String fileName = System.currentTimeMillis() + ".vcs";
            mDstPath = RcsMessageConfig.getVcalendarTempPath(mContext) + File.separator + fileName;

            File file = new File(mDstPath);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            file.delete();
            try {
                if (!file.createNewFile()) {
                    return;
                }
            } catch (IOException e) {
                Log.e(TAG, "getCalendar()");
                return;
            }
            try {
                is = mContext.getContentResolver().openInputStream(calendarUri);
                os = new BufferedOutputStream(new FileOutputStream(file));
            } catch (FileNotFoundException e) {
                Log.e(TAG, "getCalendar()");
            }
            byte[] buffer = new byte[256];
            try {
                for (int len = 0; (len = is.read(buffer)) != -1;) {
                    os.write(buffer, 0, len);
                }
                is.close();
                os.close();
            } catch (IOException e) {
                Log.e(TAG, "getCalendar()");
            }
        }
    }

    private String getNameViaContactId(long contactId) {
        if (contactId <= 0) {
            Log.w(TAG, "getNameViaContactId(): contactId is invalid!");
            return null;
        }

        String displayName = "";

        Cursor cursor = mContext.getContentResolver().query(Contacts.CONTENT_URI, new String[] {
            Contacts.DISPLAY_NAME
        }, Contacts._ID + "=?", new String[] {
            String.valueOf(contactId)
        }, null);
        if (cursor != null && cursor.moveToFirst()) {
            displayName = cursor.getString(cursor.getColumnIndex(Contacts.DISPLAY_NAME));
        }
        if (cursor != null) {
            cursor.close();
        }

        return displayName == null ? "" : displayName;
    }

    private boolean mSendSmsInJoynMode = false;

    private boolean mSendJoynMsgInSmsMode = false;
*/

    @Override
    public boolean onIpMsgOptionsItemSelected(IpConversation ipConv,MenuItem item, long threadId) {
        switch (item.getItemId()) {
            case MENU_INVITE_FRIENDS_TO_CHAT:
                if (RCSMessageServiceManager.getInstance().isFeatureSupported(
                        IpMessageConsts.FeatureId.EXTEND_GROUP_CHAT)) {
                    Intent intent = new Intent("android.intent.action.contacts.list.PICKMULTIPHONES");
                    intent.setType(Phone.CONTENT_TYPE);
                    intent.putExtra("Group", true);
                    List<String> existNumbers = new ArrayList<String>();
                    String number = getNumber();
                    existNumbers.add(number);
                    String me = RcsProfile.getInstance().getNumber();
                    if (!TextUtils.isEmpty(me)) {
                        existNumbers.add(me);
                    }
                    String[] numbers = existNumbers.toArray(new String[existNumbers.size()]);
                    intent.putExtra("ExistNumberArray", numbers);
                    mContext.startActivityForResult(intent, REQUEST_CODE_INVITE_FRIENDS_TO_CHAT);
                    return true;
                }
                break;
            case MENU_GROUP_CHAT_INFO:
                Log.i(TAG, "launch GroupChatInfo Activity");
                {
                    Intent intent = new Intent("com.mediatek.rcs.message.ui.RcsGroupManagementSetting");
                    intent.setPackage("com.mediatek.rcs.message");
                    intent.putExtra("SCHATIDKEY", mGroupChatId);
                    mContext.startActivity(intent);
                    return true;
                }
            case MENU_EDIT_BURNED_MSG:
            	Log.d(TAG, "onRequestBurnMessageCapabilityResult MENU_EDIT_BURNED_MSG mBurnedCapbility = " + mBurnedCapbility);
                if(!mBurnedCapbility){
//                    Toast.makeText(mPluginContext,R.string.ipmsg_burn_cap,
//                            Toast.LENGTH_SHORT).show();
//                	return true;
                }
                mDisplayBurned = !mDisplayBurned;
//                if( !RCSMessageManager.getInstance(mContext).isBurnAferReadCapbility(getNumber())) {
//                    return false;
//                }

                if(mDisplayBurned){
                    //mSendButtonIpMessage.setImageResource(R.drawable.ic_send_ipbar);
                    mSendButtonIpMessage.setImageDrawable(mPluginContext.
                            getResources().getDrawable(R.drawable.ic_send_ipbar));
                } else {
                    mSendButtonIpMessage.setImageDrawable(mPluginContext.
                            getResources().getDrawable(R.drawable.ic_send_ipmsg));
                    mBurnedCapbility = false;
                }

                RcsMessageConfig.setEditingDisplayBurnedMsg(mDisplayBurned);
                mCallback.resetSharePanel();
                return true;
            case MENU_CALL_RECIPIENT:
                if (mIsGroupChat) {
                    //TODO: multi call
                    List<Participant> participants = mGroup.getParticipants();
                    ArrayList<Participant> toCallParticipants = new ArrayList<Participant>();
//                    ArrayList<String> numbers = new ArrayList<String>();
                    String myNumber = RcsProfile.getInstance().getNumber();
                    PortraitManager pManager = PortraitManager.getInstance();
                    for (Participant p : participants) {
                        if (!p.getContact().equals(myNumber)) {
                            MemberInfo info = pManager.getMemberInfo(mGroup.getChatId(), p.getContact());
                            Participant partipant = new Participant(info.mNumber, info.mName);
                            toCallParticipants.add(partipant);
                        }
                    }
                    if (toCallParticipants.size() == 1) {
                        Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + toCallParticipants.get(0).getContact()));
                        mContext.startActivity(dialIntent);
                    } else if (toCallParticipants.size() > 1) {
                        showSelectContactNumberDialog(toCallParticipants);
                        
                    }
                    return true;
                } else if (getRecipientSize() > 1) {
                    //one2multi
                    ArrayList<Participant> toCallParticipants = new ArrayList<Participant>();
                    RcsConversation conversation = (RcsConversation)mCallback.getIpConversation();
                    if (conversation != null) {
                        List<IpContact> list = conversation.getIpContactList();
                        for (IpContact c : list) {
                            RcsContact contact = (RcsContact)c;
                            Participant p = new Participant(contact.getNumber(), contact.getName());
                            toCallParticipants.add(p);
                        }
                        if (toCallParticipants.size() > 1) {
                            showSelectContactNumberDialog(toCallParticipants);
                        } else {
                            Log.e(TAG, "init call data error: toCallParticipants is not bigger than 1");
                        }
                    } else {
                        Log.e(TAG, "init call data error: conversation is null");
                    }
                    return true;
                }
                break;

            case MENU_BLACK_LIST:
                String number = getNumber();
                String recipient = getRecipientStr();
                boolean isInBlackList = RCSUtils.isIpSpamMessage(mContext, number);
                CCSblacklist blist = new CCSblacklist(mContext);
                if (isInBlackList) {
                   blist.removeblackNumber(number);
                } else {
                   blist.addblackNumber(number, recipient);
                }
                break;
            default:
                break;
        }
        return false;
    }
    /*
    private void showIpMessageConvertToMmsOrSmsDialog(int mode, int convertReason) {
        String message = "";
        if (convertReason == SERVICE_IS_NOT_ENABLED) {
            message = mode == SMS_CONVERT ? IpMessageResourceMananger.getInstance(mContext)
                    .getSingleString(
                    IpMessageConsts.string.ipmsg_convert_to_sms_for_service) : IpMessageResourceMananger.getInstance(mContext).getSingleString(
                            IpMessageConsts.string.ipmsg_convert_to_mms_for_service);
        } else if (convertReason == RECIPIENTS_ARE_NOT_IP_MESSAGE_USER) {
            message = mode == SMS_CONVERT ? IpMessageResourceMananger.getInstance(mContext)
                    .getSingleString(
                    IpMessageConsts.string.ipmsg_convert_to_sms_for_recipients) : IpMessageResourceMananger.getInstance(mContext).getSingleString(
                            IpMessageConsts.string.ipmsg_convert_to_mms_for_recipients);
        }
        // / M: Fix ipmessage bug for ALPS01674002 @{
        else if (convertReason == RECIPIENTS_IP_MESSAGE_NOT_SENDABLE) {
            message = mode == SMS_CONVERT ? IpMessageResourceMananger.getInstance(mContext)
                    .getSingleString(
                    IpMessageConsts.string.ipmsg_ip_msg_not_sendable_to_sms) : IpMessageResourceMananger.getInstance(mContext).getSingleString(
                            IpMessageConsts.string.ipmsg_ip_msg_not_sendable_to_mms);
        }
        // / @}
        new AlertDialog.Builder(mContext)
                .setTitle(
                        mode == SMS_CONVERT ? IpMessageResourceMananger.getInstance(mContext)
                                .getSingleString(
                                IpMessageConsts.string.ipmsg_convert_to_sms) : IpMessageResourceMananger.getInstance(mContext).getSingleString(
                                        IpMessageConsts.string.ipmsg_convert_to_mms))
                .setMessage(message)
                .setPositiveButton(
                        IpMessageResourceMananger.getInstance(mContext)
                                .getSingleString(IpMessageConsts.string.ipmsg_continue),
                        new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mIpMessageDraft != null) {
                                    convertIpMessageToMmsOrSms(true);
                                }
                                if (mIpMessageForSend != null) {
                                    if (mIpMessageForSend.getType() == IpMessageType.TEXT) {
                                        Log.e(TAG,
                                                "convertIpMessageToMmsOrSms(): convert IP message to SMS.");
                                        mIpMessageForSend = null;
                                        mCallback.callbackCheckConditionsAndSendMessage(true);
                                    } else {
                                        mIpMessageDraft = mIpMessageForSend;
                                        mIpMessageForSend = null;
                                        convertIpMessageToMmsOrSms(true);
                                    }
                                }
                                dialog.dismiss();
                            }
                        }).setNegativeButton(android.R.string.cancel, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d(TAG, "showIpMessageConvertToMmsOrSmsDialog(): cancel.");
                        if (mIpMessageForSend != null) {
                            mIpMessageForSend = null;
                        } else {
                            saveIpMessageForAWhile(mIpMessageDraft);
                        }
                        mCallback.callbackUpdateSendButtonState();
                        mCallback.callbackUpdateButtonState(true);
                        dialog.dismiss();
                    }
                }).setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            mCallback.callbackUpdateSendButtonState();
                        }
                        return false;
                    }
                }).show();
    }

*/

/*
    private boolean convertIpMessageToMmsOrSms(boolean isAppend) {
        Log.d(TAG,
                "convertIpMessageToMmsOrSms(): IP message type = " + mIpMessageDraft.getType());
        int type = mIpMessageDraft.getType();
        String path = null;
        String text = null;
        int subId = -1;
        switch (mIpMessageDraft.getType()) {
            case IpMessageType.TEXT:
                Log.d(TAG, "convertIpMessageToMmsOrSms(): convert to SMS.");
                IpTextMessage textMessage = (IpTextMessage) mIpMessageDraft;
                text = textMessage.getBody();
                break;
            case IpMessageType.PICTURE:
                Log.d(TAG, "convertIpMessageToMmsOrSms(): convert to MMS from image.");
                IpImageMessage imageMessage = (IpImageMessage) mIpMessageDraft;
                Log.d(TAG,
                        "convertIpMessageToMmsOrSms(): imagePath = " + imageMessage.getPath());
                text = imageMessage.getCaption();
                path = imageMessage.getPath();
                break;
            case IpMessageType.VOICE:
                Log.d(TAG, "convertIpMessageToMmsOrSms(): convert to MMS from voice.");
                IpVoiceMessage voiceMessage = (IpVoiceMessage) mIpMessageDraft;
                text = voiceMessage.getCaption();
                path = voiceMessage.getPath();
                break;
            case IpMessageType.VCARD:
                Log.d(TAG, "convertIpMessageToMmsOrSms(): convert to MMS from vCard.");
                IpVCardMessage vCardMessage = (IpVCardMessage) mIpMessageDraft;
                path = vCardMessage.getPath();
                break;
            case IpMessageType.VIDEO:
                Log.d(TAG, "convertIpMessageToMmsOrSms(): convert to MMS from video.");
                IpVideoMessage videoMessage = (IpVideoMessage) mIpMessageDraft;
                text = videoMessage.getCaption();
                path = videoMessage.getPath();
                break;
            case IpMessageType.CALENDAR:
                Log.d(TAG, "convertIpMessageToMmsOrSms(): convert to MMS from vCalendar.");
                IpVCalendarMessage vCalendarMessage = (IpVCalendarMessage) mIpMessageDraft;
                path = vCalendarMessage.getPath();
                break;
            case IpMessageType.UNKNOWN_FILE:
            case IpMessageType.COUNT:
                Log.w(TAG,
                        "convertIpMessageToMmsOrSms(): Unknown IP message type. type = "
                                + mIpMessageDraft.getType());
                return false;
            case IpMessageType.GROUP_CREATE_CFG:
            case IpMessageType.GROUP_ADD_CFG:
            case IpMessageType.GROUP_QUIT_CFG:
                // / M: group chat type
                Log.w(TAG, "convertIpMessageToMmsOrSms(): Group IP message type. type = "
                        + mIpMessageDraft.getType());
                return false;
            default:
                Log.w(TAG, "convertIpMessageToMmsOrSms(): Error IP message type. type = "
                        + mIpMessageDraft.getType());
                return false;
        }
        subId = mIpMessageDraft.getSimId();
        mCallback.convertIpMessageToMmsOrSms(type, isAppend, path, text, subId);
        mIpMessageDraftId = 0;
        clearIpMessageDraft();
        return true;
    }

    private void saveMsgInSDCard(IpAttachMessage ipAttachMessage) {
        if (!IpMessageUtils.getSDCardStatus()) {
            IpMessageUtils.createLoseSDCardNotice(
                    mContext,
                    IpMessageResourceMananger.getInstance(mContext).getSingleString(
                            IpMessageConsts.string.ipmsg_cant_save));
            return;
        }

        long availableSpace = IpMessageUtils.getSDcardAvailableSpace();
        int size = ipAttachMessage.getSize();

        if (availableSpace <= IpMessageUtils.SDCARD_SIZE_RESERVED || availableSpace <= size) {
            String tips = IpMessageResourceMananger.getInstance(mContext)
                    .getSingleString(IpMessageConsts.string.ipmsg_sdcard_space_not_enough);
            Toast.makeText(mContext, tips, Toast.LENGTH_LONG).show();
            return;
        }

        String source = ipAttachMessage.getPath();
        if (TextUtils.isEmpty(source)) {
            Log.e(TAG, "saveMsgInSDCard(): save ipattachmessage failed, source empty!");
            Toast.makeText(mContext, mContext.getString(R.string.copy_to_sdcard_fail), Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        String attName = source.substring(source.lastIndexOf("/") + 1);
        String dstFile = "";
        dstFile = IpMessageUtils.getCachePath(mContext) + attName;
        int i = 1;
        while (IpMessageUtils.isExistsFile(dstFile)) {
            dstFile = IpMessageUtils.getCachePath(mContext) + "(" + i + ")" + attName;
            i++;
        }
        IpMessageUtils.copy(source, dstFile);
        String saveSuccess = String.format(IpMessageResourceMananger.getInstance(mContext)
                .getSingleString(
                        IpMessageConsts.string.ipmsg_save_file), dstFile);
        Toast.makeText(mContext, saveSuccess, Toast.LENGTH_SHORT).show();
    }

    private void openMedia(long msgId, IpAttachMessage ipAttachMessage) {
        if (ipAttachMessage == null) {
            Log.e(TAG, "openMedia(): ipAttachMessage is null!");
            return;
        }
        Log.d(TAG, "openMedia(): ipAttachMessage type = " + ipAttachMessage.getType());

        if (ipAttachMessage.getType() == IpMessageType.VCARD) {
            IpVCardMessage msg = (IpVCardMessage) ipAttachMessage;
            if (TextUtils.isEmpty(msg.getPath())) {
                Log.e(TAG, "openMedia(): open vCard failed.");
                return;
            }
            if (!IpMessageUtils.getSDCardStatus()) {
                IpMessageUtils.createLoseSDCardNotice(
                        mContext,
                        IpMessageResourceMananger.getInstance(mContext)
                                .getSingleString(IpMessageConsts.string.ipmsg_cant_share));
                return;
            }
            if (getAvailableBytesInFileSystemAtGivenRoot(StorageManagerEx.getDefaultPath()) < msg
                    .getSize()) {
                Toast.makeText(mContext, mContext.getString(R.string.export_disk_problem), Toast.LENGTH_LONG)
                        .show();
            }
            String dest = IpMessageUtils.getCachePath(mContext) + "temp"
                    + msg.getPath().substring(msg.getPath().lastIndexOf(".vcf"));
            IpMessageUtils.copy(msg.getPath(), dest);
            File vcardFile = new File(dest);
            Uri vcardUri = Uri.fromFile(vcardFile);
            Intent i = new Intent();
            i.setAction(android.content.Intent.ACTION_VIEW);
            i.setDataAndType(vcardUri, "text/x-vcard");
            mContext.startActivity(i);
        } else if (ipAttachMessage.getType() == IpMessageType.CALENDAR) {
            IpVCalendarMessage msg = (IpVCalendarMessage) ipAttachMessage;
            if (TextUtils.isEmpty(msg.getPath())) {
                Log.e(TAG, "openMedia(): open vCalendar failed.");
                return;
            }
            if (!IpMessageUtils.getSDCardStatus()) {
                IpMessageUtils.createLoseSDCardNotice(
                        mContext,
                        IpMessageResourceMananger.getInstance(mContext)
                                .getSingleString(IpMessageConsts.string.ipmsg_cant_share));
                return;
            }
            if (getAvailableBytesInFileSystemAtGivenRoot(StorageManagerEx.getDefaultPath()) < msg
                    .getSize()) {
                Toast.makeText(mContext, mContext.getString(R.string.export_disk_problem), Toast.LENGTH_LONG)
                        .show();
            }
            String dest = IpMessageUtils.getCachePath(mContext) + "temp"
                    + msg.getPath().substring(msg.getPath().lastIndexOf(".vcs"));
            IpMessageUtils.copy(msg.getPath(), dest);
            File calendarFile = new File(dest);
            Uri calendarUri = Uri.fromFile(calendarFile);
            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            intent.setDataAndType(calendarUri, "text/x-vcalendar");
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                mContext.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Log.e(TAG, "can't open calendar");
            }
        } else {
            Intent intent = new Intent(RemoteActivities.MEDIA_DETAIL);
            intent.putExtra(RemoteActivities.KEY_MESSAGE_ID, msgId);
            IpMessageUtils.startRemoteActivity(mContext, intent);
        }
    }

    private class InviteFriendsToIpMsgListener implements OnClickListener {
        public void onClick(DialogInterface dialog, int whichButton) {
            mTextEditor.setText(IpMessageResourceMananger.getInstance(mContext)
                    .getSingleString(IpMessageConsts.string.ipmsg_invite_friends_content));

        }
    }

     private void shareIpMsg(IpMessage ipMessage) {
        if (null == ipMessage) {
            Log.d(TAG, "shareIpMsg(): message item is null!");
            return;
        }
        if (ipMessage instanceof IpAttachMessage) {
            if (!IpMessageUtils.getSDCardStatus()) {
                IpMessageUtils.createLoseSDCardNotice(
                        mContext,
                        IpMessageResourceMananger.getInstance(mContext)
                                .getSingleString(IpMessageConsts.string.ipmsg_cant_share));
                return;
            }
        }
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent = setIntent(intent, ipMessage);
        intent.putExtra(Intent.EXTRA_SUBJECT, IpMessageResourceMananger.getInstance(mContext)
                .getSingleString(IpMessageConsts.string.ipmsg_logo));
        try {
            mContext.startActivity(Intent.createChooser(
                    intent,
                    IpMessageResourceMananger.getInstance(mContext).getSingleString(
                            IpMessageConsts.string.ipmsg_share_title)));
        } catch (Exception e) {
            Log.d(TAG, "shareIpMsg(): Exception:" + e.toString());
        }
    }

    private Intent setIntent(Intent intent, IpMessage ipMessage) {
        if (ipMessage.getType() == IpMessageType.TEXT) {
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, ((IpTextMessage) ipMessage).getBody());
        } else if (ipMessage.getType() == IpMessageType.PICTURE) {
            IpImageMessage msg = (IpImageMessage) ipMessage;
            int index = msg.getPath().lastIndexOf(".");
            String end = msg.getPath().substring(index);
            String dest = IpMessageUtils.getCachePath(mContext) + "temp" + end;
            IpMessageUtils.copy(msg.getPath(), dest);
            intent.setType("image/*");
            File f = new File(dest);
            Uri u = Uri.fromFile(f);
            intent.putExtra(Intent.EXTRA_STREAM, u);
            if (!TextUtils.isEmpty(msg.getCaption())) {
                intent.putExtra(SMS_BODY, msg.getCaption());
                intent.putExtra(Intent.EXTRA_TEXT, msg.getCaption());
            }
        } else if (ipMessage.getType() == IpMessageType.VOICE) {
            IpVoiceMessage msg = (IpVoiceMessage) ipMessage;
            int index = msg.getPath().lastIndexOf("/");
            String name = msg.getPath().substring(index);
            String dest = IpMessageUtils.getCachePath(mContext) + name;
            IpMessageUtils.copy(msg.getPath(), dest);
            intent.setType("audio/*");
            File f = new File(dest);
            Uri u = Uri.fromFile(f);
            intent.putExtra(Intent.EXTRA_STREAM, u);
            if (!TextUtils.isEmpty(msg.getCaption())) {
                intent.putExtra(SMS_BODY, msg.getCaption());
                intent.putExtra(Intent.EXTRA_TEXT, msg.getCaption());
            }
        } else if (ipMessage.getType() == IpMessageType.VCARD) {
            IpVCardMessage msg = (IpVCardMessage) ipMessage;
            int index = msg.getPath().lastIndexOf("/");
            String name = msg.getPath().substring(index);
            String dest = IpMessageUtils.getCachePath(mContext) + name;
            IpMessageUtils.copy(msg.getPath(), dest);
            File f = new File(dest);
            Uri u = Uri.fromFile(f);
            intent.setDataAndType(u, "text/x-vcard");
            intent.putExtra(Intent.EXTRA_STREAM, u);
        } else if (ipMessage.getType() == IpMessageType.VIDEO) {
            IpVideoMessage msg = (IpVideoMessage) ipMessage;
            int index = msg.getPath().lastIndexOf("/");
            String name = msg.getPath().substring(index);
            String dest = IpMessageUtils.getCachePath(mContext) + name;
            IpMessageUtils.copy(msg.getPath(), dest);
            intent.setType("video/*");
            File f = new File(dest);
            Uri u = Uri.fromFile(f);
            intent.putExtra(Intent.EXTRA_STREAM, u);
            if (!TextUtils.isEmpty(msg.getCaption())) {
                intent.putExtra(SMS_BODY, msg.getCaption());
                intent.putExtra(Intent.EXTRA_TEXT, msg.getCaption());
            }
        } else if (ipMessage.getType() == IpMessageType.CALENDAR) {
            IpVCalendarMessage msg = (IpVCalendarMessage) ipMessage;
            int index = msg.getPath().lastIndexOf("/");
            String name = msg.getPath().substring(index);
            String dest = IpMessageUtils.getCachePath(mContext) + name;
            IpMessageUtils.copy(msg.getPath(), dest);
            File f = new File(dest);
            Uri uri = Uri.fromFile(f);
            intent.setType("text/x-vcalendar");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
        } else {
            intent.setType("unknown");
        }
        return intent;
    }


    private void showResendConfirmDialg(final long currentMsgId, final long currentSubId,
            final long[][] allFailedIpMsgIds) {
        IpMessage ipMessage = RCSMessageManager.getInstance(mContext).getIpMsgInfo(currentMsgId);
        if (ipMessage == null) {
            Log.e(TAG, "showResendConfirmDialg(): ipMessage is null.");
            return;
        }
        // / M: add for fix ALPS01600871 @{
        if (!mIsSmsEnabled) {
            return;
        }
        // / @}
        String title = "";
        if (ipMessage.getStatus() == IpMessageStatus.FAILED) {
            title = IpMessageResourceMananger.getInstance(mContext).getSingleString(
                    IpMessageConsts.string.ipmsg_failed_title);
        } else {
            title = IpMessageResourceMananger.getInstance(mContext).getSingleString(
                    IpMessageConsts.string.ipmsg_not_delivered_title);
        }
        String sendViaMsg = "";
        if (ipMessage.getType() == IpMessageType.TEXT) {
            sendViaMsg = IpMessageResourceMananger.getInstance(mContext)
                    .getSingleString(IpMessageConsts.string.ipmsg_resend_via_sms);
        } else {
            sendViaMsg = IpMessageResourceMananger.getInstance(mContext)
                    .getSingleString(IpMessageConsts.string.ipmsg_resend_via_mms);
        }
        List<String> buttonList = new ArrayList<String>();
        buttonList.add(IpMessageResourceMananger.getInstance(mContext)
                .getSingleString(IpMessageConsts.string.ipmsg_try_again));
        if (allFailedIpMsgIds != null && allFailedIpMsgIds.length > 1) {
            buttonList.add(IpMessageResourceMananger.getInstance(mContext)
                    .getSingleString(IpMessageConsts.string.ipmsg_try_all_again));
        }
        buttonList.add(sendViaMsg);
        final int buttonCount = buttonList.size();
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext).setTitle(title);
        ArrayAdapter<String> resendAdapter = new ArrayAdapter<String>(mContext,
                R.layout.resend_dialog_item, R.id.resend_item, buttonList);
        builder.setAdapter(resendAdapter, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final int tryAgain = 0;
                final int tryAllAgain = 1;
                final int sendViaMmsOrSms = 2;
                switch (which) {
                    case tryAgain:
                        RCSMessageManager.getInstance(mContext)
                                .resendMessage(currentMsgId, (int) currentSubId);
                        break;
                    case tryAllAgain:
                        if (buttonCount == 3) {
                            for (int index = 0; index < allFailedIpMsgIds.length; index++) {
                                RCSMessageManager.getInstance(mContext)
                                        .resendMessage(allFailedIpMsgIds[index][0],
                                                (int) allFailedIpMsgIds[index][1]);
                            }
                            break;
                        } else if (buttonCount != 2) {
                            break;
                        } else {
                            // / M: listSize == 2, run case sendViaMmsOrSms.
                            // fall through
                        }
                    case sendViaMmsOrSms:
                        if (mWorkingMessage.requiresIpMms()
                                || !TextUtils.isEmpty(mTextEditor.getText().toString())
                                || mIpMessageDraft != null) {
                            showDiscardCurrentMessageDialog(currentMsgId);
                        } else {
                            sendViaMmsOrSms(currentMsgId);
                        }
                        break;
                    default:
                        break;
                }
            }
        });
        builder.show();

    }
*/

    /*
    private void sendViaMmsOrSms(long currentMsgId) {
        mJustSendMsgViaCommonMsgThisTime = true;
        final IpMessage ipMessage = RCSMessageManager.getInstance(mContext)
                .getIpMsgInfo(currentMsgId);
        if (ipMessage != null) {
            mIpMessageDraft = ipMessage;
            if (convertIpMessageToMmsOrSms(false)) {
                mContext.runOnUiThread(new Runnable() {
                    public void run() {
                        if (mCallback.isIpSubjectEditorVisible()) {
                            mCallback.showIpSubjectEditor(false);
                            mWorkingMessage.setIpSubject(null, true);
                        }
                        mCallback.drawIpBottomPanel();
                        boolean isMms = mWorkingMessage.requiresIpMms();
                        mCallback.showIpOrMmsSendButton(isMms ? true : false);
                    }
                });
                RCSMessageManager.getInstance(mContext).deleteIpMsg(
                        new long[] {
                            currentMsgId
                        }, true);
            }
        }
    }
    
    private void showDiscardCurrentMessageDialog(final long currentMsgId) {
        new AlertDialog.Builder(mContext)
                .setTitle(R.string.discard)
                .setMessage(
                        IpMessageResourceMananger.getInstance(mContext)
                                .getSingleString(
                                        IpMessageConsts.string.ipmsg_resend_discard_message))
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton(
                        IpMessageResourceMananger.getInstance(mContext)
                                .getSingleString(IpMessageConsts.string.ipmsg_continue),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sendViaMmsOrSms(currentMsgId);
                            }
                        }).show();
    }


    private long[][] getAllFailedIpMsgByThreadId(long threadId) {
        Cursor cursor = mContext.getContentResolver().query(Sms.CONTENT_URI, new String[] {
                Sms._ID, Telephony.TextBasedSmsColumns.SUB_ID
        }, "thread_id = " + threadId + " and ipmsg_id > 0 and type = " + Sms.MESSAGE_TYPE_FAILED,
                null, null);
        try {
            if (cursor == null) {
                return null;
            }
            long[][] count = new long[cursor.getCount()][2];
            int index = 0;
            while (cursor.moveToNext()) {
                count[index][0] = cursor.getLong(0);
                count[index][1] = cursor.getLong(1);
                index++;
            }
            return count;
        } finally {
            cursor.close();
        }
    }

    private static void toastNoSubCard(Context context) {
        Toast.makeText(
                context,
                IpMessageResourceMananger.getInstance(context).getSingleString(
                        IpMessageConsts.string.ipmsg_no_sim_card), Toast.LENGTH_LONG).show();
    }

    private void doMoreActionForMms(Message msg) {
        int commonAttachmentType = 0;
        Bundle bundle = msg.getData();
        int action = bundle.getInt(SHARE_ACTION);
        switch (action) {
            case IPMSG_TAKE_PHOTO:
                commonAttachmentType = AttachmentTypeSelectorAdapter.TAKE_PICTURE;
                break;

            case IPMSG_RECORD_VIDEO:
                commonAttachmentType = AttachmentTypeSelectorAdapter.RECORD_VIDEO;
                break;

            case IPMSG_SHARE_CONTACT:
                commonAttachmentType = AttachmentTypeSelectorAdapter.ADD_VCARD;
                break;

            case IPMSG_CHOOSE_PHOTO:
                commonAttachmentType = AttachmentTypeSelectorAdapter.ADD_IMAGE;
                break;

            case IPMSG_CHOOSE_VIDEO:
                commonAttachmentType = AttachmentTypeSelectorAdapter.ADD_VIDEO;
                break;

            case IPMSG_RECORD_AUDIO:
                commonAttachmentType = AttachmentTypeSelectorAdapter.RECORD_SOUND;
                break;

            case IPMSG_CHOOSE_AUDIO:
                commonAttachmentType = AttachmentTypeSelectorAdapter.ADD_SOUND;
                break;

            case IPMSG_SHARE_CALENDAR:
                commonAttachmentType = AttachmentTypeSelectorAdapter.ADD_VCALENDAR;
                break;

            case IPMSG_SHARE_SLIDESHOW:
                commonAttachmentType = AttachmentTypeSelectorAdapter.ADD_SLIDESHOW;
                break;

            default:
                Log.e(TAG, "doMoreActionForMms(): invalid share action type: " + action);
                mCallback.hideIpSharePanel();
                return;
        }
        mCallback.addIpAttachment(commonAttachmentType);
        mCallback.hideIpSharePanel();
    }
*/
    private boolean doMoreAction(Message msg) {
        Bundle bundle = msg.getData();
        int action = bundle.getInt(SHARE_ACTION);
        boolean ret = true;
        // /M: If recipient is not a user-IPMessage,Send IP multi-media message
        // via MMS/SMS {@
        // / M: Fix ipmessage bug for ALPS01674002
//        if ((mWorkingMessage.requiresIpMms() || mCurrentChatMode == IpMessageConsts.ChatMode.XMS || !isCurrentIpmessageSendable())
//                && action != IPMSG_SHARE_FILE) {
//            doMoreActionForMms(msg);
//            return;
//        }
        // /@}
        boolean isNoRecipient = (getRecipientSize() == 0 && !mIsGroupChat);
        switch (action) {
            case IPMSG_TAKE_PHOTO:
                if (isNoRecipient) {
                    toastNoRecipients(mContext);
                } else {
                    takePhoto();
                }
                break;

            case IPMSG_RECORD_VIDEO:
                if (isNoRecipient) {
                    toastNoRecipients(mContext);
                } else {
                    recordVideo();
                }
                break;

            case IPMSG_SHARE_CONTACT:
                if (isNoRecipient) {
                    toastNoRecipients(mContext);
                } else {
                    shareContact();
                }
                break;

            case IPMSG_CHOOSE_PHOTO:
                if (isNoRecipient) {
                    toastNoRecipients(mContext);
                } else {
                    choosePhoto();
                }
                break;

            case IPMSG_CHOOSE_VIDEO:
                if (isNoRecipient) {
                    toastNoRecipients(mContext);
                } else {
                    chooseVideo();
                }
                break;

            case IPMSG_RECORD_AUDIO:
                if (isNoRecipient) {
                    toastNoRecipients(mContext);
                } else {
                    recordAudio();
                }
                break;

            case IPMSG_CHOOSE_AUDIO:
                if (isNoRecipient) {
                    toastNoRecipients(mContext);
                } else {
                    chooseAudio();
                }
                break;

            case IPMSG_SHARE_POSITION:
                if (isNoRecipient) {
                    toastNoRecipients(mContext);
                } else {
                    sharePosition();
                }
                break;
//            case IPMSG_SHARE_FILE:
//                Log.v(TAG, "doMoreAction(): select from file manager: " + action);
//                if (isNoRecipient) {
//                    toastNoRecipients(mContext);
//                    return;
//                }
//                startFileManager();
//                break;

            default:
                ret = false;
                Log.e(TAG, "doMoreAction(): invalid share action type: " + action);
                break;
        }
        if (ret) {
            mCallback.hideIpSharePanel();
        }
        return ret;
    }

    private class GeoLocCallback implements GeoLocService.callback {

        public void queryGeoLocResult(boolean ret, final Location location) {

            mContext.removeDialog(DIALOG_ID_GETLOC_PROGRESS);
            mGeoLocSrv.removeCallback();
            // if success, store location and show send location confirm dialog
            // if fail, show fail toast
            if (ret == true) {
                mLocation = location;
                mContext.showDialog(DIALOG_ID_GEOLOC_SEND_CONFRIM);
            } else {
                //mContext.showDialog(DIALOG_ID_GEOLOC_SEND_CONFRIM);
                Toast.makeText(mContext, mPluginContext.getString(R.string.geoloc_get_failed), Toast.LENGTH_SHORT).show();
            }
        }
        
    }

    public void sharePosition() {
        mGeoLocSrv = new GeoLocService(mContext);
        queryGeoLocation();

       // sendGeoLocation();
        
    }

    private void queryGeoLocation() {
        if (mGeoLocSrv.isEnable()) {
            mContext.showDialog(DIALOG_ID_GETLOC_PROGRESS);
        } else {
            Toast.makeText(mContext, mPluginContext.getString(R.string.geoloc_check_gps), Toast.LENGTH_SHORT).show();
        }
    }

    public Dialog onIpCreateDialog(int id) {
        Log.d(TAG, "onCreateDialog, id:" + id);
        switch (id) {
        case DIALOG_ID_GETLOC_PROGRESS:
            mGeoLocSrv.queryCurrentGeoLocation(new GeoLocCallback());

            ProgressDialog dialog = new ProgressDialog(mContext);
            dialog.setMessage(mPluginContext.getString(R.string.geoloc_being_get));
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface arg0) {
                    mGeoLocSrv.removeCallback();
                }
            });
            dialog.setCanceledOnTouchOutside(false);
            return dialog;
        
        case DIALOG_ID_GEOLOC_SEND_CONFRIM:
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setMessage(mPluginContext.getString(R.string.geoloc_send_confrim))
                   .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                       public void onClick(DialogInterface dialog, int id) {
                           sendGeoLocation();
                       }
                   })
                   .setNegativeButton(android.R.string.cancel, null);
            // Create the AlertDialog object
            AlertDialog dialog2 = builder.create();
            return dialog2;
        default:
            break;
        }
        return super.onIpCreateDialog(id);
    }

    private void sendGeoLocation() {
        String path = RcsMessageUtils.getGeolocPath(mContext);
        String fileName = GeoLocUtils.buildGeoLocXml(mLocation, "Sender Number", "Message Id", path);
        mDstPath = fileName;

        //mDstPath = RcsMessageConfig.getGeolocTempPath(mContext) + File.separator + "geoloc_1.xml";
        Log.e(TAG, "sendGeoLocation() , mDstPath = " + mDstPath);
        
        //mDstPath = path + "geoloc_1.xml";
       mIpMsgHandler.postDelayed(mSendGeoLocation, 100);
    }

    public void takePhoto() {
        Intent imageCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        /*
        String fileName = System.currentTimeMillis() + ".jpg";
        mPhotoFilePath = RcsMessageConfig.getPicTempPath(mContext) + File.separator + fileName;
        */
        mPhotoFilePath = RcsMessageUtils.getPhotoDstFilePath(mContext);
        mDstPath = mPhotoFilePath;
        File out = new File(mPhotoFilePath);
        Uri uri = Uri.fromFile(out);

        long sizeLimit = RcsMessageUtils.getPhotoSizeLimit();
        String resolutionLimit = RcsMessageUtils.getPhotoResolutionLimit();
        
        imageCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        imageCaptureIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

       // imageCaptureIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, sizeLimit); //limit image size 10MB
       // imageCaptureIntent.putExtra("mediatek.intent.extra.EXTRA_RESOLUTION_LIMIT", resolutionLimit); //limit image radio 4K
        
        try {
            mContext.startActivityForResult(imageCaptureIntent, REQUEST_CODE_IPMSG_TAKE_PHOTO);
        } catch (Exception e) {
            Toast.makeText(mContext,
                    mPluginContext.getString(R.string.ipmsg_no_app),
                    Toast.LENGTH_SHORT).show();
            Log.e(TAG, "takePhoto()");
        }
    }

    public void choosePhoto() {
        /*
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setType("image/*");
        mDstPath = mPhotoFilePath;
        try {
            mContext.startActivityForResult(intent, REQUEST_CODE_IPMSG_CHOOSE_PHOTO);
        } catch (Exception e) {
            Toast.makeText(mContext,
                    mPluginContext.getString(R.string.ipmsg_no_app),
                    Toast.LENGTH_SHORT).show();
            Log.e(TAG, "choosePhoto()");
        }*/

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);

        intent.setType("image/*");
        intent.putExtra("com.mediatek.gallery3d.extra.RCS_PICKER",true);
        mDstPath = mPhotoFilePath;
        try {
            mContext.startActivityForResult(intent, REQUEST_CODE_IPMSG_CHOOSE_PHOTO);
        } catch (Exception e) {
            Toast.makeText(mContext,
                    mPluginContext.getString(R.string.ipmsg_no_app),
                    Toast.LENGTH_SHORT).show();
            Log.e(TAG, "choosePhoto()");
        }
    }

    public void recordVideo() {
        int durationLimit = RcsMessageUtils.getVideoCaptureDurationLimit();
        String resolutionLimit = RcsMessageUtils.getVideoResolutionLimit();
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        mVideoFilePath = RcsMessageUtils.getVideoDstFilePath(mContext);
        mDstPath = mVideoFilePath;
        File out = new File(mVideoFilePath);
        Uri uri = Uri.fromFile(out);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
        //intent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, (long) 300 * 1024);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, durationLimit);

        intent.putExtra("mediatek.intent.extra.EXTRA_RESOLUTION_LIMIT", resolutionLimit);

        try {
            mContext.startActivityForResult(intent, REQUEST_CODE_IPMSG_RECORD_VIDEO);
        } catch (Exception e) {
            Toast.makeText(mContext,
                    mPluginContext.getString(R.string.ipmsg_no_app),
                    Toast.LENGTH_SHORT).show();
            Log.e(TAG, "recordVideo()");
        }
    }

    public void chooseVideo() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("video/*");
        try {
            mContext.startActivityForResult(intent, REQUEST_CODE_IPMSG_CHOOSE_VIDEO);
        } catch (Exception e) {
            Toast.makeText(mContext,
                    mPluginContext.getString(R.string.ipmsg_no_app),
                    Toast.LENGTH_SHORT).show();
            Log.e(TAG, "chooseVideo()");
        }
    }

    public void recordAudio() {
        Log.d(TAG, "recordAudio(), enter");
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("audio/amr");
        intent.setClassName("com.android.soundrecorder",
                    "com.android.soundrecorder.SoundRecorder");
        intent.putExtra("com.android.soundrecorder.maxduration",RcsMessageUtils.getAudioDurationLimit());    
        mContext.startActivityForResult(intent, REQUEST_CODE_IPMSG_RECORD_AUDIO);
    }

    public void shareContact() {
        /*
         AlertDialog.Builder b = new AlertDialog.Builder(mContext);
         b.setTitle(mPluginContext.getString(R.string.add_vcard));
         b.setCancelable(true);

         String[] items = new String[] {mPluginContext.getString(R.string.add_contacts),
                              mPluginContext.getString(R.string.add_groups)};

         b.setItems(items, new OnClickListener(){
            public void onClick(DialogInterface dialog, int which)  {
                switch (which) {
                    case 0:
                        addContacts();
                        break;
                    case 1:
                        addGroups();
                        break;
                    default:
                        break;
                }
            }  
         }); 

         b.create().show();    
         */
         addContacts();
    }

    private void addContacts() {
        Intent intent = new Intent("android.intent.action.contacts.list.PICKMULTICONTACTS");
        intent.setType(Contacts.CONTENT_TYPE);  
        mContext.startActivityForResult(intent, REQUEST_CODE_IPMSG_SHARE_CONTACT);
    }

    private void addGroups() {
        Intent intent = new Intent("android.intent.action.rcs.contacts.GroupListActivity");
        mContext.startActivityForResult(intent, REQUEST_CODE_IPMSG_SHARE_CONTACT);
    }

    public void chooseAudio() {
        /*
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(mContext);
        alertBuilder.setTitle(mContext.getString(R.string.add_music));
        String[] items = new String[2];
        items[0] = mContext.getString(R.string.attach_ringtone);
        items[1] = mContext.getString(R.string.attach_sound);
        alertBuilder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
//                        IpMessageUtils.selectRingtone(mContext,
//                                REQUEST_CODE_IPMSG_CHOOSE_AUDIO);
                        break;
                    case 1:
//                        if (!Environment.getExternalStorageState()
//                                .equals(Environment.MEDIA_MOUNTED)) {
//                            Toast.makeText(mContext.getApplicationContext(),
//                                    mContext.getString(R.string.Insert_sdcard), Toast.LENGTH_LONG).show();
//                            return;
//                        }
//                        IpMessageUtils.selectAudio(mContext,
//                                REQUEST_CODE_IPMSG_CHOOSE_AUDIO);
                        break;
                    default:
                        break;
                }
            }
        });
        alertBuilder.create().show();
        */

        //Not support ringtone
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType(ContentType.AUDIO_UNSPECIFIED);
        String[] mimeTypess = new String[] {ContentType.AUDIO_UNSPECIFIED,
                ContentType.AUDIO_MP3, ContentType.AUDIO_3GPP,"audio/M4A",
                ContentType.AUDIO_AAC,ContentType.AUDIO_AMR};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypess);
      
        /// @}
        mContext.startActivityForResult(intent, REQUEST_CODE_IPMSG_CHOOSE_AUDIO);
    }

    public void shareCalendar() {
        Intent intent = new Intent("android.intent.action.CALENDARCHOICE");
        intent.setType("text/x-vcalendar");
        intent.putExtra("request_type", 0);
        try {
            mContext.startActivityForResult(intent, REQUEST_CODE_IPMSG_SHARE_VCALENDAR);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(mContext,
                    mPluginContext.getString(R.string.ipmsg_no_app),
                    Toast.LENGTH_SHORT).show();
            Log.e(TAG, "shareCalendar()");
        }
    }
/*
    private Runnable mHideTypingStatusRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                    "notificationsReceived(): hide typing status.");
            if (null != mTypingStatus) {
                mTypingStatus.setVisibility(View.GONE);
            }
        }
    };

    private Runnable mShowTypingStatusRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                    "notificationsReceived(): show IM status view.");
            if (null != mTypingStatus) {
                String text = "";
                if (mChatSenderName != null && !TextUtils.isEmpty(mChatSenderName)) {
                    text = mChatSenderName
                            + " "
                            + IpMessageResourceMananger.getInstance(mContext)
                                    .getSingleString(IpMessageConsts.string.ipmsg_typing_text);
                } else {
                    Log.d(IpMessageUtils.IPMSG_NOTIFICATION_TAG,
                            "mChatSenderName is null or empty");
                    return;
                }
                mTypingStatus.setText(text);
                mTypingStatus.setVisibility(View.VISIBLE);
            }
        }
    };

    // / M: Add Jump to another chat in converaged inbox mode. @{
    private void jumpToJoynChat(boolean isToJoynChat, long ipMessageId) {
        String number;
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SENDTO);
        Log.d(TAG, "jumpToJoynChat ipMessage = " + ipMessageId);
        IpMessage ipMessage = RCSMessageManager.getInstance(mContext).getIpMsgInfo(ipMessageId);
        if (isToJoynChat) {
            number = IpMessageConsts.JOYN_START + mChatModeNumber;
            intent.putExtra("chatmode", IpMessageConsts.ChatMode.JOYN);
        } else {
            number = mChatModeNumber.substring(4);
            intent.putExtra("chatmode", IpMessageConsts.ChatMode.XMS);
        }

        Uri uri = Uri.parse("smsto: " + number);
        intent.setData(uri);

        if (ipMessage != null) {
            if (ipMessage.getType() == IpMessageType.TEXT) {
                intent.putExtra("sms_body", ((IpTextMessage) ipMessage).getBody());
            } else if (ipMessage.getType() >= IpMessageType.PICTURE
                    && ipMessage.getType() <= IpMessageType.UNKNOWN_FILE) {
                intent.setAction(Intent.ACTION_SEND);
                intent.setComponent(new ComponentName("com.android.mms",
                        "com.android.mms.ui.ComposeMessageActivity"));
                intent = setIntent(intent, ipMessage);
            }
        }
        mContext.startActivity(intent);
    }
*/


    private void asyncIpAttachVCardByContactsId(final Intent data) {
        if (data == null) {
            return;
        }
       // mCallback.runIpAsyncInThreadPool(new Runnable() {
       //     public void run() {
                long[] contactsId = data.getLongArrayExtra("com.mediatek.contacts.list.pickcontactsresult");
                RCSVCardAttachment va = new RCSVCardAttachment(mPluginContext);
                int result = OK;
                mIpMessageVcardName = va.getVCardFileNameByContactsId(contactsId, true);
                mDstPath = RcsMessageUtils.getCachePath(mContext)
                        + mIpMessageVcardName;
                Log.d(TAG, "asyncIpAttachVCardByContactsId(): mIpMessageVcardName = "
                        + mIpMessageVcardName + ", mDstPath = " + mDstPath);
                mIpMsgHandler.postDelayed(mSendVcard, 100);
       //     }
       // }, null, 0);
    }

    private void toastNoRecipients(Context context) {
        Toast.makeText(
                context,
                mPluginContext.getString(R.string.ipmsg_need_input_recipients),
                Toast.LENGTH_SHORT).show();
    }

    public static Intent createIntent(Context context, long threadId) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.android.mms",
                "com.android.mms.ui.ComposeMessageActivity"));
        if (threadId > 0) {
            intent.setData(getUri(threadId));
        }
        return intent;
    }

    public static Uri getUri(long threadId) {
        return ContentUris.withAppendedId(Threads.CONTENT_URI, threadId);
    }

    public static long getAvailableBytesInFileSystemAtGivenRoot(String rootFilePath) {
        StatFs stat = new StatFs(rootFilePath);
        // final long totalBlocks = stat.getBlockCount();
        // put a bit of margin (in case creating the file grows the system by a
        // few blocks)
        final long availableBlocks = stat.getAvailableBlocks() - 128;

        // long mTotalSize = totalBlocks * stat.getBlockSize();
        long mAvailSize = availableBlocks * stat.getBlockSize();

        Log.i(TAG, "getAvailableBytesInFileSystemAtGivenRoot(): "
                + "available space (in bytes) in filesystem rooted at: " + rootFilePath + " is: "
                + mAvailSize);
        return mAvailSize;
    }

    private boolean isRecipientsEditorVisible() {
        return mCallback.isIpRecipientEditorVisible();
    }
    
    private List<String> getRecipientsList() {
        List<String> list;
        if (isRecipientsEditorVisible()) {
            list = mCallback.getRecipientsEditorInfoList();
        } else {
            String[] numbers = mCallback.getConversationInfo();
            list = new ArrayList<String>(numbers.length);
            for (String number : numbers) {
                list.add(number);
            }
        }
        return list;
    }

    private String formatRecipientsStr(List<String> list) {
        StringBuffer builder = new StringBuffer();
        for (String number : list) {
            if (!TextUtils.isEmpty(number)) {
                builder.append(number);
                builder.append(",");
            }
        }
        String recipientStr = builder.toString();
        if (recipientStr.endsWith(",")) {
            recipientStr = recipientStr.substring(0, recipientStr.lastIndexOf(","));
        }
        return recipientStr;
    }

    private String getRecipientStr() {
        String ret = "";
        List<String> list = getRecipientsList();
        if (list != null && list.size() > 0) {
            ret = formatRecipientsStr(list);
        }
        Log.d(TAG, "getRecipientStr: " +ret);
        return ret;
    }

    private String getNumber() {
        List<String> list = getRecipientsList();
        if (list != null && list.size() > 0) {
            return list.get(0);
        }
        return "";
    }

    private int getRecipientSize() {
        int ret = 0;
        List<String> list = getRecipientsList();
        if (list != null) {
            ret = list.size();
        }
        Log.d(TAG, "getRecipientSize: " +ret);
        return ret;
    }
 
    private void hideInputMethod() {
        Log.d(TAG, "hideInputMethod()");
        if (mContext.getWindow() != null && mContext.getWindow().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) mContext
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(mContext.getWindow().getCurrentFocus()
                    .getWindowToken(), 0);
        }
    }
    private int canConvertIpMessageToMessage(IpMessage ipMessage) {
        if (ipMessage == null) {
            return MESSAGETYPE_UNSUPPORT;
        }
        switch (ipMessage.getType()) {
            case IpMessageType.TEXT:
                return MESSAGETYPE_TEXT;
//            case IpMessageType.PICTURE:
//            case IpMessageType.VOICE:
//            case IpMessageType.VCARD:
//            case IpMessageType.VIDEO:
//                return MESSAGETYPE_MMS;
//            case IpMessageType.UNKNOWN_FILE:
//            case IpMessageType.COUNT:
//            case IpMessageType.GROUP_CREATE_CFG:
//            case IpMessageType.GROUP_ADD_CFG:
//            case IpMessageType.GROUP_QUIT_CFG:
//                return MESSAGETYPE_UNSUPPORT;
            default:
                return MESSAGETYPE_UNSUPPORT;
        }
    }
    
    private void getContactCapbility() {
        if(1 == getRecipientSize()) {
            RCSServiceManager.getInstance().registBurnMsgCapListener(this);
            Log.d(TAG, "onRequestBurnMessageCapabilityResult getNumber() = " + getNumber());
            RCSServiceManager.getInstance().getBurnMsgCap(getNumber());
        }
    }
    
    public void onIpRecipientsChipChanged(int number) {
        if (number > 0) {
            mCallback.enableShareButton(true);
        } else {
            mCallback.enableShareButton(false);
        }
    }
    
    @Override
    public boolean handleIpMessage(Message msg) {
        Log.d(TAG, "mIpMsgHandler handleMessage, msg.what: " + msg.what);
        boolean ret = false;
        
        switch (msg.what) {
            case ACTION_SHARE:
                if (RcsMessageConfig.isServiceEnabled(mContext)
//                        && isNetworkConnected(mContext)
//                        && IpMessageUtils.getSDCardStatus()
                        ) {
                    ret = doMoreAction(msg);
                }
                break;
            default:
                Log.d(TAG, "msg type: " + msg.what + "not handler");
                break;
        }
        return ret;
    }
    
    private void copyFile(IpAttachMessage ipAttachMessage) {
        Log.d(TAG, "copyFile type = " + ipAttachMessage.getType());
        String source = ipAttachMessage.getPath();
        if (TextUtils.isEmpty(source)) {
            Log.e(TAG, "saveMsgInSDCard(): save ipattachmessage failed, source empty!");
            Toast.makeText(mPluginContext, R.string.copy_to_sdcard_fail, Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        File inFile = new File(source);
        if (!inFile.exists()) {
            Log.e(TAG, "saveMsgInSDCard(): save ipattachmessage failed, source file not exist!");
            return;
        }
        String attName = source.substring(source.lastIndexOf("/") + 1);
        File dstFile = RcsMessageUtils.getStorageFile(attName);
        if (dstFile == null) {
            Log.i(TAG, "saveMsgInSDCard(): save ipattachmessage failed, dstFile not exist!");
            return;
        }
        RcsMessageUtils.copy(inFile, dstFile);
        Toast.makeText(mPluginContext, R.string.copy_to_sdcard_success,
            Toast.LENGTH_SHORT).show();
        // Notify other applications listening to scanner events
        // that a media file has been added to the sd card
        mPluginContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(dstFile)));
    }

    private boolean setSmsFavorite(Context mContext, long id, String mBody, String mAddress) {
        Log.d(TAG, "setSmsFavorite id =" + id + ",mBody = " + mBody + ",mAddress ="+ mAddress);
        ContentValues values = new ContentValues();
        values.put("favoriteid", id);
        values.put("type", 1);//sms
        values.put("address", mAddress);
        values.put("body", mBody);
        values.put("date", System.currentTimeMillis());
        mContext.getContentResolver().insert(FavoriteMsgProvider.CONTENT_URI, values);
        Toast.makeText(
                mPluginContext,
                R.string.toast_favorite_success,
                Toast.LENGTH_SHORT).show();
        return true;
    }
    
    private boolean setMmsFavorite(Context mContext, long id, String mSubject, String mAddress, int mBoxId, int type) {
        Log.d(TAG, "setMmsFavorite id =" + id + ",mSubject = " + mSubject + ",mAddress ="+ mAddress + 
            ",mBoxId = "+ mBoxId);
        byte[] pduMid;
        String FILE_NAME_PDU = mPluginContext.getDir("favorite", 0).getPath() + "/pdu";
        File path = new File(FILE_NAME_PDU);
        Log.d(TAG, "thiss FILE_NAME_PDU =" + FILE_NAME_PDU);
        if (!path.exists()) {
            path.mkdirs();
        }
        try {
            Log.d(TAG, "thiss cache id =" + id );
            Log.d(TAG, "thiss time =" + System.currentTimeMillis());
            Uri realUri = ContentUris.withAppendedId(Mms.CONTENT_URI, id);
            Log.d(TAG, "thiss realUri =" + realUri);
            PduPersister p = PduPersister.getPduPersister(mContext);
            Log.d(TAG, "thiss mBoxId =" + mBoxId);
            if (mBoxId == Mms.MESSAGE_BOX_INBOX) {
                if (type == MESSAGE_TYPE_NOTIFICATION_IND) {
//                    NotificationInd nPdu = (NotificationInd) p.load(realUri);
//                    pduMid = new PduComposer(mContext, nPdu).make(true);
                    pduMid = null;
                } else if (type == MESSAGE_TYPE_RETRIEVE_CONF) {
                    RetrieveConf rPdu = (RetrieveConf) p.load(realUri, false);
                    pduMid = new PduComposer(mContext, rPdu).make(false);
                } else {
                    pduMid = null;
                }
            } else {
                SendReq sPdu = (SendReq) p.load(realUri);
                pduMid = new PduComposer(mContext, sPdu).make();
                Log.d(TAG, "thiss SendReq pduMid =" + pduMid);
            }
            String mFile = FILE_NAME_PDU + "/" + System.currentTimeMillis() + ".pdu";
            if (pduMid != null) {
                byte[] pduByteArray = pduMid;
                Log.d(TAG, "thiss fileName =" + mFile);
                writeToFile(mFile, pduByteArray);
            }
            if (pduMid != null) {
                ContentValues values = new ContentValues();
                values.put("favoriteid", id);
                values.put("path", mFile);
                values.put("type", 2);//mms
                if (mBoxId != Mms.MESSAGE_BOX_INBOX) {
                    mAddress = null;
                }
                values.put("address", mAddress);
                if (!TextUtils.isEmpty(mSubject)) {
                    values.put("body", mSubject);
                }
                values.put("date", System.currentTimeMillis());
                mContext.getContentResolver().insert(FavoriteMsgProvider.CONTENT_URI, values);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(
                mPluginContext,
                R.string.toast_favorite_success,
                Toast.LENGTH_SHORT).show();
        return true;
    }
    
    private boolean setTextIpmessageFavorite(long id, int ct, String mBody, String mAddress) {
        Log.d(TAG, "setTextIpmessageFavorite id =" + id + ",mBody = " + mBody + ",mAddress ="+ mAddress);
        ContentValues values = new ContentValues();
        values.put("favoriteid", id);
        values.put("type", 3);//Ipmessage
        values.put("address", mAddress);
        values.put("body", mBody);
        values.put("ct", Integer.toString(ct));
        values.put("date", System.currentTimeMillis());
        if (mGroupChatId != null) {
            values.put("chatid", mGroupChatId);
        }
        mContext.getContentResolver().insert(FavoriteMsgProvider.CONTENT_URI, values);
        Toast.makeText(
                mPluginContext,
                R.string.toast_favorite_success,
                Toast.LENGTH_SHORT).show();
        return true;
    }
    
    private boolean setAttachIpmessageFavorite(long id, int ct, String mPath, String mAddress) {
        Log.d(TAG, "setAttachIpmessageFavorite id =" + id + ",mPath = " + mPath + ",mAddress ="+ mAddress);
        ContentValues values = new ContentValues();
        values.put("favoriteid", id);
        values.put("type", 3);//Ipmessage
        values.put("address", mAddress);
        values.put("ct", Integer.toString(ct));
        values.put("date", System.currentTimeMillis());
        if (mGroupChatId != null) {
            values.put("chatid", mGroupChatId);
        }
        if (mPath != null) {
            String FILE_NAME_IM = mPluginContext.getDir("favorite", 0).getPath() + "/Ipmessage";
            File path = new File(FILE_NAME_IM);
            Log.d(TAG, "thiss FILE_NAME_IM =" + FILE_NAME_IM);
            if (!path.exists()) {
                path.mkdirs();
            }
            String mFileName = FILE_NAME_IM + "/" + getFileName(mPath);
            String mNewpath = RcsMessageUtils.getUniqueFileName(mFileName);
            Log.d(TAG, "thiss mNewpath =" + mNewpath);
            RcsMessageUtils.copy(mPath,mNewpath);
            values.put("path", mNewpath);
        }
        mContext.getContentResolver().insert(FavoriteMsgProvider.CONTENT_URI, values);
        Toast.makeText(
                mPluginContext,
                R.string.toast_favorite_success,
                Toast.LENGTH_SHORT).show();
        return true;
    }

    private String getFileName(String mFile) {
        return mFile.substring(mFile.lastIndexOf("/") + 1);
    }
    
    private void writeToFile(String fileName, byte[] buf) {
        try {
            FileOutputStream outStream = new FileOutputStream(fileName);
            // byte[] buf = inBuf.getBytes();
            outStream.write(buf, 0, buf.length);
            outStream.flush();
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public boolean onIpKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown(): keyCode = " + keyCode);
        switch (keyCode) {
            case KeyEvent.KEYCODE_DEL:
                break;

            case KeyEvent.KEYCODE_BACK:
                if (mEmoji != null && mEmoji.isEmojiPanelShow()) {
                    mEmoji.showEmojiPanel(false);
                    return true;
                }
                break;
        }
        return false;
    }


    @Override
    public boolean onIPQueryMsgList(AsyncQueryHandler mQueryHandler, int token, Object cookie, Uri uri, String[] 
            projection, String selection, String[] selectionArgs, String orderBy) {
        if (!mIsGroupChat) {
//            return false;
            mQueryHandler.startQuery(token, cookie, uri, projection, selection, selectionArgs, orderBy);
        } else {
            uri = ContentUris.withAppendedId(Uri.parse("content://mms-sms/rcs/conversations"), mThreadId);
            mQueryHandler.startQuery(token, cookie, uri, projection, selection, selectionArgs, /*orderBy*/ null);
        }
        setChatType();
        return true;
    }

    public void onIpConfig(Configuration newConfig) {
        Log.d(TAG, "onIpConfig()");
        if (mEmoji != null && mEmoji.isEmojiPanelShow()) {
            mEmoji.resetEmojiPanel(newConfig);
        }
    }
    
    private Dialog createInvitationDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setTitle(mPluginContext.getString(R.string.group_chat))
        .setCancelable(false)
        .setIconAttribute(android.R.attr.alertDialogIcon)
        .setMessage(mPluginContext.getString(R.string.group_invitation_indicater))
        .setPositiveButton(mPluginContext.getString(R.string.group_accept), new OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                // accept invitation
                MapInfo info = ThreadMapCache.getInstance().getInfoByChatId(mGroupChatId);
                if (info != null && info.getStatus() == IpMessageConsts.GroupActionList.GROUP_STATUS_INVITE_EXPAIRED) {
                    showInvitaionTimeOutDialog();
                } else {
                    try {
                        boolean ret = GroupManager.getInstance(mContext).acceptGroupInvitation(mGroupChatId);
                        if (ret == false) {
                            Toast.makeText(mContext, R.string.group_aborted, Toast.LENGTH_SHORT).show();
                            mContext.finish();
                        } else {
//                            setChatActive(true);
                            showProgressDialog(mPluginContext.getString(R.string.pref_please_wait));
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "e: " + e);
                        
                    }
                }
            }
        })
        .setNegativeButton(mPluginContext.getString(R.string.group_reject), new OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                // reject invitation
                GroupManager.getInstance(mContext).rejectGroupInvitation(mGroupChatId);
                if (mGroupStatus == IpMessageConsts.GroupActionList.GROUP_STATUS_INVITING ||
                        mRcsMessageListAdapter == null ||
                        mRcsMessageListAdapter.isOnlyHasSystemMsg()) {
                    HashSet<Long> set = new HashSet<Long>();
                    set.add(mThreadId);
                    RCSMessageManager.getInstance(mContext).deleteRCSThreads(set, 0, true);
                    mContext.finish();
                } else if (mGroupStatus == IpMessageConsts.GroupActionList.GROUP_STATUS_INVITING_AGAIN) {
                    setChatActive(false);
                    ContentValues values = new ContentValues();
                    values.put(Threads.STATUS, IpMessageConsts.GroupActionList.GROUP_STATUS_INVALID);
                    Uri uri = ContentUris.withAppendedId(RCSUtils.URI_THREADS_UPDATE_STATUS, mThreadId);
                    try {
                        mContext.getContentResolver().update(uri, values, null, null);
                    } catch (Exception e) {
                        // TODO: handle exception
                        Log.e(TAG, "[showInvitaionTimeOutDialog]: e = " + e);
                    }
                }
            }
        })
        .setNeutralButton(mPluginContext.getString(R.string.group_shelve), new OnClickListener() {
            
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                // don't process now. do noting but exit this screen
                mContext.finish();
            }
        });
        Dialog dialog = dialogBuilder.create();
        return dialog;
    }
    
    private void showInvitaionDialog(final int status) {
        mGroupStatus = status;
        if (mInvitationDialog == null) {
            mInvitationDialog = createInvitationDialog();
        }
        if (!mInvitationDialog.isShowing()) {
            mInvitationDialog.show();
        }
    }
    private void dismissInvitationDialog() {
        if (mInvitationDialog != null && mInvitationDialog.isShowing()) {
            mInvitationDialog.dismiss();
        }
    }

    private Dialog createInvitationTimeOutDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setTitle(mPluginContext.getString(R.string.group_chat))
        .setCancelable(false)
        .setIconAttribute(android.R.attr.alertDialogIcon)
        .setMessage(mPluginContext.getString(R.string.group_invitation_expired))
        .setPositiveButton(android.R.string.ok, new OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                if (mRcsMessageListAdapter == null || mRcsMessageListAdapter.isOnlyHasSystemMsg()) {
                    HashSet<Long> set = new HashSet<Long>();
                    set.add(mThreadId);
                    RCSMessageManager.getInstance(mContext).deleteRCSThreads(set, 0, true);
                    mContext.finish();
                } else {
                    ContentValues values = new ContentValues();
                    values.put(Threads.STATUS, IpMessageConsts.GroupActionList.GROUP_STATUS_INVALID);
                    Uri uri = ContentUris.withAppendedId(RCSUtils.URI_THREADS_UPDATE_STATUS, mThreadId);
                    try {
                        mContext.getContentResolver().update(uri, values, null, null);
                    } catch (Exception e) {
                        // TODO: handle exception
                        Log.e(TAG, "[showInvitaionTimeOutDialog]: e = " + e);
                    }
                    setChatActive(false);
                }
            }
        });
        Dialog dialog = dialogBuilder.create();
        return dialog;
    }

    private void showInvitaionTimeOutDialog() {
        if (mInvitationExpiredDialog == null) {
            mInvitationExpiredDialog = createInvitationTimeOutDialog();
        }
        if (!mInvitationExpiredDialog.isShowing()) {
            mInvitationExpiredDialog.show();
        }
    }

    private void dismissInvitationTimeOutDialog() {
        if (mInvitationExpiredDialog != null &&mInvitationExpiredDialog.isShowing()) {
            mInvitationExpiredDialog.dismiss();
        }
    }


    @Override
    public boolean onIpCheckRecipientsCount() {
        if (mIsGroupChat) {
            mCallback.callbackCheckConditionsAndSendMessage(true);
            return true;
            
        } else {
            return super.onIpCheckRecipientsCount();
        }
    }
    
    public boolean isPreparedForSending() {
        return false;
    }
    
    public boolean showIpMessageDetails(IIpMessageItem msgItem) {
        if (msgItem == null) {
            return false;
        }
        RcsMessageItem rcsMsgItem = (RcsMessageItem) msgItem;
        if (rcsMsgItem.mIpMessageId != 0) {
            Resources res = mPluginContext.getResources();
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle(res.getString(R.string.message_details_title))
            .setMessage(RcsMessageListItem.getIpTextMessageDetails(mPluginContext, rcsMsgItem))
            .setCancelable(true)
            .show();
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isIpRecipientCallable(String[] numbers) {
        if (mIsGroupChat) {
            if (mIsChatActive && numbers.length > 0) {
                return true;
            }
        } else if(numbers.length > 0){
            return true;
        }
        return super.isIpRecipientCallable(numbers);
    }

    public boolean isMms() {
        boolean ret = false;
        if (mWorkingMessage != null) {
            ret = mWorkingMessage.requiresIpMms();
        }
        return ret;
    }
    
    public boolean isGroupChat() {
        return mIsGroupChat;
    }
    
    public void invalidateOptionMenu() {
        if (mCallback != null) {
            mCallback.invalidateIpOptionsMenu();
        }
    }

    private void setChatType() {
        if (mRcsMessageListAdapter == null) {
            return;
        }
        if(mIsGroupChat) {
            mRcsMessageListAdapter.setChatType(mRcsMessageListAdapter.CHAT_TYPE_GROUP);
        } else if (getRecipientSize() == 1) {
            mRcsMessageListAdapter.setChatType(mRcsMessageListAdapter.CHAT_TYPE_ONE2ONE);
        } else if (getRecipientSize() > 1) {
            mRcsMessageListAdapter.setChatType(mRcsMessageListAdapter.CHAT_TYPE_ONE2MULTI);
        } else {
            Log.d(TAG, "setChatType(): unknown chat type");
        }
    }

    private void setChatActive(final boolean active) {
        mIsChatActive = active;
        mContext.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                showEditZone(active);
                if (mRcsMessageListAdapter != null) {
                    mRcsMessageListAdapter.setChatActive(active);
                }
                invalidateOptionMenu();
                if (!active) {
                    mCallback.hideIpInputMethod();
                }
            }
        });
        
    }

    private void showEditZone(boolean show) {
        int visible = show ? View.VISIBLE : View.GONE;
        if (mEditZone != null) {
            mEditZone.setVisibility(visible);
        }
    }

    private int getLastSendMode() {
        SharedPreferences preferece = mContext.getSharedPreferences(
                PREFERENCE_SEND_WAY_CHANGED_NAME,Context.MODE_WORLD_READABLE);
        return preferece.getInt(PREFERENCE_KEY_SEND_WAY, PREFERENCE_VALUE_SEND_WAY_UNKNOWN);
    }
    
    private boolean isNeedShowSendWayChangedDialog(final boolean canUseIM, final int subId) {
        SharedPreferences preferece = mContext.getSharedPreferences(
                PREFERENCE_SEND_WAY_CHANGED_NAME,Context.MODE_WORLD_READABLE);
        boolean mode = RcsSettingsActivity.getSendMSGMode(mPluginContext);
        if (!mode) {
            return false;
        }
        int lastSendWay = preferece.getInt(PREFERENCE_KEY_SEND_WAY, PREFERENCE_VALUE_SEND_WAY_UNKNOWN);
        int nowSendWay = canUseIM ? PREFERENCE_VALUE_SEND_WAY_IM : PREFERENCE_VALUE_SEND_WAY_SMS;
        if (lastSendWay == nowSendWay) {
            return true;
        } else {
            return false;
        }
    }

    void showSendWayChanged(boolean useIM, int subId) {
        DialogFragment fragment = new DialogFragment() {
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                final boolean isIM = getArguments().getBoolean("useIM");
                View contents = View.inflate(mPluginContext, R.layout.send_way_changed_dialog_view, null);
                TextView msg = (TextView)contents.findViewById(R.id.message);
                final CheckBox rememberCheckBox = (CheckBox)contents.findViewById(R.id.remember_choice);
                if (isIM) {
                    msg.setText(mPluginContext.getString(R.string.send_way_use_im_indicate));
                } else {
                    msg.setText(mPluginContext.getString(R.string.send_way_use_sms_indicate));
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                return builder.setTitle(mPluginContext.getString(R.string.send_way_changed))
                    .setIconAttribute(android.R.attr.alertDialogIcon)
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok,  new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            boolean checked = rememberCheckBox.isChecked();
                            SharedPreferences preferece = mContext.getSharedPreferences(
                                    PREFERENCE_SEND_WAY_CHANGED_NAME,Context.MODE_WORLD_WRITEABLE);
                            Editor editor = preferece.edit();
                            String pName = isIM ? PREFERENCE_KEY_SEND_WAY_IM : PREFERENCE_KEY_SEND_WAY_SMS;
                            if (isIM) {
                                editor.putInt(PREFERENCE_KEY_SEND_WAY, PREFERENCE_VALUE_SEND_WAY_IM);
                                editor.putBoolean(PREFERENCE_KEY_SEND_WAY_IM, checked);
                                //TODO use IM send
                            } else {
                                editor.putInt(PREFERENCE_KEY_SEND_WAY, PREFERENCE_VALUE_SEND_WAY_SMS);
                                editor.putBoolean(PREFERENCE_KEY_SEND_WAY_SMS, checked);
                                //TODO use sms send
                            }
                            editor.apply();
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,  new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            boolean checked = rememberCheckBox.isChecked();
                            SharedPreferences preferece = mContext.getSharedPreferences(
                                    PREFERENCE_SEND_WAY_CHANGED_NAME,Context.MODE_WORLD_WRITEABLE);
                            Editor editor = preferece.edit();
                            if (isIM) {
                                //TODO use sms send?
                            } else {
                                //TODO not to send 
                            }
//                            String pName = isIM ? PREFERENCE_SEND_WAY_IM : PREFERENCE_SEND_WAY_SMS;
//                            editor.putBoolean(pName, checked);
                        }
                    })
                    .setView(contents)
                    .create();
            }
        };
        Bundle args = new Bundle();
        args.putBoolean("useIM", useIM);
        fragment.show(mContext.getFragmentManager(), "showSendWayChanged");
    }

    void initDualSimState() {
        if (!RcsMessageUtils.getConfigStatus()) {
            return;
        }
        List<SubscriptionInfo> mSubInfoList =  SubscriptionManager.from(mContext).getActiveSubscriptionInfoList();
        int mSubCount = (mSubInfoList == null ||mSubInfoList.isEmpty()) ? 0 : mSubInfoList.size();
        int subIdinSetting = SubscriptionManager.getDefaultSmsSubId();
        int mainCardSubId = SubscriptionManager.getDefaultDataSubId();
        Log.d(TAG, "initDualSimState: mSubCount=" + mSubCount + ", subIdinSetting=" + subIdinSetting
            + ", mainCardSubId=" + mainCardSubId);
        int subId = -1;
        if (mSubCount > 1) {
            if (subIdinSetting == (int)Settings.System.SMS_SIM_SETTING_AUTO) {
                subId = mRcsMessageListAdapter.getAutoSelectSubId();
                // the SIM card has been removed
                if (subId == -1) {
                    subId = getIpAutoSelectSubId();
                }
            } else {
                // SIM1 or SIM2
                subId = subIdinSetting;
            }
            int capatibily = 0;
            if (SubscriptionManager.isValidSubscriptionId(mainCardSubId) && subId == mainCardSubId) {
                capatibily = RcsMessageUtils.getMainCardSendCapability();
            } else {
                // if not main card, can send sms or mms
                mCallback.enableShareButton(true);
                return;
            }
            
            Log.d(TAG, "initDualSimState: capatibily=" + capatibily);
            // if not support mms, disable share button
            if (capatibily == 1) {
                mCallback.enableShareButton(false);
            } else {
                mCallback.enableShareButton(true);
            }
        }
    }

    void showSelectContactNumberDialog(ArrayList<Participant> participants) {
//        String[] contacts = new String[] {"111111", "222222", "33333"};
        boolean[] checkedItems = new boolean[participants.size()];
        
        DialogFragment fragment = new DialogFragment() {
            int mCheckedNumber = 0;
            final int MAX_NUMBER = 5;
            AlertDialog mDialog;
            @Override
            public Dialog onCreateDialog(Bundle savedInstanceState) {
                Bundle arguments = getArguments();
                final ArrayList<Participant> participants = arguments.getParcelableArrayList("participants");
                final String[] sourceArray = new String[participants.size()];
                final boolean[] checkedArray = arguments.getBooleanArray("checklist");
                int index = 0;
                for (Participant p : participants) {
                    String content = p.getDisplayName();
                    if (TextUtils.isEmpty(content)) {
                        content = p.getContact();
                    }
                    sourceArray[index] = content;
                    index ++;
                }
                int totalNumber = checkedArray.length;
                for (int checkedIndex = 0; checkedIndex < totalNumber; checkedIndex ++) {
                    if (checkedArray[checkedIndex]) {
                        mCheckedNumber ++;
                    }
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                
                builder.setMultiChoiceItems(sourceArray, checkedArray, new OnMultiChoiceClickListener() {
                    
                    @Override
                    public void onClick(DialogInterface arg0, int posion, boolean checked) {
                        // TODO Auto-generated method stub
                        Log.d(TAG, "setMultiChoiceItems: " + posion + ", " + checked);
                        if (checked) {
                            mCheckedNumber ++;
                            if (mCheckedNumber == MAX_NUMBER + 1) {
                                //TODO
                                Toast.makeText(mContext, "the max number is " + MAX_NUMBER, Toast.LENGTH_SHORT).show();
                                mDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                                
                            }
                        } else {
                            mCheckedNumber --;
                            if (mCheckedNumber == MAX_NUMBER) {
                                mDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                            }
                        }
                    }
                })
                .setTitle(mPluginContext.getString(R.string.multi_select_contact_title))
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok,  new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ArrayList<String> selectedNumbers = new ArrayList<String>();
                        int totalNumber = checkedArray.length;
                        StringBuilder contentBuilder = new StringBuilder("");
                        for (int checkedIndex = 0; checkedIndex < totalNumber; checkedIndex ++) {
                            if (checkedArray[checkedIndex]) {
                                selectedNumbers.add(participants.get(checkedIndex).getContact());
                                if (TextUtils.isEmpty(contentBuilder.toString())) {
                                    contentBuilder.append(participants.get(checkedIndex).getContact());
                                } else {
                                    contentBuilder.append("\n").append(participants.get(checkedIndex).getContact());
                                    
                                }
                            }
                        }
                        Toast.makeText(mContext, contentBuilder.toString(), Toast.LENGTH_SHORT).show();
//                        Uri uri = Uri.fromParts("tel", selectedNumbers.get(0), null); //numbers,  ArrayList<String> numbers 
//                        final Intent confCallIntent = new Intent(Intent.ACTION_CALL, uri); 
//                        confCallIntent.putExtra(TelecomManagerEx.EXTRA_VOLTE_CONF_CALL_DIAL, true); 
//                        confCallIntent.putStringArrayListExtra(
//                                TelecomManagerEx.EXTRA_VOLTE_CONF_CALL_NUMBERS, numbers);
//                        mContext.startActivity(confCallIntent);
                        //TODO start multi call
                    }
                });
                mDialog = builder.create();
                mDialog.setCanceledOnTouchOutside(false);
                return mDialog;
            }
        };
        Bundle args = new Bundle();
        args.putParcelableArrayList("participants", participants);
//        args.putStringArray("sourcelist", contacts);
        args.putBooleanArray("checklist", checkedItems);
        fragment.setArguments(args);
        fragment.show(mContext.getFragmentManager(), "showSelectContactNumberDialog");
    }

    @Override
    public int getIpAutoSelectSubId() {
        if (RcsMessageUtils.getConfigStatus()) {
            int mainCardSubId = SubscriptionManager.getDefaultDataSubId();
            if (SubscriptionManager.isValidSubscriptionId(mainCardSubId)) {////data unset
                return mainCardSubId;
            }
        } else {
            SubscriptionInfo info = SubscriptionManager.from(mContext)
                             .getActiveSubscriptionInfoForSimSlotIndex(0);
            if (info != null) {
                return info.getSubscriptionId();
            }
        }
        return -1;
    }

    public static RcsComposeActivity getRcsComposer() {
        return sComposer;
    }

    public boolean processNewInvitation(long threadId) {
        Log.d(TAG, "processNewInvitation: threadId = " + threadId);
        if (sComposer != null && mThreadId == threadId) {
            mContext.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    showInvitaionDialog(IpMessageConsts.GroupActionList.GROUP_STATUS_INVITING_AGAIN);
                }
            });
            return true;
        } else {
            return false;
        }
    }

    private OnMessageListChangedListener mMsgListChangedListener = new OnMessageListChangedListener() {
        
        @Override
        public void onChanged() {
            // TODO Auto-generated method stub
            mCallback.resetSharePanel();
            if (mShowInviteMsg || mIsGroupChat) {
                mUiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        mCallback.hideIpRecipientEditor();
                    }
                });
            }
        }
    };

    public boolean isRcsMode() {
        if (mIsGroupChat) {
            return true;
        }
        //not configured
        if (!RCSMessageServiceManager.getInstance().isActivated()) {
            Log.d(TAG, "service is not configured");
            return false;
        }
        // have mms
        if(isMms()) {
            Log.d(TAG, "current is mms mode");
            return false;
        }

        /* TODO: for it, can release the code below when QC */
        int rcsSubId = RcsMessageUtils.getRcsSubId(mContext);
        Log.d(TAG, "rcsSubId is: " + rcsSubId);
        if (rcsSubId == -1) {
            return false;
        } else {
            int userSelectedId = RcsMessageUtils.getUserSelectedId(mContext);
            Log.d(TAG, "userSelectedId is: " + userSelectedId);
            if (userSelectedId == (int)Settings.System.SMS_SIM_SETTING_AUTO) {
                //auto 
                if (mRcsMessageListAdapter != null) {
                    int lastMsgSubId = (int)mRcsMessageListAdapter.getAutoSelectSubId();
                    Log.d(TAG, "last message subid is: " + lastMsgSubId);
                    if (rcsSubId != lastMsgSubId && lastMsgSubId != -1) {
                        //not rcs subid
                        return false;
                    }
                }
            } else if (userSelectedId != rcsSubId) {
                return false;
            }
        } 
        return true;
    }

    private BroadcastReceiver mChangeSubReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals(TelephonyIntents.ACTION_DEFAULT_SMS_SUBSCRIPTION_CHANGED)) {
                int subIdinSetting = (int)intent.getLongExtra(PhoneConstants.SUBSCRIPTION_KEY,
                        SubscriptionManager.INVALID_SUBSCRIPTION_ID);
                mCallback.resetSharePanel();
                initDualSimState();
/*
                Toast.makeText(
                        mPluginContext,
                        "subid =" + subIdinSetting,
                        Toast.LENGTH_SHORT).show();
*/
            }
        }
    };

    private ProgressDialog createProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(mContext);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setIndeterminate(true);
        return dialog;
    }

    private void showProgressDialog(String msg) {
        if (mProgressDialog == null) {
            mProgressDialog = createProgressDialog();
        }
        if (TextUtils.isEmpty(msg)) {
            mProgressDialog.setMessage("");
        } else {
            mProgressDialog.setMessage(msg);
        }
        mProgressDialog.show();
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    IInitGroupListener mInitGroupListener = new IInitGroupListener() {

        @Override
        public void onRejectGroupInvitationResult(final int result, final long threadId,
                final String chatId) {
            // TODO Auto-generated method stub
            Log.d(TAG, "onRejectGroupInvitationResult: result : " + result);
            mContext.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
//                    if (result == IpMessageConsts.GroupActionList.VALUE_SUCCESS) {
//                        //success
//                        Toast.makeText(mContext, "Reject result sucess", Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(mContext, "Reject result fail", Toast.LENGTH_SHORT).show();
//                    }
                }
            });
            
        }

        @Override
        public void onInitGroupResult(int result, long threadId, String chatId) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onAcceptGroupInvitationResult(final int result, final long threadId,
                final String chatId) {
            Log.d(TAG, "onAcceptGroupInvitationResult: result : " + result);
            mContext.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mThreadId == threadId) {
                        if (result == IpMessageConsts.GroupActionList.VALUE_SUCCESS) {
                            //success
//                            Toast.makeText(mContext, "Accept result sucess", Toast.LENGTH_SHORT).show();
                            dismissProgressDialog();
                            setChatActive(true);
                        } else {
                            Toast.makeText(mContext, mPluginContext.getString(R.string.group_accept_fail),
                                    Toast.LENGTH_SHORT).show();
                            showInvitaionDialog(mGroupStatus);
                        }
                    }
                }
            });
        }
    };

    private void setupCallStatusServiceConnection() {
        Log.i(TAG, "setupCallStatusServiceConnection.");
        if (mCallStatusService == null || mCallConnection == null) {
            mCallConnection = new CallStatusServiceConnection();
            boolean failedConnection = false;
            
            Intent intent = getCallStatusServiceIntent();
            if (!mContext.bindService(intent, mCallConnection,
                    Context.BIND_AUTO_CREATE)) {
                Log.d(TAG, "Bind service failed!");
                mCallConnection = null;
                failedConnection = true;
            } else {
                Log.d(TAG, "Bind service successfully!");
            }
        } else {
            Log.d(TAG, "Alreay bind service!");
        }
    }
    
    private void unbindCallStatusService() {
        Log.i(TAG, "unbindCallStatusService.");
        if (mCallStatusService != null) {
            unregisterCallListener();
            mContext.unbindService(mCallConnection);
            mCallStatusService = null;
        }
        mCallConnection = null;
    }
    
    private Intent getCallStatusServiceIntent() {
        Log.i(TAG, "getCallStatusServiceIntent.");
        final Intent intent = new Intent(ICallStatusService.class.getName());
        final ComponentName component = new ComponentName("com.mediatek.rcs.phone",
                                                    "com.mediatek.rcs.incallui.service.CallStatusService");
        intent.setComponent(component);
        return intent;
    }

    private final IServiceMessageCallback.Stub mCallServiceListener = new IServiceMessageCallback.Stub() {
        @Override
        public void updateMsgStatus(final String name, final String status, final String time) {
            try {
//                updatedMessageInfo(name, status, time);
                Log.i(TAG, "updateMsgStatus: name = " + name + ", status = " + status + ", time = " + time);
                mContext.runOnUiThread(new Runnable() {
                    
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        mShowCallTimeText.setVisibility(View.VISIBLE);
                        mShowCallTimeText.setText(status + "  " + time);
                    }
                });
            } catch (Exception e) {
                Log.e(TAG, "Error updateMsgStatus", e);
            }
        }

        @Override
        public void stopfromClient() {
            try {
                Log.i(TAG, "[IServiceMessageCallback]stopfromClient");
                unbindCallStatusService();
                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        mShowCallTimeText.setVisibility(View.GONE);
                        mIsNeedShowCallTime = false;
                    }
                });
            } catch (Exception e) {
                Log.e(TAG, "Error stopfromClient", e);
            }
        }
    };
    
    private void registerCallListener() {
        Log.d(TAG, "registerCallback.");
          try {
              if (mCallStatusService !=  null) {
                  mCallStatusService.registerMessageCallback(mCallServiceListener);
              }
          } catch (RemoteException e) {
              e.printStackTrace();
          }
      }

    private void unregisterCallListener() {
        Log.d(TAG, "unregisterCallback.");
        try {
            if (mCallStatusService != null) {
                mCallStatusService.unregisterMessageCallback(mCallServiceListener);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private class CallStatusServiceConnection implements ServiceConnection {
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(TAG, "onServiceConnected.");
            if (mCallStatusService != null) {
                Log.d(TAG, "Service alreay connected, service = " + mCallStatusService);
                return;
            }
            mCallStatusService = ICallStatusService.Stub.asInterface(service);
            registerCallListener();
        }

        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "onServiceDisconnected.");
            if (mCallStatusService != null) {
                unregisterCallListener();
                mContext.unbindService(mCallConnection);
                mCallStatusService = null;
                mUiHandler.postDelayed(new Runnable() {
                    
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        mShowCallTimeText.setVisibility(View.GONE);
                        mIsNeedShowCallTime = false;
                    }
                }, 1500);
            }
            mCallConnection = null;
        }
    }
}
