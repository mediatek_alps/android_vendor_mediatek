package com.mediatek.rcs.message.plugin;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony;
import android.provider.Telephony.Threads;
import android.provider.Telephony.ThreadSettings;
import android.util.Log;

import com.mediatek.mms.ipmessage.IpContact;
import com.mediatek.mms.ipmessage.IpConversation;
import com.mediatek.mms.ipmessage.IpConversationCallback;

import com.mediatek.rcs.common.IpMessageConsts;
import com.mediatek.rcs.message.utils.RcsMessageUtils;
import com.mediatek.rcs.message.utils.ThreadNumberCache;

public class RcsConversation extends IpConversation {
    private final String TAG = "RcsConversation";
    
    public static final String[] PROJECTION_NEW_GROUP_INVITATION = {
        Threads._ID, 
        Threads.DATE, 
        Threads.READ,
        Telephony.Threads.STATUS
    };
    public static final Uri URI_CONVERSATION =
            Uri.parse("content://mms-sms/conversations").buildUpon()
                    .appendQueryParameter("simple", "true").build();
    public static final String SELECTION_NEW_GROUP_INVITATION_BY_STATUS = "(" + Telephony.Threads.STATUS + 
                                                                        " = " + IpMessageConsts.GroupActionList.GROUP_STATUS_INVITING
                                                                        + " OR " + Telephony.Threads.STATUS +
                                                                        " = " + IpMessageConsts.GroupActionList.GROUP_STATUS_INVITING_AGAIN
                                                                        + " OR " + Telephony.Threads.STATUS +
                                                                        " = " + IpMessageConsts.GroupActionList.GROUP_STATUS_INVITE_EXPAIRED
                                                                        + ")";
    public static final String SELECTION_NEW_GROUP_INVITATION_BY_THREAD = "threads._id = ?";
    
        /// M: use this instead of the google default to query more columns in thread_settings
    public static final String[] ALL_THREADS_PROJECTION_EXTEND = {
        Threads._ID, Threads.DATE, Threads.MESSAGE_COUNT, Threads.RECIPIENT_IDS,
        Threads.SNIPPET, Threads.SNIPPET_CHARSET, Threads.READ, Threads.ERROR,
        Threads.HAS_ATTACHMENT
        /// M:
        , Threads.TYPE , Telephony.Threads.READ_COUNT , Telephony.Threads.STATUS,
        Telephony.ThreadSettings._ID, /// M: add for common
        Telephony.ThreadSettings.NOTIFICATION_ENABLE,
        Telephony.ThreadSettings.SPAM, Telephony.ThreadSettings.MUTE,
        Telephony.ThreadSettings.MUTE_START,
        Telephony.Threads.DATE_SENT
    };

    /** M: this type is used by mType, a convenience for identify a group conversation.
     *  so currently mType value maybe 0 sms 1 mms 2 wappush 3 cellbroadcast 10 guide 110 group
     *  the matched number maybe not right, but the type is as listed.
     */
    public static final int TYPE_GROUP = 110;
    public static final int STICKY     = 18;

//  public static final int SPAM           = 14;
    private long mThreadId = 0;
    private String mNumber;
    private boolean mIsSticky;
    private boolean mIsGroup;
    IpConversationCallback mConversationCallback;
    
    public int onIpFillFromCursor(Context context, Cursor c, int recipientSize, String number, int type, long date) {
        mThreadId = c.getLong(0);
        Log.d(TAG, "onIpFillFromCursor, threadId = " + mThreadId);
        Log.d(TAG, "onIpFillFromCursor, stick Time = " + c.getLong(STICKY));
        mIsSticky = (c.getLong(STICKY) != 0);
        if(RcsConversationList.mStickyThreadsSet != null) {
            if (mIsSticky) {
                RcsConversationList.mStickyThreadsSet.add(mThreadId);
            } else {
                RcsConversationList.mStickyThreadsSet.remove(mThreadId);
            }
        }
        /// M: if ipmessage is activated, we get extra columns by another query.
        mIsGroup = RcsMessageUtils.isGroupchat(mThreadId);
        if (mIsGroup) {
            return TYPE_GROUP;
        } else {
            //no group
            if (recipientSize == 1) {
                ThreadNumberCache.saveThreadandNumbers(mThreadId, new String[]{number});
            } else {
                //TODO: one2Multi Chat
            }
        }
        return type;
    }
    
//        @Override
//        public boolean onIpStartRemoteActivity(Context context, int type, long threadId) {
//            switch (type) {
//                /// M: add for ipmessage guide thread
//                case Telephony.Threads.IP_MESSAGE_GUIDE_THREAD:
//                    Log.d("MmsWidgetProxyActivity", "conversation mode -- ipmessage guide");
//                    Intent it = new Intent(RemoteActivities.SERVICE_CENTER);
//                    RCSMessageActivitiesManager.getInstance(context)
//                                       .startRemoteActivity(context, it);
//                    return true;
//                /// M: add for ipmessage group thread
//                case TYPE_GROUP:
//                    Log.d("MmsWidgetProxyActivity", "conversation mode -- ipmessage group");
//                    Intent it2 = new Intent(RemoteActivities.CHAT_DETAILS_BY_THREAD_ID);
//                    it2.putExtra(RemoteActivities.KEY_THREAD_ID, threadId);
//                    it2.putExtra(RemoteActivities.KEY_BOOLEAN, false);
//                    RCSMessageActivitiesManager.getInstance(context).startRemoteActivity(context, it2);
//                    return true;
//            }
//            return false;
//        }

    /// @}

    public long getThreadId() {
        return mThreadId;
    }

    public boolean isSticky() {
        return mIsSticky;
    }
    
    public long guaranteeIpThreadId(long threadId) {
        if (!mIsGroup) {
            return 0;
        }
        if (mThreadId != threadId) {
            Log.e(TAG, "[guaranteeIpThreadId]: threadId is changed. origin = " + mThreadId + ", threadId = " + threadId);
        }
        return mThreadId;
    }

    public void onIpInit(IpConversationCallback callback) {
        mConversationCallback = callback;
    }

    public List<IpContact> getIpContactList() {
        return mConversationCallback.getIpContactList();
    }
}
