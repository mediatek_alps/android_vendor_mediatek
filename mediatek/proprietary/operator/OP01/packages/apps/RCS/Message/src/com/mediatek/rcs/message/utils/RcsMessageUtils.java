package com.mediatek.rcs.message.utils;

import java.util.Set;
import java.util.TreeSet;

import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings;
import android.provider.Telephony;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Mms;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;
import android.widget.Toast;

import com.mediatek.mms.ipmessage.IpContactInfo;
import com.mediatek.mms.ipmessage.IpUtils;
import com.mediatek.mms.ipmessage.IpUtilsCallback;
import com.mediatek.mms.ipmessage.IpMessageItem;
import com.mediatek.rcs.common.INotificationsListener;
import com.mediatek.rcs.common.RCSMessageManager;
import com.mediatek.rcs.common.RCSMessageNotificationsManager;
import com.mediatek.rcs.message.R;
import com.mediatek.storage.StorageManagerEx;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;

import com.mediatek.rcs.message.data.RcsProfile;

//import com.mediatek.rcs.message.data.FileStruct;


import org.gsma.joyn.ft.FileTransferService;
import org.gsma.joyn.ft.FileTransferServiceConfiguration;
import org.gsma.joyn.JoynServiceException;

import com.mediatek.rcs.common.binder.RCSServiceManager;
import com.mediatek.rcs.common.GroupManager;
import com.mediatek.rcs.common.IpMessage;
import com.mediatek.rcs.common.IpMessageConsts;
import com.mediatek.rcs.common.IpMessageConsts.IpMessageStatus;
import com.mediatek.rcs.common.IpMessageConsts.IpMessageType;
import com.mediatek.rcs.common.IpAttachMessage;
import com.mediatek.rcs.common.IpImageMessage;
import com.mediatek.rcs.common.IpTextMessage;
import com.mediatek.rcs.common.IpVideoMessage;
import com.mediatek.rcs.common.IpVoiceMessage;
import com.mediatek.rcs.common.IpVCardMessage;
import com.mediatek.rcs.common.RCSMessageServiceManager;
import com.mediatek.rcs.common.provider.ThreadMapCache;
import com.mediatek.rcs.common.provider.ThreadMapCache.MapInfo;
import com.mediatek.rcs.common.utils.ContextCacher;
import com.mediatek.rcs.common.utils.Logger;
import com.mediatek.rcs.common.utils.RCSUtils;

import android.graphics.Rect;
import android.media.ExifInterface;

//add by feng
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.telephony.PhoneNumberUtils;
//import android.telephony.PhoneRatFamily;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.mediatek.telephony.TelephonyManagerEx;
import com.android.i18n.phonenumbers.PhoneNumberUtil;
import com.android.i18n.phonenumbers.Phonemetadata.PhoneMetadata;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import java.lang.reflect.Method;

import android.os.RemoteException;
import android.os.StatFs;
import android.os.SystemProperties;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

import android.media.MediaFile;
import android.webkit.MimeTypeMap;

import com.mediatek.rcs.message.group.PortraitManager;
import com.mediatek.rcs.message.plugin.EmojiImpl;
import com.mediatek.rcs.message.plugin.RcsMessageItem;

import com.mediatek.rcs.message.chat.GsmaManager;
import org.gsma.joyn.ft.FileTransferService;
import org.gsma.joyn.ft.FileTransferServiceConfiguration;
import org.gsma.joyn.JoynServiceException;
import com.mediatek.rcs.common.service.IRCSChatService;
public class RcsMessageUtils extends IpUtils{
    
    public static final String TAG = "RcsMessageUtils";
    
    public static final int DEFAULT_MESSAGE_ID = 0x100;
    
    public static final Uri SMS_CONTENT_URI = Uri.parse("content://sms");
    private static final Uri SMS_OUTBOX_URI = Uri.parse("content://sms/outbox");
    private static final Uri SMS_INBOX_URI = Uri.parse("content://sms/inbox");
    private static final String FILE_SCHEMA = "file://";
    public static final Uri THREAD_SETTINGS = Uri.parse("content://mms-sms/thread_settings/");
    
    private static final String[] PROJECTION_WITH_THREAD = {Sms._ID, Sms.THREAD_ID, Sms.ADDRESS, Sms.BODY};
//    private static final String[] PROJECTION_ONLY_ID = {Sms._ID};
//    private static final String[] PROJECTION_MESSAGE_ID = {Sms._ID};
    private static final String SELECTION_RELOAD_IP_MSG = Sms.IPMSG_ID + ">?";
    private static final String SELECTION_IP_MSG_ID = Sms.IPMSG_ID + "=?";
    private static final String SELECTION_MMS_THREAD_ID = Sms.THREAD_ID + "=?";
    
    public static final int UNCONSTRAINED = -1;
    public static final String IP_MESSAGE_FILE_PATH = File.separator + ".Rcse" + File.separator;
    public static final String CACHE_PATH = File.separator + ".Rcse" + "/Cache/";
    
    public static final int INBOX_MESSAGE = 1;
    public static final int OUTBOX_MESSAGE = 2;
    //add by feng
    /** Image */
    public static final String FILE_TYPE_IMAGE = "image";
    /** Audio */
    public static final String FILE_TYPE_AUDIO = "audio";
    /** Video */
    public static final String FILE_TYPE_VIDEO = "video";
    /** Text */
    public static final String FILE_TYPE_TEXT = "text";
    /** Application */
    public static final String FILE_TYPE_APP = "application"; 
    private static String COUNTRY_CODE = "+34";  
     /**
      * M: Added to avoid the magic number problem @{T-Mobile
      */
     private static final String INTERNATIONAL_PREFIX = "00";
     /** T-Mobile@} */
     /** M: add for format UUSD and star codes @{T-Mobile */
     private static final String Tel_URI_PREFIX = "tel:";
     private static final String SIP_URI_PREFIX = "sip:";
     private static final String AT_SIGN = "@";
     private static final String POUND_SIGN = "#";
     private static final String POUND_SIGN_HEX_VALUE = "23%";
     /** T-Mobile@} */
     /**
      * Country area code
      */
     private static String COUNTRY_AREA_CODE = "0";  
     private static String COUNTRY_CODE_PLUS = "+";  
     private static final String METHOD_GET_METADATA = "getMetadataForRegion";    
     private static final String REGION_TW = "TW";
     private static final String INTERNATIONAL_PREFIX_TW = "0(?:0[25679] | 16 | 17 | 19)";
    
    private static RcsMessageUtils sInstance;
    private Context mPluginContext;
    public RcsMessageUtils (Context context) {
        mPluginContext = context;
    }

//    public static RcsMessageUtils getInstance() {
//        if (sInstance == null) {
//            sInstance = new RcsMessageUtils();
//        }
//        return sInstance;
//    }

    private static IpUtilsCallback sCallback;
    
    @Override
    public boolean initIpUtils(IpUtilsCallback ipUtilsCallback) {
        sCallback = ipUtilsCallback;
        return true;
    }

    @Override
    public boolean onIpBootCompleted(Context context) {
        Log.d(TAG, "onIpBootCompleted()");
        return false;
    }

    @Override
    public boolean onIpMmsCreate(final Context context) {
        ContextCacher.setHostContext(context);

        Logger.d(TAG, "onIpMmsCreate. start");
        String processName = getCurProcessName(context);
        Logger.d(TAG, "createManager, processName=" + processName);
        if (processName != null && processName.equalsIgnoreCase("com.android.mms")) {
            GroupManager.createInstance(ContextCacher.getHostContext());
            RCSMessageServiceManager.initialize(context);
            PortraitManager.init(mPluginContext);
            RCSServiceManager.createManager(context);
            NewMessageReceiver.init(context);
            RcsProfile.init(context);
            new Thread(new Runnable() {
                
                public void run() {
                    Logger.d(TAG, "RCSCreateChatsThread start run");
                    ThreadMapCache.createInstance(ContextCacher.getHostContext());
                    
                    // delete ip burned message
//                SharedPreferences sp = ContextCacher.getPluginContext().getSharedPreferences(IpMessageConsts.BurnedMsgStoreSP.PREFS_NAME, Context.MODE_WORLD_READABLE);
//            	Set<String> burnedMsgList = sp.getStringSet(IpMessageConsts.BurnedMsgStoreSP.PREF_PREFIX_KEY, null);
//            	if (burnedMsgList == null) {
//            		Log.d(TAG, "[BurnedMsg]: onIpMmsCreate() burnedMsgList is null");
//            		return;
//            	}
//            	Log.d(TAG, "[BurnedMsg]: onIpMmsCreate() burnedMsgList = "+burnedMsgList);
//            	for (String id : burnedMsgList) {
//	            	if (Long.valueOf(id)>0){
//	            		RCSMessageManager.getInstance(context).deleteIpMsg(Long.valueOf(id));
//	            	} else {
//	            		RCSMessageManager.getInstance(context).deleteFTIpMsg(Long.valueOf(id));
//	            	}
//                }
                    
                }
            }, "RCSCreateChatsThread").start();
        }
        return false;
    }
    
    @Override
    public CharSequence formatIpMessage(CharSequence inputChars, boolean showImg, CharSequence inputBuf) {
        Log.d(TAG, "formatIpMessage(): inputChars = " + inputChars);
        if (inputChars == null) {
            return "";
        }

        EmojiImpl emoji = EmojiImpl.getInstance(ContextCacher.getPluginContext());
        CharSequence outChars = emoji.getEmojiExpression(inputChars, showImg);
        if (inputBuf == null) {
            return outChars;
        } else {
            String bufStr = inputBuf.toString();
            String inputStr = inputChars.toString();
            int start = bufStr.indexOf(inputStr);
            if (start == -1) {
                return inputBuf;
            }   

            CharSequence bufChars = emoji.getEmojiExpression(inputBuf, showImg);
            return bufChars;
        }
    }
    
    @Override
    public void onIpDeleteMessage(Context context, Collection<Long> threadIds, int maxSmsId, boolean deleteLockedMessages) {
        Log.d(TAG, "onIpDeleteMessage, threads = " + threadIds);
        RCSMessageManager.getInstance(context).deleteRCSThreads(threadIds, maxSmsId, deleteLockedMessages);
    }

    @Override
    public String getIpTextMessageType(IpMessageItem item) {
        if (item == null) {
            return null;
        }
        RcsMessageItem rcsItem = (RcsMessageItem) item;
        if(rcsItem.getIpMessageId() != 0) {
            return mPluginContext.getString(R.string.rcs_message_type);
        }
        return null;
    }

    @Override
    public long getKey(String type, long msgId) {
        if (type.equals("rcs")) {
            return msgId + 10000000;
        }
        return 0;
    }
    

    public static IpUtilsCallback getHostUtilsCallback() {
        return sCallback;
    }

    public static int getNotificationResourceId() {
        if (sCallback != null) {
            return sCallback.getNotificationResourceId();
        }else {
            return 0;
        }
    }

    public static void saveToMmsDb(String ipMsg_id, String contact, String content) {
        
    }

    static long getIdInMmsDb(int ipMsgId) {
        Log.d(TAG, "getIdInMmsDb() entry, ipMsgId: " + ipMsgId);
        ContentResolver contentResolver = ContextCacher.getHostContext().getApplicationContext().getContentResolver();
        
        Cursor cursor = null;
        try {
            final String[] args = {Integer.toString(ipMsgId)};
            cursor = contentResolver.query(SMS_CONTENT_URI, PROJECTION_WITH_THREAD, SELECTION_IP_MSG_ID, args, null);
            if (cursor.moveToFirst()) {
                long mmsDbId = cursor.getLong(cursor.getColumnIndex(Sms._ID));
                long threadId = cursor.getLong(cursor.getColumnIndex(Sms.THREAD_ID));
                String contact = cursor.getString(cursor.getColumnIndex(Sms.ADDRESS));
                Log.d(TAG, "getIdInMmsDb() contact is " + contact + " threadId is " + threadId);
                Log.d(TAG, "getIdInMmsDb() mmsDbId: " + mmsDbId);
                return mmsDbId;
            } else {
                Log.d(TAG, "getIdInMmsDb() empty cursor");
                return -1l;
            }
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
    }

    public static long storeMessageToMmsDb(String msg_id, String text, String remote, int MessageType, long threadId) {
        return storeMessageToMmsDb(findIdInRcseDb(msg_id), text, remote, MessageType, threadId);
    }
    
    public static long storeMessageToMmsDb(int ipMsgId, String text, String remote, int messageType, long threadId) {
        long idInMmsDb = getIdInMmsDb(ipMsgId);
        if (-1 != idInMmsDb) {
            Log.d(TAG, "storeMessageInDatabase() message found in Mms DB, id: " + idInMmsDb);
            return idInMmsDb;
        } else {
            Log.d(TAG, "storeMessageInDatabase() message not found in Mms DB");
            if (TextUtils.isEmpty(remote)) {
                Log.w(TAG, "storeMessageInDatabase() invalid remote: " + remote);
                return -1L;
            }
            return insertDatabase(text, remote, ipMsgId, messageType, threadId);
            //TODO: insert for group chat
//            if (remote.startsWith(PluginGroupChatWindow.GROUP_CONTACT_STRING_BEGINNER)) {
//                return insertDatabase(text, remote, ipMsgId, messageType, threadId);
//            } else {
//                final String contact = PhoneUtils.extractNumberFromUri(remote);
//                return insertDatabase(text, contact, ipMsgId, messageType, threadId);
//            }
        }
    }
    
    public static Long insertDatabase(String body, String contact, int ipMsgId, int messageType, long targetThreadId) {
        Log.d(TAG, "InsertDatabase(), body = " + body + "contact is " + contact + " , messageType: " + messageType
                + " , threadId: " + targetThreadId);
        Long messageIdInMms = Long.MIN_VALUE;
        Uri messageUri = null;
        ContentValues cv = new ContentValues();
       
        cv.put(Sms.ADDRESS, contact);
        cv.put(Sms.BODY, body);
        cv.put(Sms.IPMSG_ID, ipMsgId);
        //TODO: changto subid for L version
        cv.put(Sms.SUBSCRIPTION_ID, RcsMessageConfig.getSubId());
        if (targetThreadId > 0) {
            cv.put(Sms.THREAD_ID, targetThreadId);
            ThreadTranslater.saveThreadandTag(targetThreadId, contact);
        }
        ContentResolver contentResolver = ContextCacher.getHostContext().getContentResolver();
        if (messageType == Sms.MESSAGE_TYPE_INBOX) {
            cv.put(Sms.TYPE, Sms.MESSAGE_TYPE_INBOX);
            messageUri = contentResolver.insert(SMS_INBOX_URI, cv);
        } else if (messageType == Sms.MESSAGE_TYPE_OUTBOX) {
            cv.put(Sms.TYPE, Sms.MESSAGE_TYPE_OUTBOX);
            messageUri = contentResolver.insert(SMS_OUTBOX_URI, cv);
        }

        if (null == messageUri) {
            Log.w(TAG, "InsertDatabase() messageUri is null");
            return -1L;
        }
        
        //TODO: for cache group chat id
//        if (!ThreadTranslater.tagExistInCache(contact)) {
//            cacheThreadIdForGroupChat(contact, contentResolver);
//        } else {
//            Log.d(TAG, "InsertDatabase() contact not start group identify or not cached");
//        }

        String messageId = messageUri.getLastPathSegment();
        Log.d(TAG, "InsertDatabase(), messageId = " + messageId);
        messageIdInMms = Long.valueOf(messageId);

        Log.d(TAG, "InsertDatabase(), messageIdInMms = " + messageIdInMms);
        return messageIdInMms;
    }
    
    public static int findIdInRcseDb(String msgId) {
        Log.d(TAG, "findIdInRcseDb() entry, msgId: " + msgId);
        ContentResolver contentResolver = ContextCacher.getPluginContext().getContentResolver();

        if (TextUtils.isEmpty(msgId)) {
            Log.e(TAG, "findIdInRcseDb(), invalid msgId: " + msgId);
            return DEFAULT_MESSAGE_ID;
        }
        String[] argument = {msgId};
        Cursor cursor = null;
        //TODO: get ipmessage_id from rcs DB by msgId in rcs DB
//        try {
//            cursor = contentResolver.query(RichMessagingData.CONTENT_URI, 
//                    PROJECTION_ONLY_ID, EventLogData.KEY_EVENT_MESSAGE_ID + "=?", argument, null);
//            if (null != cursor && cursor.moveToFirst()) {
//                int rowId =  cursor.getInt(cursor.getColumnIndex(EventLogData.KEY_EVENT_ROW_ID));
//                Log.d(TAG, "findIdInRcseDb() row id for message: " + msgId + ", is " + rowId);
//                return rowId;
//            } else {
//                Log.w(TAG, "findIdInRcseDb() invalid cursor: " + cursor);
//            }
//        } finally {
//            if (null != cursor) {
//                cursor.close();
//            }
//        }
        return -1;
    }
    
    public static Set<String> collectMultiContact(String contact, String split) {
        String[] contacts = contact.split(split);
        Set<String> contactSet = new TreeSet<String>();
        for (String singleContact : contacts) {
            contactSet.add(singleContact);
        }
        return contactSet;
    }
    
    public static long getOrCreateThreadId(Context context, Set<String> contacts, boolean isCreate) {
        long thread_id = 0;
        if (contacts == null || contacts.size() == 0) {
            throw new RuntimeException("RCSUtils: getOrCreateThreadId, contacts can not be null or empty");
        }
        if (isCreate) {
            //TODO: always to new a thread_id for group chat when invite, depends mengjie
        } else {
            thread_id = Telephony.Threads.getOrCreateThreadId(context, contacts);
        }
        return thread_id;
    }
    
    public static long getOrCreateThreadId(Context context, String contact) {
        long thread_id = 0;
        if (contact == null) {
            throw new RuntimeException("RCSUtils: getOrCreateThreadId, contacts can not be null or empty");
        }
        thread_id = Telephony.Threads.getOrCreateThreadId(context, contact);
        return thread_id;
    }
    
    /**
     * Extract user part phone number from a SIP-URI or Tel-URI or SIP address
     * 
     * @param uri SIP or Tel URI
     * @return Number or null in case of error
     */
//    public static String extractNumberFromUri(String uri) {
//        if (uri == null) {
//            return null;
//        }
//
//        try {
//            // Extract URI from address
//            int index0 = uri.indexOf("<");
//            if (index0 != -1) {
//                uri = uri.substring(index0+1, uri.indexOf(">", index0));
//            }           
//            
//            // Extract a Tel-URI
//            int index1 = uri.indexOf("tel:");
//            if (index1 != -1) {
//                uri = uri.substring(index1+4);
//            }
//            
//            // Extract a SIP-URI
//            index1 = uri.indexOf("sip:");
//            if (index1 != -1) {
//                int index2 = uri.indexOf("@", index1);
//                uri = uri.substring(index1+4, index2);
//            }
//            
//            // Remove URI parameters
//            int index2 = uri.indexOf(";"); 
//            if (index2 != -1) {
//                uri = uri.substring(0, index2);
//            }
//            
//            // Format the extracted number (username part of the URI)
//            return formatNumberToInternational(uri);
//        } catch(Exception e) {
//            return null;
//        }
//    }
    
    public static int getVideoCaptureDurationLimit() {
        return 500;
    }
    
    public static boolean isGroupchat(long threadId) {
        MapInfo info = ThreadMapCache.getInstance().getInfoByThreadId(threadId);
        if (info != null && info.getChatId() != null) {
            return true;
        }
        return false;
    }
    
    public static String getGroupChatIdByThread(long threadId) {
        MapInfo info = ThreadMapCache.getInstance().getInfoByThreadId(threadId);
        if (info != null) {
            return info.getChatId();
        }
        return null;
    }
    
    public static Drawable getGroupDrawable(long threadId) {
        return ContextCacher.getPluginContext().getResources().getDrawable(R.drawable.group_example);
    }
    
    public static Drawable getMeDrawADrawable() {
        return null;
    }
    
    public static void addIpMsgNotificationListeners(Context context, INotificationsListener notiListener) {
        RCSMessageNotificationsManager.getInstance(context).registerNotificationsListener(notiListener);
    }

    public static void removeIpMsgNotificationListeners(Context context, INotificationsListener notiListener) {
        RCSMessageNotificationsManager.getInstance(context).unregisterNotificationsListener(notiListener);
    }
    
//    public static void deleteIpMessage(Context ct, Collection<Long> threads, int maxSmsId) {
//          long[] ids;
//          String threadList;
//          String selection = Telephony.Sms.IPMSG_ID + " > 0 AND " + Sms._ID + " <= " + maxSmsId;
//          if (threads != null) {
//              if (threads.size() < 1) {
//                  Log.w(TAG, "threads list is empty!");
//                  return;
//              }
//              threadList = getThreadListString(threads);
//              Log.d(TAG, "threadList:" + threadList);
//              selection += " AND " + Sms.THREAD_ID + " IN (" + threadList + ")";
//          }
//          Cursor cursor = null;
//          ct.getContentResolver().query(Sms.CONTENT_URI, 
//                  new String[]{Sms._ID}, selection, null, null);
////          cursor = SqliteWrapper.query(ct, ct.getContentResolver(),
////                  Sms.CONTENT_URI, new String[]{Sms._ID}, selection, null, null);
//          if (cursor != null) {
//              ids = new long[cursor.getCount()];
//              int i = 0;
//              while (cursor.moveToNext()) {
//                  ids[i++] = cursor.getLong(0);
//                  Log.d(TAG, "id" + (i - 1) + ":" + ids[i - 1]);
//              }
//              cursor.close();
//          } else {
//              Log.w(TAG, "delete ipmessage query get cursor null!");
//              return;
//          }
//          RCSMessageManager.getInstance(ct).deleteIpMsg(ids, false, false);
//    }
    
    public static String getThreadListString(Collection<Long> threads) {
        StringBuilder threadList = new StringBuilder();
        for (long thread : threads) {
            threadList.append(thread);
            threadList.append(",");
        }
        Log.d(TAG, "threadList:" + threadList);
        return threadList.substring(0, threadList.length() - 1);
    }
    
    public static boolean getSDCardStatus() {
        boolean ret = false;
        String sdStatus = Environment.getExternalStorageState();
        Log.d(TAG, "getSDCardStatus(): sdStatus = " + sdStatus);
        if (sdStatus.equals(Environment.MEDIA_MOUNTED)) {
            ret = true;
        }
        return ret;
    }

    public static String getSDCardPath(Context c) {
        File sdDir = null;
        String sdStatus = Environment.getExternalStorageState();

        if (TextUtils.isEmpty(sdStatus)) {
            return c.getFilesDir().getAbsolutePath();
        }

        boolean sdCardExist = sdStatus.equals(android.os.Environment.MEDIA_MOUNTED);

        if (sdCardExist) {
            sdDir = Environment.getExternalStorageDirectory();
            return sdDir.toString();
        }
        return c.getFilesDir().getAbsolutePath();
    }
    
    public static String getCachePath(Context c) {
        String path = null;
        String sdCardPath = getSDCardPath(c);
        if (!TextUtils.isEmpty(sdCardPath)) {
            path = sdCardPath + CACHE_PATH;
        }
        return path;
    }
    
    public static Drawable getAvataByThreadId(long threadId) {
        boolean isGroup = isGroupchat(threadId);
        if (isGroup) {
            return getGroupDrawable(threadId);
        } else {
            String[] numbers = ThreadNumberCache.getNumbersbyThreadId(threadId);
            if (numbers != null) {
                if (numbers.length > 1) {
                    //One2Multchat
                    return ContextCacher.getPluginContext().getResources()
                            .getDrawable(R.drawable.ic_contact_picture);
                } else {
                    return sCallback.getContactDrawalbeFromNumber(
                                 ContextCacher.getHostContext(), null, numbers[0], false);
                }
            } else {
                return null;
            }
        }
    }
    
    public static Drawable getContactDrawableByNumber(String number) {
        if (sCallback == null) {
            Log.e(TAG, "[getContactDrawableByNumber]: sCallback is null");
            return null;
        }
        return sCallback.getContactDrawalbeFromNumber(
                ContextCacher.getHostContext(), null, number, false); 
    }

    public static String getContactNameByNumber(String number) {
        String[] numbers = new String[] {number};
        List<IpContactInfo> infoList = sCallback.getContactInfoListByNumbers(numbers, true);
        if (infoList != null && infoList.size() > 0) {
            return infoList.get(0).getName();
        }
        return null;
    }

    public static List<String> getContactNumbersByIds(long[] ids) {
        List<IpContactInfo> infoList = sCallback.getContactInfoListByIds(ids);
        List<String> numberList = new ArrayList<String>();
        for (IpContactInfo info : infoList) {
            numberList.add(info.getNumber());
        }
        return numberList;
    }

    public static File getStorageFile(String filename) {
        String dir = "";
        String path = StorageManagerEx.getDefaultPath();
        if (path == null) {
            Log.e(TAG, "default path is null");
            return null;
        }
        dir = path + "/" + Environment.DIRECTORY_DOWNLOADS + "/";
        Log.i(TAG, "copyfile,  file full path is " + dir + filename);
        File file = getUniqueDestination(dir + filename);

        // make sure the path is valid and directories created for this file.
        File parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            Log.i(TAG, "[RCS] copyFile: mkdirs for " + parentFile.getPath());
            parentFile.mkdirs();
        }
        return file;
    }

    public static File getUniqueDestination(String fileName) {
        File file ;
        final int index = fileName.indexOf(".");
        if (index > 0) {
            final String extension = fileName.substring(index + 1, fileName.length());
            final String base = fileName.substring(0, index);
            file = new File(base + "." + extension);
            for (int i = 2; file.exists(); i++) {
                file = new File(base + "_" + i + "." + extension);
            }
        } else {
            file = new File(fileName);
            for (int i = 2; file.exists(); i++) {
                file = new File(fileName + "_" + i);
            }
        }
        return file;
    }

    public static String getUniqueFileName(String fileName) {
    	String mName = fileName;
    	File file ;
        final int index = fileName.lastIndexOf(".");
        if (index > 0) {
            final String extension = fileName.substring(index + 1, fileName.length());
            final String base = fileName.substring(0, index);
            file = new File(base + "." + extension);
            for (int i = 1; file.exists(); i++) {
            	mName = base + "(" + i + ")." + extension;
                file = new File(mName);
            }
        } else {
            file = new File(fileName);
            for (int i = 1; file.exists(); i++) {
            	mName = fileName + "(" + i + ")";
                file = new File(mName);
            }
        }
        return mName;
    }

    public static void copy(File src, File dest) {
        InputStream is = null;
        OutputStream os = null;

        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            is = new FileInputStream(src);
            os = new FileOutputStream(dest);
            byte[] b = new byte[256];
            int len = 0;
            try {
                while ((len = is.read(b)) != -1) {
                    os.write(b, 0, len);

                }
                os.flush();
            } catch (IOException e) {
                Log.e(TAG, "", e);
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        Log.e(TAG, "", e);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, "", e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    Log.e(TAG, "", e);
                }
            }
        }
    }

    public static void copy(String src, String dest) {
        InputStream is = null;
        OutputStream os = null;

        File out = new File(dest);
        if (!out.getParentFile().exists()) {
            out.getParentFile().mkdirs();
        }

        try {
            is = new BufferedInputStream(new FileInputStream(src));
            os = new BufferedOutputStream(new FileOutputStream(dest));

            byte[] b = new byte[256];
            int len = 0;
            try {
                while ((len = is.read(b)) != -1) {
                    os.write(b, 0, len);

                }
                os.flush();
            } catch (IOException e) {
                Log.e(TAG, "", e);
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        Log.e(TAG, "", e);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, "", e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    Log.e(TAG, "", e);
                }
            }
        }
    }


    public static Bitmap resizeImage(Bitmap bitmap, int w, int h, boolean needRecycle) {
        if (null == bitmap) {
            return null;
        }

        Bitmap bitmapOrg = bitmap;
        int width = bitmapOrg.getWidth();
        int height = bitmapOrg.getHeight();
        int newWidth = w;
        int newHeight = h;

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        Bitmap resizedBitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);
        if (needRecycle && !bitmapOrg.isRecycled() && bitmapOrg != resizedBitmap) {
            bitmapOrg.recycle();
        }
        return resizedBitmap;
    }

    public static byte[] resizeImg(String path, float maxLength) {
        //int d = getExifOrientation(path);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
        options.inJustDecodeBounds = false;

        int l = Math.max(options.outHeight, options.outWidth);
        int be = (int) (l / maxLength);
        if (be <= 0) {
            be = 1;
        }
        options.inSampleSize = be;

        bitmap = BitmapFactory.decodeFile(path, options);
        if (null == bitmap) {
            return null;
        }
        /*
        if (d != 0) {
            bitmap = rotate(bitmap, d);
        }
        */

        String[] tempStrArry = path.split("\\.");
        String filePostfix = tempStrArry[tempStrArry.length - 1];
        CompressFormat formatType = null;
        if (filePostfix.equalsIgnoreCase("PNG")) {
            formatType = Bitmap.CompressFormat.PNG;
        } else if (filePostfix.equalsIgnoreCase("JPG") || filePostfix.equalsIgnoreCase("JPEG")) {
            formatType = Bitmap.CompressFormat.JPEG;
        //} else if (filePostfix.equalsIgnoreCase("GIF")) {
        //    formatType = Bitmap.CompressFormat.PNG;
        } else if (filePostfix.equalsIgnoreCase("BMP")) {
            formatType = Bitmap.CompressFormat.PNG;
        } else {
            Log.d(TAG, "resizeImg(): Can't compress the image,because can't support the format:" + filePostfix);
            return null;
        }

        int quality = 100;
        if (be == 1) {
            if (getFileSize(path) > 50 * 1024) {
                quality = 30;
            }
        } else {
            quality = 30;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(formatType, quality, baos);
        final byte[] tempArry = baos.toByteArray();
        if (baos != null) {
            try {
                baos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            baos = null;
        }

        return tempArry;
    }

/*
    public static int getExifOrientation(String filepath) {
        int degree = 0;
        ExifInterface exif = null;

        try {
            exif = new ExifInterface(filepath);
        } catch (IOException ex) {
            MmsLog.e(TAG, "getExifOrientation():", ex);
        }

        if (exif != null) {
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
            if (orientation != -1) {
                // We only recognize a subset of orientation tag values.
                switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
                default:
                    break;
                }
            }
        }

        return degree;
    }

    */

     /**
     *
     *
     * @param stream
     * @return
     */
    public static void nmsStream2File(byte[] stream, String filepath) throws Exception {
        FileOutputStream outStream = null;
        try {
            File f = new File(filepath);
            if (!f.getParentFile().exists()) {
                f.getParentFile().mkdirs();
            }
            if (f.exists()) {
                f.delete();
            }
            f.createNewFile();
            outStream = new FileOutputStream(f);
            outStream.write(stream);
            outStream.flush();
        } catch (IOException e) {
            Log.e(TAG, "nmsStream2File():", e);
            throw new RuntimeException(e.getMessage());
        } finally {
            if (outStream != null) {
                try {
                    outStream.close();
                    outStream = null;
                } catch (IOException e) {
                    Log.e(TAG, "nmsStream2File():", e);
                    throw new RuntimeException(e.getMessage());
                }
            }
        }
    }

    public static boolean isPic(String name) {
        if (TextUtils.isEmpty(name)) {
            return false;
        }
        String path = name.toLowerCase();
        if (path.endsWith(".png") || path.endsWith(".jpg") || path.endsWith(".jpeg")
                || path.endsWith(".bmp") || path.endsWith(".gif")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValidAttach(String path, boolean inspectSize) {
        if (!isExistsFile(path) || getFileSize(path) == 0) {
            Log.e(TAG, "isValidAttach: file is not exist, or size is 0");
            return false;
        }
        return true;
    }

    public static boolean isExistsFile(String filepath) {
        try {
            if (TextUtils.isEmpty(filepath)) {
                return false;
            }
            File file = new File(filepath);
            return file.exists();
        } catch (Exception e) {
            Log.e(TAG, "isExistsFile():", e);
            return false;
        }
    }

    public static int getFileSize(String filepath) {
        try {
            if (TextUtils.isEmpty(filepath)) {
                return -1;
            }
            File file = new File(filepath);
            return (int) file.length();
        } catch (Exception e) {
            Log.e(TAG, "getFileSize():", e);
            return -1;
        }
    }
    

     /**
     * Return the file transfer IpMessage
     * 
     * @param remote remote user
     * @param FileStructForBinder file strut
     * @return The file transfer IpMessage
     */
     /*
    public static IpMessage analysisFileType(String remote, FileStruct fileTransfer) {

        String fileName = fileTransfer.mName;
        if (fileName != null) {
            String mimeType = MediaFile.getMimeTypeForFile(fileName);
            if (mimeType == null) {
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                        getFileExtension(fileName));
            }
            if (mimeType != null) {
                if (mimeType.contains(FILE_TYPE_IMAGE)) {
                    return new IpImageMessage(fileTransfer, remote);
                } else if (mimeType.contains(FILE_TYPE_AUDIO)
                        || mimeType.contains("application/ogg")) {
                    return new IpVoiceMessage(fileTransfer, remote);
                } else if (mimeType.contains(FILE_TYPE_VIDEO)) {
                    return new IpVideoMessage(fileTransfer, remote);
                } else if (fileName.toLowerCase().endsWith(".vcf")) {
                    return new IpVCardMessage(fileTransfer, remote);
                } else {
                    // Todo
                    Log.d(TAG, "analysisFileType() other type add here!");
                }
            }
        } else {
            Log.w(TAG, "analysisFileType(), file name is null!");
        }
        return null;
    }
    */
        

     public static String getFileExtension(String fileName) {
        Log.d(TAG, "getFileExtension() entry, the fileName is " + fileName);
        String extension = null;
        if (TextUtils.isEmpty(fileName)) {
            Log.d(TAG, "getFileExtension() entry, the fileName is null");
            return null;
        }
        int lastDot = fileName.lastIndexOf(".");
        extension = fileName.substring(lastDot + 1).toLowerCase();
        Log.d(TAG, "getFileExtension() entry, the extension is " + extension);
        return extension;
    }

    public static long getPhotoSizeLimit() {
       return 10 * 1024 * 1024;
    } 


    public static String getPhotoResolutionLimit() {
        return "4000x4000";
    }

    public static String getVideoResolutionLimit() {
        return "1920x1080";
    }

    public static long getAudioDurationLimit() {
        return 180000L;
    }


    public static boolean isVideo(String name) {
        if (TextUtils.isEmpty(name)) {
            return false;
        }
        String path = name.toLowerCase();
        if (path.endsWith(".mp4") || path.endsWith(".3gp")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isAudio(String name) {
        if (TextUtils.isEmpty(name)) {
            return false;
        }
        ///M: add 3gpp audio file{@
        String extArrayString[] = {".amr", ".ogg", ".mp3", ".aac", ".ape", ".flac", ".wma", ".wav", ".mp2", ".mid",".3gpp"};
        ///@}
        String path = name.toLowerCase();
        for (String ext : extArrayString) {
            if (path.endsWith(ext)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isFileStatusOk(Context context, String path) {
        if (TextUtils.isEmpty(path)) {
            Toast.makeText(context, R.string.ipmsg_no_such_file, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!isExistsFile(path)) {
            Toast.makeText(context, R.string.ipmsg_no_such_file, Toast.LENGTH_SHORT).show();
            return false;
        }
        //if (getFileSize(path) > (2 * 1024 * 1024)) {
        //    Toast.makeText(context, R.string.ipmsg_over_file_limit, Toast.LENGTH_SHORT).show();
        //    return false;
        //}
        return true;
    }

    public static String formatFileSize(int size) {
        String result = "";
        int oneMb = 1024 * 1024;
        int oneKb = 1024;
        if (size > oneMb) {
            int s = size % oneMb / 100;
            if (s == 0) {
                result = size / oneMb + "MB";
            } else {
                result = size / oneMb + "." + s + "MB";
            }
        } else if (size > oneKb) {
            int s = size % oneKb / 100;
            if (s == 0) {
                result = size / oneKb + "KB";
            } else {
                result = size / oneKb + "." + s + "KB";
            }
        } else if (size > 0) {
            result = size + "B";
        } else {
            result = "invalid size";
        }
        return result;
    }

    public static String formatAudioTime(int duration) {
        String result = "";
        if (duration > 60) {
            if (duration % 60 == 0) {
                result = duration / 60 + "'";
            } else {
                result = duration / 60 + "'" + duration % 60 + "\"";
            }
        } else if (duration > 0) {
            result = duration + "\"";
        } else {
            // TODO IP message replace this string with resource
            result = "no duration";
        }
        return result;
    }

        /**
     * Get bitmap
     *
     * @param path
     * @param options
     * @return
     */
    public static Bitmap getBitmapByPath(String path, Options options, int width, int height) {
        if (TextUtils.isEmpty(path) || width <= 0 || height <= 0) {
            Log.w(TAG, "parm is error.");
            return null;
        }

        File file = new File(path);
        if (!file.exists()) {
            Log.w(TAG, "file not exist!");
            return null;
        }
        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "FileNotFoundException:" + e.toString());
        }
        if (options != null) {
            Rect r = getScreenRegion(width, height);
            int w = r.width();
            int h = r.height();
            int maxSize = w > h ? w : h;
            int inSimpleSize = computeSampleSize(options, maxSize, w * h);
            options.inSampleSize = inSimpleSize;
            options.inJustDecodeBounds = false;
        }
        Bitmap bm = null;
        try {
            bm = BitmapFactory.decodeStream(in, null, options);
        } catch (java.lang.OutOfMemoryError e) {
            Log.e(TAG, "bitmap decode failed, catch outmemery error");
        }
        try {
            in.close();
        } catch (IOException e) {
            Log.e(TAG, "IOException:" + e.toString());
        }
        return bm;
    }

    private static Rect getScreenRegion(int width, int height) {
        return new Rect(0, 0, width, height);
    }

     public static int computeSampleSize(BitmapFactory.Options options, int minSideLength,
            int maxNumOfPixels) {
        int initialSize = computeInitialSampleSize(options, minSideLength, maxNumOfPixels);

        int roundedSize;
        if (initialSize <= 8) {
            roundedSize = 1;
            while (roundedSize < initialSize) {
                roundedSize <<= 1;
            }
        } else {
            roundedSize = (initialSize + 7) / 8 * 8;
        }

        return roundedSize;
    }

    private static int computeInitialSampleSize(BitmapFactory.Options options, int minSideLength,
            int maxNumOfPixels) {
        double w = options.outWidth;
        double h = options.outHeight;

        int lowerBound = (maxNumOfPixels == UNCONSTRAINED) ? 1 : (int) Math.ceil(Math.sqrt(w * h
                / maxNumOfPixels));
        int upperBound = (minSideLength == UNCONSTRAINED) ? 128 : (int) Math.min(
                Math.floor(w / minSideLength), Math.floor(h / minSideLength));

        if (upperBound < lowerBound) {
            return lowerBound;
        }

        if ((maxNumOfPixels == UNCONSTRAINED) && (minSideLength == UNCONSTRAINED)) {
            return 1;
        } else if (minSideLength == UNCONSTRAINED) {
            return lowerBound;
        } else {
            return upperBound;
        }
    }

    public static int getExifOrientation(String filepath) {
        int degree = 0;
        ExifInterface exif = null;

        try {
            exif = new ExifInterface(filepath);
        } catch (IOException ex) {
            Log.e(TAG, "getExifOrientation():", ex);
        }

        if (exif != null) {
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
            if (orientation != -1) {
                // We only recognize a subset of orientation tag values.
                switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
                default:
                    break;
                }
            }
        }

        return degree;
    }

    public static Options getOptions(String path) {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        return options;
    }

    public static Bitmap rotate(Bitmap b, int degrees) {
        if (degrees != 0 && b != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);
            try {
                Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
                if (b != b2) {
                    b.recycle();
                    b = b2;
                }
            } catch (OutOfMemoryError ex) {
                // We have no memory to rotate. Return the original bitmap.
                Log.w(TAG, "OutOfMemoryError.");
            }
        }

        return b;
    }


    public static void unblockingNotifyNewIpMessage(Context context, long newMsgThreadId, long newIpMessageId) {
        unblockingIpUpdateNewMessageIndicator(context, newMsgThreadId, false, null);
        if (RcsMessageUtils.isIpPopupNotificationEnable()) {
            notifyNewIpMessageDialog(context, newIpMessageId);
        }
        RcsMessageUtils.notifyIpWidgetDatasetChanged(context);
    }
    
    
    public static void unblockingIpUpdateNewMessageIndicator(final Context context, final long newMsgThreadId,
            final boolean isStatusMessage, final Uri statusMessageUri) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                sCallback.blockingIpUpdateNewMessageIndicator(context, newMsgThreadId, isStatusMessage, statusMessageUri);
            }
        }, "MessagingNotification.nonBlockingUpdateNewMessageIndicator").start();
    }
    
    public static void blockingIpUpdateNewMessageIndicator(Context context, long newMsgThreadId,
            boolean isStatusMessage, Uri statusMessageUri) {
        sCallback.blockingIpUpdateNewMessageIndicator(context, newMsgThreadId, isStatusMessage, statusMessageUri);
    }

    public static boolean isIpPopupNotificationEnable() {
        return sCallback.isIpPopupNotificationEnable();
    }

    public static void notifyIpWidgetDatasetChanged(Context context) {
        sCallback.notifyIpWidgetDatasetChanged(context);
    }

//    public static boolean isIpHome(Context context) {
//        return sCallback.isIpHome(context);
//    }
//
//    public static Intent getDialogModeIntent(Context context) {
//        return sCallback.getDialogModeIntent(context);
//    }
    
    private static void notifyNewIpMessageDialog(Context context, long id) {
        Log.d(TAG, "notifyNewIpMessageDialog,id:" + id);
        if (sCallback.isIpHome(context)) {
            Log.d(TAG, "at launcher");
            Intent smsIntent = sCallback.getDialogModeIntent(context);
            Uri smsUri = Uri.parse("content://sms/" + id);
            smsIntent.putExtra("com.android.mms.transaction.new_msg_uri", smsUri.toString());
            smsIntent.putExtra("ipmessage", true);
            smsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(smsIntent);
        } else {
            Log.d(TAG, "not at launcher");
        }
    }
    //add by feng
    /** M: add for log @{ */

    //add by feng
    /**
	 * Extract user part phone number from a SIP-URI or Tel-URI or SIP address
	 * 
	 * @param uri SIP or Tel URI
	 * @return Number or null in case of error
	 */
	public static String extractNumberFromUri(String uri) {
		if (uri == null) {
			return null;
		}

		try {
			// Extract URI from address
			int index0 = uri.indexOf("<");
			if (index0 != -1) {
				uri = uri.substring(index0+1, uri.indexOf(">", index0));
			}			
			
			// Extract a Tel-URI
			int index1 = uri.indexOf("tel:");
			if (index1 != -1) {
				uri = uri.substring(index1+4);
			}
			
			// Extract a SIP-URI
			index1 = uri.indexOf("sip:");
			if (index1 != -1) {
				int index2 = uri.indexOf("@", index1);
				uri = uri.substring(index1+4, index2);
			}
			
			// Remove URI parameters
			int index2 = uri.indexOf(";"); 
			if (index2 != -1) {
				uri = uri.substring(0, index2);
			}
			
			// Format the extracted number (username part of the URI)
			return formatNumberToInternational(uri);
		} catch(Exception e) {
			return null;
		}
	}

     /**
	 * Format a phone number to international format
	 * 
	 * @param number Phone number
	 * @return International number
	 */
	public static String formatNumberToInternational(String number) {
	    String sInternationalPrefix = null;
		if (number == null) {
			return null;
		}
		
		// Remove spaces
		number = number.trim();

		// Strip all non digits
		String phoneNumber = PhoneNumberUtils.stripSeparators(number);
        if(phoneNumber.equals(""))
        {
        	return "";
        }
        if (sInternationalPrefix == null) {
            String countryIso = null;
            try {
                countryIso = getDefaultSimCountryIso();
            } catch (ClassCastException e) {
                e.printStackTrace();
                Log.e(TAG,
                        "formatNumberToInternational() plz check whether your load matches your code base");
            }
            if (countryIso != null) {
                sInternationalPrefix = getInternationalPrefix(countryIso.toUpperCase());
            }
            Log.d(TAG, "formatNumberToInternational() countryIso: " + countryIso
                    + " sInternationalPrefix: " + sInternationalPrefix);
        }
        if (sInternationalPrefix != null) {
            Pattern pattern = Pattern.compile(sInternationalPrefix);
            Matcher matcher = pattern.matcher(number);
            StringBuilder formattedNumberBuilder = new StringBuilder();
            if (matcher.lookingAt()) {
                int startOfCountryCode = matcher.end();
                formattedNumberBuilder.append(COUNTRY_CODE_PLUS);
                formattedNumberBuilder.append(number.substring(startOfCountryCode));
                phoneNumber = formattedNumberBuilder.toString();
            }
        }
        Log.d(TAG, "formatNumberToInternational() number: " + number + " phoneNumber: "
                + phoneNumber + " sInternationalPrefix: " + sInternationalPrefix);
		// Format into international
		if (phoneNumber.startsWith("00" + COUNTRY_CODE.substring(1))) {
			// International format
			phoneNumber = COUNTRY_CODE + phoneNumber.substring(4);
		} else
		if ((COUNTRY_AREA_CODE != null) && (COUNTRY_AREA_CODE.length() > 0) &&
				phoneNumber.startsWith(COUNTRY_AREA_CODE)) {
			// National number with area code
			phoneNumber = COUNTRY_CODE + phoneNumber.substring(COUNTRY_AREA_CODE.length());
		} else
		if (!phoneNumber.startsWith("+")) {
			// National number
			phoneNumber = COUNTRY_CODE + phoneNumber;
		}
		return phoneNumber;
	}

     private static String getDefaultSimCountryIso() {
		   int simId;
		   String iso = null;
		   TelephonyManagerEx mTelephonyManagerEx = null;
		   //read if gemini support is present (change duet to L-migration)
		   boolean geminiSupport = false;
		   geminiSupport = SystemProperties.get("ro.mtk_gemini_support").equals("1");
		   
		   //if (FeatureOption.MTK_GEMINI_SUPPORT == true) {
		   if(geminiSupport){
			   //simId = SystemProperties.getInt(PhoneConstants.GEMINI_DEFAULT_SIM_PROP, -1);
		       simId = 0; //changes as per L-migration , defualt SIM is SIM_ID_1 value = 0
			   if (simId == -1) {// No default sim setting
				   simId = PhoneConstants.SIM_ID_1;
			   }

                           if(mTelephonyManagerEx ==null){
                              mTelephonyManagerEx  = TelephonyManagerEx.getDefault();
                           }

			   if (!mTelephonyManagerEx.getDefault().hasIccCard(simId)) {
				   simId = PhoneConstants.SIM_ID_2 ^ simId;
			   }

                             iso = mTelephonyManagerEx.getSimCountryIso(simId);

		   } else {
			   iso = TelephonyManager.getDefault().getSimCountryIso();
		   }
		   return iso;
	   }

     private static String getInternationalPrefix(String countryIso) {
        try {
            PhoneNumberUtil util = PhoneNumberUtil.getInstance();
            Method method = PhoneNumberUtil.class.getDeclaredMethod(METHOD_GET_METADATA,
                    String.class);
            method.setAccessible(true);
            PhoneMetadata metadata = (PhoneMetadata) method.invoke(util, countryIso);
            if (metadata != null) {
                String prefix = metadata.getInternationalPrefix();
                if (countryIso.equalsIgnoreCase(REGION_TW)) {
                    prefix = INTERNATIONAL_PREFIX_TW;
                }
                return prefix;
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

     /**
     * Get the current available storage size in byte;
     * 
     * @return available storage size in byte; -1 for no external storage
     *         detected
     */
    public static long getFreeStorageSize() {
        boolean isExist = Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
        if (isExist) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            int availableBlocks = stat.getAvailableBlocks();
            int blockSize = stat.getBlockSize();
            long result = (long) availableBlocks * blockSize;
            Log.d(TAG, "getFreeStorageSize() blockSize: " + blockSize + " availableBlocks: "
                    + availableBlocks + " result: " + result);
            return result;
        }
        return -1;
    }

    public static boolean sendMms(Uri uri, long messageSize, int subId, long threadId) {
        if (sCallback != null) {
            return sCallback.sendMms(ContextCacher.getHostContext(), uri, messageSize, subId, threadId);
        }
        return false;
    }

    /**
     * this method is from message app.
     * @param context It used to get string from res.
     * @param when time.
     * @return string.
     */
    public static String formatTimeStampStringExtend(Context context, long when) {
        Time then = new Time();
        then.set(when);
        Time now = new Time();
        now.setToNow();

        // Basic settings for formatDateTime() we want for all cases.
        int formatFags = DateUtils.FORMAT_NO_NOON_MIDNIGHT
                | DateUtils.FORMAT_ABBREV_ALL | DateUtils.FORMAT_CAP_AMPM;

        // If the message is from a different year, show the date and year.
        if (then.year != now.year) {
            formatFags |= DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE;
        } else if (then.yearDay != now.yearDay) {
            // If it is from a different day than today, show only the date.
            if ((now.yearDay - then.yearDay) == 1) {
                return context.getString(R.string.str_ipmsg_yesterday);
            } else {
                formatFags |= DateUtils.FORMAT_SHOW_DATE;
            }
        } else if ((now.toMillis(false) - then.toMillis(false)) < 60000) {
            return context.getString(R.string.time_now);
        } else {
            // Otherwise, if the message is from today, show the time.
            formatFags |= DateUtils.FORMAT_SHOW_TIME;
        }
        return DateUtils.formatDateTime(context, when, formatFags);
    }

    /**
     * this method is from message app.
     * @param context It used to get string from res.
     * @param when time.
     * @param fullFormat flag.
     * @return time string.
     */
    public static String formatTimeStampString(Context context, long when, boolean fullFormat) {
        Time then = new Time();
        then.set(when);
        Time now = new Time();
        now.setToNow();

        // Basic settings for formatDateTime() we want for all cases.
        int formatFags = DateUtils.FORMAT_NO_NOON_MIDNIGHT |
        // / M: Fix ALPS00419488 to show 12:00, so mark
        // DateUtils.FORMAT_ABBREV_ALL
        // DateUtils.FORMAT_ABBREV_ALL |
                DateUtils.FORMAT_CAP_AMPM;

        // If the message is from a different year, show the date and year.
        if (then.year != now.year) {
            formatFags |= DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE;
        } else if (then.yearDay != now.yearDay) {
            // If it is from a different day than today, show only the date.
            formatFags |= DateUtils.FORMAT_SHOW_DATE;
        } else {
            // Otherwise, if the message is from today, show the time.
            formatFags |= DateUtils.FORMAT_SHOW_TIME;
        }

        // If the caller has asked for full details, make sure to show the date
        // and time no matter what we've determined above (but still make
        // showing
        // the year only happen if it is a different year from today).
        if (fullFormat) {
            formatFags |= (DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME);
        }

        return DateUtils.formatDateTime(context, when, formatFags);
    }

    /**
     * na.
     * @param bytes file size.
     * @param context caller of this method.
     * @return file size string.
     */
    public static String getDisplaySize(long bytes, Context context) {
        String displaySize = context.getString(R.string.unknown);
        long iKb = bytes / 1024;
        if (iKb == 0 && bytes >= 0) {
            // display "less than 1KB"
            displaySize = context.getString(R.string.less_1K);
        } else if (iKb >= 1024) {
            // diplay MB
            double iMb = ((double) iKb) / 1024;
            iMb = round(iMb, 2, BigDecimal.ROUND_UP);
            StringBuilder builder = new StringBuilder(new Double(iMb).toString());
            builder.append("MB");
            displaySize = builder.toString();
        } else {
            // display KB
            StringBuilder builder = new StringBuilder(new Long(iKb).toString());
            builder.append("KB");
            displaySize = builder.toString();
        }
        return displaySize;
    }

    private static double round(double value, int scale, int roundingMode) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(scale, roundingMode);
        double d = bd.doubleValue();
        bd = null;
        return d;
    }

    /**
     * get the 3G/4G Capability subId.
     * @return the 3G/4G Capability subId
     */
    public static long get34GCapabilitySubId() {
        ITelephony iTelephony = ITelephony.Stub.asInterface(ServiceManager.getService("phone"));
        TelephonyManager telephonyManager = TelephonyManager.getDefault();
        long subId = -1;
        /*
        if (iTelephony != null) {
            for (int i = 0; i < telephonyManager.getPhoneCount(); i++) {
                try {
                    Log.d(TAG, "get34GCapabilitySubId, iTelephony.getPhoneRat(" + i + "): "
                            + iTelephony.getPhoneRat(i));
                    if (((iTelephony.getPhoneRat(i) & (PhoneRatFamily.PHONE_RAT_FAMILY_3G
                            | PhoneRatFamily.PHONE_RAT_FAMILY_4G)) > 0)) {
                        subId = PhoneFactory.getPhone(i).getSubId();
                        Log.d(TAG, "get34GCapabilitySubId success, subId: " + subId);
                        return subId;
                    }
                } catch (RemoteException e) {
                    Log.d(TAG, "get34GCapabilitySubId FAIL to getPhoneRat i" + i +
                            " error:" + e.getMessage());
                }
            }
        }
        */
        return subId;
    }
    
    //add for forward
    public static int getSendSubid(Context  context) {
        List<SubscriptionInfo> mSubInfoList = SubscriptionManager.from(context).
                getActiveSubscriptionInfoList();
        int mSubCount = mSubInfoList.isEmpty() ? 0 : mSubInfoList.size();
        Log.v(TAG, "getSimInfoList(): mSubCount = " + mSubCount);
        if (mSubCount > 1) {
        int subIdinSetting = SubscriptionManager.getDefaultSmsSubId();
            if (subIdinSetting == Settings.System.SMS_SIM_SETTING_AUTO) {
        //getZhuKa
        int mainCardSubId = SubscriptionManager.getDefaultDataSubId();
                if (!SubscriptionManager.isValidSubscriptionId(mainCardSubId)){//data unset 
                    return mSubInfoList.get(0).getSubscriptionId();//SIM1
                }
                return mainCardSubId;
            } else {//SIM1/SIM2
                return subIdinSetting;
            }
        } else if (mSubCount == 1) {
            return mSubInfoList.get(0).getSubscriptionId();
        } else {
            return SubscriptionManager.INVALID_SUBSCRIPTION_ID;
        }
    }

//add for forward
    public static int getForwardSubid(Context context) {
        List<SubscriptionInfo> mSubInfoList = SubscriptionManager.from(context).
                getActiveSubscriptionInfoList();
        int mSubCount = mSubInfoList.isEmpty() ? 0 : mSubInfoList.size();
        Log.v(TAG, "getSimInfoList(): mSubCount = " + mSubCount);
        if (mSubCount > 1) {
            int subIdinSetting = SubscriptionManager.getDefaultSmsSubId();
            if (subIdinSetting == Settings.System.SMS_SIM_SETTING_AUTO) {
                //getZhuKa
                int mainCardSubId = SubscriptionManager.getDefaultDataSubId();
                if (!SubscriptionManager.isValidSubscriptionId(mainCardSubId)){//data unset 
                	return SubscriptionManager.INVALID_SUBSCRIPTION_ID;
                }
                return mainCardSubId;
            } else {//SIM1/SIM2
                return subIdinSetting;
            }
        } else if (mSubCount == 1) {
            return mSubInfoList.get(0).getSubscriptionId();
        } else {
            return SubscriptionManager.INVALID_SUBSCRIPTION_ID;
        }
    }
    
    public static boolean isSupportRcsForward(Context context) {
    	if (!getConfigStatus()) {
    		Log.d(TAG, "isSupprotRcsForward() result = false");
    		return false;
    	}
    	boolean result;
        int subid = RcsMessageUtils.getForwardSubid(context);
        Log.d(TAG, "subid = " + subid);
        int mainCardSubId = SubscriptionManager.getDefaultDataSubId();
        if (SubscriptionManager.isValidSubscriptionId(subid) && 
        		SubscriptionManager.isValidSubscriptionId(mainCardSubId) && subid == mainCardSubId) {
            result = true;
        } else {
            result = false;
        }
        Log.d(TAG, "isSupprotRcsForward = " + result);
        return result;
    }

    public static Intent createForwardIntentFromMms(Context context, Uri uri) {
    	if (isSupportRcsForward(context)) {
            Intent sendIntent = new Intent();
            sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
            sendIntent.setAction("android.intent.action.ACTION_RCS_MESSAGING_SEND");
            sendIntent.setType("mms/pdu");
            return sendIntent;
    	}
    	return null;
    }

    public static Intent createForwordIntentFromSms(Context context, String mBody) {
    	if (isSupportRcsForward(context)) {
    		Intent sendIntent = new Intent();
            sendIntent.setAction("android.intent.action.ACTION_RCS_MESSAGING_SEND");
            sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        	sendIntent.setType("text/plain");
        	sendIntent.putExtra(Intent.EXTRA_STREAM, mBody);
        	return sendIntent;
    	}
    	return null;
    }

    public static Intent createForwordIntentFromIpmessage(Context context, IpMessage ipMessage) {
    	if (isSupportRcsForward(context)) {
    		Intent sendIntent = new Intent();
            sendIntent.setAction("android.intent.action.ACTION_RCS_MESSAGING_SEND");
            sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (ipMessage.getType() == IpMessageType.TEXT) {
                IpTextMessage textMessage = (IpTextMessage) ipMessage;
            	sendIntent.setType("text/plain");
            	sendIntent.putExtra(Intent.EXTRA_STREAM, textMessage.getBody());
                return sendIntent;
            } else {
                if (RCSMessageServiceManager.getInstance().serviceIsReady()) {
                    IpAttachMessage attachMessage = (IpAttachMessage) ipMessage;
                    if (ipMessage.getType() == IpMessageType.PICTURE) {
                        sendIntent.putExtra(Intent.EXTRA_STREAM, attachMessage.getPath());
                        sendIntent.setType("image/jpeg");
                    } else if (ipMessage.getType() == IpMessageType.VIDEO) {
                        sendIntent.putExtra(Intent.EXTRA_STREAM, attachMessage.getPath());
                        sendIntent.setType("video/mp4");
                    } else if (ipMessage.getType() == IpMessageType.VCARD) {
                        sendIntent.putExtra(Intent.EXTRA_STREAM, attachMessage.getPath());
                        sendIntent.setType("text/x-vcard");
                    } else if (ipMessage.getType() == IpMessageType.GEOLOC) {
                        sendIntent.putExtra(Intent.EXTRA_STREAM, attachMessage.getPath());
                        sendIntent.setType("geo/*");
                    } else if (ipMessage.getType() == IpMessageType.VOICE) {
                        sendIntent.putExtra(Intent.EXTRA_STREAM, attachMessage.getPath());
                        sendIntent.setType("audio/*");
                    }
                    return sendIntent;
                } else {
                    return null;
                }
            }
    	}
    	return null;
    }

    public static int getMainCardSendCapability() {
        if (getConfigStatus()) {
            if (RCSMessageServiceManager.getInstance().serviceIsReady()) {
                Log.v(TAG, "getMainCardSendCapability(): mSendSubid SubCapatibily=7");
                return 7;//send all
            } else {
                Log.v(TAG, "getMainCardSendCapability(): mSendSubid SubCapatibily=1");
                return 1;//send Sms
            }
        } else {
            Log.v(TAG, "getMainCardSendCapability(): mSendSubid SubCapatibily=3 ");
            return 3;//send Mms\Sms
        }
    }

    public static String formatIpTimeStampString(long when, boolean fullFormat) {
        if (sCallback != null) {
            return sCallback.formatIpTimeStampString(ContextCacher.getHostContext(), when, fullFormat);
        }
        return null;
    }

    public static boolean getConfigStatus() {
    	//return GsmaManager.getInstance().getConfigurationState();
    	boolean isActive = RCSMessageServiceManager.getInstance().isActivated();
    	Log.v(TAG, "getConfigStatus: isActive" + isActive);
    	return isActive;
    }

    public static int getUserSelectedId(Context context) {
        int subIdinSetting = (int)SubscriptionManager.getDefaultSmsSubId();
        return subIdinSetting;
    }
    
    public static int getRcsSubId(Context context) {
        int rcsSubId = SubscriptionManager.getDefaultDataSubId();
        if (!SubscriptionManager.isValidSubscriptionId(rcsSubId)) {////data unset
            rcsSubId = -1;
        }
        return rcsSubId;
    }

    private static String getCurProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : am.getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }
    public static String getPhotoDstFilePath(String filePath,Context context) {
         // for choose a picture
         int index = filePath.lastIndexOf("/");
         String fileName = filePath.substring(index + 1); 
         return RcsMessageConfig.getPicTempPath(context) + File.separator + fileName;
    }

    public static String getPhotoDstFilePath(Context context) {
         // only for take photo
         String fileName = System.currentTimeMillis() + ".jpg";
         return RcsMessageConfig.getPicTempPath(context) + File.separator + fileName;
    }

    public static String getVideoDstFilePath(String filePath, Context context) {
         // for choose a video
         int index = filePath.lastIndexOf("/");
         String fileName = filePath.substring(index + 1); 
         return RcsMessageConfig.getVideoTempPath(context) + File.separator + fileName;
    }

    public static String getVideoDstFilePath(Context context) {
         // only for record a video
         String fileName = System.currentTimeMillis() + ".3gp";
         return RcsMessageConfig.getVideoTempPath(context) + File.separator + fileName;
    }

    public static String getAudioDstPath(String filePath, Context context) {
        int index = filePath.lastIndexOf("/");
        String fileName = filePath.substring(index + 1);
        return RcsMessageConfig.getAudioTempPath(context) + File.separator + fileName;
    }

    public static String getGeolocPath(Context context) {
        return RcsMessageConfig.getGeolocTempPath(context) + File.separator;
    }

    public static void copyFileToDst(String src, String dst) {
       if (src == null || dst == null) {
            return;
       }
       RcsMessageUtils.copy(src,dst);
    }
}
