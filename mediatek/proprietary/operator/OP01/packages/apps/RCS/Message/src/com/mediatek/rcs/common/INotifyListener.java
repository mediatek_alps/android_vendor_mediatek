package com.mediatek.rcs.common;

import android.content.Intent;

public interface INotifyListener {

    void notificationsReceived(Intent intent);
}
