package com.mediatek.rcs.common.binder;

import java.util.List;

import org.gsma.joyn.chat.ChatLog;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.provider.Telephony.Mms;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Threads;

import com.mediatek.rcs.common.GroupManager;
import com.mediatek.rcs.common.IBurnMessageCapabilityListener;
import com.mediatek.rcs.common.INotifyListener;
import com.mediatek.rcs.common.IpMessageConsts;
import com.mediatek.rcs.common.RCSMessageManager;
import com.mediatek.rcs.common.RCSMessageNotificationsManager;
import com.mediatek.rcs.common.binder.RCSServiceManager.GroupInfo;
import com.mediatek.rcs.common.provider.GroupMemberData;
import com.mediatek.rcs.common.provider.RCSDataBaseUtils;
import com.mediatek.rcs.common.provider.ThreadMapCache;
import com.mediatek.rcs.common.provider.ThreadMapCache.MapInfo;
import com.mediatek.rcs.common.service.IRCSChatService;
import com.mediatek.rcs.common.service.IRCSChatServiceListener;
import com.mediatek.rcs.common.service.Participant;
import com.mediatek.rcs.common.utils.ContextCacher;
import com.mediatek.rcs.common.utils.Logger;
import com.mediatek.rcs.common.utils.RCSUtils;
import com.mediatek.rcs.common.MessageStatusUtils.IFileTransfer.Status;

import android.telephony.PhoneNumberUtils;
import android.util.Log;
import com.mediatek.rcs.common.RCSCacheManager;

public class RCSChatServiceListener extends IRCSChatServiceListener.Stub {


    public RCSChatServiceListener() {
    }

    private static final String TAG = "RCSChatServiceListener";

    public void onNewMessage(long msgId) throws RemoteException {
        Logger.d(TAG, "onNewMessage, msgId=" + msgId);
        long threadId = 0;
        long ipmsgId = 0;
        String number = null;
        Cursor cursor = getMessageInfo(msgId);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                threadId = cursor.getLong(cursor.getColumnIndex(Sms.THREAD_ID));
                ipmsgId = cursor.getLong(cursor.getColumnIndex(Sms.IPMSG_ID));
                number = cursor.getString(cursor.getColumnIndex(Sms.ADDRESS));
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Logger.d(TAG, "notifyNewMessage, entry: threadId=" + threadId + ", ipmsgId=" + ipmsgId);
        notifyNewMessage(threadId, ipmsgId, number);
    }

    public void onNewGroupMessage(String chatId, long msgId, String number)
            throws RemoteException {
        Logger.d(TAG, "onNewGroupMessage, chatId=" + chatId);
        MapInfo info = ThreadMapCache.getInstance().getInfoByChatId(chatId);
        long threadId = 0;
        if (info != null) {
            threadId = info.getThreadId();
        }
        long ipmsgId = 0;
        Cursor cursor = getMessageInfo(msgId);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                ipmsgId = cursor.getLong(cursor.getColumnIndex(Sms.IPMSG_ID));
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        ContentResolver resolver = ContextCacher.getHostContext().getContentResolver();
        ContentValues values = new ContentValues();
        values.put(Sms.THREAD_ID, threadId);
        Uri uri = ContentUris.withAppendedId(Sms.CONTENT_URI, msgId);
        resolver.update(uri, values, null, null);
        Logger.d(TAG, "notifyNewMessage, entry: threadId=" + threadId + ", ipmsgId=" + ipmsgId);
        notifyNewMessage(threadId, ipmsgId, number);
    }

    private void notifyNewMessage(long threadId, long ipmsgId, String number) {
        Intent intent = new Intent();
        intent.setAction(IpMessageConsts.MessageAction.ACTION_NEW_MESSAGE);
        intent.putExtra(IpMessageConsts.MessageAction.KEY_THREAD_ID, threadId);
        intent.putExtra(IpMessageConsts.MessageAction.KEY_IPMSG_ID, ipmsgId);
        intent.putExtra(IpMessageConsts.MessageAction.KEY_NUMBER, number);
        notifyAllListeners(intent);
        // TODO :should remove this
        RCSMessageNotificationsManager.getInstance(ContextCacher.getPluginContext()).notify(intent);

    }

    private Cursor getMessageInfo(long smsId) {
        Uri uri = ContentUris.withAppendedId(Sms.CONTENT_URI, smsId);
        ContentResolver resolver = ContextCacher.getHostContext().getContentResolver();
        String[] projection = {Sms.THREAD_ID, Sms.ADDRESS, Sms.BODY, Sms.IPMSG_ID};
        return resolver.query(uri, projection, null, null, null);
    }

    private boolean getRequestDeliveryReport(long subId) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ContextCacher.getHostContext());
        return prefs.getBoolean(subId + "_" + RCSUtils.SMS_DELIVERY_REPORT_MODE,
                RCSUtils.DEFAULT_DELIVERY_REPORT_MODE);
    }

    public void onSendO2OMessageFailed(long msgId) throws RemoteException {
        // TODO: 
        boolean sendBySms = true;
        Uri uri = ContentUris.withAppendedId(Sms.CONTENT_URI, msgId);
        if (!sendBySms) {
            RCSUtils.updateSmsBoxType(uri, Sms.MESSAGE_TYPE_FAILED);
            return;
        }
        boolean requestDeliveryReport = getRequestDeliveryReport(RCSUtils.getRCSSubId());
        Logger.d(TAG, "SMS DR request=" + requestDeliveryReport);
        long ipmsgId = 0;
        String body = null;
        Cursor cursor = getMessageInfo(msgId);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                ipmsgId = cursor.getLong(cursor.getColumnIndex(Sms.IPMSG_ID));
                body = cursor.getString(cursor.getColumnIndex(Sms.BODY));
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        if (body.length() > RCSUtils.MAX_LENGTH_OF_TEXT_MESSAGE) {
            RCSUtils.updateSmsBoxType(uri, Sms.MESSAGE_TYPE_FAILED);
            return;
        }
        ContentValues cv = new ContentValues();
        cv.put(Sms.TYPE, Sms.MESSAGE_TYPE_QUEUED);
        cv.put(Sms.IPMSG_ID, 0);
        if (requestDeliveryReport) {
            cv.put(Sms.STATUS, Sms.STATUS_PENDING);
        }
        ContentResolver resolver = ContextCacher.getHostContext().getContentResolver();
        resolver.update(uri, cv, null, null);
        
        Intent intent = new Intent(RCSUtils.ACTION_SEND_MESSAGE);
        intent.setClassName("com.android.mms", "com.android.mms.transaction.SmsReceiver");
        ContextCacher.getPluginContext().sendBroadcast(intent);
        RCSMessageManager.getInstance(ContextCacher.getPluginContext()).deleteStackMessage(ipmsgId);
    }

    public void onSendO2MMessageFailed(long msgId) throws RemoteException {
        // TODO: 
        boolean sendBySms = true;
        Uri uri = ContentUris.withAppendedId(Sms.CONTENT_URI, msgId);
        if (!sendBySms) {
            RCSUtils.updateSmsBoxType(uri, Sms.MESSAGE_TYPE_FAILED);
            return;
        }

        boolean requestDeliveryReport = getRequestDeliveryReport(RCSUtils.getRCSSubId());
        Logger.d(TAG, "SMS DR request=" + requestDeliveryReport);
        String messageContent = null;
        long threadId = 0;
        String recipients = null;
        long ipmsgId = 0;
        Cursor cursor = getMessageInfo(msgId);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                messageContent = cursor.getString(cursor.getColumnIndex(Sms.BODY));
                threadId = cursor.getLong(cursor.getColumnIndex(Sms.THREAD_ID));
                recipients = cursor.getString(cursor.getColumnIndex(Sms.ADDRESS));
                ipmsgId = cursor.getLong(cursor.getColumnIndex(Sms.IPMSG_ID));
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        if (messageContent.length() > RCSUtils.MAX_LENGTH_OF_TEXT_MESSAGE) {
            RCSUtils.updateSmsBoxType(uri, Sms.MESSAGE_TYPE_FAILED);
            return;
        }
        ContentResolver resolver = ContextCacher.getHostContext().getContentResolver();
        String[] contacts = recipients.split(RCSMessageManager.COMMA);
        for (int i = 0; i < contacts.length; i++) {
            Sms.addMessageToUri(RCSUtils.getRCSSubId(),
                    resolver,
                    Uri.parse("content://sms/queued"), contacts[i],
                    messageContent, null, System.currentTimeMillis(),
                    true /* read */,
                    requestDeliveryReport,
                    threadId);
        }
        // delete message in mms db
        resolver.delete(uri, null, null);
        // send broadcast to notify SmsReceiverService
        Intent intent = new Intent();
        intent.setAction(RCSUtils.ACTION_SEND_MESSAGE);
        intent.setClassName("com.android.mms", "com.android.mms.transaction.SmsReceiver");
        ContextCacher.getPluginContext().sendBroadcast(intent);
        RCSMessageManager.getInstance(ContextCacher.getPluginContext()).deleteStackMessage(ipmsgId);
    }

    public void onSendGroupMessageFailed(long msgId) throws RemoteException {
        Logger.d(TAG, "sendGroupMessageFailed, msgId=" + msgId);
        Uri uri = ContentUris.withAppendedId(Sms.CONTENT_URI, msgId);
        RCSUtils.updateSmsBoxType(uri, Sms.MESSAGE_TYPE_FAILED);
    }

    public void onParticipantJoined(String chatId, Participant participant)
            throws RemoteException {
        Logger.d(TAG, "onParticipantJoined, chatId=" + chatId + ", " + participant.getContact());
        updateGroupAddress(chatId);
        notifyParticipantOperation(chatId, IpMessageConsts.GroupActionList.VALUE_PARTICIPANT_JOIN, participant);
    }

    public void onParticipantLeft(String chatId, Participant participant)
            throws RemoteException {
        Logger.d(TAG, "onParticipantLeft, chatId=" + chatId + ", " + participant.getContact());
        updateGroupAddress(chatId);
        notifyParticipantOperation(chatId, IpMessageConsts.GroupActionList.VALUE_PARTICIPANT_LEFT, participant);
    }

    public void onParticipantRemoved(String chatId, Participant participant)
            throws RemoteException {
        Logger.d(TAG, "onParticipantRemoved, chatId=" + chatId + ", " + participant.getContact());
        updateGroupAddress(chatId);
        notifyParticipantOperation(chatId, IpMessageConsts.GroupActionList.VALUE_PARTICIPANT_REMOVED, participant);
    }

    public void onChairmenChanged(String chatId, Participant participant, boolean isMe)
            throws RemoteException {
        Logger.d(TAG, "onChairmenChanged, chatId=" + chatId + ", " + participant.getContact());
        ThreadMapCache.getInstance().updateIsMeChairmenByChatId(chatId, isMe);
        notifyParticipantOperation(chatId, IpMessageConsts.GroupActionList.VALUE_CHAIRMEN_CHANGED, participant);
    }

    public void onSubjectModified(String chatId, String subject)
            throws RemoteException {
        Logger.d(TAG, "onSubjectModified, chatId=" + chatId + ", subject=" + subject);
        ThreadMapCache.getInstance().updateSubjectByChatId(chatId, subject);
        notifyStringOperation(chatId, IpMessageConsts.GroupActionList.VALUE_SUBJECT_MODIFIED,
                              IpMessageConsts.GroupActionList.KEY_SUBJECT, subject);
    }

    public void onParticipantNickNameModified(String chatId, String contact,
            String nickName) throws RemoteException {
        Logger.d(TAG, "onParticipantNickNameChanged, contact=" + contact + ", nickName=" + nickName);
        Intent intent = getNotifyIntent(chatId, IpMessageConsts.GroupActionList.ACTION_GROUP_NOTIFY,
                                        IpMessageConsts.GroupActionList.VALUE_PARTICIPANT_NICKNAME_MODIFIED);
        intent.putExtra(IpMessageConsts.GroupActionList.KEY_CONTACT_NUMBER, contact);
        intent.putExtra(IpMessageConsts.GroupActionList.KEY_NICK_NAME, nickName);
        notifyAllListeners(intent);
        // TODO : should remove this
        RCSMessageNotificationsManager.getInstance(ContextCacher.getPluginContext()).notify(intent);
    }

    public void onMeRemoved(String chatId, String contact)
            throws RemoteException {
        Logger.d(TAG, "onMeRemoved, contact = " + contact);
        markGroupUnavailable(chatId);
        notifyStringOperation(chatId,
                              IpMessageConsts.GroupActionList.VALUE_ME_REMOVED,
                              IpMessageConsts.GroupActionList.KEY_CONTACT_NUMBER, contact);
    }

    public void onAbort(String chatId) throws RemoteException {
        Logger.d(TAG, "onAbort, chatId=" + chatId);
        markGroupUnavailable(chatId);

        notifyNullParamOperation(chatId, IpMessageConsts.GroupActionList.VALUE_GROUP_ABORTED);
    }

    public void onNewInvite(Participant participant, String subject,
            String chatId) throws RemoteException {
        //TODO
        MapInfo info = ThreadMapCache.getInstance().getInfoByChatId(chatId);
        long threadId = 0;
        if (info != null) {
            threadId = info.getThreadId();
            if (threadId == RCSMessageManager.DELETED_THREAD_ID) {
                threadId = createGroupThread(participant, true);
                ThreadMapCache.getInstance().updateThreadId(chatId, threadId);
                updateGroupStatus(chatId, IpMessageConsts.GroupActionList.GROUP_STATUS_INVITING);
            } else {
                updateGroupStatus(chatId, IpMessageConsts.GroupActionList.GROUP_STATUS_INVITING_AGAIN);
            }
            ThreadMapCache.getInstance().updateSubjectByChatId(chatId, subject);
        } else {
            threadId = createGroupThread(participant, true);
            ThreadMapCache.getInstance().addMapInfo(threadId, chatId, subject,
                             IpMessageConsts.GroupActionList.GROUP_STATUS_INVITING, false);
        }
        Logger.d(TAG, "onNewInvite, receive new invite, chatId=" + chatId + ", threadId=" + threadId);

        String name = RCSDataBaseUtils.getContactDisplayName(ContextCacher.getPluginContext(), participant.getContact());
        Participant contact = new Participant(participant.getContact(), name);
        Intent intent = getNotifyIntent(chatId, IpMessageConsts.GroupActionList.ACTION_GROUP_NOTIFY,
                                        IpMessageConsts.GroupActionList.VALUE_NEW_INVITE_RECEIVED);
        intent.putExtra(IpMessageConsts.GroupActionList.KEY_PARTICIPANT, contact);
        intent.putExtra(IpMessageConsts.GroupActionList.KEY_SUBJECT, subject);
        notifyAllListeners(intent);
        // TODO : should remove this
        RCSMessageNotificationsManager.getInstance(ContextCacher.getPluginContext()).notify(intent);
    }

    public void onInvitationTimeout(String chatId) throws RemoteException {
        Logger.d(TAG, "onInvitationTimeout, chatId=" + chatId);
        markGroupInvitationTimeOut(chatId);
    }


    public void onAddParticipantFail(Participant participant, String chatId)
            throws RemoteException {
        Logger.d(TAG, "onAddParticipantFail, number=" + participant.getContact() + ", chatId=" + chatId);
        notifyParticipantOperation(chatId, IpMessageConsts.GroupActionList.VALUE_ADD_PARTICIPANT_FAIL, participant);
    }

    public void onAddParticipantsResult(String chatId, boolean result)
            throws RemoteException {
        Logger.d(TAG, "onAddParticipantsResult, result=" + result + ", chatId=" + chatId);
        notifyResultOperation(chatId, IpMessageConsts.GroupActionList.VALUE_ADD_PARTICIPANTS, result);
    }

    public void onRemoveParticipantsResult(String chatId, boolean result)
            throws RemoteException {
        Logger.d(TAG, "onRemoveParticipantsResult, result=" + result + ", chatId=" + chatId);
        notifyResultOperation(chatId, IpMessageConsts.GroupActionList.VALUE_REMOVE_PARTICIPANTS, result);
    }

    public void onTransferChairmenResult(String chatId, boolean result)
            throws RemoteException {
        Logger.d(TAG, "onTransferChairmenResult, result = " + result + ", chatId=" + chatId);
        if (result) {
            ThreadMapCache.getInstance().updateIsMeChairmenByChatId(chatId, false);
        }
        notifyResultOperation(chatId, IpMessageConsts.GroupActionList.VALUE_TRANSFER_CHAIRMEN, result);
    }

    public void onMyNickNameModifiedResult(String chatId, boolean result)
            throws RemoteException {
        Logger.d(TAG, "onMyNickNameModifiedResult, result = " + result + ", chatId=" + chatId);
        String myNickName = getMyNickName(chatId);
        
        notifyResultOperationWithStringArg(chatId, IpMessageConsts.GroupActionList.VALUE_MODIFY_SELF_NICK_NAME, result
                , IpMessageConsts.GroupActionList.KEY_SELF_NICK_NAME, myNickName);
    }

    public void onSubjectModifiedResult(String chatId, boolean result)
            throws RemoteException {
        Logger.d(TAG, "onSubjectModifiedResult, result=" + result + ", chatId=" + chatId);
        String subject = getSubject(chatId);
        ThreadMapCache.getInstance().updateSubjectByChatId(chatId, subject);
        notifyResultOperationWithStringArg(chatId, IpMessageConsts.GroupActionList.VALUE_MODIFY_SUBJECT, result,
                    IpMessageConsts.GroupActionList.KEY_SUBJECT, subject);
    }

    public void onQuitConversationResult(String chatId, boolean result)
            throws RemoteException {
        Logger.d(TAG, "onQuitConversationResult, result=" + result + ", chatId=" + chatId);
        if (result) {
            markGroupUnavailable(chatId);
        }
        notifyResultOperation(chatId, IpMessageConsts.GroupActionList.VALUE_EXIT_GROUP, result);
    }

    public void onAbortResult(String chatId, boolean result)
            throws RemoteException {
        Logger.d(TAG, "onAbortResult, result=" + result + ", chatId=" + chatId);
        if (result) {
            markGroupUnavailable(chatId);
        }
        notifyResultOperation(chatId, IpMessageConsts.GroupActionList.VALUE_DESTROY_GROUP, result);
    }

    public void onInitGroupResult(boolean result, String chatId)
            throws RemoteException {
        Logger.d(TAG, "onInitGroupResult, result=" + result + ", chatId=" + chatId);
        String subject = null;
        List<String> participants = null;
        GroupInfo groupInfo = RCSServiceManager.getInstance().getInvitingGroup(chatId);
        if (groupInfo != null) {
            subject = groupInfo.getSubject();
            participants = groupInfo.getParticipants();
        }
        if (result) {
            long threadId = createGroupThread(null, false);
            Logger.d(TAG, "onInitGroupResult, threadId=" + threadId + ", chatId=" + chatId);
            if (threadId > 0) {
                ThreadMapCache.getInstance().addMapInfo(threadId, chatId, subject, RCSUtils.getRCSSubId(), true);
            }
        }
        RCSServiceManager.getInstance().removeInvitingGroup(chatId);
        notifyInitGroupResult(chatId, IpMessageConsts.GroupActionList.VALUE_INIT_GROUP, result, participants);
    }

    public void onRequestBurnMessageCapabilityResult(String contact, boolean result)
            throws RemoteException {
    	Log.d(TAG, "onRequestBurnMessageCapabilityResult contact = " + contact+ " result = " + result);
        List<IBurnMessageCapabilityListener> listeners = RCSServiceManager.getInstance().getBurnMsgCapListeners();
        for (IBurnMessageCapabilityListener listener : listeners) {
            listener.onRequestBurnMessageCapabilityResult(contact, result);
        }
    }

    public void onAcceptInvitationResult(String chatId, boolean result)
            throws RemoteException {
        // TODO Auto-generated method stub
        long status = result ? RCSUtils.getRCSSubId() : IpMessageConsts.GroupActionList.GROUP_STATUS_ACCEPT_FAIL;
        updateGroupStatus(chatId, status);
        notifyResultOperation(chatId, IpMessageConsts.GroupActionList.VALUE_ACCEPT_GROUP_INVITE, result);
    }

    public void onRejectInvitationResult(String chatId, boolean result)
            throws RemoteException {
        // TODO Auto-generated method stub
        if (result) {
            updateGroupStatus(chatId, IpMessageConsts.GroupActionList.GROUP_STATUS_INVALID);
        }
        notifyResultOperation(chatId, IpMessageConsts.GroupActionList.VALUE_REJECT_GROUP_INVITE, result);
    }

    public void onUpdateFileTransferStatus(long ipMsgId, int rcsStatus, int smsStatus) {
        Log.d(TAG,"onUpdateFileTransferStatus , ipMsgId = " + -ipMsgId + " rcsStatus = " + rcsStatus + " smsStatus " + smsStatus);
        Status stat = RCSUtils.getRcsStatus(rcsStatus);
        Intent it = new Intent();
        it.setAction(IpMessageConsts.IpMessageStatus.ACTION_MESSAGE_STATUS);
        it.putExtra(IpMessageConsts.STATUS, stat);
        it.putExtra(IpMessageConsts.IpMessageStatus.IP_MESSAGE_ID, -ipMsgId);
        RCSMessageNotificationsManager.getInstance(ContextCacher.getPluginContext()).notify(it);
        RCSCacheManager.updateStatus(-ipMsgId,stat,smsStatus);
    }

    private int updateGroupAddress(String chatId) {
        Logger.d(TAG, "updateGroupAddress, entry");
        List<String> participants = GroupManager.getInstance(ContextCacher.getPluginContext()).getGroupParticipants(chatId);
        long threadId = 0;
        MapInfo info = ThreadMapCache.getInstance().getInfoByChatId(chatId);
        if (info != null) {
            threadId = info.getThreadId();
        }
        Uri.Builder uriBuilder = RCSUtils.MMS_SMS_URI_GROUP_ADDRESS.buildUpon();
        String recipient = null;
        for (String participant : participants) {
            if (Mms.isEmailAddress(participant)) {
                recipient = Mms.extractAddrSpec(recipient);
            }
            Logger.d(TAG, "recipient = " + recipient);
            uriBuilder.appendQueryParameter("recipient", recipient);
        }
        return ContextCacher.getHostContext().getContentResolver().update(
                ContentUris.withAppendedId(uriBuilder.build(), threadId), new ContentValues(1), null, null);
    }

    private void notifyInitGroupResult(String chatId, int actionType, boolean result, List<String> participants) {
        Logger.d(TAG, "notifyInitGroupResult, entry: actionType = " + actionType + ", result=" + result);
        int success = (result == true) ? IpMessageConsts.GroupActionList.VALUE_SUCCESS : IpMessageConsts.GroupActionList.VALUE_FAIL;
        Intent intent = getNotifyIntent(chatId, IpMessageConsts.GroupActionList.ACTION_GROUP_OPERATION_RESULT, actionType);
        intent.putExtra(IpMessageConsts.GroupActionList.KEY_ACTION_RESULT, success);
        String[] parts = new String[participants.size()];
        int i = 0;
        for (String participant : participants) {
            parts[i++] = RCSDataBaseUtils.getContactDisplayName(ContextCacher.getPluginContext(), participant);
        }
        intent.putExtra(IpMessageConsts.GroupActionList.KEY_PARTICIPANT_LIST, parts/*(String[])participants.toArray()*/);
        notifyAllListeners(intent);
        // TODO :should remove this
        RCSMessageNotificationsManager.getInstance(ContextCacher.getPluginContext()).notify(intent);
    }

    private void notifyParticipantOperation(String chatId, int actionType, Participant participant) {
        Logger.d(TAG, "notifyParticipantOperation, entry: actionType = " + actionType);
        Intent intent = getNotifyIntent(chatId, IpMessageConsts.GroupActionList.ACTION_GROUP_NOTIFY, actionType);
        String name = RCSDataBaseUtils.getContactDisplayName(ContextCacher.getPluginContext(), participant.getContact());
        Participant contact = new Participant(participant.getContact(), name);
        intent.putExtra(IpMessageConsts.GroupActionList.KEY_PARTICIPANT, contact);
        notifyAllListeners(intent);
        // TODO : should remove this
        RCSMessageNotificationsManager.getInstance(ContextCacher.getPluginContext()).notify(intent);
    }

    private Intent getNotifyIntent(String chatId, String action, int actionType) {
        long threadId = 0;
        MapInfo info = ThreadMapCache.getInstance().getInfoByChatId(chatId);
        if (info != null) {
            threadId = info.getThreadId();
        }
        Intent intent = new Intent();
        intent.setAction(action);
        intent.putExtra(IpMessageConsts.GroupActionList.KEY_ACTION_TYPE, actionType);
        intent.putExtra(IpMessageConsts.GroupActionList.KEY_CHAT_ID, chatId);
        intent.putExtra(IpMessageConsts.GroupActionList.KEY_THREAD_ID, threadId);
        return intent;
    }

    private void notifyStringOperation(String chatId, int actionType, String key, String value) {
        Logger.d(TAG, "notifyStringOperation, entry: actionType = " + actionType + ", key=" + key + ", value=" + value);
        Intent intent = getNotifyIntent(chatId, IpMessageConsts.GroupActionList.ACTION_GROUP_NOTIFY, actionType);
        intent.putExtra(key, value);
        notifyAllListeners(intent);
        // TODO : should remove this
        RCSMessageNotificationsManager.getInstance(ContextCacher.getPluginContext()).notify(intent);
    }

    private void markGroupUnavailable(String chatId) {
        Logger.d(TAG, "markGroupUnavailable, chatId=" + chatId);
        updateGroupStatus(chatId, IpMessageConsts.GroupActionList.GROUP_STATUS_INVALID);
        Intent intent = new Intent("com.mediatek.rcs.groupchat.STATE_CHANGED");
        intent.putExtra("chat_id", chatId);
        ContextCacher.getPluginContext().sendBroadcast(intent);
    }

    private void updateGroupStatus(String chatId, long status) {
        Logger.d(TAG, "updateGroupStatus, chatId=" + chatId + ", status=" + status);
        ContentValues values = new ContentValues();
        values.put(Threads.STATUS, status);
        MapInfo info = ThreadMapCache.getInstance().getInfoByChatId(chatId);
        if (info != null) {
            long threadId = info.getThreadId();
            Uri uri = ContentUris.withAppendedId(RCSUtils.URI_THREADS_UPDATE_STATUS, threadId);
            ContextCacher.getHostContext().getContentResolver().update(uri, values, null, null);
            ThreadMapCache.getInstance().updateStatusByChatId(chatId, status);
        } else {
            Logger.e(TAG, "error: can not find threadId, maybe no invite come to AP");
        }
    }

    private void notifyNullParamOperation(String chatId, int actionType) {
        Logger.d(TAG, "notifyNullParamOperation, entry: actionType = " + actionType);
        Intent intent = getNotifyIntent(chatId, IpMessageConsts.GroupActionList.ACTION_GROUP_NOTIFY, actionType);
        notifyAllListeners(intent);
        // TODO : should remove this
        RCSMessageNotificationsManager.getInstance(ContextCacher.getPluginContext()).notify(intent);
    }

    private long createGroupThread(Participant participant, boolean fromInvite) {
        Logger.d(TAG, "createGroupThread, participant = " + participant + ", fromInvite= " + fromInvite);
        String contact = null;
        Uri.Builder builder = RCSUtils.MMS_SMS_URI_ADD_THREAD.buildUpon();
        if (participant != null) {
            contact = participant.getContact();
            Logger.d(TAG, "createGroupThread, participant number = " + contact);
            builder.appendQueryParameter("recipient", contact);
        }
        ContentValues values = new ContentValues();
        if (fromInvite) {
            values.put(Threads.STATUS, IpMessageConsts.GroupActionList.GROUP_STATUS_INVITING);
        } else {
            values.put(Threads.STATUS, RCSUtils.getRCSSubId());
        }
        Uri result = ContextCacher.getHostContext().getContentResolver().insert(
                builder.build(), values);
        Logger.d(TAG, "createGroupThread, uri=" + result);
        return Long.parseLong(result.getLastPathSegment());
    }

    private void markGroupInvitationTimeOut(String chatId) {
        Logger.d(TAG, "markGroupInvitationTimeOut, chatId= " + chatId);
        updateGroupStatus(chatId, IpMessageConsts.GroupActionList.GROUP_STATUS_INVITE_EXPAIRED);
    }

    private void notifyResultOperation(String chatId, int actionType, boolean result) {
        Logger.d(TAG, "notifyResultOperation, entry: actionType = " + actionType + ", result=" + result);
        int success = (result == true) ? IpMessageConsts.GroupActionList.VALUE_SUCCESS : IpMessageConsts.GroupActionList.VALUE_FAIL;
        Intent intent = getNotifyIntent(chatId, IpMessageConsts.GroupActionList.ACTION_GROUP_OPERATION_RESULT, actionType);
        intent.putExtra(IpMessageConsts.GroupActionList.KEY_ACTION_RESULT, success);
        notifyAllListeners(intent);
        // TODO :should remove this
        RCSMessageNotificationsManager.getInstance(ContextCacher.getPluginContext()).notify(intent);
    }

    private void notifyResultOperationWithStringArg(String chatId, int actionType, boolean result, String key, String value) {
        Logger.d(TAG, "notifyResultOperationWithStringArg, entry: actionType = " + actionType + ", result=" + result);
        int success = (result == true) ? IpMessageConsts.GroupActionList.VALUE_SUCCESS : IpMessageConsts.GroupActionList.VALUE_FAIL;
        Intent intent = getNotifyIntent(chatId, IpMessageConsts.GroupActionList.ACTION_GROUP_OPERATION_RESULT, actionType);
        intent.putExtra(key, value);
        intent.putExtra(IpMessageConsts.GroupActionList.KEY_ACTION_RESULT, success);
        notifyAllListeners(intent);
        // TODO :should remove this
        RCSMessageNotificationsManager.getInstance(ContextCacher.getPluginContext()).notify(intent);
    }
    
    private void notifyAllListeners(Intent intent) {
        List<INotifyListener> listeners = RCSServiceManager.getInstance().getNotifyListeners();
        for (INotifyListener listener : listeners) {
            listener.notificationsReceived(intent);
        }
    }

    private String getSubject(String chatId) {
        Logger.d(TAG, "getSubject, chatId=" + chatId);
        String chatSelection = ChatLog.GroupChat.CHAT_ID + "='" + chatId + "'";
        Cursor chatCursor = null;
        try {
            chatCursor = ContextCacher.getPluginContext().getContentResolver().query(
                    RCSUtils.RCS_URI_GROUP_CHAT, RCSUtils.PROJECTION_GROUP_INFO, chatSelection, null, null);
            if (chatCursor.moveToFirst()) {
                return chatCursor.getString(chatCursor.getColumnIndex(ChatLog.GroupChat.SUBJECT));
            }
        } finally {
            if (chatCursor != null) {
                chatCursor.close();
            }
        }
        return null;
    }

    private String getMyNickName(String chatId) {
        String memberSelection = GroupMemberData.COLUMN_CHAT_ID + "='" + chatId + "'";
        Cursor cursor = ContextCacher.getPluginContext().getContentResolver().query(
                RCSUtils.RCS_URI_GROUP_MEMBER, RCSUtils.PROJECTION_GROUP_MEMBER, memberSelection, null, null);
        String myNickName = null;
        IRCSChatService service = RCSServiceManager.getInstance().getChatService();
        String myNumber = null;
        try {
            myNumber = service.getMSISDN();

        } catch (RemoteException e) {
            e.printStackTrace();
        }
        try {
            while (cursor.moveToNext()) {
                String number = cursor.getString(cursor.getColumnIndex(GroupMemberData.COLUMN_CONTACT_NUMBER));
                String name = cursor.getString(cursor.getColumnIndex(GroupMemberData.COLUMN_CONTACT_NAME));
                if (PhoneNumberUtils.compare(number, myNumber)) {
                    myNickName = name;
                    break;
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return myNickName;
    }

}
