/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.rcs.common;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.util.Vector;

import com.mediatek.rcs.common.utils.Logger;

import org.gsma.joyn.ft.FileTransferService;
import org.gsma.joyn.chat.ChatService;
import org.gsma.joyn.JoynServiceException;
import org.gsma.joyn.chat.SpamReportListener;
import org.gsma.joyn.ft.FileSpamReportListener;

/**
 * Ip notification manager
 */
public class RCSMessageNotificationsManager {
    private static final String TAG = "RCSMessageNotificationsManager";
    private static final Object LISTENER_SYNC = new Object();
Context mContext;
    /**
     * IP message notification listeners
     */
    private static Vector<INotificationsListener> sNotificationsListeners = new Vector<INotificationsListener>();
    private static RCSMessageNotificationsManager sInstance;
    private RCSMessageNotificationsManager(Context context) {
    	mContext = context;
    }
    
    static public  RCSMessageNotificationsManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new RCSMessageNotificationsManager(context);
        }
        return sInstance;
    }

    public void registerNotificationsListener(INotificationsListener notiListener) {
        synchronized (LISTENER_SYNC) {
            Log.d(TAG, "registerNotificationsListener,listener = " + notiListener);
            if (sNotificationsListeners.contains(notiListener)) {
                Log.d(TAG, "registerNotificationsListener, already contains.");
                return;
            }
            sNotificationsListeners.addElement(notiListener);
            int num = sNotificationsListeners.size();
            Log.d(TAG, "after add, num = " + num);
        }
    }

    public void unregisterNotificationsListener(INotificationsListener notiListener) {
        synchronized (LISTENER_SYNC) {
            Log.d(TAG, "unregisterNotificationsListener, listener = " + notiListener);
            if (!sNotificationsListeners.contains(notiListener)) {
                Log.d(TAG, "unregisterNotificationsListener, do not contain !!");
                return;
            }
            sNotificationsListeners.removeElement(notiListener);
            int num = sNotificationsListeners.size();
            Log.d(TAG, "after remove, num = " + num);
        }
    }

    public void registerSpamNotificationsListener() {
/*        synchronized (LISTENER_SYNC) {
            Log.d(TAG, "registerSpamNotificationsListener,listener ");
            try {
           	 ChatService chatService = GsmaManager.getInstance().getChatApi();
                TextSpamReportListener textListener = new TextSpamReportListener();
        		 if (chatService != null) {
        			 chatService.addSpamReportListener(textListener);
        		 }
        		 FileTransferService fileTransferService = GsmaManager.getInstance().getFileTransferApi();
        		 MultiSpamReportListener fileListener = new MultiSpamReportListener();
        		 if (fileTransferService != null) {
        			 fileTransferService.addFileSpamReportListener(fileListener);
        		 }
           } catch (JoynServiceException e1) {
               // TODO Auto-generated catch block
               e1.printStackTrace();
           }
        }*/
    }

    public void unregisterSpamNotificationsListener() {
/*        synchronized (LISTENER_SYNC) {
            Log.d(TAG, "unregisterSpamNotificationsListener, listener ");
            try {
                ChatService chatService = GsmaManager.getInstance().getChatApi();
                TextSpamReportListener textListener = new TextSpamReportListener();
        		 if (chatService != null) {
        			 chatService.removeSpamReportListener(textListener);
        		 }
        		 FileTransferService fileTransferService = GsmaManager.getInstance().getFileTransferApi();
        		 MultiSpamReportListener fileListener = new MultiSpamReportListener();
        		 if (fileTransferService != null) {
        			 fileTransferService.removeFileSpamReportListener(fileListener);
        		 }
            } catch (JoynServiceException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }*/
    }
    
    
    /**
     * Notify listeners when ip message notifications received
     */
    public void notify(Intent intent) {
        synchronized (LISTENER_SYNC) {
            final int num = sNotificationsListeners.size();
            Log.d(TAG, "notify(), num = " + num);
            INotificationsListener iNotiListener = null;
            for (int i = 0; i < num; i++) {
                iNotiListener = (INotificationsListener) sNotificationsListeners.elementAt(i);
                Log.d(TAG, "notify(), notification listener = " + iNotiListener);
                iNotiListener.notificationsReceived(intent);
            }
        }
    }
    
   private class TextSpamReportListener extends SpamReportListener {
    	
    	
		public TextSpamReportListener() {
		}
		
		/**
		 * Callback called when a text spam report message has been delivered Successfully
		 * 
		 * @param contact Contact
		 * @param msgId Message ID
		 */
		public void onSpamReportSuccess(String contact, String msgId) {
			Logger.d(TAG, "spam-report: Text  onSpamReportSuccess() contact "+ contact + "  msgId:" + msgId);
            Toast.makeText(mContext, "Text report success", Toast.LENGTH_SHORT).show();
		}
		
		/**
		 * Callback called when a text spam report message has been delivered failed
		 * 
		 * @param contact Contact
		 * @param msgId Message ID
		 * @param errorCode Error code
		 */
	    public void onSpamReportFailed( String contact, String msgId, int errorCode){
	    	Logger.d(TAG, "spam-report: Text  onSpamReportFailed() contact "+ contact + "  msgId:" + msgId + "errorCode " + errorCode);
            Toast.makeText(mContext, "Text report failed", Toast.LENGTH_SHORT).show();
	    }

    }
    
    private class MultiSpamReportListener extends FileSpamReportListener {
    	
    	
		public MultiSpamReportListener() {
		}
		
		/**
		 * Callback called when a file spam report message has been delivered Successfully
		 * 
		 * @param contact Contact
		 * @param msgId Message ID
		 */
		public void onFileSpamReportSuccess(String contact, String msgId) {
			Logger.d(TAG, "spam-report: File onSpamReportSuccess() contact "+ contact + "  msgId:" + msgId);
            Toast.makeText(mContext, "File report failed", Toast.LENGTH_SHORT).show();
		}
		
		/**
		 * Callback called when a file spam report message has been delivered failed
		 * 
		 * @param contact Contact
		 * @param msgId Message ID
		 * @param errorCode Error code
		 */
	    public void onFileSpamReportFailed( String contact, String msgId, int errorCode){
	    	Logger.d(TAG, "spam-report: File onSpamReportFailed() contact "+ contact + "  msgId:" + msgId + "errorCode " + errorCode);
            Toast.makeText(mContext, "File report failed", Toast.LENGTH_SHORT).show();
	    }

    }
}

