/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <pthread.h>
#include <log/log.h>
#include <hardware/memtrack.h>
#include "memtrack_mtk.h"


#define RECORDS_NUM 2

int gl_memtrack_get_memory(pid_t pid, int type,
                             struct memtrack_record *records,
                             size_t *num_records)
{
    int i;
    FILE *fp;
    char line[1024];
    char va[17];
    char g3dva[17];
    unsigned int glmem;
    int curr_pid;
	int hasMatchUserPid = 0;
	

    if (type != MEMTRACK_TYPE_GL)
    {
        return -ENODEV;
    }

    if (*num_records == 0)
    {
        //return num of record;
        *num_records = RECORDS_NUM;
        return 0;
    }
    else if ((*num_records != RECORDS_NUM) || (records == NULL))
    {
        return -ENODEV;
    }
    
    memset(records, 0, sizeof(struct memtrack_record)*RECORDS_NUM);
    records[0].flags = MEMTRACK_FLAG_SMAPS_ACCOUNTED | MEMTRACK_FLAG_PRIVATE | MEMTRACK_FLAG_NONSECURE;
    records[1].flags = MEMTRACK_FLAG_SMAPS_UNACCOUNTED | MEMTRACK_FLAG_SHARED | MEMTRACK_FLAG_NONSECURE;



    fp = fopen("/sys/kernel/debug/yolig3d/Profiler/Memory/MemList", "r");
	
    if (fp == NULL) {
        return -errno;
    }

    while (fgets(line, sizeof(line), fp) != NULL) {
        if (strncmp(line," User", 5) == 0) {
	        int ret = sscanf(line, " User %u %s %u %s\n", &curr_pid, va, &glmem, g3dva);

            if (ret == 4) {
			
                if (curr_pid == pid) {
					//ALOGD("[MEMTRACK] User %u %s %u %s\n", curr_pid, va, glmem, g3dva); 
                    records[0].size_in_bytes += glmem;
					hasMatchUserPid = 1;
                }
            }
        }
	    else if (hasMatchUserPid && (strncmp(line," Kernel", 7) == 0)){
            int ret = sscanf(line, " Kernel %u %s %u %s\n", &curr_pid, va, &glmem, g3dva);
			
            if (ret == 4) {
				//ALOGD("[MEMTRACK]  Kernel %u %s %u %s\n", curr_pid, va, glmem, g3dva); 
                records[1].size_in_bytes += glmem;
            }
        }
    }

    return 0;
}

int gl_memtrack_init()
{
    return 0;
}
