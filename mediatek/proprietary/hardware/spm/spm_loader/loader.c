
#include "loader.h"
//#include <syslog.h>
#include <private/android_filesystem_config.h>
#include <utils/Log.h>

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "spm_loader"

#define SPM_FIRMWARE_DEBUGFS				"/sys/kernel/debug/spm/firmware"
#define SPM_FIRMWARE_CHAR_DEVICE			"/dev/spm"


static int gLoaderFd = -1;

int main(int argc, char *argv[])
{
	int iRet = -1;
	int count = 0;
	char buf[4];

#if 0
	do{
		gLoaderFd = open(SPM_FIRMWARE_DEBUGFS, O_RDONLY | O_NOCTTY);
		if(gLoaderFd < 0)
		{
			count ++;
			ALOGI("Can't open device node(%s) count(%d)\n", SPM_FIRMWARE_DEBUGFS,count);
			usleep(300000);
		}
		else
			break;
	}while(1);

	iRet = read(gLoaderFd, buf, 1);
	close(gLoaderFd);
#else
	do{
		gLoaderFd = open(SPM_FIRMWARE_CHAR_DEVICE, O_RDONLY | O_NOCTTY);
		if(gLoaderFd < 0)
		{
			count ++;
			ALOGI("Can't open device node(%s) count(%d)\n", SPM_FIRMWARE_CHAR_DEVICE,count);
			usleep(300000);
		}
		else
			break;
	}while(1);

	iRet = read(gLoaderFd, buf, 1);
	close(gLoaderFd);
#endif

	return iRet;
}



