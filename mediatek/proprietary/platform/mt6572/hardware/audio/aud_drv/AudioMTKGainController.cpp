
#define LOG_TAG "AudioMTKGainController"
#include <cutils/xlog.h>
#include <math.h>
#include <linux/fm.h>
#include "audio_custom_exp.h"
#include <media/AudioSystem.h>
#include "SpeechDriverFactory.h"
#include "AudioMTKGainController.h"
#include "SpeechEnhancementController.h"
#include "AudioAMPControlInterface.h"

#ifndef MTK_BASIC_PACKAGE
#include <IATVCtrlService.h>
#endif
#ifdef MTK_BASIC_PACKAGE
#include "AudioTypeExt.h"
#endif

namespace android
{

#ifndef MTK_BASIC_PACKAGE
static sp<IATVCtrlService> spATVCtrlService = NULL;
#endif

AudioMTKGainController *AudioMTKGainController::UniqueVolumeInstance = NULL;

// here can change to match audiosystem

// total 64 dB
static const float keydBPerStep = 0.25f;
static const float keyvolumeStep = 255.0f;


// shouldn't need to touch these
static const float keydBConvert = -keydBPerStep * 2.302585093f / 20.0f;
static const float keydBConvertInverse = 1.0f / keydBConvert;

//hw spec db
const int keyAudioBufferStep       =   7;
const int KeyAudioBufferGain[]     =  { -5, -3, -1, 1, 3, 5, 7, 9};

const int keyVoiceBufferStep       =   15;
const int KeyVoiceBufferGain[]     =  { -21, -19, -17, -15, -13, -11, -9, -7, -5, -3, -1, 1, 3, 5, 7, 9};

const int keyULStep                =   5;
const int KeyULGain[]              =  { -6, 0, 6, 12, 18, 24};
const int keyULGainOffset          = 2;

const int keySPKStep               =   15;
const int KeySPKgain[]             =  { -60, 0, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17};

const int keyDLDigitalDegradeMax   = 63;

//const int keyULDigitalIncreaseMax  = 32;

static const int keySidetoneSize   = 47;

static const uint16_t SwAgc_Gain_Map[AUDIO_SYSTEM_UL_GAIN_MAX + 1] =
{
    19, 18, 17, 16, 15, 14, 13, 12, 11,
    16, 15,14, 13, 12, 11,
    16, 15,14, 13, 12, 11,
    16, 15,14, 13, 12, 11,
    16, 15,14, 13, 12, 11,
    16, 15,14, 13, 12, 11,
    10,9, 8, 7, 6, 5, 4
};


static const uint16_t PGA_Gain_Map[AUDIO_SYSTEM_UL_GAIN_MAX + 1] =
{
    0, 0, 0, 0, 0, 0, 0, 0, 0,
    6, 6, 6, 6, 6, 6,
    12, 12, 12, 12, 12, 12,
    18, 18, 18, 18, 18, 18,
    24, 24, 24, 24, 24, 24,
    30, 30, 30, 30, 30, 30,
    30, 30, 30, 30, 30, 30, 30
};


static const uint16_t keySideTone[] =
{
    32767, 29204, 26027, 23196, 20674, 18426, 16422, 14636, 13044, 11625,  /*1dB per step*/
    10361, 9234,  8230,  7335,  6537,  5826,  5193,  4628,  4125,  3676,
    3276,  2919,  2602,  2319,  2066,  1841,  1641,  1463,  1304,  1162,
    1035,  923,   822,   733,   653,   582,   519,   462,   412,   367,
    327,   291,   260,   231,   206,   183,   163,   145
};

float AudioMTKGainController::linearToLog(int volume)
{
    //ALOGD("linearToLog(%d)=%f", volume, v);
    return volume ? exp(float(keyvolumeStep - volume) * keydBConvert) : 0;
}

int AudioMTKGainController::logToLinear(float volume)
{
    //ALOGD("logTolinear(%d)=%f", v, volume);
    return volume ? keyvolumeStep - int(keydBConvertInverse * log(volume) + 0.5) : 0;
}

AudioMTKGainController *AudioMTKGainController::getInstance()
{
    if (UniqueVolumeInstance == 0)
    {
        SXLOGV("+UniqueVolumeInstance\n");
        UniqueVolumeInstance = new AudioMTKGainController();
        SXLOGV("-UniqueVolumeInstance\n");
    }

    return UniqueVolumeInstance;
}

AudioMTKGainController::AudioMTKGainController()
{
    SXLOGD("AudioMTKGainController contructor");
    mAudioAnalogControl = AudioAnalogControlFactory::CreateAudioAnalogControl();
    mAudioDigitalControl = AudioDigitalControlFactory::CreateAudioDigitalControl();
    mAmpControl          = AudioDeviceManger::createInstance();
    mVoiceVolume = 1.0f;
    mMasterVolume = 1.0f;
    //mFmVolume = 0xFF;
    //mFmChipVolume = 0xFFFFFFFF;
    mMatvVolume = 0xFF;
    mSpeechNB = true;
    mSupportBtVol = false;
    memset(&mHwVolume, 0xFF, sizeof(mHwVolume));
    memset(&mHwStream, 0xFF, sizeof(mHwStream));
    initVolumeController();
    mInitDone = true;
}

status_t AudioMTKGainController::initVolumeController()
{
    GetAudioGainTableParamFromNV(&mCustomVolume);
    return NO_ERROR;
}


status_t AudioMTKGainController::initCheck()
{
    return mInitDone;
}

status_t AudioMTKGainController::speechBandChange(bool nb)
{
    SXLOGD("speechBandChange nb %d",nb);

    AutoMutex lock(mLock);
    if(mSpeechNB != nb)
    {
        mSpeechNB = nb;
        setAnalogVolume_l(mHwStream.stream,mHwStream.device,mHwStream.index,AUDIO_MODE_IN_CALL);
    }
    return NO_ERROR;
}

bool AudioMTKGainController::isNbSpeechBand()
{
    AutoMutex lock(mLock);
    return mSpeechNB;
}

status_t AudioMTKGainController::setBtVolumeCapability(bool support)
{
    AutoMutex lock(mLock);
    mSupportBtVol = !support; // if bt device do not support volume , we should
    return NO_ERROR;
}

status_t AudioMTKGainController::setAnalogVolume_l(int stream, int device, int index, audio_mode_t mode)
{
    SXLOGD("setAnalogVolume stream %d device 0x%x index %d mode %d", stream, device, index, mode);
    mHwStream.stream = stream;
    mHwStream.device = device;
    mHwStream.index  = index;
    if (isInVoiceCall(mode))
    {
        setVoiceVolume(index, device, mode);
    }
    else
    {
        setNormalVolume(stream, index, device, mode);
    }

    return NO_ERROR;
}

status_t AudioMTKGainController::setAnalogVolume(int stream, int device, int index, audio_mode_t mode)
{
   AutoMutex lock(mLock);
   return setAnalogVolume_l(stream,device,index,mode);
}

status_t AudioMTKGainController::setNormalVolume(int stream, int index, int device, audio_mode_t mode)
{
    SXLOGD("setNormalVolume stream %d, index 0x%x, device 0x%x, mode 0x%x", stream, index, device, mode);
    mAudioResourceManager = AudioResourceManager::getInstance();
    int arrDevices[]={AUDIO_DEVICE_OUT_SPEAKER, AUDIO_DEVICE_OUT_WIRED_HEADSET, AUDIO_DEVICE_OUT_WIRED_HEADPHONE, AUDIO_DEVICE_OUT_EARPIECE};
    bool blnFlag = false;
    for (int i = 0; i < 4; i++)
    {
        if ((arrDevices[i] & device) == arrDevices[i])
        {
            blnFlag = true;
            output_gain_device gainDevice = getGainDevice(arrDevices[i]);
            AUDIO_GAIN_TABLE_STRUCT *gainTable = &mCustomVolume;
            STREAM_GAIN *streamGain = &gainTable->voiceCall + stream;
            uint8_t analog = streamGain->stream[gainDevice].analog;
            //set speaker gain
            if (arrDevices[i] == AUDIO_DEVICE_OUT_SPEAKER)
            {
                uint8_t spkGain =  streamGain->stream[gainDevice].amp[4];
                setSpeakerGain(spkGain);
            }
            //set PGA gain
            ApplyAudioGain(analog, mode, arrDevices[i]);
            setAMPGain(streamGain->stream[gainDevice].amp, AMP_CONTROL_POINT, arrDevices[i]);
            device &= (~arrDevices[i]);
        }
    }
    if (!blnFlag)
    {
        output_gain_device gainDevice = GAIN_OUTPUT_HEADSET;
        AUDIO_GAIN_TABLE_STRUCT *gainTable = &mCustomVolume;
        STREAM_GAIN *streamGain = &gainTable->voiceCall + stream;
        uint8_t analog = streamGain->stream[gainDevice].analog;
        //set PGA gain
        ApplyAudioGain(analog, mode, device);
        setAMPGain(streamGain->stream[gainDevice].amp, AMP_CONTROL_POINT, device);
    }
    mAudioResourceManager->SetInputDeviceGain();
    return NO_ERROR;
}

status_t AudioMTKGainController::setVoiceVolume(int index, int device, audio_mode_t mode)
{
    SXLOGD("setVoiceVolume index %d, device 0x%x, mode %d", index, device, mode);
    output_gain_device gainDevice = getGainDevice(device);
    AUDIO_GAIN_TABLE_STRUCT *gainTable = &mCustomVolume;
    uint8_t analog =0;
    uint8_t digitalDegradeDb =0;
    void * ampgain = NULL;
    SXLOGD("mSupportBtVol is %d, mSpeechNB is %d", mSupportBtVol, mSpeechNB);
    if(audio_is_bluetooth_sco_device(device))
    {
        STREAM_ITEM_GAIN *streamGain =  &gainTable->blueToothSco.stream[gainDevice];
        if(mSupportBtVol)
            digitalDegradeDb = streamGain->digital[index];
        else
            digitalDegradeDb = streamGain->digital[15];
        ApplyMdDlGain(digitalDegradeDb);  // modem dl gain

        ApplyMdUlGain(0);
        return NO_ERROR;
    }
    else
    {
        SPEECH_GAIN *streamGain = mSpeechNB ? &gainTable->speechNB : &gainTable->speechWB;
        SPEECH_ITEM_GAIN *item = &streamGain->speech[gainDevice][index];
        analog  = item->analog;
        digitalDegradeDb = item->digital ;
        ampgain = item->amp;
    }
    //set apk gain
    if (device  == AUDIO_DEVICE_OUT_SPEAKER)
    {
       setSpeakerGain(((uint8_t *)ampgain)[4]);
    }
    ApplyAudioGain(analog, mode, device); // audiobuffer / voice buffer
    setAMPGain(ampgain, AMP_CONTROL_POINT,device);
    ApplyMdDlGain(digitalDegradeDb);  // modem dl gain
    ApplyMicGainByDevice(device, mode);      // mic gain & modem UL gain
    ApplySideTone(gainDevice);
    return NO_ERROR;
}


void  AudioMTKGainController::ApplyMicGainByDevice(uint32 device, audio_mode_t mode)
{
    ALOGD("ApplyMicGainByDevice device=%d, mode=%d", device, mode);

    if (device & AUDIO_DEVICE_OUT_EARPIECE)
    {
        ApplyMicGain(mSpeechNB ? Normal_Mic : Normal_WB__Mic, mode); // set incall mic gain
    }
    else if (device & AUDIO_DEVICE_OUT_WIRED_HEADSET ||  device & AUDIO_DEVICE_OUT_WIRED_HEADPHONE)
    {
        ApplyMicGain(mSpeechNB ? Headset_Mic : Headset_WB_Mic, mode); // set incall mic gain
    }
    else if (device & AUDIO_DEVICE_OUT_SPEAKER)
    {
        ApplyMicGain(mSpeechNB ? Handfree_Mic : Handfree_WB_Mic, mode); // set incall mic gain
    }
    else if ((device & AUDIO_DEVICE_OUT_BLUETOOTH_SCO) || (device & AUDIO_DEVICE_OUT_BLUETOOTH_SCO_HEADSET) || (device & AUDIO_DEVICE_OUT_BLUETOOTH_SCO_HEADSET))
    {
        //when use BT_SCO , apply digital to 0db.
        ApplyMdUlGain(0);
    }
}

BUFFER_TYPE AudioMTKGainController::getBufferType(int device, audio_mode_t mode)
{

#ifdef USING_EXTAMP_ALL_VOICE_BUFFER
    if(isInVoiceCall(mode))
    {
        return VOICE_BUFFER;
    }
#endif
    if (device  ==  AUDIO_DEVICE_OUT_EARPIECE)
    {
        return VOICE_BUFFER;
    }
    else if ((device  == AUDIO_DEVICE_OUT_WIRED_HEADSET)
             || device == AUDIO_DEVICE_OUT_WIRED_HEADPHONE
)
    {
        return AUDIO_BUFFER;
    }
    else if (device  == AUDIO_DEVICE_OUT_SPEAKER)
    {
        return VOICE_BUFFER;
    }
    else
    {
        return INVALID_BUFFFER;
    }
}

output_gain_device   AudioMTKGainController::getGainDevice(int device)
{
    output_gain_device gainDevice = GAIN_OUTPUT_SPEAKER;

    if (device == AUDIO_DEVICE_OUT_SPEAKER)
    {
        gainDevice = GAIN_OUTPUT_SPEAKER;
    }
    else if (device == AUDIO_DEVICE_OUT_WIRED_HEADSET
             || device == AUDIO_DEVICE_OUT_WIRED_HEADPHONE
             )
    {
        gainDevice = GAIN_OUTPUT_HEADSET;
    }
    else if (device == AUDIO_DEVICE_OUT_EARPIECE)
    {
        gainDevice = GAIN_OUTPUT_EARPIECE ;
    }
    else
    {
        gainDevice = GAIN_OUTPUT_HEADSET;
    }

    return gainDevice;
}


void AudioMTKGainController::ApplyMdDlGain(int32 degradeDb)
{
    // set degarde db to mode side, DL part, here use degrade dbg
    ALOGD("ApplyMdDlGain degradeDb = %d", degradeDb);
#if 0
    if (degradeDb >= keyDLDigitalDegradeMax)
    {
        degradeDb = keyDLDigitalDegradeMax;
    }

    SpeechDriverFactory::GetInstance()->GetSpeechDriver()->SetDownlinkGain((-1 * degradeDb) << 2); // degrade db * 4
#endif
    SpeechDriverFactory::GetInstance()->GetSpeechDriver()->SetDownlinkGain((-1 * degradeDb));
}

void AudioMTKGainController::ApplyMdDlEhn1Gain(int32 Gain)
{
    // set degarde db to mode side, DL part, here use degrade dbg
    ALOGD("ApplyMdDlEhn1Gain degradeDb = %d", Gain);
    //SpeechDriverFactory::GetInstance()->GetSpeechDriver()->SetEnh1DownlinkGain(-1 * (Gain) << 2); // degrade db * 4
    SpeechDriverFactory::GetInstance()->GetSpeechDriver()->SetEnh1DownlinkGain(-1 * (Gain));
}


void AudioMTKGainController::ApplyMdUlGain(int32 IncreaseDb)
{
    // set degarde db to mode side, UL part, here use positive gain becasue SW_agc always positive
    ALOGD("ApplyMdUlGain degradeDb = %d", IncreaseDb);

    //if (mHwVolume.swAgc != IncreaseDb)
    {
        mHwVolume.swAgc = IncreaseDb;
        SpeechDriverFactory::GetInstance()->GetSpeechDriver()->SetUplinkGain(IncreaseDb << 2); // degrade db * 4
    }
}


void AudioMTKGainController::ApplyAudioGain(int gain, uint32 mode, uint32 device)
{
    ALOGD("ApplyAudioGain  gain = %d mode= %d device = %d", gain, mode, device);
    BUFFER_TYPE bufferType = getBufferType(device, (audio_mode_t)mode);
    int bufferGain = gain;
/*
    if (device  == AUDIO_DEVICE_OUT_SPEAKER)
    {
        if (isInVoiceCall((audio_mode_t) mode))
        {
            gain = 10; //fixed speak at 12db;
        }
        //else
        //{
        //   bufferGain = 11; //fixed hs at 1db;
        //}

        setSpeakerGain(gain);
    }
*/
    if (bufferType == VOICE_BUFFER)
    {
        setVoiceBufferGain(bufferGain);
    }
    else if (bufferType == AUDIO_BUFFER)
    {
        setAudioBufferGain(bufferGain);
    }
}


void   AudioMTKGainController::setAudioBufferGain(int gain)
{
    if (gain >= keyAudioBufferStep)
    {
        gain = keyAudioBufferStep;
    }

    int regIndex =  mAudioAnalogControl->GetAnalogGain(AudioAnalogType::VOLUME_HPOUTL,true);
    ALOGD("setAudioBufferGain, gain %d, mHwVolume.audioBuffer %d,regIndex:%d", gain, mHwVolume.audioBuffer,regIndex);
    mHwVolume.audioBuffer = regIndex;

    if (mHwVolume.audioBuffer != gain)
    {
        mHwVolume.audioBuffer = gain;
        mAudioAnalogControl->SetAnalogGain(AudioAnalogType::VOLUME_HPOUTL, gain);
        mAudioAnalogControl->SetAnalogGain(AudioAnalogType::VOLUME_HPOUTR, gain);
    }
}

void  AudioMTKGainController::setVoiceBufferGain(int gain)
{
    if (gain >= keyVoiceBufferStep)
    {
        gain = keyVoiceBufferStep;
    }

    int regIndex =  mAudioAnalogControl->GetAnalogGain(AudioAnalogType::VOLUME_HSOUTL,true);
    ALOGD("setVoiceBufferGain, gain %d, mHwVolume.voiceBuffer %d,regIndex:%d", gain, mHwVolume.voiceBuffer,regIndex);
    mHwVolume.voiceBuffer = regIndex;

    if (mHwVolume.voiceBuffer != gain)
    {
        mHwVolume.voiceBuffer = gain;
        mAudioAnalogControl->SetAnalogGain(AudioAnalogType::VOLUME_HSOUTL, gain);
        mAudioAnalogControl->SetAnalogGain(AudioAnalogType::VOLUME_HSOUTR, gain);
    }
}
void   AudioMTKGainController::setSpeakerGain( int gain)
{
    if (gain >= keySPKStep)
    {
        gain = keySPKStep;
    }

    int regIndex =  mAudioAnalogControl->GetAnalogGain(AudioAnalogType::VOLUME_SPKL,true);
    ALOGD("setSpeakerGain, gain %d, mHwVolume.speaker %d,regIndex:%d", gain, mHwVolume.speaker,regIndex);
    mHwVolume.speaker = regIndex;

    if (mHwVolume.speaker != gain)
    {
        mHwVolume.speaker = gain;
        mAudioAnalogControl->SetAnalogGain(AudioAnalogType::VOLUME_SPKL, gain);
        mAudioAnalogControl->SetAnalogGain(AudioAnalogType::VOLUME_SPKR, gain);
    }
}

void   AudioMTKGainController::setAMPGain(void * points, int num, int device)
{
    ALOGD("setAMPGain, device %d", device);
#ifdef USING_EXTAMP_TC1
    if(mAmpControl && points)
    {
        mAmpControl->setVolume(points,num,device);
    }
#endif
}

status_t AudioMTKGainController::ApplyMicGain(uint32_t MicType, int mode)
{
    ALOGD("ApplyMicGain  MicType = %d mode= %d", MicType, mode);

    uint8_t analog  = mCustomVolume.mic.mic[MicType].digital;

    uint8_t degradedb = AUDIO_SYSTEM_UL_GAIN_MAX - analog;
    uint8_t analogmap = PGA_Gain_Map[degradedb];
    uint8_t swagcmap  = SwAgc_Gain_Map[degradedb];

    //if (mHwVolume.micGain != analog)
    //{
        mHwVolume.micGain = analog;
        mAudioAnalogControl->SetAnalogGain(AudioAnalogType::VOLUME_MICAMPL, analogmap);
        mAudioAnalogControl->SetAnalogGain(AudioAnalogType::VOLUME_MICAMPR, analogmap);
    //}

    mHwVolume.swAgc = swagcmap;
    if (isInVoiceCall((audio_mode_t)mode))
    {
        ApplyMdUlGain(swagcmap);
    }
    return NO_ERROR;
}

uint8_t  AudioMTKGainController::GetSWMICGain()
{
    return mHwVolume.swAgc ;
}


status_t AudioMTKGainController::ApplySideTone(uint32_t Mode)
{
    // here apply side tone gain, need base on UL and DL analog gainQuant
    AUDIO_GAIN_TABLE_STRUCT *gainTable = &mCustomVolume;
    SIDETONE_GAIN *sideTone = NULL;
    if (mSpeechNB)
        sideTone = &gainTable->sideToneNB;
    else
        sideTone = &gainTable->sideToneWB;
    uint8_t sidetone =  sideTone->sidetone[Mode];

    if (sidetone > keySidetoneSize)
    {
        sidetone = keySidetoneSize;
    }

    //if (mHwVolume.sideTone != sidetone)
    {
        mHwVolume.sideTone = sidetone;
        uint16_t value = sidetone;

        if (Mode == EarPiece_SideTone_Gain)
        {
            value = updateSidetone(mAudioAnalogControl->GetAnalogGain(AudioAnalogType::VOLUME_HSOUTL), sidetone, mHwVolume.swAgc);
        }
        else if (Mode == Headset_SideTone_Gain)
        {
            value = updateSidetone(mAudioAnalogControl->GetAnalogGain(AudioAnalogType::VOLUME_HPOUTL), sidetone, mHwVolume.swAgc);
        }
        else if (Mode == LoudSpk_SideTone_Gain)
        {
            value = updateSidetone(mAudioAnalogControl->GetAnalogGain(AudioAnalogType::VOLUME_HSOUTL), sidetone, mHwVolume.swAgc);
        }

        SXLOGD("ApplySideTone Mode %d, sidetone %u, value %u",Mode,sidetone,value);
        SpeechDriverFactory::GetInstance()->GetSpeechDriver()->SetSidetoneGain(value);
    }

    return NO_ERROR;
}

uint16_t AudioMTKGainController::updateSidetone(int dlPGAGain, int  sidetone, uint8_t ulGain)
{

    int vol = 0;
    uint16_t DSP_ST_GAIN = 0;
    SXLOGD("updateSidetone dlPGAGain %d, sidetone %d, ulGain %u",dlPGAGain,sidetone,ulGain);

    if (sidetone == 0)
    {
        DSP_ST_GAIN = 0 ;
    }
    else
    {
        vol = sidetone + ulGain; //1dB/step
        vol = dlPGAGain - vol + 67 - keyULGainOffset;
        if (vol < 0)
        {
            vol = 0;
        }
        if (vol > keySidetoneSize)
        {
            vol = keySidetoneSize;
        }
        DSP_ST_GAIN = keySideTone[vol];
    }
    SXLOGD("DSP_ST_GAIN=%d",DSP_ST_GAIN);
    return DSP_ST_GAIN;
}

bool AudioMTKGainController::isInVoiceCall(audio_mode_t mode)
{
    return (mode == AUDIO_MODE_IN_CALL ||
            mode == AUDIO_MODE_IN_CALL_2);
}

bool AudioMTKGainController::isInVoipCall(audio_mode_t mode)
{
    return mode == AUDIO_MODE_IN_COMMUNICATION;
}

bool AudioMTKGainController::isInCall(audio_mode_t mode)
{
    return (isInVoiceCall(mode) || isInVoipCall(mode));
}


//static functin to get FM power state
#define BUF_LEN 1
static char rbuf[BUF_LEN] = {'\0'};
static char wbuf[BUF_LEN] = {'1'};
static const char *FM_POWER_STAUTS_PATH = "/proc/fm";
static const char *FM_DEVICE_PATH = "dev/fm";

bool AudioMTKGainController::SetFmVolume(int volume)
{
    ALOGD("%s is deprecated and not used", __FUNCTION__);
    return false;
}
bool AudioMTKGainController::SetFmVolume(float volume)
{
    ALOGD("%s is deprecated and not used", __FUNCTION__);
    return false;
}

bool AudioMTKGainController::SetFmChipVolume(int volume)
{
    ALOGD("%s is deprecated and not used", __FUNCTION__);
    return false;
}
int  AudioMTKGainController::GetFmVolume(void)
{
    ALOGD("%s is deprecated and not used", __FUNCTION__);
    return 0;
}

//matv adjust digital
void AudioMTKGainController::GetMatvService()
{
#ifndef MTK_BASIC_PACKAGE

    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder;

    do
    {
        binder = sm->getService(String16("media.ATVCtrlService"));

        if (binder != 0)
        {
            break;
        }

        ALOGW("ATVCtrlService not published, waiting...");
        usleep(1000 * 1000); // 1 s
    }
    while (true);

    spATVCtrlService = interface_cast<IATVCtrlService>(binder);
#endif
}

bool AudioMTKGainController::SetMatvMute(bool b_mute)
{
    ALOGD("SetMatvMute(%d), mMatvVolume(%d)", b_mute, mMatvVolume);

    return true;
}

bool AudioMTKGainController::setMatvVolume(int volume)
{
    ALOGD("setMatvVolume volume=%d", volume);
    mMatvVolume = volume;
    return true;
}

int AudioMTKGainController::GetMatvVolume(void)
{
    return mMatvVolume;
}

/********************************************************************************
*
*
*
*                                                            UnUsed API
*
*
*
***********************************************************************************/

uint16_t AudioMTKGainController::MappingToDigitalGain(unsigned char Gain)
{
    return 0;
}

uint16_t AudioMTKGainController::MappingToPGAGain(unsigned char Gain)
{
    return 0;
}

status_t AudioMTKGainController::setMasterVolume(float v, audio_mode_t mode, uint32_t devices)
{
    mMasterVolume = v;
    ALOGD("setMasterVolume call setNormalVolume");
    setNormalVolume(mHwStream.stream,mHwStream.index, devices, mode);
    return NO_ERROR;
}

float AudioMTKGainController::getMasterVolume()
{
    ALOGD("AudioMTKGainController getMasterVolume");
    return mMasterVolume;
}


status_t AudioMTKGainController::setVoiceVolume(float v, audio_mode_t mode, uint32_t device)
{
    mVoiceVolume = v;
    ALOGD("call setVoiceVolume");
    setVoiceVolume(mHwStream.index,device,mode);
    return NO_ERROR;
}

float AudioMTKGainController::getVoiceVolume(void)
{
    ALOGD("AudioMTKGainController getVoiceVolume");
    return mVoiceVolume;
}

status_t AudioMTKGainController::setStreamVolume(int stream, float v)
{
    return INVALID_OPERATION;
}

status_t AudioMTKGainController::setStreamMute(int stream, bool mute)
{
    return INVALID_OPERATION;
}

float AudioMTKGainController::getStreamVolume(int stream)
{
    return 1.0;
}

// should depend on different usage , FM ,MATV and output device to setline in gain
status_t AudioMTKGainController::SetLineInPlaybackGain(int type)
{
    return INVALID_OPERATION;
}

status_t AudioMTKGainController::SetLineInRecordingGain(int type)
{
    return INVALID_OPERATION;
}

status_t AudioMTKGainController::SetSideTone(uint32_t Mode, uint32_t Gain)
{

    return INVALID_OPERATION;
}

uint32_t AudioMTKGainController::GetSideToneGain(uint32_t device)
{

    return INVALID_OPERATION;
}


status_t AudioMTKGainController::SetMicGain(uint32_t Mode, uint32_t Gain)
{
    return INVALID_OPERATION;
}


status_t AudioMTKGainController::SetULTotalGain(uint32_t Mode, unsigned char Volume)
{
    return INVALID_OPERATION;
}



status_t AudioMTKGainController::SetDigitalHwGain(uint32_t Mode, uint32_t Gain , uint32_t routes)
{
    return INVALID_OPERATION;
}



uint8_t  AudioMTKGainController::GetULTotalGain()
{
    return 0;
}


status_t AudioMTKGainController::SetMicGainTuning(uint32_t Mode, uint32_t Gain)
{
    return INVALID_OPERATION;
}

}
