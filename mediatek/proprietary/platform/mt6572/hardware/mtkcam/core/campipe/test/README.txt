WHAT IT DOES?
=============
camera pipeline manager related unit test files.


HOW IT WAS BUILT?
==================
It can build the following executable file:
campipetest.exe


HOW TO USE IT?
==============
Provide related api for unit test of camera pipeline manager.