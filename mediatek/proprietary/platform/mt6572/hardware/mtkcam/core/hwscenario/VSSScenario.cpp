#define LOG_TAG "MtkCam/VSSScen"
//
#include <vector>
using namespace std;
//
#include <utils/Vector.h>
#include <mtkcam/common.h>
#include <imageio/IPipe.h>
#include <imageio/ICamIOPipe.h>
#include <imageio/IPostProcPipe.h>
#include <imageio/ispio_stddef.h>
#include <drv/isp_drv.h>
#include <mtkcam/hal/sensor_hal.h>
using namespace NSImageio;
using namespace NSIspio;
//

//
#include <hwscenario/IhwScenarioType.h>
using namespace NSHwScenario;
#include <hwscenario/IhwScenario.h>
#include "hwUtility.h"
#include "VSSScenario.h"
//
#include <cutils/atomic.h>
#include <cutils/properties.h>
//
//#if defined(HAVE_AEE_FEATURE)
#include <aee.h>
//#endif
//
/*******************************************************************************
*
********************************************************************************/
#include <mtkcam/Log.h>
#define MY_LOGV(fmt, arg...)    CAM_LOGV("[%s] "fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)    CAM_LOGD("[%s] "fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)    CAM_LOGI("[%s] "fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)    CAM_LOGW("[%s] "fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)    CAM_LOGE("[%s] "fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, arg...)    if (cond) { MY_LOGV(arg); }
#define MY_LOGD_IF(cond, arg...)    if (cond) { MY_LOGD(arg); }
#define MY_LOGI_IF(cond, arg...)    if (cond) { MY_LOGI(arg); }
#define MY_LOGW_IF(cond, arg...)    if (cond) { MY_LOGW(arg); }
#define MY_LOGE_IF(cond, arg...)    if (cond) { MY_LOGE(arg); }
//
#define FUNCTION_LOG_START      MY_LOGD("+");
#define FUNCTION_LOG_END        MY_LOGD("-");
#define ERROR_LOG               MY_LOGE("Error");
//
#define _PASS1_CQ_CONTINUOUS_MODE_
#define ENABLE_LOG_PER_FRAME    (1)
//
#if defined(HAVE_AEE_FEATURE)
    #define AEE_ASSERT(String) \
        do { \
            aee_system_exception( \
                "VSSScenario", \
                NULL, \
                DB_OPT_DEFAULT, \
                String); \
        } while(0)
#else
    #define AEE_ASSERT(String)
#endif
//
/*******************************************************************************
*
********************************************************************************/
VSSScenario*
VSSScenario::createInstance(EScenarioFmt rSensorType, halSensorDev_e const &dev, ERawPxlID const &bitorder)
{
    return new VSSScenario(rSensorType, dev, bitorder);
}


/*******************************************************************************
*
********************************************************************************/
MVOID
VSSScenario::destroyInstance()
{
    //
    delete this;
}


/*******************************************************************************
*
********************************************************************************/
VSSScenario::VSSScenario(EScenarioFmt rSensorType, halSensorDev_e const &dev, ERawPxlID const &bitorder)
            : mpCamIOPipe(NULL)
            , mpPostProcPipe(NULL)
            , mSensorType(rSensorType)
            , mSensorDev(dev)
            , mSensorBitOrder(bitorder)
            , mModuleMtx()
            , debugScanLine(DebugScanLine::createInstance())
{
    //
    MY_LOGD("mSensorBitOrder:%d", mSensorBitOrder);
    MY_LOGD("this=%p, sizeof:%d", this, sizeof(VSSScenario));
}


/*******************************************************************************
*
********************************************************************************/
VSSScenario::~VSSScenario()
{
    MY_LOGD("");
}


/*******************************************************************************
*
********************************************************************************/
MBOOL
VSSScenario::
init()
{
    CAM_TRACE_NAME("VSSScen::init");
    FUNCTION_LOG_START;
    //(1)
    mpCamIOPipe = ICamIOPipe::createInstance(eScenarioID_VSS, mSensorType);
    CAM_TRACE_BEGIN("CamIOPipe::init");
    if ( ! mpCamIOPipe || ! mpCamIOPipe->init())
    {
        MY_LOGE("ICamIOPipe init error");
        return MFALSE;
    }
    CAM_TRACE_END();
    //(2)
    CAM_TRACE_BEGIN("PostProcPipe::init");
    mpPostProcPipe = IPostProcPipe::createInstance(eScenarioID_VSS, mSensorType);
    if ( ! mpPostProcPipe || ! mpPostProcPipe->init())
    {
        MY_LOGE("IPostProcPipe init error");
        CAM_TRACE_END();
        return MFALSE;
    }
    CAM_TRACE_END();
    //(3)
    CAM_TRACE_BEGIN("CamIOPipe::EPIPECmd_SET_CQ_CHANNEL");
    mpCamIOPipe->sendCommand(EPIPECmd_SET_CQ_CHANNEL,(MINT32)EPIPE_PASS1_CQ0, 0, 0);
    CAM_TRACE_END();

    CAM_TRACE_BEGIN("CamIOPipe::EPIPECmd_SET_CQ_TRIGGER_MODE");
    mpCamIOPipe->sendCommand(EPIPECmd_SET_CQ_TRIGGER_MODE,
                            (MINT32)EPIPE_PASS1_CQ0,
                            (MINT32)EPIPECQ_TRIGGER_SINGLE_IMMEDIATE,
                            (MINT32)EPIPECQ_TRIG_BY_START);
    CAM_TRACE_END();

    CAM_TRACE_BEGIN("PostProcPipe::EPIPECmd_SET_CQ_CHANNEL");
    mpPostProcPipe->sendCommand(EPIPECmd_SET_CQ_CHANNEL,
                               (MINT32)EPIPE_PASS2_CQ1, 0, 0);
    CAM_TRACE_END();

    CAM_TRACE_BEGIN("CamIOPipe::EPIPECmd_SET_CONFIG_STAGE");
    mpCamIOPipe->sendCommand(EPIPECmd_SET_CONFIG_STAGE,(MINT32)eConfigSettingStage_Init, 0, 0);
    CAM_TRACE_END();

    CAM_TRACE_BEGIN("PostProcPipe::EPIPECmd_SET_CONFIG_STAGE");
    mpPostProcPipe->sendCommand(EPIPECmd_SET_CONFIG_STAGE,(MINT32)eConfigSettingStage_Init, 0, 0);
    CAM_TRACE_END();
    //
    FUNCTION_LOG_END;
    //
    return MTRUE;
}


/*******************************************************************************
*
********************************************************************************/
MBOOL
VSSScenario::uninit()
{
    CAM_TRACE_NAME("VSSScen::uninit");
    FUNCTION_LOG_START;
    //
    MBOOL ret = MTRUE;
    //(1)
    if ( mpCamIOPipe )
    {
        CAM_TRACE_BEGIN("CamIOPipe::uninit");
        if ( ! mpCamIOPipe->uninit())
        {
            MY_LOGE("mpCamIOPipe uninit fail");
            CAM_TRACE_END();
            ret = MFALSE;
        }
        CAM_TRACE_END();
        mpCamIOPipe->destroyInstance();
        mpCamIOPipe = NULL;
    }
    //(2)
    if ( mpPostProcPipe )
    {
        CAM_TRACE_BEGIN("PostProcPipe::uninit");
        if ( ! mpPostProcPipe->uninit())
        {
            MY_LOGE("mpPostProcPipe uninit fail");
            CAM_TRACE_END();
            ret = MFALSE;
        }
        CAM_TRACE_END();
        mpPostProcPipe->destroyInstance();
        mpPostProcPipe = NULL;
    }
    //
    debugScanLine->destroyInstance();
    debugScanLine = NULL;
    //
    FUNCTION_LOG_END;
    //
    return ret;
}


/*******************************************************************************
* wait hardware interrupt
********************************************************************************/
MVOID
VSSScenario::wait(EWaitType rType)
{
    CAM_TRACE_NAME("VSSScen::wait");
    switch(rType)
    {
        case eIRQ_VS:
            if ((mSensorDev == SENSOR_DEV_MAIN) || (mSensorDev == SENSOR_DEV_ATV))
            {
                mpCamIOPipe->irq(EPipePass_PASS1_TG1, EPIPEIRQ_VSYNC);
            }
            else
            {
                mpCamIOPipe->irq(EPipePass_PASS1_TG2, EPIPEIRQ_VSYNC);
            }
        break;
        default:
        break;
    }
}


/*******************************************************************************
*
********************************************************************************/
MBOOL
VSSScenario::start()
{
    CAM_TRACE_NAME("VSSScen::start");
    FUNCTION_LOG_START;
    // (1) start CQ
    CAM_TRACE_BEGIN("CamIOPipe::startCQ0");
    mpCamIOPipe->startCQ0();
    CAM_TRACE_END();
#if defined(_PASS1_CQ_CONTINUOUS_MODE_)
    CAM_TRACE_BEGIN("CamIOPipe::EPIPECmd_SET_CQ_TRIGGER_MODE");
    mpCamIOPipe->sendCommand(EPIPECmd_SET_CQ_TRIGGER_MODE,
                             (MINT32)EPIPE_PASS1_CQ0,
                             (MINT32)EPIPECQ_TRIGGER_CONTINUOUS_EVENT,
                             (MINT32)EPIPECQ_TRIG_BY_PASS1_DONE);
    CAM_TRACE_END();
#else
    CAM_TRACE_BEGIN("CamIOPipe::EPIPECmd_SET_CQ_CHANNEL");
    mpCamIOPipe->sendCommand(EPIPECmd_SET_CQ_CHANNEL,(MINT32)EPIPE_CQ_NONE, 0, 0);
    CAM_TRACE_END();
#endif
    // (2) pass1 start
    CAM_TRACE_BEGIN("CamIOPipe::start");
    if ( ! mpCamIOPipe->start())
    {
        MY_LOGE("mpCamIOPipe->start() fail");
        CAM_TRACE_END();
        return MFALSE;
    }
    CAM_TRACE_END();
    // align to Vsync
    CAM_TRACE_BEGIN("CamIOPipe::irq::EPipePass_PASS1_TG1");
    if ((mSensorDev == SENSOR_DEV_MAIN) || (mSensorDev == SENSOR_DEV_ATV))
    {
        mpCamIOPipe->irq(EPipePass_PASS1_TG1, EPIPEIRQ_VSYNC);
    }
    else
    {
        mpCamIOPipe->irq(EPipePass_PASS1_TG2, EPIPEIRQ_VSYNC);
    }
    CAM_TRACE_END();
    MY_LOGD("- wait IRQ: ISP_DRV_IRQ_INT_STATUS_VS1_ST");
    //
    FUNCTION_LOG_END;
    //
    return MTRUE;
}


/*******************************************************************************
*
********************************************************************************/
MBOOL
VSSScenario::stop()
{
    CAM_TRACE_NAME("VSSScen::stop");
    FUNCTION_LOG_START;
    //
    PortID rPortID;
    mapPortCfg(eID_Pass1Out, rPortID);
    PortQTBufInfo dummy(eID_Pass1Out);
    CAM_TRACE_BEGIN("CamIOPipe::dequeOutBuf");
    mpCamIOPipe->dequeOutBuf(rPortID, dummy.bufInfo);
    CAM_TRACE_END();
    //
    CAM_TRACE_BEGIN("CamIOPipe::stop");
    if ( ! mpCamIOPipe->stop())
    {
        MY_LOGE("mpCamIOPipe->stop() fail");
        CAM_TRACE_END();
        return MFALSE;
    }
    CAM_TRACE_END();
    //
    FUNCTION_LOG_END;
    //
    return MTRUE;
}


/*******************************************************************************
*
********************************************************************************/
MBOOL
VSSScenario::
setConfig(vector<PortImgInfo> *pImgIn)
{
    if ( ! pImgIn )
    {
        MY_LOGE("pImgIn==NULL");
        return MFALSE;
    }

    bool isPass1 = false;

    defaultSetting();

    for (MUINT32 i = 0; i < pImgIn->size(); i++)
    {
        PortImgInfo rSrc = pImgIn->at(i);
        //
        // Pass 1 config will be fixed. Pass 2 config can be updated later.
        //
        if (rSrc.ePortIdx == eID_Pass1In)
        {
            isPass1 = true;
            mapFormat(rSrc.sFormat, mSettingPorts.tgi.eImgFmt);
            mSettingPorts.tgi.u4ImgWidth = rSrc.u4Width;
            mSettingPorts.tgi.u4ImgHeight = rSrc.u4Height;
            mSettingPorts.tgi.u4Stride[ESTRIDE_1ST_PLANE] = rSrc.u4Stride[ESTRIDE_1ST_PLANE];
            mSettingPorts.tgi.u4Stride[ESTRIDE_2ND_PLANE] = rSrc.u4Stride[ESTRIDE_2ND_PLANE];
            mSettingPorts.tgi.u4Stride[ESTRIDE_3RD_PLANE] = rSrc.u4Stride[ESTRIDE_3RD_PLANE];
            mSettingPorts.tgi.crop.x = rSrc.crop.x;
            mSettingPorts.tgi.crop.y = rSrc.crop.y;
            mSettingPorts.tgi.crop.floatX = rSrc.crop.floatX;
            mSettingPorts.tgi.crop.floatY = rSrc.crop.floatY;
            mSettingPorts.tgi.crop.w = rSrc.crop.w;
            mSettingPorts.tgi.crop.h = rSrc.crop.h;
        }
        else if (rSrc.ePortIdx == eID_Pass1Out)
        {
            mapFormat(rSrc.sFormat, mSettingPorts.imgo.eImgFmt);
            mSettingPorts.imgo.u4ImgWidth = rSrc.u4Width;
            mSettingPorts.imgo.u4ImgHeight = rSrc.u4Height;
            mSettingPorts.imgo.u4Stride[ESTRIDE_1ST_PLANE] = rSrc.u4Stride[ESTRIDE_1ST_PLANE];
            mSettingPorts.imgo.u4Stride[ESTRIDE_2ND_PLANE] = rSrc.u4Stride[ESTRIDE_2ND_PLANE];
            mSettingPorts.imgo.u4Stride[ESTRIDE_3RD_PLANE] = rSrc.u4Stride[ESTRIDE_3RD_PLANE];
            mSettingPorts.imgo.crop.x = rSrc.crop.x;
            mSettingPorts.imgo.crop.y = rSrc.crop.y;
            mSettingPorts.imgo.crop.floatX = rSrc.crop.floatX;
            mSettingPorts.imgo.crop.floatY = rSrc.crop.floatY;
            mSettingPorts.imgo.crop.w = rSrc.crop.w;
            mSettingPorts.imgo.crop.h = rSrc.crop.h;
        }
        else if (rSrc.ePortIdx == eID_Pass2In)
        {
            mapFormat(rSrc.sFormat, mSettingPorts.imgi.eImgFmt);
            mSettingPorts.imgi.u4ImgWidth = rSrc.u4Width;
            mSettingPorts.imgi.u4ImgHeight = rSrc.u4Height;
            mSettingPorts.imgi.u4Stride[ESTRIDE_1ST_PLANE] = rSrc.u4Stride[ESTRIDE_1ST_PLANE];
            mSettingPorts.imgi.u4Stride[ESTRIDE_2ND_PLANE] = rSrc.u4Stride[ESTRIDE_2ND_PLANE];
            mSettingPorts.imgi.u4Stride[ESTRIDE_3RD_PLANE] = rSrc.u4Stride[ESTRIDE_3RD_PLANE];
            mSettingPorts.imgi.crop.x = rSrc.crop.x;
            mSettingPorts.imgi.crop.y = rSrc.crop.y;
            mSettingPorts.imgi.crop.floatX = rSrc.crop.floatX;
            mSettingPorts.imgi.crop.floatY = rSrc.crop.floatY;
            mSettingPorts.imgi.crop.w = rSrc.crop.w;
            mSettingPorts.imgi.crop.h = rSrc.crop.h;
        }
        else if (rSrc.ePortIdx == eID_Pass2DISPO)
        {
            mapFormat(rSrc.sFormat, mSettingPorts.dispo.eImgFmt);
            mSettingPorts.dispo.u4ImgWidth = rSrc.u4Width;
            mSettingPorts.dispo.u4ImgHeight = rSrc.u4Height;
            mSettingPorts.dispo.u4Stride[ESTRIDE_1ST_PLANE] = rSrc.u4Stride[ESTRIDE_1ST_PLANE];
            mSettingPorts.dispo.u4Stride[ESTRIDE_2ND_PLANE] = rSrc.u4Stride[ESTRIDE_2ND_PLANE];
            mSettingPorts.dispo.u4Stride[ESTRIDE_3RD_PLANE] = rSrc.u4Stride[ESTRIDE_3RD_PLANE];
#if (PLATFORM_VERSION_MAJOR == 2)
            mSettingPorts.dispo.eImgRot = rSrc.eRotate;
#endif
            mSettingPorts.dispo.crop.x = rSrc.crop.x;
            mSettingPorts.dispo.crop.y = rSrc.crop.y;
            mSettingPorts.dispo.crop.floatX = rSrc.crop.floatX;
            mSettingPorts.dispo.crop.floatY = rSrc.crop.floatY;
            mSettingPorts.dispo.crop.w = rSrc.crop.w;
            mSettingPorts.dispo.crop.h = rSrc.crop.h;
        }
        else if (rSrc.ePortIdx == eID_Pass2VIDO)
        {
            mapFormat(rSrc.sFormat, mSettingPorts.vido.eImgFmt);
            mSettingPorts.vido.u4ImgWidth = rSrc.u4Width;
            mSettingPorts.vido.u4ImgHeight = rSrc.u4Height;
            mSettingPorts.vido.u4Stride[ESTRIDE_1ST_PLANE] = rSrc.u4Stride[ESTRIDE_1ST_PLANE];
            mSettingPorts.vido.u4Stride[ESTRIDE_2ND_PLANE] = rSrc.u4Stride[ESTRIDE_2ND_PLANE];
            mSettingPorts.vido.u4Stride[ESTRIDE_3RD_PLANE] = rSrc.u4Stride[ESTRIDE_3RD_PLANE];
            mSettingPorts.vido.eImgRot = rSrc.eRotate;
            mSettingPorts.vido.crop.x = rSrc.crop.x;
            mSettingPorts.vido.crop.y = rSrc.crop.y;
            mSettingPorts.vido.crop.floatX = rSrc.crop.floatX;
            mSettingPorts.vido.crop.floatY = rSrc.crop.floatY;
            mSettingPorts.vido.crop.w = rSrc.crop.w;
            mSettingPorts.vido.crop.h = rSrc.crop.h;
        }
        else
        {
            MY_LOGE("Not done yet!!");
        }
    }

    //mSettingPorts.dump();

    if (isPass1)
    {
        // Note:: must to config cameraio pipe before irq
        //        since cameio pipe won't be changed later, do it here
        vector<PortInfo const*> vCamIOInPorts;
        vector<PortInfo const*> vCamIOOutPorts;
        vCamIOInPorts.push_back(&mSettingPorts.tgi);
        vCamIOOutPorts.push_back(&mSettingPorts.imgo);
        CAM_TRACE_BEGIN("VSSScen::CamIOPipe::configPipe");
        mpCamIOPipe->configPipe(vCamIOInPorts, vCamIOOutPorts);
        CAM_TRACE_END();
    }
    //
    return MTRUE;
}


/*******************************************************************************
*
********************************************************************************/
MVOID
VSSScenario::sDefaultSetting_Ports::
dump()
{
    MY_LOGD_IF(ENABLE_LOG_PER_FRAME, "[TG]:F(%d),W(%d),H(%d),Str(%d)",
        tgi.eImgFmt,
        tgi.u4ImgWidth,
        tgi.u4ImgHeight,
        tgi.u4Stride[ESTRIDE_1ST_PLANE]);
    MY_LOGD_IF(ENABLE_LOG_PER_FRAME, "[IMGI]:F(%d),W(%d),H(%d),Str(%d,%d,%d)",
        imgi.eImgFmt,
        imgi.u4ImgWidth,
        imgi.u4ImgHeight,
        imgi.u4Stride[ESTRIDE_1ST_PLANE],
        imgi.u4Stride[ESTRIDE_2ND_PLANE],
        imgi.u4Stride[ESTRIDE_3RD_PLANE]);
    MY_LOGD_IF(ENABLE_LOG_PER_FRAME,"[DISPO]:F(%d),W(%d),H(%d),Str(%d,%d,%d)",
        dispo.eImgFmt,
        dispo.u4ImgWidth,
        dispo.u4ImgHeight,
        dispo.u4Stride[ESTRIDE_1ST_PLANE],
        dispo.u4Stride[ESTRIDE_2ND_PLANE],
        dispo.u4Stride[ESTRIDE_3RD_PLANE]);
    MY_LOGD_IF(ENABLE_LOG_PER_FRAME,"[VIDO]:F(%d),W(%d),H(%d),Str(%d,%d,%d),Rot(%d)",
        vido.eImgFmt,
        vido.u4ImgWidth,
        vido.u4ImgHeight,
        vido.u4Stride[ESTRIDE_1ST_PLANE],
        vido.u4Stride[ESTRIDE_2ND_PLANE],
        vido.u4Stride[ESTRIDE_3RD_PLANE],
        vido.eImgRot);
}

/*******************************************************************************
*
********************************************************************************/
MVOID
VSSScenario::
defaultSetting()
{
    CAM_TRACE_NAME("VSSScen::defaultSetting");
    ////////////////////////////////////////////////////////////////////
    //      Pass 1 setting (default)                                  //
    ////////////////////////////////////////////////////////////////////

    // (1.1) tgi
    PortInfo &tgi = mSettingPorts.tgi;
    tgi.eRawPxlID = mSensorBitOrder; //only raw looks this
    tgi.type = EPortType_Sensor;
    tgi.inout  = EPortDirection_In;
    tgi.index = ((mSensorDev == SENSOR_DEV_MAIN) || (mSensorDev == SENSOR_DEV_ATV))?EPortIndex_TG1I:EPortIndex_TG1I; 

    // (1.2) imgo
    PortInfo &imgo = mSettingPorts.imgo;
    imgo.type = EPortType_Memory;
    imgo.index = EPortIndex_IMGO;
    imgo.inout  = EPortDirection_Out;


    ////////////////////////////////////////////////////////////////////
    //Pass 2 setting (default)
    ////////////////////////////////////////////////////////////////////

    //(2.1)
    PortInfo &imgi = mSettingPorts.imgi;
    imgi.eImgFmt = eImgFmt_UNKNOWN;
    imgi.type = EPortType_Memory;
    imgi.index = EPortIndex_IMGI;
    imgi.inout = EPortDirection_In;
    imgi.pipePass = EPipePass_PASS2;
    imgi.u4ImgWidth = 0;
    imgi.u4ImgHeight = 0;
    imgi.u4Offset = 0;
    imgi.u4Stride[0] = 0;
    imgi.u4Stride[1] = 0;
    imgi.u4Stride[2] = 0;

    //(2.2)
    PortInfo &dispo = mSettingPorts.dispo;
    dispo.eImgFmt = eImgFmt_UNKNOWN;
    dispo.eImgRot = eImgRot_0;                  //dispo NOT support rotation
    dispo.eImgFlip = eImgFlip_OFF;              //dispo NOT support flip
    dispo.type = EPortType_DISP_RDMA;           //EPortType
    dispo.index = EPortIndex_DISPO;
    dispo.inout  = EPortDirection_Out;
    dispo.u4ImgWidth = 0;
    dispo.u4ImgHeight = 0;
    dispo.u4Offset = 0;
    dispo.u4Stride[0] = 0;
    dispo.u4Stride[1] = 0;
    dispo.u4Stride[2] = 0;

    //(2.3)
    PortInfo &vido = mSettingPorts.vido;
    vido.eImgFmt = eImgFmt_UNKNOWN;
    vido.eImgRot = eImgRot_0;
    vido.eImgFlip = eImgFlip_OFF;
    vido.type = EPortType_VID_RDMA;
    vido.index = EPortIndex_VIDO;
    vido.inout  = EPortDirection_Out;
    vido.u4ImgWidth = 0;
    vido.u4ImgHeight = 0;
    vido.u4Offset = 0;
    vido.u4Stride[0] = 0;
    vido.u4Stride[1] = 0;
    vido.u4Stride[2] = 0;
}


/*******************************************************************************
*  enque:
********************************************************************************/
MBOOL
VSSScenario::
enque(vector<IhwScenario::PortQTBufInfo> const &in)
{
    MY_LOGW_IF(in.size() > 1, "in.size() > 1");  //shouldn't happen
    //
    if(in.size() == 0)
    {
        MY_LOGE("Size is 0");
        return MFALSE;
    }
    //
    vector<IhwScenario::PortBufInfo> vEnBufPass1Out;

    for (MUINT32 i = 0; i < in.at(0).bufInfo.vBufInfo.size(); i++)
    {
        IhwScenario::PortBufInfo one(eID_Pass1Out,
                          in.at(0).bufInfo.vBufInfo.at(i).u4BufVA,
                          in.at(0).bufInfo.vBufInfo.at(i).u4BufPA,
                          in.at(0).bufInfo.vBufInfo.at(i).u4BufSize,
                          in.at(0).bufInfo.vBufInfo.at(i).memID,
                          in.at(0).bufInfo.vBufInfo.at(i).bufSecu,
                          in.at(0).bufInfo.vBufInfo.at(i).bufCohe);

        vEnBufPass1Out.push_back(one);
    };

    enque(NULL, &vEnBufPass1Out);

    return MTRUE;
}


/*******************************************************************************
*  enque:
********************************************************************************/
MBOOL
VSSScenario::
enque( vector<PortBufInfo> *pBufIn, vector<PortBufInfo> *pBufOut)
{
    if ( !pBufIn ) // pass 1
    {
        enquePass1(pBufOut);
    }
    else  // pass 2
    {
        enquePass2(pBufIn,pBufOut);
    }
    return MTRUE;
}


/*******************************************************************************
*  enque Pass 1:
********************************************************************************/
MBOOL
VSSScenario::
enquePass1(vector<PortBufInfo> *pBufOut)
{
    CAM_TRACE_NAME("VSSScen::enquePass1");
    // Note:: can't update config, but address
    //
    MUINT32 size = pBufOut->size();
    for (MUINT32 i = 0; i < size; i++)
    {
        PortID rPortID;
        QBufInfo rQbufInfo;
        mapConfig(pBufOut->at(i), rPortID, rQbufInfo);
        CAM_TRACE_BEGIN("CamIO enque");
        mpCamIOPipe->enqueOutBuf(rPortID, rQbufInfo);
        CAM_TRACE_END();
        //
        if(size > 1)
        {
            MY_LOGD_IF(0, "P1(%d-%d/0x%08X)",
                i,
                rQbufInfo.vBufInfo.at(0).memID,
                rQbufInfo.vBufInfo.at(0).u4BufVA);
        }
        else
        {
            MY_LOGD_IF(0, "P1(%d/0x%08X)",
                rQbufInfo.vBufInfo.at(0).memID,
                rQbufInfo.vBufInfo.at(0).u4BufVA);
        }
    }
    return MTRUE;
}


/*******************************************************************************
*  enque Pass
********************************************************************************/
MBOOL
VSSScenario::
enquePass2(
    vector<PortBufInfo> *pBufIn,
    vector<PortBufInfo> *pBufOut)
{
    CAM_TRACE_NAME("VSSScen::enquePass2");
    //(1)
    MUINT32 size = 0;
    // [pass 2 In]
    vector<PortInfo const*> vPostProcInPorts;
    vPostProcInPorts.push_back(&mSettingPorts.imgi);
    // [pass 2 Out]
    vector<PortInfo const*> vPostProcOutPorts;
    vector<PortID> vPortID;
    vector<QBufInfo> vQbufInfo;
    //
    size = pBufOut->size();
    for (MUINT32 i = 0; i < size; i++)
    {
        PortID rPortID;
        QBufInfo rQbufInfo;
        mapConfig(pBufOut->at(i), rPortID, rQbufInfo);
        vPortID.push_back(rPortID);
        vQbufInfo.push_back(rQbufInfo);
        //
        if (rPortID.index == EPortIndex_DISPO)
        {
            vPostProcOutPorts.push_back(&mSettingPorts.dispo);
        }
        else if (rPortID.index == EPortIndex_VIDO)
        {
            vPostProcOutPorts.push_back(&mSettingPorts.vido);
        }
    }
    //
    CAM_TRACE_BEGIN("PostProc configPipe");
    mpPostProcPipe->configPipe(vPostProcInPorts, vPostProcOutPorts);
    CAM_TRACE_END();
    // (2)
    size = pBufIn->size();
    for (MUINT32 i = 0; i < size; i++)
    {
        PortID rPortID;
        QBufInfo rQbufInfo;
        mapConfig(pBufIn->at(i), rPortID, rQbufInfo);
        CAM_TRACE_BEGIN("PostProc enque in");
        mpPostProcPipe->enqueInBuf(rPortID, rQbufInfo);
        CAM_TRACE_END();
        //
        if(size > 1)
        {
            MY_LOGD_IF(0, "P2I(%d-%d/0x%08X)",
                        i,
                        rQbufInfo.vBufInfo.at(0).memID,
                        rQbufInfo.vBufInfo.at(0).u4BufVA);
        }
        else
        {
            MY_LOGD_IF(0, "P2I(%d/0x%08X)",
                        rQbufInfo.vBufInfo.at(0).memID,
                        rQbufInfo.vBufInfo.at(0).u4BufVA);
        }
    }
    //
    size = vPortID.size();
    for (MUINT32 i = 0; i < size; i++)
    {
        CAM_TRACE_BEGIN("PostProc enque out");
        mpPostProcPipe->enqueOutBuf(vPortID.at(i), vQbufInfo.at(i));
        CAM_TRACE_END();
        //
        if(size > 1)
        {
            MY_LOGD_IF(0, "P2O(%d-%d/0x%08X)",
                        i,
                        vQbufInfo.at(i).vBufInfo.at(0).memID,
                        vQbufInfo.at(i).vBufInfo.at(0).u4BufVA);
        }
        else
        {
            MY_LOGD_IF(0, "P2O(%d/0x%08X)",
                        vQbufInfo.at(i).vBufInfo.at(0).memID,
                        vQbufInfo.at(i).vBufInfo.at(0).u4BufVA);
        }
    }
    // revise config to "update" mode after the first configPipe
    CAM_TRACE_BEGIN("PostProc Trigge");
    mpPostProcPipe->sendCommand(EPIPECmd_SET_CONFIG_STAGE, (MINT32)eConfigSettingStage_UpdateTrigger, 0, 0);
    CAM_TRACE_END();
    //
    return MTRUE;
}



/*******************************************************************************
*  deque:
********************************************************************************/
MBOOL
VSSScenario::
deque(
    EHwBufIdx port,
    vector<PortQTBufInfo> *pBufIn)
{
    MBOOL result = MTRUE;
    if ( ! pBufIn )
    {
        MY_LOGE("pBufIn==NULL");
        return MFALSE;
    }

    if ( port == eID_Unknown )
    {
        MY_LOGE("port == eID_Unknown");
        return MFALSE;
    }

    MY_LOGD_IF(0, "+ port(0x%X)",port);

    //(1.1) wait pass 1 done
    if (port & eID_Pass1Out)
    {
        result = dequePass1(port, pBufIn);
    }
    //(1.2) wait pass 2 done
    if ((port & eID_Pass2DISPO) || (port & eID_Pass2VIDO))
    {
        result = dequePass2(port, pBufIn);
    }
    MY_LOGD_IF(0, "- port(0x%X)",port);
    //
    return result;
}


/*******************************************************************************
*  deque:
********************************************************************************/
MBOOL
VSSScenario::
dequePass1(
    EHwBufIdx port,
    vector<PortQTBufInfo> *pBufIn)
{
    //for debug scan line
    char propertyValue[PROPERTY_VALUE_MAX] = {'\0'};

    CAM_TRACE_NAME("VSSScen::dequePass1");
    MUINT32 size;
    PortID rPortID;
    mapPortCfg(eID_Pass1Out, rPortID);
    PortQTBufInfo one(eID_Pass1Out);
    //
    MY_LOGD_IF(ENABLE_LOG_PER_FRAME, "DQP1");
    //
    CAM_TRACE_BEGIN("CamIOPipe deque");
    if ( ! mpCamIOPipe->dequeOutBuf(rPortID, one.bufInfo))
    {
        MY_LOGE("mpCamIOPipe->dequeOutBuf fail");
        aee_system_exception(
            LOG_TAG,
            NULL,
            DB_OPT_DEFAULT,
            "\nCRDISPATCH_KEY:VSSScenario:ISP deque fail:Pass1");
        return MFALSE;
    }
    CAM_TRACE_END();
    //
    pBufIn->push_back(one);
    //
    MY_LOGE_IF(one.bufInfo.vBufInfo.size()==0, "Pass 1 deque without buffer");
    //
    size = one.bufInfo.vBufInfo.size();
    for (MUINT32 i = 0; i < size; i++)
    {
        if(size > 1)
        {
            MY_LOGD_IF(ENABLE_LOG_PER_FRAME, "P1(%d-%d/0x%08X/0x%08X/%d/%d.%06d)",
                i,
                one.bufInfo.vBufInfo.at(i).memID,
                one.bufInfo.vBufInfo.at(i).u4BufVA,
                one.bufInfo.vBufInfo.at(i).u4BufPA,
                one.bufInfo.vBufInfo.at(i).u4BufSize,
                one.bufInfo.vBufInfo.at(i).i4TimeStamp_sec,
                one.bufInfo.vBufInfo.at(i).i4TimeStamp_us);
        }
        else
        {
            MY_LOGD_IF(ENABLE_LOG_PER_FRAME, "P1(%d/0x%08X/0x%08X/%d/%d.%06d)",
                one.bufInfo.vBufInfo.at(i).memID,
                one.bufInfo.vBufInfo.at(i).u4BufVA,
                one.bufInfo.vBufInfo.at(i).u4BufPA,
                one.bufInfo.vBufInfo.at(i).u4BufSize,
                one.bufInfo.vBufInfo.at(i).i4TimeStamp_sec,
                one.bufInfo.vBufInfo.at(i).i4TimeStamp_us);
        }
        //for debug scan line
        property_get( "debug.cam.scanline.P1", propertyValue, "0");
        if( atoi(propertyValue) == 1 )
        {
            debugScanLine->drawScanLine(mSettingPorts.imgo.u4ImgWidth,
                                        mSettingPorts.imgo.u4ImgHeight,
                                        (void*)one.bufInfo.vBufInfo.at(i).u4BufVA,
                                        one.bufInfo.vBufInfo.at(i).u4BufSize,
                                        mSettingPorts.imgo.u4Stride[0]);
        }
    }
    //
    return MTRUE;
}


/*******************************************************************************
*  deque Pass 2 Simple:
********************************************************************************/
MBOOL
VSSScenario::
dequePass2(
    EHwBufIdx port,
    vector<PortQTBufInfo> *pBufIn)
{
    //for debug scan line
    char propertyValue[PROPERTY_VALUE_MAX] = {'\0'};
    int32_t dispoBufSize = 0, vidoBufSize = 0;
    void *dispoVirtAddr = 0, *vidoVirtAddr = 0;

    CAM_TRACE_NAME("VSSScen::dequePass2");
    MY_LOGD_IF(0, "SCB");
    //
    mpPostProcPipe->sendCommand((MINT32)EPIPECmd_SET_CURRENT_BUFFER, (MINT32)EPortIndex_IMGI,0,0);
    //
    if (port & eID_Pass2DISPO)
    {
        mpPostProcPipe->sendCommand((MINT32)EPIPECmd_SET_CURRENT_BUFFER, (MINT32)EPortIndex_DISPO,0,0);
    }
    if (port & eID_Pass2VIDO)
    {
        mpPostProcPipe->sendCommand((MINT32)EPIPECmd_SET_CURRENT_BUFFER, (MINT32)EPortIndex_VIDO,0,0);
    }
    //
    MY_LOGD_IF(0, "SR");
    CAM_TRACE_BEGIN("PostProc start");
    mpPostProcPipe->start();
    CAM_TRACE_END();
    MY_LOGD_IF(0, "IRQ");
    mpPostProcPipe->irq(EPipePass_PASS2,EPIPEIRQ_PATH_DONE);
    //
    if (port & eID_Pass2DISPO)
    {
        PortID rPortID;
        mapPortCfg(eID_Pass2DISPO, rPortID);
        PortQTBufInfo one(eID_Pass2DISPO);
        MY_LOGD_IF(ENABLE_LOG_PER_FRAME, "DQD");
        CAM_TRACE_BEGIN("PostProc deque disp");
        if(!mpPostProcPipe->dequeOutBuf(rPortID, one.bufInfo))
        {
            MY_LOGE("MDP deque DISPO fail");
            aee_system_exception(
                LOG_TAG,
                NULL,
                DB_OPT_DEFAULT,
                "\nCRDISPATCH_KEY:VSSScenario:MDP deque fail:DISPO");
            goto EXIT;
        }
        CAM_TRACE_END();
        pBufIn->push_back(one);
        //
        MY_LOGD_IF(ENABLE_LOG_PER_FRAME, "DISPO(%d/0x%08X)",
            one.bufInfo.vBufInfo.at(0).memID,
            one.bufInfo.vBufInfo.at(0).u4BufVA);
        //for debug scan line
        dispoVirtAddr = (void*)one.bufInfo.vBufInfo.at(0).u4BufVA;
        dispoBufSize = one.bufInfo.vBufInfo.at(0).u4BufSize;
    }
    if (port & eID_Pass2VIDO)
    {
        PortID rPortID;
        mapPortCfg(eID_Pass2VIDO, rPortID);
        PortQTBufInfo one(eID_Pass2VIDO);
        MY_LOGD_IF(0, "DQV");
        CAM_TRACE_BEGIN("PostProc deque vido");
        if(!mpPostProcPipe->dequeOutBuf(rPortID, one.bufInfo))
        {
            MY_LOGE("MDP deque VIDO fail");
            aee_system_exception(
                LOG_TAG,
                NULL,
                DB_OPT_DEFAULT,
                "\nCRDISPATCH_KEY:VSSScenario:MDP deque fail:VIDO");
            goto EXIT;
        }
        CAM_TRACE_END();
        pBufIn->push_back(one);
        //
        MY_LOGD_IF(ENABLE_LOG_PER_FRAME, "VIDO(%d/0x%08X)",
            one.bufInfo.vBufInfo.at(0).memID,
            one.bufInfo.vBufInfo.at(0).u4BufVA);
        //for debug scan line
        vidoVirtAddr = (void*)one.bufInfo.vBufInfo.at(0).u4BufVA;
        vidoBufSize = one.bufInfo.vBufInfo.at(0).u4BufSize;
    }
    // deque out pass2 in buffer
    {
        PortID rPortID;
        mapPortCfg(eID_Pass2In, rPortID);
        QTimeStampBufInfo dummy;
        MY_LOGD_IF(0, "DQI");
        CAM_TRACE_BEGIN("PostProc deque imgi");
        mpPostProcPipe->dequeInBuf(rPortID, dummy);
        CAM_TRACE_END();
    }
    //
    EXIT:
    MY_LOGD_IF(0, "SP");
    mpPostProcPipe->stop();
    //for debug scan line
    property_get( "debug.cam.scanline.P2", propertyValue, "0");
    if( atoi(propertyValue) == 1 )
    {
        if (port & eID_Pass2DISPO)
        {
            debugScanLine->drawScanLine(mSettingPorts.dispo.u4ImgWidth,
                                        mSettingPorts.dispo.u4ImgHeight,
                                        dispoVirtAddr,
                                        dispoBufSize,
                                        mSettingPorts.dispo.u4Stride[0]);
        }
        if (port & eID_Pass2VIDO)
        {
            debugScanLine->drawScanLine(mSettingPorts.vido.u4ImgWidth,
                                        mSettingPorts.vido.u4ImgHeight,
                                        vidoVirtAddr,
                                        vidoBufSize,
                                        mSettingPorts.vido.u4Stride[0]);
        }
    }
    //
    return MTRUE;
}


/*******************************************************************************
*  replaceQue:
********************************************************************************/
MBOOL
VSSScenario::
replaceQue(vector<PortBufInfo> *pBufOld, vector<PortBufInfo> *pBufNew)
{
    PortID rPortID;
    QBufInfo rQbufInfo;
    //
    mapConfig(pBufOld->at(0), rPortID, rQbufInfo);
    mapConfig(pBufNew->at(0), rPortID, rQbufInfo);
    mpCamIOPipe->enqueOutBuf(rPortID, rQbufInfo);
    //
    MY_LOGD("P1:Old(%d/0x%08X),New(%d/0x%08X)",
            rQbufInfo.vBufInfo.at(0).memID,
            rQbufInfo.vBufInfo.at(0).u4BufVA,
            rQbufInfo.vBufInfo.at(1).memID,
            rQbufInfo.vBufInfo.at(1).u4BufVA);
    //
    return MTRUE;
}


/******************************************************************************
* This is used to check whether width or height exceed limitations of HW.
*******************************************************************************/
#define VSS_HW_LIMIT_LINE_BUF      (3264)
#define VSS_HW_LIMIT_FRAME_PIXEL   (3264*1836)
//
MVOID
VSSScenario::
getHwValidSize(MUINT32 &width, MUINT32 &height)
{
    MY_LOGD("In:W(%d),H(%d)",width,height);
    //
    if(width > VSS_HW_LIMIT_LINE_BUF)
    {
        MY_LOGW("W(%d) is larger than limitation(%d)",
                width,
                VSS_HW_LIMIT_LINE_BUF);
        width = VSS_HW_LIMIT_LINE_BUF;
        MY_LOGW("W is forced to set %d",width);
    }
    //
    if((width * height) > VSS_HW_LIMIT_FRAME_PIXEL)
    {
        MY_LOGW("Frame pixel(%d x %d = %d) is larger than limitation(%d)",
                width,
                height,
                (width * height),
                VSS_HW_LIMIT_FRAME_PIXEL);
        height = (width*9/16)&(~0x1);
        MY_LOGW("H is forced to set %d in 16:9 ratio",height);
    }
    //
    MY_LOGD("Out:W(%d),H(%d)",width,height);
}


